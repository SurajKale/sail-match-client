import {
    GET_PRIVATE_PROFILE_BEGIN ,
    GET_PRIVATE_PROFILE_SUCCESS ,
    GET_PRIVATE_PROFILE_FAILURE,

    PROFILE_UPDATE_BEGIN,
    PROFILE_UPDATE_SUCCESS,
    PROFILE_UPDATE_FAILURE

} from '../actions/index'

const initialState = {
    profile: {},
    loading: false,
    error: null
};

export const ProfileReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_PRIVATE_PROFILE_BEGIN:
            return {
                ...state,
                loading:true,
                error:null
            }
        case GET_PRIVATE_PROFILE_SUCCESS:
            return action.payload ? {
                ...state,
                profile : {
                    ...state.profile,
                    [action.payload.UserName]: action.payload
                },
                loading: false,
            } : {
                ...state,
                loading: false
            }
        case GET_PRIVATE_PROFILE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        case PROFILE_UPDATE_BEGIN:
            return {
                ...state,
                loading: true,
                error: null
            }
        case PROFILE_UPDATE_SUCCESS:
            return action.payload ? {
                ...state,
                profile : {
                    ...state.profile,
                    [action.payload.UserName]: action.payload
                },
                loading: false,
            } : {
                ...state,
                loading: false
            }
        case PROFILE_UPDATE_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
        default:
            return state;
    }
}