import { 
    GET_ACTIVATION_BEGIN,
    GET_ACTIVATION_SUCCESS,
    GET_ACTIVATION_FAILURE 
   } from '../actions'

const initialState = {
   Message: {},
   loading: false,
   error: null,
}

export const ActivationLinkReducer = (state = initialState, action) => {
   switch(action.type) { 
       case GET_ACTIVATION_BEGIN:           
           return {
               ...state,
               loading: true
           }
       case GET_ACTIVATION_SUCCESS:{
           return {
               ...state,
               Message: action.payload,
               loading: false
           }
       }
       case GET_ACTIVATION_FAILURE:
           return {
               ...state,
               loading: false,
               error: action.payload
           }
       default:
               return state;
   }
}