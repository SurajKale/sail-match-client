import { GET_LIKE_COUNT } from '../actions/LikeActions';
const initialState = {
    likeCount: '',
};

export const likeCountReducer = (state = initialState, action) => {
    switch(action.type) {          
        case GET_LIKE_COUNT:
            return {
                ...state,
                likeCount: action.payload,
            }
        default:
            return state;
    }
}
