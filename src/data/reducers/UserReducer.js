import { GET_USER_BEGIN, 
     GET_USER_SUCCESS,
     GET_USER_FAILURE ,
     CLEAR_USERDATA ,
     SET_TOKEN,
     UPDATE_PROFILE
    } from '../actions'

const initialState = {
    userDetails: {},
    loading: false,
    error: null,
    token: '',
    UserName: '',
    update_profile:'',
}

export const UserReducer = (state = initialState, action) => {
    switch(action.type) {
        case GET_USER_BEGIN:           
            return {
                ...state,
                loading: true
            }
        case GET_USER_SUCCESS:{
            return {
                ...state,
                userDetails: action.payload,
                token: action.payload.Token,
                UserName: action.payload.UserName,
                loading: false
            }
        }
        case GET_USER_FAILURE:
            return {
                ...state,
                loading: false,
                error: action.payload
            }
            case CLEAR_USERDATA:
                return {
                    ...state,
                    loading: false,
                    sessionUser: {},
                    token:null,
                    UserName: null
                }
                case SET_TOKEN:{
                    return {
                        ...state,
                        userDetails:action.payload,
                        // token: action.payload.token,

                        // UserName: action.payload.UserName,
                        loading: false
                    }
                }
                case UPDATE_PROFILE:{
                    return {
                        ...state,
                        update_profile:action.payload,
                    }
                }
        default:
                return state;
    }
}