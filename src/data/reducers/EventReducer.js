import {
    CREATE_EVENT_FAILURE,
    CREATE_EVENT_SUCCESS

} from '../actions/index'

const initialState = {
    event: {},
    loading: false,
    error: null
};

export const EventReducer = (state = initialState, action) => {
    switch(action.type) {
        case CREATE_EVENT_SUCCESS:
            return {
                ...state,
                event: action.payload,
                loading: false
            }
        case CREATE_EVENT_FAILURE:
            return {
                ...state,
                error: action.payload,
                loading: false
            }
        default:
            return state;
    }
}