import {
    GET_ALL_ARTICLES_BEGIN,
    GET_ALL_ARTICLES_SUCCESS,
    GET_ALL_ARTICLES_FAILURE
  } from '../actions/index';
const initialState = {
    articles: [],
    loading: false,
    error: null,
    after: true,
    offSet: 0
  };
export const AllArticleReducer = (state = initialState, action) => {
    switch (action.type) {
        case GET_ALL_ARTICLES_BEGIN:
            {
                return state.after ? {
                    ...state,
                    loading: true,
                    error: null
                } : {
                    ...state,
                    loading: false
                }
            }
        case GET_ALL_ARTICLES_SUCCESS: {
            return state.loading ? { 
                ...state,
                articles: [...state.articles,...action.payload],
                loading: false,
                offSet: state.offSet + 1
            } : {
                ...state,
                loading: false
            }
        }
        case GET_ALL_ARTICLES_FAILURE: {
            return {
                ...state,
                loading: false,
                error: action.payload
             }
        }
        case 'IS_MORE_DATA': {
            return { ...state, after : action.payload, loading: false }
        }
        default:
            return state;
    }
};
