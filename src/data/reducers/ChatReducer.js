import {
    GET_CHATS,
    AFTER_POST_MESSAGE,
    GET_PRIVATE_CHATS
} from '../actions/ChatActions';

const initialState = {
    chats: {}
};
 
export const ChatReducer = (state=initialState,action) => {
    switch(action.type){
        case GET_CHATS:
            return {...state, chats: action.payload }
        case GET_PRIVATE_CHATS:
            return {...state, chats: action.payload }
        case AFTER_POST_MESSAGE:
                return {...state, chats: action.payload }
        default:
            return state;
    }
}