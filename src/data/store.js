import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
// import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import { AllArticleReducer, UserReducer, ProfileReducer, ActivationLinkReducer, EventReducer,likeCountReducer, ChatReducer } from './reducers/index';
// import { GET_USER_SUCCESS,SET_TOKEN} from '../utils/actions';

// const saveAuthToken = store => next => action => {
//   if(action.type === 'GET_USER_SUCCESS' || action.type === 'SET_TOKEN') {
//     // console.log('login')
//     // after a successful login, update the token in the API
//     global.token = action.payload.token;
//     global.UserName = action.payload.UserName
//   }
//   else{
//     // console.log('no login')
//     global.token = typeof localStorage != 'undefined' ? `${localStorage.getItem('sessionUser') ? `${JSON.parse(localStorage.getItem('sessionUser')).token}` : ''}` : '';
//     global.UserName = typeof localStorage != 'undefined' ? `${localStorage.getItem('sessionUser') ? `${JSON.parse(localStorage.getItem('sessionUser')).ID}` : ''}` : ''
//     // debugger 
//     // if(typeof localStorage != 'undefined'){
//     //   // let userparse = localStorage.getItem('sessionUser')
//     //   // let user = JSON.parse(userparse)
//     //   store.dispatch({ type: SET_TOKEN, payload: user })
//     // }
//   }
//   return next(action);
// }

const rootReducer = combineReducers({
    allArticles: AllArticleReducer,
    profiles: ProfileReducer,
    userDetails: UserReducer,
    activationLink:ActivationLinkReducer,
    event: EventReducer,
    likeCount:likeCountReducer,
    chat: ChatReducer

  })

const initialState = {};

const middleware = [thunk];

const store = createStore(rootReducer, initialState, compose(
  applyMiddleware(...middleware, promiseMiddleware),))

export default store;