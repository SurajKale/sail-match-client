import { gql } from "apollo-boost";

export const deleteEvent = gql`
  mutation($EventID: Int,$UserID: Int) {
    deleteEvent(EventID:$EventID,UserID:$UserID) {
        ID
        EventTitle
    }
  }
`;

export const deleteMatch = gql`
  mutation($MatchID: Int,$UserID: Int) {
    deleteMatch(MatchID:$MatchID,UserID:$UserID) {
        ID
        Title
    }
  }
`;
export const deleteClubMaster = gql`
  mutation($ClubID: Int) {
    deleteClubMaster(ClubID:$ClubID) {
        ID
        Name
    }
  }
`;

export const deleteCrew = gql`
  mutation($CrewID: Int,$UserID: Int) {
    deleteCrew(CrewID:$CrewID,UserID:$UserID) {
        ID
        Title
    }
  }
`;

export const deleteBoatProfile = gql`
  mutation($BoatID: Int,$UserID: Int) {
    deleteBoatProfile(BoatID:$BoatID,UserID:$UserID) {
        ID
        Title
    }
  }
`;
export const deleteClub = gql`
  mutation($ClubID: Int,$UserID: Int) {
    deleteClub(ClubID:$ClubID,UserID:$UserID) {
        ID
        Title
    }
  }
`;
export const deleteEventType = gql`
  mutation($ID: Int) {
    deleteEventType(ID:$ID) {
        ID
        EventTitle
    }
  }
`;
export const deleteSkill = gql`
mutation($SkillID: Int,$UserID:Int) {
  deleteSkill(SkillID:$SkillID,UserID:$UserID) {
      ID
      Title
  }
}
`;
export const deleteClubAmenities = gql`
mutation($AmenityID: Int,$UserID:Int) {
    deleteClubAmenities(AmenityID:$AmenityID,UserID:$UserID) {
      ID
      AmenityName
  }
}
`;
export const deleteSchool = gql`
mutation($SchoolID: Int,$UserID:Int) {
    deleteSchool(SchoolID:$SchoolID,UserID:$UserID) {
      ID
      Title
  }
}
`;
export const deleteServices = gql`
mutation($ServiceID: Int,$UserID:Int) {
    deleteServices(ServiceID:$ServiceID,UserID:$UserID) {
      ID
      Title
  }
}
`;

export const deleteRide = gql`
mutation($RideID: Int,$UserID:Int) {
    deleteRide(RideID:$RideID,UserID:$UserID) {
      ID
      Title
  }
}
`;  

export const deleteBoatRentalProfile = gql`
mutation($BoatRentalID: Int,$UserID:Int) {
    deleteBoatRentalProfile(BoatRentalID:$BoatRentalID,UserID:$UserID) {
      ID
      Title
  }
}
`;

export const deleteCourseMaster = gql`
mutation($CourseID: Int,$UserID:Int) {
    deleteCourseMaster(CourseID:$CourseID,UserID:$UserID) {
      ID
      Title
  }
}
`;  
export const deleteMasterCourses = gql`
mutation($CourseMasterID: Int,$UserID:Int) {
    deleteMasterCourses(CourseMasterID:$CourseMasterID,UserID:$UserID) {
      ID
      Title
  }
}
`;

export const deleteGroup = gql`
  mutation( $GroupID: Int, $UserID: Int) {
    deleteGroup( GroupID: $GroupID, UserID: $UserID ) {
        ID
        Title
        Status
    }
  }
`;

export const objectLikeDisLike = gql`
mutation($UserID: Int,$ReferenceID:Int,$ReferenceUserID:Int,$ObjectType:String) {
  objectLikeDisLike(UserID:$UserID,ReferenceID:$ReferenceID,ReferenceUserID:$ReferenceUserID,ObjectType:$ObjectType) {
     TotalCount
  }
}
`;  


export const deletePost = gql`
  mutation($PostID: Int,$UserID: Int) {
    deletePost(PostID:$PostID,UserID:$UserID) {
        ID
    }
  }
`;


export const deleteCommentReply = gql`
  mutation($ID: Int,$UserID: Int) {
    deleteCommentReply(ID:$ID,UserID:$UserID) {
        ID
        ReferenceID

    }
  }
`;

export const deleteNotification = gql`
  mutation($NotificationID: Int) {
    deleteNotification(NotificationID:$NotificationID) {
        ID
    }
  }
`;