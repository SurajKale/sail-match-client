import { addEditGroups, getGroupByUrl } from '../query';
export const CREATE_GROUP_SUCCESS   = 'CREATE_GROUP_SUCCESS';
export const CREATE_GROUP_FAILURE   = 'CREATE_GROUP_FAILURE';
export const GET_GROUP_PROFILE_BEGIN = 'GET_GROUP_PROFILE_BEGIN';

export const getGroupSuccess = userdata => ({
  type: CREATE_GROUP_SUCCESS,
  payload: userdata
});

export const getGroupFailure = error => ({
    type: CREATE_GROUP_FAILURE,
    payload: error
});

export const getGroupProfileBegin = () => ({
  type: GET_GROUP_PROFILE_BEGIN
})

export function getGroupCreateAction(client, groupObject, ID) {
  const { groupName, groupType, groupTopic, groupDescription, groupFacebookUrl, groupLinkdInUrl,
    profilePic, locationObject, images, updatedImages, url, groupID, organizerList,
    inviteUserList, otherEmailList, UpdatedCoList, videos } = groupObject;
    
    return(dispatch) => {
      return client.mutate({
        mutation: addEditGroups(),
        variables: {
            GroupID: groupID,
            Url: url,
            Title: groupName,
            GroupType: groupType,
            Topic: groupTopic,
            Description: groupDescription,
            UserID: ID,
            FacebookUrl: groupFacebookUrl,
            LinkedInUrl: groupLinkdInUrl,
            CoOrgnizers: UpdatedCoList ? UpdatedCoList : organizerList,
            InviteMembers: inviteUserList,
            OtherEmail: otherEmailList,
            ProfilePicture: profilePic,
            Pictures: updatedImages ? updatedImages : images,
            Location: locationObject,
            Videos: videos
        }
      }).then((result) => {
        if(result.error){
          dispatch(getGroupFailure(result.error))
        }
        else{
          dispatch(getGroupSuccess(result.data.editProfile)) 
        }
      }).catch((err) => dispatch(getGroupFailure(err)));
    }
  }

  export function getGroupDetailsByID(client, url) {
    return(dispatch) => {
      dispatch(getGroupProfileBegin());
      return client.query({
        query: getGroupByUrl(),
        variables: {
          GroupUrl: url
        },
        fetchPolicy: 'network-only'
      })
    }
  }
