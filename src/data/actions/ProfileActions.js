import { getPrivateProfile, getPublicProfile, getUserProfile, editProfile} from "../query"

export const GET_PRIVATE_PROFILE_BEGIN = 'GET_PRIVATE_PROFILE_BEGIN'
export const GET_PRIVATE_PROFILE_SUCCESS = 'GET_PRIVATE_PROFILE_SUCCESS'
export const GET_PRIVATE_PROFILE_FAILURE = 'GET_PRIVATE_PROFILE_FAILURE'

export const PROFILE_UPDATE_BEGIN = 'PROFILE_UPDATE_BEGIN'
export const PROFILE_UPDATE_SUCCESS = 'PROFILE_UPDATE_SUCCESS'
export const PROFILE_UPDATE_FAILURE = 'PROFILE_UPDATE_FAILURE'

export const getPrivateProfileBegin = () => ({
    type: GET_PRIVATE_PROFILE_BEGIN
  })
export const getPrivateProfileSuccess = profile => ({
    type: GET_PRIVATE_PROFILE_SUCCESS,
    payload: profile
})
export const getPrivateProfileFailure = error => ({
    type: GET_PRIVATE_PROFILE_FAILURE,
    payload: error
})

export const getProfileUpdateBegin = () => ({
  type: PROFILE_UPDATE_BEGIN
})
export const getProfileUpdateSuccess = profile => ({
  type: PROFILE_UPDATE_SUCCESS,
  payload: profile
})
export const getProfileUpdateFailure = error => ({
  type: PROFILE_UPDATE_FAILURE,
  payload: error
})

export function getPrivateProfileAction(client) {
    return(dispatch) => {
      dispatch(getPrivateProfileBegin());
      return client.query({
        query: getPrivateProfile(),
        fetchPolicy: 'network-only'
      })
    }
  }

export function getPublicProfileAction(client, ID) {
    return dispatch => {
      dispatch(getPrivateProfileBegin());
        return client.query({query: getPublicProfile(ID), fetchPolicy: 'network-only'})
                .then((result) => {
                    if(result.error){
                        dispatch(getPrivateProfileFailure(result.error))
                    }
                    else {
                        dispatch(getPrivateProfileSuccess(result.data.getAuthorProfileDetails))
                    }
                })
                .catch((err)=> dispatch(getPrivateProfileFailure(err)))
    }
}

export function getProfileUpdateAction(client, profileObject) {
  const { id, coptions, city, drinker, experience, gender, options,
    marijuana, firstName, lastName, bio, smoker, state, mobileNo, boatOwner,
    club, selectedClubID, locationObject, profilePic } = profileObject;
    
  return(dispatch) => {
    dispatch(getProfileUpdateBegin());
    return client.mutate({
      mutation: editProfile(),
      variables: { 
        UserID: id,
        FirstName: firstName,
        LastName: lastName,
        City: city,
        State: state,
        Gender: gender,
        Experience: experience,
        Desire: options.map(item => item.Type),
        Certifications: coptions.map(item => item.Type),
        Smoke: smoker,
        Drink: drinker,
        Pot: marijuana,
        Bio: bio,
        Phone: mobileNo,
        isBoatOwner: boatOwner,
        isClubMember: club,
        ClubID: selectedClubID,
        Location: locationObject,
        ProfileImage: profilePic
      }
    });
  }
}