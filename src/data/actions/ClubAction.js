import { createClub, getClubByUrl } from '../query';
export const CREATE_CLUB_SUCCESS   = 'CREATE_CLUB_SUCCESS';
export const CREATE_CLUB_FAILURE   = 'CREATE_CLUB_FAILURE';
export const GET_CLUB_PROFILE_BEGIN = 'GET_CLUB_PROFILE_BEGIN';

export const getClubSuccess = userdata => ({
  type: CREATE_CLUB_SUCCESS,
  payload: userdata
});

export const getClubFailure = error => ({
    type: CREATE_CLUB_FAILURE,
    payload: error
});

export const getClubProfileBegin = () => ({
  type: GET_CLUB_PROFILE_BEGIN
})

export function getClubCreateAction(client, clubObject, selectedAmenities, UID) {
  const { clubName, clubDescription, profilePic, locationObject, images, clubID, videos } = clubObject;
    
    return(dispatch) => {
      return client.mutate({
        mutation: createClub(),
        variables: {
            ClubID : clubID ? clubID : null,
            Title: clubName,
            Description: clubDescription,
            UserID: UID,
            ProfilePicture: profilePic,
            Amenities: selectedAmenities,
            Location: locationObject,
            Pictures: images,
            Videos: videos
            }
      }).then((result) => {
        if(result.error){
          dispatch(getClubFailure(result.error))
        }
        else{
          dispatch(getClubSuccess(result.data.createClub)) 
        }
      }).catch((err) => dispatch(getClubFailure(err)));
    }
  }

  export function getClubDetailsByID(client, url) {
    return(dispatch) => {
      dispatch(getClubProfileBegin());
      return client.query({
        query: getClubByUrl(),
        variables: {
          ClubUrl: url
        },
        fetchPolicy: 'network-only'
      })
    }
  }