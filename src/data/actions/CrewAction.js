import {createCrew,getAllCrews,getCrewByUrl} from "../query"

export const CREATE_CREW_BEGIN = 'CREATE_CREW_BEGIN'
export const CREATE_CREW_SUCCESS = 'CREATE_CREW_SUCCESS'
export const CREATE_CREW_FAILURE = 'CREATE_CREW_FAILURE'

export const GET_ALL_CREW_BEGIN = 'GET_ALL_CREW_BEGIN'
export const GET_ALL_CREW_SUCCESS = 'GET_ALL_CREW_SUCCESS'
export const GET_ALL_CREW_FAILURE = 'GET_ALL_CREW_FAILURE'

export const GET_CREW_BYID_BEGIN   = 'GET_CREW_BYID_BEGIN'
export const GET_CREW_BYID_SUCCESS = 'GET_CREW_BYID_SUCCESS'
export const GET_CREW_BYID_FAILURE = 'GET_CREW_BYID_FAILURE'

export const createCrewBegin = () => ({
    type: CREATE_CREW_BEGIN
  })
export const createCrewSuccess = crew => ({
    type: CREATE_CREW_SUCCESS,
    payload: crew
})
export const createCrewFailure = error => ({
    type: CREATE_CREW_FAILURE,
    payload: error
})

export const getAllCrewBegin = () => ({
  type: GET_ALL_CREW_BEGIN
})
export const  getAllCrewSuccess = crew => ({
  type: GET_ALL_CREW_SUCCESS,
  payload: crew
})
export const  getAllCrewFailure = error => ({
  type: GET_ALL_CREW_FAILURE,
  payload: error
})

export const getCrewByIdBegin = () => ({
    type: GET_ALL_CREW_BEGIN
  })
  export const  getCrewByIdSuccess = crew => ({
    type: GET_ALL_CREW_SUCCESS,
    payload: crew
  })
  export const  getCrewByIdFailure = error => ({
    type: GET_ALL_CREW_FAILURE,
    payload: error
  })

export function getCrewByIdAction(client,CrewID) {
    return dispatch => {
      dispatch(getCrewByIdBegin());
        return client.query({query: getCrewByUrl(),
          variables: { 
            CrewUrl:CrewID,
          },
          fetchPolicy: 'network-only'
        })
    }
}

export function getAllCrewsAction(client) {
  return dispatch => {
    dispatch(getAllCrewBegin());
      return client.query({
        query: getAllCrews()
        })
              .then((result) => {
                  if(result.error){
                      dispatch(getAllCrewFailure(result.error))
                  }
                  else {
                      dispatch(getAllCrewSuccess(result.data.getAllCrews))
                  }
              })
              .catch((err)=> dispatch(getAllCrewFailure(err)))
  }
}



export function createCrewAction(client,Title,MemberID,Description,EventType,SkillLevel,StartDate,EndDate,StartTime,EndTime,Location,Status,crewID) {
  return(dispatch) => {
    dispatch(createCrewBegin());
    return client.mutate({
      mutation: createCrew(),
      variables: { 
            CrewID : crewID ? crewID : null,
            Title:Title,
            MemberID:MemberID,
            Description:Description,
            EventType:EventType,
            SkillLevel:SkillLevel,
            StartDate: StartDate,
            EndDate: EndDate,
            StartTime: StartTime,
            EndTime: EndTime,
            Location: Location,
            Status: Status
      }
    }).then((result) => {
      if(result.error){
        dispatch(createCrewFailure(result.error))
      }
      else{
        dispatch(createCrewSuccess(result.data.createCrew)) 
      }
    }).catch((err) => dispatch(createCrewFailure(err)));
  }
}