import { addEditMasterCourse, getCourseByUrl } from '../query';
export const CREATE_COURSE_SUCCESS   = 'CREATE_COURSE_SUCCESS';
export const CREATE_COURSE_FAILURE   = 'CREATE_COURSE_FAILURE';
export const GET_COURSE_PROFILE_BEGIN = 'GET_COURSE_PROFILE_BEGIN';

export const getCourseSuccess = userdata => ({
  type: CREATE_COURSE_SUCCESS,
  payload: userdata
});

export const getCourseFailure = error => ({
    type: CREATE_COURSE_FAILURE,
    payload: error
});

export const getCourseProfileBegin = () => ({
  type: GET_COURSE_PROFILE_BEGIN
})

export function getCourseCreateAction(client, courseObject, ID, courseprerequisite, courseID, selctedData) {
  const { startDate, endDate, startTime, endTime, courseInstructor, videos, courseDesription,
    profilePic, locationObject, selectedC, images } = courseObject;
    
  return(dispatch) => {
    return client.mutate({
      mutation: addEditMasterCourse(),
      variables: {
          CourseID: courseID ? courseID : null,
          NameOfCourse: courseID ? selctedData: selectedC,
          Prerequisite: courseprerequisite,
          Title: courseID ? selctedData.CourseName :selectedC.CourseName,
          Description: courseDesription,
          Instructor: { Name: courseInstructor },
          UserID: ID,
          Location: locationObject,
          StartDate: courseID ? startDate : startDate.toISOString().substring(0,10),
          EndDate: courseID ? endDate : endDate.toISOString().substring(0,10),
          StartTime: startTime,
          EndTime: endTime,
          ProfilePicture: profilePic,
          Pictures: images,
          Videos: videos
      }
    }).then((result) => {
      if(result.error){
        dispatch(getCourseFailure(result.error))
      }
      else{
        dispatch(getCourseSuccess(result.data.editProfile)) 
      }
    }).catch((err) => dispatch(getCourseFailure(err)));
  }
}

export function getCourseDetailsByID(client, url) {
  return(dispatch) => {
    dispatch(getCourseProfileBegin());
    return client.query({
      query: getCourseByUrl(),
      variables: {
        CourseUrl: url
      },
      fetchPolicy: 'network-only'
    })
  }
}
