import {signIn,signUp,ActivationsignIn,FBSignUpIn,GoogleSignUpIn,updateProfilePic} from '../query';
export const GET_USER_BEGIN   = 'GET_USER_BEGIN';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';
export const GET_USER_FAILURE = 'GET_USER_FAILURE';
export const CLEAR_USERDATA = 'CLEAR_USERDATA';
export const SET_TOKEN = 'SET_TOKEN';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

export const getUserBegin = () => ({
  type: GET_USER_BEGIN
});

export const getUserSuccess = userdata => ({
  type: GET_USER_SUCCESS,
  payload: userdata
});

export const getUserFailure = error => ({
  type: GET_USER_FAILURE,
  payload: error 
});

export const setUserToken = (data) => ({
  type: SET_TOKEN,
  payload: data
})

export const clearUserData = (error) => ({
  type: CLEAR_USERDATA,
})



// export const setUserDataAction = (data) => {
//     return (dispatch) => {
//         dispatch(getUserBegin())
//         try {
//             return dispatch(getUserSuccess(data))
//         }
//         catch(error){
//             dispatch(getUserFailure(error));
//         }
//     }
// }


// export function getSignInAction(client,Email,Password,Phone) {
//   return (dispatch) => {
//       dispatch(getUserBegin());
//       return client.query({
//             query: signIn(),
//             variables: { 
//               Email: Email,
//               Password:Password,
//               Phone:Phone      
//             }
//            })
//             .then((result) => {
//               console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX')
//                 if(result.error){
//                     dispatch(getUserFailure(result.error));
//                 }
//                 else {
//                   dispatch(getUserSuccess(result.data.signin));
//                   dispatch(setUserToken(result.data.signin));
//                 }
//             })
//             .catch((err)=> dispatch(getUserFailure(err)));
//   }
// }


export function getSignInAction(client,Email,Password,Phone) {
  // console.log(client)
  return (dispatch) => {
      // dispatch(getUserBegin());
      return client.query({
            query: signIn(),
            variables: { 
              Email: Email,
              Password:Password,
              Phone:Phone,
              isAutoLogin:false     
            }
           });
    }
}



// export function getSignUpAction(client,FirstName,LastName,Email,Password,SignUpMethod,Phone,IpAddress) {
//   return  (dispatch) => {
//       dispatch(getUserBegin());
//       return client.mutate({
//         mutation: signUp(),
//             variables: {
//               FirstName:FirstName,
//               LastName:LastName,
//               Email: Email,
//               Password:Password,
//               SignUpMethod:SignUpMethod,
//               Phone:Phone,
//               IpAddress: IpAddress
//             }
//            })
//             .then( async(result) => {
//                 if(result.error){
//                     dispatch(getUserFailure(result.error));
//                 }
//                 else {
//                await dispatch(getUserSuccess(result.data.userSignup));
//                await dispatch(setUserToken(result.data.userSignup));
//                 }
//             })
//             .catch((err)=> dispatch(getUserFailure(err)));
//   }
// }

export function getSignUpAction(client,Email,Password,SignUpMethod) {
  return  (dispatch) => {
      dispatch(getUserBegin());
      return client.mutate({
        mutation: signUp(),
            variables: {
              Email: Email,
              Password:Password,
              SignUpMethod:SignUpMethod,
            }
           });    
  }
}

export function facebookSignInSignUp(client,FirstName,LastName,Email,ExternalSiteID,ExternalProfileImage) {
  return  (dispatch) => {
     return client.mutate({
        mutation: FBSignUpIn(),
            variables: {
                FirstName: FirstName,
                LastName:LastName,
                Email:Email,
                ExternalSiteID:ExternalSiteID,
                ExternalProfileImage:ExternalProfileImage
               }
           });    
     }
}


export function googleSignInSignUp(client,FirstName,LastName,Email,ExternalSiteID,ExternalProfileImage) {
  return  (dispatch) => {
     return client.mutate({
        mutation: GoogleSignUpIn(),
            variables: {
                FirstName: FirstName,
                LastName:LastName,
                Email:Email,
                ExternalSiteID:ExternalSiteID,
                ExternalProfileImage:ExternalProfileImage
               }
           });    
     }
}

export function updateProfile(client,UserID,ProfileImage) {
  return  (dispatch) => {
     return client.mutate({
        mutation: updateProfilePic(),
            variables: {
                UserID: UserID,
                ProfileImage:ProfileImage
               }
           });    
     }
}

export function ActivationsignInAction(client,Email,UniqueAutoSinginKey) {
  return (dispatch) => {
      dispatch(getUserBegin());
      return client.query({
            query: ActivationsignIn(),
            variables: { 
              Email: Email,
              isAutoLogin:true ,    
              UniqueAutoSinginKey:UniqueAutoSinginKey,
            }
           })
  }
}


export function _profileReducer(image) {
  return {
    type: UPDATE_PROFILE,
    payload : image
};
}

