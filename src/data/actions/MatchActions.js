import { addEditMatch, addEditPublicMatch } from "../query";

export const CREATE_MATCH_BEGIN = "CREATE_MATCH_BEGIN";
export const CREATE_MATCH_SUCCESS = "CREATE_MATCH_SUCCESS";
export const CREATE_MATCH_FAILURE = "CREATE_MATCH_FAILURE";

export const createMatchBegin = () => ({
  type: CREATE_MATCH_BEGIN,
});
export const createMatchSuccess = (match) => ({
  type: CREATE_MATCH_SUCCESS,
  payload: match,
});
export const createMatchFailure = (error) => ({
  type: CREATE_MATCH_FAILURE,
  payload: error,
});

export function createMatchAction(
  client,
  MatchType,
  Title,
  ProfilePicture,
  CreatedBy,
  AttendedBy,
  OtherEmail,
  Description,
  StartDate,
  EndDate,
  StartTime,
  EndTime,
  Location,
  Pictures,
  videos
) {
  return (dispatch) => {
    dispatch(createMatchBegin());
    return client.mutate({
      mutation: addEditMatch(),
      variables: {
        MatchType: MatchType,
        Title: Title,
        ProfilePicture: ProfilePicture,
        CreatedBy: CreatedBy,
        AttendedBy: AttendedBy,
        OtherEmail: OtherEmail,
        Description: Description,
        StartDate: StartDate,
        EndDate: EndDate,
        StartTime: StartTime,
        EndTime: EndTime,
        Location: Location,
        Pictures: Pictures,
        Videos: videos,
      },
    });
  };
}

export function createPublicMatchAction(
  client,
  MatchType,
  ObjectType,
  ReferenceID,
  Title,
  ProfilePicture,
  CreatedBy,
  AttendedBy,
  OtherEmail,
  Description,
  StartDate,
  EndDate,
  StartTime,
  isPublic,
  EndTime,
  Location,
  Pictures,
  Videos
) {
  return (dispatch) => {
    dispatch(createMatchBegin());
    return client.mutate({
      mutation: addEditPublicMatch(),
      variables: {
        MatchType: MatchType,
        ObjectType: ObjectType,
        ReferenceID: ReferenceID,
        Title: Title,
        ProfilePicture: ProfilePicture,
        CreatedBy: CreatedBy,
        AttendedBy: AttendedBy,
        OtherEmail: OtherEmail,
        Description: Description,
        StartDate: StartDate,
        EndDate: EndDate,
        StartTime: StartTime,
        isPublic: isPublic,
        EndTime: EndTime,
        Location: Location,
        Pictures: Pictures,
        Videos: Videos,
      },
    });
  };
}

export function updatePublicMatchAction( client, matchObject ) {
  const { title, description, profilePic, MatchID } = matchObject;
  return (dispatch) => {
    dispatch(createMatchBegin());
    return client.mutate({
      mutation: addEditMatch(),
      variables: {
        MatchID: MatchID,
        Title: title,
        ProfilePicture: profilePic,
        Description: description
      },
    });
  };
}