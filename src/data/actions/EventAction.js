import { createEvents, getEventByUrl } from '../query';
export const CREATE_EVENT_SUCCESS   = 'CREATE_EVENT_SUCCESS';
export const CREATE_EVENT_FAILURE   = 'CREATE_EVENT_FAILURE';
export const GET_EVENT_PROFILE_BEGIN = 'GET_EVENT_PROFILE_BEGIN';

export const getEventSuccess = userdata => ({
  type: CREATE_EVENT_SUCCESS,
  payload: userdata
});

export const getEventFailure = error => ({
    type: CREATE_EVENT_FAILURE,
    payload: error
});

export const getEventProfileBegin = () => ({
  type: GET_EVENT_PROFILE_BEGIN
})

export function getEventCreateAction(client, eventObject, eventType) {
  const { startDate, endDate, skill, startTime, endTime, streetName, city, videos,
    state, zipcode, eventTitle, description, profilePic, ID, locationObject, eventID } = eventObject;
    
    return(dispatch) => {
      return client.mutate({
        mutation: createEvents(),
        variables: {
          EventID: eventID ? eventID : null,
          EventTitle: eventTitle,
          EventType: eventType,
          EventOwnerId: ID,
          SkillLevel: skill,
          Description: description,
          ProfilePicture: profilePic,
          Location: locationObject,
          EventStartDate: eventID ? startDate : startDate.toISOString().substring(0,10),
          EventEndDate: eventID ? endDate :  endDate.toISOString().substring(0,10),
          EventStartTime: startTime,
          EventEndTime: endTime,
          Status: 1,
          Videos: videos
        }
      }).then((result) => {
        if(result.error){
          dispatch(getEventFailure(result.error))
        }
        else{
          dispatch(getEventSuccess(result.data.editProfile)) 
        }
      }).catch((err) => dispatch(getEventFailure(err)));
    }
  }

  export function getEventDetailsByID(client, url) {
    return(dispatch) => {
      dispatch(getEventProfileBegin());
      return client.query({
        query: getEventByUrl(),
        variables: {
          EventUrl: url
        },
        fetchPolicy: 'network-only'
      })
    }
  }
