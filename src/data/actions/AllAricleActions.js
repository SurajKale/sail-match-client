export const GET_ALL_ARTICLES_BEGIN   = 'GET_ALL_ARTICLES_BEGIN';
export const GET_ALL_ARTICLES_SUCCESS = 'GET_ALL_ARTICLES_SUCCESS';
export const GET_ALL_ARTICLES_FAILURE = 'GET_ALL_ARTICLES_FAILURE';

export const getAllArticlesBegin = () => ({
  type: GET_ALL_ARTICLES_BEGIN
});

export const getAllArticlesSuccess = products => ({
  type: GET_ALL_ARTICLES_SUCCESS,
  payload: products
});

export const getAllArticlesFailure = error => ({
  type: GET_ALL_ARTICLES_FAILURE,
  payload: error 
});


// export function getAllArticle(client,limit,offset) {
//     return (dispatch, getState) => {      
//         dispatch(getAllArticlesBegin());
//         let ids = [];
//         getState().allArticles.articles.map(article => ids.push(article.ID))
//         return client.query({
//               query: getAllArticles(limit,offset,ids), variables: { ArticleIds: ids}})
//               .then((result) => {
//                   if(result.data.getAllArticles.length !== 0){
//                       dispatch(getAllArticlesSuccess(result.data.getAllArticles));
//                   }
//                   else {
//                     dispatch({type: 'IS_MORE_DATA', payload: false})
//                   }
//               })
//               .catch((err)=> dispatch(getAllArticlesFailure(err)));
//     }
//   }

  