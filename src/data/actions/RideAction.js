import {createRide} from "../query"


export function createRideAction(client,Title,MemberID,Description,EventType,ProfileImage,SkillLevel,StartDate,EndDate,StartTime,EndTime,Location,CreatedBy,Gender,BoatID,
  rideID) {
    return(dispatch) => {
      return client.mutate({
        mutation: createRide(),
        variables: {
              RideID : rideID ? rideID : null,
              Title:Title,
              MemberID:MemberID,
              Description:Description,
              EventType:EventType,
              ProfileImage:ProfileImage,
              SkillLevel:SkillLevel,
              StartDate: rideID ? StartDate :  StartDate.toISOString().substring(0,10),
              EndDate: rideID ? EndDate :  EndDate.toISOString().substring(0,10),
              StartTime: StartTime,
              EndTime: EndTime,
              Location:Location,
              CreatedBy:CreatedBy,
              Gender:Gender,
              BoatID:BoatID
        }
      });
    }
  }