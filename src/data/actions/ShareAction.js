import { sharePost } from "../query"

export const SHARE_POST_BEGIN = 'SHARE_POST_BEGIN'
export const SHARE_POST_SUCCESS = 'SHARE_POST_SUCCESS'
export const SHARE_POST_FAILURE = 'SHARE_POST_FAILURE'

export const sharePostBegin = () => ({
    type: SHARE_POST_BEGIN
  })
export const sharePostSuccess = post => ({
    type: SHARE_POST_SUCCESS,
    payload: post
})
export const sharePostFailure = error => ({
    type: SHARE_POST_FAILURE,
    payload: error
})

export function sharePostAction(client, shareObject) {
  const { shareContent, userList, ObjectType, ReferenceID, ReferenceUserID, PostUrl,
     ShareType, otherEmailList } = shareObject

  return(dispatch) => {
    dispatch(sharePostBegin());
    return client.mutate({
      mutation: sharePost(),
      variables: { 
        UserID: userList,
        ReferenceUserID: ReferenceUserID,
        ReferenceID: ReferenceID,
        PostUrl: PostUrl,
        PostContent: shareContent,
        ObjectType: ObjectType,
        ShareType: ShareType,
        // isMember: Boolean,
        OtherEmail: otherEmailList
      }
    })
  }
}