import {createPosts,addComment} from "../query"

export function createPostAction(client,PostID,UserID,ReferenceID,ObjectType,Comment,Pictures,Videos) {
    return(dispatch) => {
      return client.mutate({
        mutation: createPosts(),
        variables: { 
              PostID:PostID,
              UserID:UserID,
              ReferenceID:ReferenceID,
              ObjectType:ObjectType,
              Comment:Comment,
              Pictures:Pictures,
              Videos:Videos
        }
      });
    }
  }

  export function createCommentAction(client,ParentCommentID,Reply,ID,UserID,ReferenceID,Comment,Pictures,type) {
    return(dispatch) => {
      return client.mutate({
        mutation: addComment(),
        variables: {
            ParentCommentID:ParentCommentID,
            Reply:Reply,
            ID:ID,
            UserID:UserID,
            ReferenceID:ReferenceID,
            Comment:Comment,
            Pictures:Pictures,
            ReferenceType: type
        }
      });
    }
  }

  export function createCommentActionForPhotos(client,refID,cmt,UserID) {
    return(dispatch) => {
      return client.mutate({
        mutation: addComment(),
        variables: {
            ReferenceID: refID,
            ReferenceType:"Pictures",
            Comment:cmt,
            UserID: UserID
        }
      });
    }
  }
