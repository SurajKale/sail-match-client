import {viewAllObjectTypePictures} from "../query"

export const GET_VIEWALL_PICTURES_BEGIN = 'GET_VIEWALL_PICTURES_BEGIN'

export const getViewAllPicturesBegin = () => ({
    type: GET_VIEWALL_PICTURES_BEGIN
  })

export function getViewAllPictures(client, ReferenceID,UserID,ObjectType) {
  return(dispatch) => {
    dispatch(getViewAllPicturesBegin());
    return client.query({
      query: viewAllObjectTypePictures(),
      variables: {
        ReferenceID: ReferenceID,
        UserID:UserID,
        ObjectType:ObjectType
      },
      fetchPolicy: 'network-only'
    })
  }
}