import { addEditSkills, getSkillByUrl } from '../query';
export const CREATE_SKILL_SUCCESS   = 'CREATE_SKILL_SUCCESS';
export const CREATE_SKILL_FAILURE   = 'CREATE_SKILL_FAILURE';
export const GET_SKILL_PROFILE_BEGIN = 'GET_SKILL_PROFILE_BEGIN';

export const getSkillSuccess = userdata => ({
  type: CREATE_SKILL_SUCCESS,
  payload: userdata
});

export const getSkillFailure = error => ({
    type: CREATE_SKILL_FAILURE,
    payload: error
});

export const getSkillProfileBegin = () => ({
  type: GET_SKILL_PROFILE_BEGIN
})

export function getSkillCreateAction(client, skillObject, UID) {
  const { skillType, skillName, skillDescription, profilePic, locationObject, images,
    skillID, selectedType, videos } = skillObject;
    
    return(dispatch) => {
      return client.mutate({
        mutation: addEditSkills(),
        variables: {
            SkillID: skillID ? skillID : null,   
            UserID: UID,
            Title: skillName,
            SkillType: skillID ? selectedType : skillType,
            Description: skillDescription,
            Location: locationObject,
            ProfilePicture: profilePic,
            Pictures: images,
            Videos: videos
        }
      }).then((result) => {
        if(result.error){
          dispatch(getSkillFailure(result.error))
        }
        else{
          dispatch(getSkillSuccess(result.data.addEditSkills)) 
        }
      }).catch((err) => dispatch(getSkillFailure(err)));
    }
  }

  export function getSkillDetailsByID(client, url) {
    return(dispatch) => {
      dispatch(getSkillProfileBegin());
      return client.query({
        query: getSkillByUrl(),
        variables: {
          SkillUrl: url
        },
        fetchPolicy: 'network-only'
      })
    }
  }
