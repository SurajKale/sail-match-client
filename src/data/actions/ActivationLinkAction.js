import {isAcccontActivationLinkValid} from '../query';
export const GET_ACTIVATION_BEGIN   = 'GET_ACTIVATION_BEGIN';
export const GET_ACTIVATION_SUCCESS = 'GET_ACTIVATION_SUCCESS';
export const GET_ACTIVATION_FAILURE = 'GET_ACTIVATION_FAILURE';

export const getActivationBegin = () => ({
  type: GET_ACTIVATION_BEGIN
}); 

export const getActivationSuccess = link => ({
  type: GET_ACTIVATION_SUCCESS,
  payload: link
});

export const getActivationFailure = error => ({
  type: GET_ACTIVATION_FAILURE,
  payload: error 
});


export function getActivationLinkAction(client,ActivationLink) {
  debugger
  return (dispatch) => {
      dispatch(getActivationBegin());
      return client.query({
            query: isAcccontActivationLinkValid(),
            variables: { 
                ActivationLink: ActivationLink
            }
           })
            .then((result) => {
                if(result.error){
                    dispatch(getActivationFailure(result.error));
                }
                else {
                  dispatch(getActivationSuccess(result.data.isAcccontActivationLinkValid));
                }
            })
            .catch((err)=> dispatch(getActivationFailure(err)));
  }
}

