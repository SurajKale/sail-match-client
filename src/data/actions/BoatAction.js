import { createBoatProfile, getBoatProfileByUrl } from '../query';
export const CREATE_BOAT_SUCCESS   = 'CREATE_EVENT_SUCCESS';
export const CREATE_BOAT_FAILURE   = 'CREATE_EVENT_FAILURE';
export const GET_BOAT_PROFILE_BEGIN = 'GET_EVENT_PROFILE_BEGIN';

export const getBoatSuccess = userdata => ({
  type: CREATE_BOAT_SUCCESS,
  payload: userdata
});

export const getBoatFailure = error => ({
    type: CREATE_BOAT_FAILURE,
    payload: error
});

export const getBoatProfileBegin = () => ({
  type: GET_BOAT_PROFILE_BEGIN
})

export function getBoatCreateAction(client, boatObject, sailType, UID) {
  const { boatName, boatLength, boatMake, boatDescription, boatProfilePic,
      locationObject, boatType, boatID, videos, images, boatYear, boatModal } = boatObject;
    
    return(dispatch) => {
      return client.mutate({
        mutation: createBoatProfile(),
        variables: {
            BoatID: boatID ? boatID : null,
            BoatType: boatType,
            Title: boatName,
            Make: boatMake,
            SailingType: sailType,
            Description: boatDescription,
            ProfilePicture: boatProfilePic,
            Location: locationObject,
            Length: boatLength,
            CreatedBy: UID,
            Pictures: images,
            Videos: videos,
            BoatModel: boatModal,
            ManufactureYear: boatYear
        }
      }).then((result) => {
        if(result.error){
          dispatch(getBoatFailure(result.error))
        }
        else{
          dispatch(getBoatSuccess(result.data.editProfile)) 
        }
      }).catch((err) => dispatch(getBoatFailure(err)));
    }
  }

  export function getBoatDetailsByID(client, url) {
    return(dispatch) => {
      dispatch(getBoatProfileBegin());
      return client.query({
        query: getBoatProfileByUrl(),
        variables: {
          BoatUrl: url
        },
        fetchPolicy: 'network-only'
      })
    }
  }
