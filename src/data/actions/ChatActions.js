import axios from 'axios';
import { CHAT_SERVER } from '../../components/config';

export const LOGIN_USER = 'login_user';
export const REGISTER_USER = 'register_user';
export const AUTH_USER = 'auth_user';
export const LOGOUT_USER = 'logout_user';
export const GET_USER = 'get_user';


export const GET_CHATS = 'get_chat';
export const GET_PRIVATE_CHATS = 'get_private_chat';
export const AFTER_POST_MESSAGE = 'after_post_message';

export function getChats(){
    const request = axios.get(`${CHAT_SERVER}/getChats`)
        .then(response => response.data);
    
    return {
        type: GET_CHATS,
        payload: request
    }
}

export function getPrivateChats(userData){
    const request = axios.post(`${CHAT_SERVER}/getPrivateChats`, userData)
        .then(response => response.data);
    
    return {
        type: GET_PRIVATE_CHATS,
        payload: request
    }
}

export function afterPostMessage(data){

    return {
        type: AFTER_POST_MESSAGE,
        payload: data
    }
}

