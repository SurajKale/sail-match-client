export const GET_LIKE_COUNT   = 'GET_LIKE_COUNT';


export const _setLikeCount = count => ({
    type: GET_LIKE_COUNT,
    payload: count
})
  


export function _getLikeCount(count) {
    return (dispatch) => {     
    dispatch(_setLikeCount(count));
    }
  }

//   export function _getLikeCount(count) {
//     return (dispatch, getState) => {      
//         dispatch(getAllArticlesBegin());
//         let ids = [];
//         getState().allArticles.articles.map(article => ids.push(article.ID))
//         return client.query({
//               query: getAllArticles(limit,offset,ids), variables: { ArticleIds: ids}})
//               .then((result) => {
//                   if(result.data.getAllArticles.length !== 0){
//                       dispatch(getAllArticlesSuccess(result.data.getAllArticles));
//                   }
//                   else {
//                     dispatch({type: 'IS_MORE_DATA', payload: false})
//                   }
//               })
//               .catch((err)=> dispatch(getAllArticlesFailure(err)));
//     }
//   }

