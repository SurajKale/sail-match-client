import {addEditServices,getServiceByUrl} from "../query"

export const CREATE_SERVICE_BEGIN = 'CREATE_SERVICE_BEGIN'
export const CREATE_SERVICE_SUCCESS = 'CREATE_SERVICE_SUCCESS'
export const CREATE_SERVICE_FAILURE = 'CREATE_SERVICE_FAILURE'

export const createServiceBegin = () => ({
    type: CREATE_SERVICE_BEGIN
  })
export const createServiceSuccess = match => ({
    type: CREATE_SERVICE_SUCCESS,
    payload: match
})
export const createServiceFailure = error => ({
    type: CREATE_SERVICE_FAILURE,
    payload: error
})

export function createServiceAction(client,ServiceType,Title,WebsiteUrl,Hours,Rating,ProfilePicture,Description,Location,CreatedBy,images,mobileNumber,videos,serviceObject) {
  return(dispatch) => {
    dispatch(createServiceBegin());
    return client.mutate({
      mutation: addEditServices(),
      variables: { 
        ServiceType:ServiceType,
        Title:Title,
        WebsiteUrl:WebsiteUrl,
        Hours:Hours,
        Rating:Rating,
        ProfilePicture:ProfilePicture,
        Description:Description,
        Location:Location,
        CreatedBy:CreatedBy,
        Pictures: images,
        Phone: mobileNumber,
        Videos: videos
      }
    })
  }
}

export function updateServiceAction( client, serviceObject ) {
  const { profilePic, website, businessName, description, mobileNumber, serviceID} = serviceObject
  return(dispatch) => {
    dispatch(createServiceBegin());
    return client.mutate({
      mutation: addEditServices(),
      variables: { 
        ServiceID: serviceID,
        Title:businessName,
        WebsiteUrl:website,
        ProfilePicture:profilePic,
        Description:description,
        Phone: mobileNumber
      }
    })
  }
}

export function getServiceDetailsByID(client, id) {
  return(dispatch) => {
    dispatch(createServiceBegin());
    return client.query({
      query: getServiceByUrl(),
      variables: {
        ServiceUrl: id
      },
      fetchPolicy: 'network-only'
    })
  }
}