import {createRental} from "../query"

export function createRentalAction(client,BoatID,BoatType,Description,ProfilePicture,Location,CreatedBy,WebsiteUrl,Title,Hours) {
    return(dispatch) => {
      return client.mutate({
        mutation: createRental(),
        variables: { 
              BoatID:BoatID,
              BoatType:BoatType,
              Description:Description,
              ProfilePicture:ProfilePicture,
              Location:Location,
              CreatedBy:CreatedBy,
              WebsiteUrl: WebsiteUrl,
              Title: Title,
              Hours:Hours
        }
      });
    }
  }