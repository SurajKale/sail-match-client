import { gql } from "apollo-boost";

export const getAllArticles = (limit,offset) =>{
return  gql`
    query getAllArticles($ArticleIds: [Int]){
        getAllArticles(
            limit: ${limit}
            offset: ${offset}
            ArticleIds: $ArticleIds
        ) {
            ID
            Authors
            SubTitle   
            Sequence
            Slug
            CreatedDate
            AuthorID
            Title
            FeatureImage
            ReadMinutes
            ViewCount
            TotalClapCount
            TotalArticleCount
            isBookmark
            isFollowed
        }
    }
   `
}

export const getPrivateProfile = () => {      
      return gql`
      {
        getUserProfile {
          ID
          Token
          RefreshToken
          UserType
          FirstName
          LastName
          City
          State
          ProfileImage
          UserName
          Phone
          Email
          Password
          Gender
          DateOfBirth
          Experience
          Desire
          Certifications
          Smoke
          Drink
          Pot
          Bio
          isVerified
          isBoatOwner
          isClubMember
          ClubID
          ClubDetails
          ClubName
          ActivationLink
          ExpiryDate
          SignUpMethod
          IpAddress
          Status
          CreatedDate
          BoatProfileList
          Location{
            Latitude
            Longitude
            Address
            City
            State
            Country
            ZipCode
            StreetName
          }
          }
      }`
  }

  export const getPublicProfile = (AuthorUserName) => {
      return gql`
      {
          getAuthorProfileDetails(AuthorUserName: "${AuthorUserName}"){
              ID
              Name
              Email
              Description
              Status
              Password
              RoleID
              Avatar
              SignUpMethod
              FaceBookUrl
              Follower
              Following
              UserName
              CreatedDate
              MobileNo
              Dob
              Gender
              ClubID
              ClubDetails
              ClubName
          }
        }    
      `
  }
export const forgotPassword = gql`
  mutation($Email: String) {
    forgotPassword(Email: $Email) {
      Message
    }
  }
`;

export const signIn = () => {
  return  gql`
      query signin($Email:String!,$Password:String!,$Phone:String,$isAutoLogin:Boolean){
        signin(
              Email: $Email
              Password: $Password
              Phone: $Phone  
              isAutoLogin:$isAutoLogin         
          ) {
            ID
            UserType
            FirstName
            LastName
            City
            Token
            RefreshToken
            State
            ProfileImage
            UserName
            Phone
            Email
            Password
            Gender
            DateOfBirth
            Experience
            Desire
            Certifications
            Smoke
            Drink
            Pot
            Bio
            isVerified
            isBoatOwner
            isProfile
            isClubMember
            ClubID
            ActivationLink
            SignUpMethod
            IpAddress
            Location {
              Latitude
              Longitude
              Address
              MapLocation
              StreetName
              City
              State
              Country
              ZipCode
            }
            isSchoolAdmin
          }
      }
     `
  }

  
  export const FBSignUpIn = () =>{
    return  gql`
    mutation facebookSignInSignUp(
      $FirstName: String,   
      $LastName:String,
      $Email:String!,
      $ExternalSiteID:String!,
      $ExternalProfileImage:String,
  
    ){
      facebookSignInSignUp(
        FirstName: $FirstName,   
        LastName:$LastName,
        Email:$Email,
        ExternalSiteID:$ExternalSiteID,
        ExternalProfileImage:$ExternalProfileImage
       
      ){
        ID
        UserType
        FirstName
        LastName
        City
        Token
        RefreshToken
        State
        ProfileImage
        UserName
        Phone
        Email
        Password
        Gender
        DateOfBirth
        Experience
        Desire
        Certifications
        Smoke
        Drink
        Pot
        Bio
        isVerified
        isBoatOwner
        isProfile
        isClubMember
        ClubID
        ActivationLink
        SignUpMethod
        IpAddress
        Status
        
      }
  }
  `
}



export const GoogleSignUpIn = () =>{
  return  gql`
  mutation googleSignInSignUp(
  $FirstName: String,   
  $LastName:String,
  $Email:String!,
  $ExternalSiteID:String!,
  $ExternalProfileImage:String,

  ){
    googleSignInSignUp(
      FirstName: $FirstName,   
      LastName:$LastName,
      Email:$Email,
      ExternalSiteID:$ExternalSiteID,
      ExternalProfileImage:$ExternalProfileImage
     
    ){
      ID
      UserType
      FirstName
      LastName
      City
      Token
      RefreshToken
      State
      ProfileImage
      UserName
      Phone
      Email
      Password
      Gender
      DateOfBirth
      Experience
      Desire
      Certifications
      Smoke
      Drink
      Pot
      Bio
      isVerified
      isBoatOwner
      isProfile
      isClubMember
      ClubID
      ActivationLink
      SignUpMethod
      IpAddress
      Status
      
    }
}
`
}

export const signUp = () =>{
  return  gql`
      mutation userSignup(
      $Email: String!,   
      $Password:String!,
      $SignUpMethod:String,
      ){
        userSignup(
          Email:$Email
          Password:$Password
          SignUpMethod:$SignUpMethod
          ) {
            ID
            UserType
            FirstName
            LastName
            City
            State
            ProfileImage
            UserName
            Phone
            Email
            Password
            Gender
            DateOfBirth
            Experience
            Desire
            Certifications
            ActivationLink
            Smoke
            Drink
            Pot
            Bio
            isVerified
            isBoatOwner
            isClubMember
            ClubID
            ActivationLink
            SignUpMethod
            IpAddress
            Status
          }
      }
    `
  }

export const resetPassword = gql`
  mutation($ActivationLink: String, $Password: String) {
    resetPassword(ActivationLink: $ActivationLink, Password: $Password) {
      ID
      UserType
      FirstName
      LastName
      City
      Token
      RefreshToken
      State
      ProfileImage
      UserName
      Phone
      Email
      Password
      Gender
      DateOfBirth
      Experience
      Desire
      Certifications
      Smoke
      Drink
      Pot
      Bio
      isVerified
      isBoatOwner
      isClubMember
      ClubID
      ActivationLink
      SignUpMethod
      IpAddress
      Status
      Location {
        Latitude
        Longitude
        Address
        MapLocation
        StreetName
        City
        State
        Country
        ZipCode
      }
      isSchoolAdmin
    }
  }
`;

export const isAcccontActivationLinkValid = gql`
query($ActivationLink: String) {
  isAcccontActivationLinkValid(ActivationLink: $ActivationLink) {
    Email
    UniqueAutoSinginKey
    }
  }
`;

// export const facebookSignIn = gql`
//   query($Name:String,$Email: Email!) {
//     facebookSignIn(Name:$Name,Email: $Email) {
//       ID
//     }
//   }
// `;

// export const googleSignIn = gql`
//   query($Name:String,$Email: Email!) {
//     googleSignIn(Name:$Name,Email: $Email) {
//       ID
//     }
//   }
// `;




export const isEmailExists = gql`
  query($Email: String) {
    isEmailPhoneExist(Email: $Email) {
      Message
    }
  }
`;

export const isPhoneExists = gql`
  query($Phone: String, $UserID: Int ) {
    isEmailPhoneExist(Phone: $Phone, UserID: $UserID ) {
      Message
    }
  }
`;

export const editProfile = () =>{
  return  gql`
      mutation editProfile(
        $UserID: Int
        $FirstName: String
        $LastName: String
        $City: String
        $State: String
        $Phone: String
        $Gender: String
        $DateOfBirth: Date
        $Experience: String
        $Desire: [String]
        $Certifications: [String]
        $Smoke: String
        $Drink: String
        $Pot: String
        $Bio: String
        $isBoatOwner: Boolean
        $isClubMember: Boolean
        $ClubID: Int
        $Location: LocationInputType
        $ProfileImage: String
      ){
        editProfile(
          UserID: $UserID
          FirstName: $FirstName
          LastName: $LastName
          City: $City
          State: $State
          Phone: $Phone
          Gender: $Gender
          DateOfBirth: $DateOfBirth
          Experience: $Experience
          Desire: $Desire
          Certifications: $Certifications
          Smoke: $Smoke
          Drink: $Drink
          Pot: $Pot
          Bio: $Bio
          isBoatOwner: $isBoatOwner
          isClubMember: $isClubMember
          ClubID: $ClubID
          Location: $Location
          ProfileImage: $ProfileImage
          ) {
            ID
            Token
            RefreshToken
            UserType
            FirstName
            LastName
            City
            State
            ProfileImage
            UserName
            Phone
            Email
            Password
            Gender
            DateOfBirth
            Experience
            Desire
            Certifications
            Smoke
            Drink
            Pot
            Bio
            isVerified
            isBoatOwner
            isClubMember
            ClubID
            ClubDetails
            ClubName
            ActivationLink
            ExpiryDate
            SignUpMethod
            IpAddress
            Status
            CreatedDate
            Location{
              Latitude
              Longitude
              Address
              MapLocation
            }
          }
      }
    `
  }

  export const ActivationsignIn = () => {
    return  gql`
        query signin($Email:String,$isAutoLogin:Boolean,$UniqueAutoSinginKey:String){
          signin(
                Email: $Email
                isAutoLogin:$isAutoLogin  
                UniqueAutoSinginKey:$UniqueAutoSinginKey       
            ) {
              ID
              UserType
              FirstName
              LastName
              City
              Token
              RefreshToken
              State
              ProfileImage
              UserName
              Phone
              Email
              Password
              Gender
              DateOfBirth
              Experience
              Desire
              Certifications
              Smoke
              Drink
              Pot
              Bio
              isVerified
              isBoatOwner
              isClubMember
              ClubID
              ActivationLink
              SignUpMethod
              IpAddress
              Status
              UniqueAutoSinginKey
              isProfile
            }
        }
       `
    }

export const getAllStatesCities = gql`
query {
  getAllStatesCities {
    StateID,
    StateName,
    Cities {
      Name
    },
    Status
  }
}
`;

export const getStateByID = gql`
query($StateID: Int) {
  getStateByID( StateID: $StateID) {
    StateID,
    StateName,
    Cities {
      Name
    },
    Status
  }
}
`;

export const isResetURLValid = gql`
query($ActivationLink: String) {
  isResetURLValid(ActivationLink: $ActivationLink) {
    Message
    }
  }
`;

export const createEvents = () =>{
  return  gql`
      mutation createEvents(
        $EventID: Int   
        $EventTitle: String
        $EventType: [EventTypeInput]
        $EventOwnerId: Int
        $SkillLevel: String
        $Description: String
        $ProfilePicture: String
        $Location: LocationInputType
        $EventStartDate: Date
        $EventEndDate: Date
        $EventStartTime: String
        $EventEndTime: String
        $Status: Int
        $Videos: [String]
      ){
        createEvents(
          EventID: $EventID
          EventTitle: $EventTitle
          EventType: $EventType
          EventOwnerId: $EventOwnerId
          SkillLevel: $SkillLevel
          Description: $Description
          ProfilePicture: $ProfilePicture
          Location: $Location
          EventStartDate: $EventStartDate
          EventEndDate: $EventEndDate
          EventStartTime: $EventStartTime
          EventEndTime: $EventEndTime
          Status: $Status
          Videos: $Videos
          ) {
            ID
            EventOwnerId
            Description
            EventUrl
            EventStartDate
            EventEndDate
            EventStartTime
            EventEndTime
            Status
            LikeCount
            ShareCount
            CommentCount
            CreatedDate
            ModifiedDate
          }
      }
    `
  }

  export const getEventByUrl = () => {
    return  gql`
        query getEventByUrl($EventUrl:String){
          getEventByUrl(
            EventUrl: $EventUrl        
            ) {
              ID
              EventTitle
              EventType {
                EventTypeID
                Name
              }
              SkillLevel
              EventOwnerId
              Description
              ObjectType
              EventUrl
              Location {
                Latitude
                Longitude
                Address
                MapLocation
              }
              EventMatch{
                ID
                MatchType{
                  ID
                  Name
                }
                Title
                ObjectType
                ReferenceID
                Description
                ProfilePicture
                isPublic
                Url
                StartDate
                EndDate
                StartTime
                EndTime
                Location{
                  Address
                }
                Pictures
              }
              ProfilePicture
              EventStartDate
              EventEndDate
              EventStartTime
              EventEndTime
              Status
              LikeCount
              isLike
              ShareCount
              CommentCount
              CreatedDate
              ModifiedDate
              PostPictures
              TotalPictureCount
              TotalPostPictureCount
              Videos
            }}`}

  export const createCrew = () =>{
    return  gql`
        mutation createCrew(
        $CrewID: Int
        $Title: String
        $MemberID: Int
        $Description: String
        $EventType: [EventTypeInput]
        $SkillLevel: String
        $StartDate: Date
        $EndDate: Date
        $StartTime: String
        $EndTime: String
        $Location: LocationInputType
        $Status: Int
        ){
          createCrew(
            CrewID: $CrewID
            Title: $Title
            MemberID: $MemberID
            Description: $Description
            EventType: $EventType
            SkillLevel: $SkillLevel
            StartDate: $StartDate
            EndDate: $EndDate
            StartTime: $StartTime
            EndTime: $EndTime
            Location: $Location
            Status: $Status
            ) {
              ID
              Title
            }
        }
      `
    }

  export const getAllEventTypes = gql`
  query  {
    getAllEventTypes {
      ID
      EventTitle
      Description
      Url
      Status
    }
  }
`;

// export const getAllSkills = gql`
// query  {
//   getAllSkills {
//     ID
//     UserID
//     UserProfile
//     SkillTitle
//   }
// }
// `;
export const getAllCrews = () =>{
return  gql`
query {
  getAllCrews {
      ID
      Title
      MemberID
      ProfileImage
      Description
      EventType{
        EventTypeID
        Name
      }
      Url
      ActivityType
      SkillLevel
      StartDate
      EndDate
      StartTime
      EndTime
      Location{
        Latitude
        Longitude
        Address
        MapLocation
      }
    }
}
`
}

  export const getCrewByUrl = () => {
    return  gql`
        query getCrewByUrl($CrewUrl:String){
          getCrewByUrl(
                 CrewUrl: $CrewUrl                   
            ) {
              ID
              Title
              MemberID
              ProfileImage
              Description
              EventType{
                EventTypeID
                Name
              }
              Url
              ActivityType
              SkillLevel
              StartDate
              EndDate
              StartTime
              LikeCount
              ShareCount
              isLike
              ObjectType
              EndTime
              Location{
                Latitude
                Longitude
                Address
                MapLocation
              }
              Status
              CommentCount
            }
        }
       `
    }

    export const createRide = () =>{
      return  gql`
          mutation createRide(
          $RideID: Int
          $Title: String
          $ProfileImage:String,
          $Description: String
          $EventType: [EventTypeInput]
          $SkillLevel: String
          $StartDate: Date
          $EndDate: Date
          $StartTime: String
          $EndTime: String
          $Location: LocationInputType,
          $CreatedBy:Int,
          $Gender:String,
          $BoatID:Int
          ){
            createRide(
              RideID: $RideID
              Title: $Title
              Description: $Description
              ProfileImage:$ProfileImage
              EventType: $EventType
              SkillLevel: $SkillLevel
              StartDate: $StartDate
              EndDate: $EndDate
              StartTime: $StartTime
              EndTime: $EndTime
              Location: $Location,
              CreatedBy:$CreatedBy,
              Gender:$Gender,
              BoatID:$BoatID
              ) {
                ID
                Title,
                CreatedBy
              }
          }
        `
      }

      export const createPosts = () =>{
        return  gql`
            mutation createPosts(

            $PostID : Int
            $ObjectType: String
            $ReferenceID: Int
            $UserID: Int
            $Pictures: [String]
            $Videos: [String]
            $Comment: String
            ){
              createPosts(
                PostID: $PostID
                ObjectType: $ObjectType
                ReferenceID: $ReferenceID
                UserID: $UserID
                Pictures: $Pictures
                Videos: $Videos
                Comment: $Comment
                ) {
                  ID
                  ObjectType
                  ReferenceID
                  UserID
                  Pictures
                  Videos
                  Comment
                  LikeCount
                  ShareCount
                  CommentCount
                  Status
                  CreatedDate
                  ModifiedDate
                }
            }
          `
        }


export const getRideByUrl = gql`
query($RideUrl: String) {
  getRideByUrl( RideUrl: $RideUrl) {
  
        ID
        Title
        ProfileImage
        Description
        EventType{
          Name
          EventTypeID
        }
        ActivityType
        SkillLevel
        BoatID
        StartDate
        EndDate
        LikeCount
        ShareCount
        StartTime
        ObjectType
        EndTime
        Gender
        Location{
          Address
          Latitude
          Longitude
          MapLocation
        }
        Status
        CreatedBy
        ModifiedBy
        CreatedDate
        isLike
  }
}
`;

export const getAllUsersPosts = gql`
query($UserID: Int,$ObjectType:String,$limit: Int, $offset: Int, $Location: LocationInputType, $Radius: Int) {
  getAllUsersPosts( UserID: $UserID,ObjectType: $ObjectType, limit:$limit, offset: $offset, Location: $Location, Radius: $Radius) {
    TotalCount
    AllUserPost{
    ID
    Title
    ObjectType
    ReferenceID
    Url
    ProfileImage
    Subject
    PostContent
    Description
    Location {
      Latitude
      Longitude
      Address
      MapLocation
      StreetName
      City
      State
      Country
      ZipCode
    }
    UserID
    UserDetails
    LikeCount
    isLike
    ShareCount
    CommentCount
    Status
    CreatedDate
    CreatedBy
    ModifiedBy
    ObjectTypeDetails
    }
  }
}
`;

export const myBoatProfileList = gql`
query($UserID: Int) {
  myBoatProfileList( UserID: $UserID) {
    ID
    Title
    }
}
`

export const getAllObjectTypes = gql`
  {
    getAllObjectTypes {
      ID
      ObjectType
      Url
      ProfileImage
      CreatedBy
      ModifiedBy
      CreatedDate
      ModifiedDate
      }
  }
  `
export const createBoatProfile = () =>{
  return  gql`
      mutation createBoatProfile(
        $BoatID: Int   
        $BoatType: String
        $Title: String
        $Make: String
        $SailingType: [Sailtypes]
        $Description: String
        $ProfilePicture: String
        $Location: LocationInputType
        $Length: String
        $CreatedBy: Int
        $Pictures: [String]
        $Videos: [String]
        $BoatModel: String
        $ManufactureYear: String
      ){
        createBoatProfile(
          BoatID: $BoatID   
          BoatType: $BoatType
          Title: $Title
          Make: $Make
          SailingType: $SailingType
          Description: $Description
          ProfilePicture: $ProfilePicture
          Location: $Location
          Length: $Length
          CreatedBy: $CreatedBy
          Pictures: $Pictures
          Videos: $Videos
          BoatModel: $BoatModel
          ManufactureYear: $ManufactureYear
          ) {
            ID
            BoatType
            Title
            Make
            BoatModel
            ManufactureYear
            Description
            ProfilePicture
            Length
            Url
            Status
            CreatedBy
            isRental
            LikeCount
            CommentCount
            ShareCount
            CreatedDate
            ModifiedDate
          }
      }
    `
  }

  export const getBoatProfileByUrl = () => {
    return  gql`
      query getBoatProfileByUrl($BoatUrl:String){
        getBoatProfileByUrl(
          BoatUrl: $BoatUrl        
          ) {
            Title
            SailingType {
              ID
              Name
            }
            Location {
              Latitude
              Longitude
              Address
              MapLocation
            }
            ID
            BoatType
            Make
            ObjectType
            Description
            ProfilePicture
            Length
            Url
            Status
            CreatedBy
            isRental
            LikeCount
            isLike
            CommentCount
            ShareCount
            CreatedDate
            ModifiedDate
            Pictures
            Videos
            BoatModel
            ManufactureYear
          }}`}

export const addEditSkills = () =>{
  return  gql`
      mutation addEditSkills(
        $SkillID: Int   
        $UserID: Int
        $Title: String
        $SkillType: [SkillTypes]
        $Description: String
        $Location: LocationInputType
        $ProfilePicture: String
        $Pictures: [String]
        $Videos: [String]
      ){
        addEditSkills(
          SkillID: $SkillID   
          UserID: $UserID
          Title: $Title
          SkillType: $SkillType
          Description: $Description
          Location: $Location
          ProfilePicture: $ProfilePicture
          Pictures: $Pictures
          Videos: $Videos
          ) {
            ID
            UserID
            Title
            SkillType {
              ID
              Name
            }
            Url
            Description
            Pictures
            Videos
            Status
            Location {
              Latitude
              Longitude
              Address
              MapLocation
            }
            LikeCount
            CommentCount
            ShareCount
            CreatedDate
            ModifiedDate
          }
      }
    `
  }

  export const getSkillByUrl = () => {
    return  gql`
      query getSkillByUrl($SkillUrl:String){
        getSkillByUrl(
          SkillUrl: $SkillUrl        
          ) {
            ID
            UserID
            Title
            TotalPictureCount
            ObjectType
            SkillType {
              ID
              Name
            }
            ProfilePicture
            Url
            UserProfile
            Description
            Pictures
            Videos
            Status
            Location {
              Latitude
              Longitude
              Address
              MapLocation
            }
            LikeCount
            isLike
            CommentCount
            ShareCount
            CreatedDate
            ModifiedDate
            PostPictures
            TotalPostPictureCount
          }}`}

          export const getUserDetailsByUserName = gql`
          query($UserName: String) {
            getUserDetailsByUserName( UserName: $UserName) {
              ID
              Token
              RefreshToken
              isLike
              LikeCount
              ObjectType
              PhotoVideoGallery
              UserType
              FirstName
              LastName
              City
              State
              ProfileImage
              UserName
              Phone
              Email
              Password
              Gender
              UniqueAutoSinginKey
              DateOfBirth
              Experience
              Desire
              Certifications
              Smoke
              Drink
              Pot
              Bio
              ProfileObjects
              ClubDetails
              BoatProfileList
              isVerified
              isProfile
              isBoatOwner
              BoatProfile
              isClubMember
              ClubID
              ClubName
              ActivationLink
              SignUpMethod
              ExternalSiteID
              ExternalProfileImage
              IpAddress
              Location{
                Latitude
                Longitude
                Address
              }
              Status
              CreatedDate
              ModifiedDate
              }
          }
          `

          export const getMatchListByUserName = gql`
          query($UserName: String) {
            getMatchListByUserName( UserName: $UserName) {
              MemberList
              ProfilePicture
              ObjectUrl
              Title
              LikeCount
              CommentCount
              }
          }
          `
            export const updateProfilePic = () =>{
              return  gql`
              mutation updateUsersProfilePic(
              $UserID: Int,   
              $ProfileImage:String,
              ){
                updateUsersProfilePic(
                  UserID: $UserID,   
                  ProfileImage:$ProfileImage,
                ){
                  Avatar
                }
            }
            `
            }

          
            export const getSailMatchMemberList = gql`
            query($Name: String) {
              getSailMatchMemberList( Name: $Name) {
                ID
                FirstName
                LastName
                ProfileImage
                Email
                Phone
              }
            }
            `;
            
            export const addEditMatch = () =>{
              return  gql`
                  mutation addEditMatch(
                    $MatchID: Int
                    $MatchType: [MatchTypelist]
                    $Title: String
                    $ProfilePicture: String
                    $CreatedBy: Int
                    $AttendedBy: [Int]
                    $OtherEmail:[String]
                    $Description: String
                    $StartDate: Date
                    $EndDate: Date
                    $StartTime: String
                    $EndTime: String
                    $Location: LocationInputType
                    $Pictures: [String]
                    $Videos: [String]
                  ){
                    addEditMatch(
                      MatchID: $MatchID
                      MatchType: $MatchType
                      Title: $Title
                      ProfilePicture: $ProfilePicture
                      CreatedBy: $CreatedBy
                      AttendedBy: $AttendedBy
                      OtherEmail:$OtherEmail
                      Description: $Description
                      StartDate:  $StartDate
                      EndDate: $EndDate
                      StartTime: $StartTime
                      EndTime: $EndTime
                      Location: $Location
                      Pictures: $Pictures
                      Videos: $Videos
                      ) {
                        ID
                        MatchType{
                          ID
                          Name
                        }
                        Title
                        CreatedBy
                        Description
                        MemberList
                        ProfilePicture
                        StartDate
                        EndDate
                        StartTime
                        EndTime
                        Location{
                          Latitude
                          Longitude
                          Address
                        }
                        Pictures
                        Videos
                        Status
                      }
                  }
                `
              }
            
  export const getMatchByUrl = gql`
    query($MatchUrl: String) {
      getMatchByUrl( MatchUrl: $MatchUrl) {
        ID
        isLike
        MatchType{
          Name
          ID
        }
        Title
        CreatedBy
        TotalPictureCount
        MemberList
        Description
        ProfilePicture         
        StartDate
        ObjectType
        EndDate
        StartTime
        EndTime
        MemberList
        UserDetails
        Location{
          Latitude
          Longitude
          Address
        }
        Pictures
        Videos
        Status
        LikeCount
        ShareCount
        CommentCount
        CreatedDate
        ModifiedDate
        PostPictures
        TotalPostPictureCount
  }
}
`;

  export const myBackdatedEventList = gql`
    query($UserID: Int) {
      myBackdatedEventList( UserID: $UserID) {
        ID
        EventTitle
        EventType{
          EventTypeID
          Name
        }
        EventOwnerId
        ObjectType
        EventUrl
        Location{
          Latitude
          Longitude
          Address
        }
        ProfilePicture
        EventStartDate
        EventEndDate
        EventStartTime
        EventEndTime
  }
}
`;

export const myBackdatedCoursesList = gql`
query($UserID: Int) {
  myBackdatedCoursesList( UserID: $UserID) {
ID
NameOfCourse{
  ID
  CourseName
}
Title
Url
ProfilePicture
UserID
Location{
  Latitude
  Longitude
  Address
}
StartDate
EndDate
ObjectType
StartTime
EndTime
}
}
`;


  
export const isMatchAlreadyExists = gql`
query($ObjectType: String,$ReferenceID: Int) {
  isMatchAlreadyExists( ObjectType: $ObjectType,ReferenceID: $ReferenceID) {
    IsExists
}
}
`;

export const addEditServices = () =>{
  return  gql`
      mutation addEditServices(
        $ServiceID: Int
        $ServiceType: [servicesInput]
        $Title: String
        $WebsiteUrl: String
        $Hours: [HoursDetails]
        $Rating: String
        $ProfilePicture: String
        $Description: String
        $Location: LocationInputType
        $CreatedBy:Int
        $Pictures: [String]
        $Phone: String
        $Videos: [String]
      ){
        addEditServices(
          ServiceID: $ServiceID
          ServiceType: $ServiceType
          Title:$Title
          WebsiteUrl: $WebsiteUrl
          Hours:$Hours
          Rating: $Rating
          ProfilePicture:  $ProfilePicture
          Description:  $Description
          Location: $Location
          CreatedBy:$CreatedBy
          Pictures: $Pictures
          Phone: $Phone
          Videos: $Videos
          ) {
            ID
            ServiceType {
              ID
              Name
            }
            Title
            ProfilePicture
            Description
            WebsiteUrl
            Rating
            Pictures
            Status
            CreatedBy
            ModifiedBy
            Location{
              Latitude
              Longitude
              Address
            }
          }
      }
    `
  }

export const addEditMasterCourse  = () =>{
  return  gql`
      mutation addEditMasterCourse( 
        $CourseID: Int
        $NameOfCourse: [CoursesNames]
        $Prerequisite: [String]
        $Title: String
        $Description: String
        $Instructor: InstructorInfo
        $UserID: Int
        $Location: LocationInputType
        $StartDate: Date
        $EndDate: Date
        $StartTime: String
        $EndTime: String
        $ProfilePicture: String
        $Pictures: [String]
        $Videos: [String]
      ){
        addEditMasterCourse(
          CourseID: $CourseID
          NameOfCourse: $NameOfCourse
          Prerequisite: $Prerequisite
          Title: $Title
          Description: $Description
          Instructor: $Instructor
          UserID: $UserID
          Location: $Location
          StartDate: $StartDate
          EndDate: $EndDate
          StartTime: $StartTime
          EndTime: $EndTime
          ProfilePicture: $ProfilePicture
          Pictures: $Pictures
          Videos: $Videos
          ) {
            ID
          }
      }
    `
  }

  export const getCourseByUrl = () => {
    return  gql`
      query getCourseByUrl($CourseUrl: String){
        getCourseByUrl(
          CourseUrl: $CourseUrl        
          ) {
            ID
            Url
            CreatedDate
            isLike
            Pictures
            Prerequisite
            UserDetails
            NameOfCourse{
              ID
              CourseName
            }
            Status
            StartDate
            UserID
            EndDate
            StartTime
            EndTime
            Title
            LikeCount
            ShareCount
            ObjectType
            Description
            TotalPictureCount
            Instructor {
              Name
              Phone
              Description
            }
            Location {
              Latitude
              Longitude
              MapLocation
              Address
            }
            CourseMatch{
              ID
              MatchType{
              ID
              Name
            }
             Title
            AttendedBy
            OtherEmail
            ReferenceID
            Description
            ProfilePicture
            isPublic
            Url
            StartDate
            EndDate
            StartTime
            EndTime
            Pictures
            }
            ProfilePicture
            PostPictures
            TotalPostPictureCount
            CommentCount
            Videos
          }}`}

export const getCourseMasterList = gql`
query {
  getCourseMasterList {
    Title
    Prerequisite
    Url
    ID
  }
}
`;

export const getListOfBoatTypes = gql`
  {
    getListOfBoatTypes {
      ID
      Name
      }
  }
  `
  
  export const createRental = () =>{
    return  gql`
        mutation createBoatRentalProfile(
        $BoatID: Int
        $BoatType: [Boattypes]
        $Description:String,
        $ProfilePicture: String,
        $Location: LocationInputType,
        $CreatedBy:Int,
        $WebsiteUrl: String
        $Title: String
        $Hours: [HoursDetails]
        ){
          createBoatRentalProfile(
            BoatID: $BoatID
            BoatType:$BoatType
            Description: $Description
            ProfilePicture:$ProfilePicture
            Location: $Location,
            CreatedBy:$CreatedBy,
            WebsiteUrl: $WebsiteUrl
            Title: $Title
            Hours: $Hours
            ) {
              ID
              Title,
              CreatedBy
            }
          }
          `
        }
        
  export const getEventCourseDataByID = gql`
  query($ObjectType: String ,$ReferenceID:Int) {
    getEventCourseDataByID(ObjectType:$ObjectType,ReferenceID:$ReferenceID) {
      ObjectType
      ReferenceID
      Title
      ProfileImage 
      ObjectType    
      Location {
        Latitude
        Longitude
        Address
        StreetName
        City
        State
        Country
        ZipCode
      }
      DateAndTime
  }
  }
  `;


  export const addEditPublicMatch = () =>{
    return  gql`
        mutation addEditMatch(
          $MatchID: Int
          $MatchType: [MatchTypelist]
          $ObjectType: String
          $ReferenceID: Int
          $Title: String
          $ProfilePicture: String
          $CreatedBy: Int
          $AttendedBy: [Int]
          $OtherEmail:[String]
          $Description: String
          $StartDate: Date
          $EndDate: Date
          $StartTime: String
          $isPublic: Boolean
          $EndTime: String
          $Location: LocationInputType
          $Pictures: [String]
          $Videos: [String]
        ){
          addEditMatch(
            MatchID: $MatchID
            MatchType: $MatchType
            ObjectType:$ObjectType
            ReferenceID:$ReferenceID
            Title: $Title
            ProfilePicture: $ProfilePicture
            CreatedBy: $CreatedBy
            AttendedBy: $AttendedBy
            OtherEmail:$OtherEmail
            Description: $Description
            StartDate:  $StartDate
            EndDate: $EndDate
            StartTime: $StartTime
            isPublic: $isPublic
            EndTime: $EndTime
            Location: $Location
            Pictures: $Pictures
            Videos: $Videos
            ) {
              ID
              MatchType{
                ID
                Name
              }
              Title
              CreatedBy
              Description
              MemberList
              ProfilePicture
              StartDate
              EndDate
              StartTime
              EndTime
              Location{
                Latitude
                Longitude
                Address
              }
              Pictures
              Videos
              Status
              Like
              Shares
              CommentCount
            }
        }
      `
    }
  

    export const getBoatRentalProfileByUrl = gql`
    query($BoatRentalUrl: String) {
      getBoatRentalProfileByUrl( BoatRentalUrl: $BoatRentalUrl) {
  
        ID
        Title
        isLike
        ProfilePicture
        BoatType{
          ID
          Name
        }
        BoatID
        Url
        WebsiteUrl
        Hours{
          Day
          StartTime
          EndTime
        }
        Description
        ObjectType
        Pictures
        Rating
        Location{
          Latitude
          Longitude
          Address
        }
        isAvailable
        CreatedBy
        ModifiedBy
        Status
        CreatedDate
        ModifiedDate
        LikeCount
        ShareCount
        CommentCount
        PostPictures
        TotalPostPictureCount
  }
}
`;

    export const getServiceByUrl = () => {
      return  gql`
        query getServiceByUrl($ServiceUrl:String){
          getServiceByUrl(
            ServiceUrl: $ServiceUrl        
            ) {
              ID
              ServiceType{
                ID
                Name
              }
              Title
              CreatedBy
              TotalPictureCount
              ProfilePicture
              Description
              UserDetails
              CreatedDate
              isLike
              ObjectType
              WebsiteUrl
              Rating
              Pictures
              PostPictures
              TotalPostPictureCount
              Status
              LikeCount
              SharesCount
              Location {
                Latitude
                Longitude
                Address
              }
              Phone
              CommentCount
              Videos
            }}`}

export const getAllMasterClubs = gql`
  query($Name: String) {
    getAllMasterClubs(Name: $Name) {
      ID
      Name
      City
      State
      Email
      Url
      Phone
      Status
      Notes
    }
  }
`;



             export const dashBoarSeach = gql`
                    query($Name: String) {
                      dashBoarSeach( Name: $Name) {
                        CreatedBy
                        Title
                        ID
                        
                        Url
                        Description
                        ProfileImage
                        CreatedDate
                        ObjectTypeDetails
                        }
                    }
                    `

export const createClub  = () =>{
  return  gql`
      mutation createClub(
        $ClubID: Int
        $Title: String
        $Description: String
        $UserID: Int
        $ProfilePicture: String
        $BurgeeImage: String
        $Amenities: [Amenitestypes]
        $Location: LocationInputType
        $Pictures: [String]
        $Videos: [String]
      ){
        createClub(
          ClubID: $ClubID
          Title: $Title
          Description: $Description
          UserID: $UserID
          ProfilePicture: $ProfilePicture
          BurgeeImage: $BurgeeImage
          Amenities: $Amenities
          Location: $Location
          Pictures: $Pictures
          Videos: $Videos
          ) {
            ID
            Videos
          }
      }
    `
  }

export const getAllClubAmenities = gql`
query {
  getAllClubAmenities {
    ID
    AmenityName
    Description
    Status
    }
}
`
export const getClubByUrl = () => {
  return  gql`
    query getClubByUrl($ClubUrl:String){
      getClubByUrl(
        ClubUrl: $ClubUrl        
        ) {
          ID
          Title
          isLike
          Description
          Amenities {
            ID
            Name
          }
          ProfilePicture
          Pictures
          PostPictures
          TotalPostPictureCount
          Url
          Status
          LikeCount
          ShareCount
          ObjectType
          UserDetails
          TotalPictureCount
          Location {
            State
            StreetName
            Address
            Latitude
            Longitude
            MapLocation
            City
            Country
            ZipCode
          }
          UserID
          CommentCount
          Videos
        }
      }
`}

export const addEditGroups  = () =>{
  return  gql`
      mutation addEditGroups( 
        $GroupID: Int
        $Url: String
        $Title: String
        $GroupType: String
        $Topic: String
        $Description: String
        $UserID: Int
        $FacebookUrl: String
        $LinkedInUrl: String
        $CoOrgnizers: [MemberInput]
        $InviteMembers: [inviteInput]
        $OtherEmail: [String]
        $ProfilePicture: String
        $GroupIcon: String
        $Pictures: [String]
        $Videos: [String]
        $Location: LocationInputType
      ){
        addEditGroups(
          GroupID: $GroupID
          Url: $Url
          Title: $Title
          GroupType: $GroupType
          Topic: $Topic
          Description: $Description
          UserID: $UserID
          FacebookUrl: $FacebookUrl
          LinkedInUrl: $LinkedInUrl
          CoOrgnizers: $CoOrgnizers
          InviteMembers: $InviteMembers
          OtherEmail: $OtherEmail
          ProfilePicture: $ProfilePicture
          GroupIcon: $GroupIcon
          Pictures: $Pictures
          Videos: $Videos
          Location: $Location
          ) {
            ID
            Url
          }
      }
    `
  }

  export const getGroupByUrl = () => {
    return  gql`
      query getGroupByUrl($GroupUrl:String){
        getGroupByUrl(
          GroupUrl: $GroupUrl        
          ) {
            ID
            Title
            GroupType
            TotalPictureCount
            TotalPostPictureCount
            PostPictures
            UserDetails
            CoOrgnizers{
              ID
              Email
              Name
              Phone
              ProfilePicture
            }
            Topic
            Status
            Url
            LikeCount
            ShareCount
            isLike
            ObjectType
            Description
            UserID
            InviteMembers{
              Name
              UserID
            }
            OtherEmail
            Location{
              Latitude
              Longitude
              Address
              StreetName
              State
              City
              MapLocation
              }
            LinkedInUrl
            FacebookUrl
            Pictures
            Videos
            ProfilePicture
            CommentCount
            isJoinGroupMember
            JoinGroupMember {
              UserID
              UserData
              Status
              JoinDate
            }
          }
        }
  `}

    export const viewAllObjectTypePictures = () => {
      return  gql`
        query viewAllObjectTypePictures($ReferenceID:Int $UserID:Int $ObjectType:String){
          viewAllObjectTypePictures(
            ReferenceID: $ReferenceID   
            UserID:$UserID
            ObjectType:$ObjectType     
            ) {
              Pictures
              UserDetails
              CreatedDate
            }
          }
    `}
    export const viewAllForProfile = gql`
    query viewAllObjectTypePictures($UserID:Int $ObjectType:String){
      viewAllObjectTypePictures( 
        UserID:$UserID
        ObjectType:$ObjectType     
        ) {
          Pictures
          UserID
          UserDetails
          CreatedDate
        }
      }
`

export const getPostList = gql`
  query($ObjectType: String,$limit: Int, $offset: Int, $ReferenceID: Int) {
    getPostList( ObjectType: $ObjectType, limit:$limit, offset: $offset, ReferenceID: $ReferenceID) {
      TotalPostCount
      AllPosts{
        ID
        UserID
        ObjectType
        Pictures
        Videos
       UserDetails
         Comment
        Status
        CommentCount
        LikeCount
        ShareCount
        CreatedDate
        isLike
        }
  }
}
`;


export const addComment = () =>{
  return  gql`
      mutation addComment(
      $ParentCommentID: Int
      $Reply: String
      $ID: Int
      $UserID: Int
      $ReferenceID: Int
      $ReferenceType: String
      $Comment: String      
      $Pictures: String
      ){
        addComment(
          ParentCommentID: $ParentCommentID
          Reply: $Reply
          ID: $ID
          UserID: $UserID
          ReferenceID: $ReferenceID
          Comment: $Comment
          Pictures: $Pictures
          ReferenceType: $ReferenceType
          ) {
            ID
            ParentCommentID
            UserID
            ReferenceID
            isFromComment
            Comment
            Pictures
            Videos
            Reply
            ReferenceType
          }
      }
    `
  }

export const viewChatNotification = gql`
  query($UserID: Int) {
    viewChatNotification(UserID: $UserID) {
      Count
    }
  }
`;

export const sharePost  = () =>{
  return  gql`
      mutation sharePost( 
        $ID: Int
        $UserID: [Int]
        $ReferenceUserID: Int
        $ReferenceID: Int
        $PostUrl: String
        $PostContent: String
        $ObjectType: String
        $ShareType: String
        $isMember: Boolean
        $OtherEmail: [String]
      ){
        sharePost(
          ID: $ID
          UserID: $UserID
          ReferenceUserID: $ReferenceUserID
          ReferenceID: $ReferenceID
          PostUrl: $PostUrl
          PostContent: $PostContent
          ObjectType: $ObjectType
          ShareType: $ShareType
          isMember: $isMember
          OtherEmail: $OtherEmail
          ) {
            TotalShareCount
            sailMatchMemberShareCount
            EmailShareCount
            FacebookShareCount
            TwitterShareCount
          }
      }
    `
  }



  export const getListofComments = gql`
  query($ReferenceID: Int, $ReferenceType: String) {
    getListofComments( ReferenceID: $ReferenceID, ReferenceType: $ReferenceType) {
      TotalCmmtCount
      AllComments{
      ID
      UserID
      Status
      Pictures
      Comment
      Reply
      UserDetails
      isFromComment
      ParentCommentID
      ObjectType
      Videos
      ReplyCount
      LikeCount
      ReferenceID
      CreatedDate
      ModifiedDate
      isLike
        }
  }
}
`;


export const getListofReplies = gql`
  query($CommentID: Int) {
    getListofReplies( CommentID: $CommentID) {
      TotalReplCount
      AllReply{
        ID
     UserID
     UserDetails
       Comment
       Reply
       isLike
       LikeCount
       ReplyCount
      CreatedDate
        ModifiedDate
        Pictures
      }
  }
}
`;

export const getChatNotificationList = gql`
  query($UserID: Int) {
    getChatNotificationList(UserID: $UserID) {
      isRead
      isView
      ObjectDetails
      image
      room
      message
      sender
      type
      chatType
      objectType
      createdAt
      receiver
    }
  }
`;

export const reportPost = gql`
  mutation(
    $ID: Int
    $ReferenceID: Int
    $ReferenceType: String
    $ObjectType: String
    $ReferenceUserID: Int
    $ReporterDetails: ReporterInput
  ) 
  {
    reportPost(
      ID: $ID
      ReferenceID: $ReferenceID
      ReferenceType: $ReferenceType
      ObjectType: $ObjectType
      ReferenceUserID: $ReferenceUserID
      ReporterDetails: $ReporterDetails
    ) {
      ID
      ReferenceID
      ReferenceType
      ReporterDetails {
        UserID
        Reason
      }
      ReferenceUserID
    }
  }
`;

  export const getAllPublishBlogs = gql`
  query($isSeqRequired: Boolean) {
    getAllPublishBlogs(isSeqRequired: $isSeqRequired) {
      ID
      Title
      FeatureImage
      CreatedDate
      Url
    }
  }
`;

export const getBlogPostByUrl = gql`
  query($BlogUrl: String) {
    getBlogPostByUrl(BlogUrl: $BlogUrl) {
      ID
      Title
      FeatureImage
      SubTitle
      UserID
      UserDetails
      Description
      Pictures
      isPublish
      PrioritySeq
      Slug
      Url
      AmpSlug
      Thumbnail
      Tags
      Status
      Categories{ ID, Name }
      ViewCount
      LikeCount
      ShareCount
      CommentCount
      CreatedDate
      ModifiedDate
      isLike
    }
  }
`;

export const getUsersBoatCount = gql`
  query($UserID: Int) {
    getUsersBoatCount(UserID: $UserID) {
      BoatCount
    }
  }
`;

export const viewNotificationList = gql`
  query($UserID: Int, $limit: Int, $offset: Int) {
    viewNotificationList(UserID: $UserID,limit: $limit,offset: $offset) {
      TotalCount
      AllNotifications{
        ID
        SenderID
        SenderDetails
        RecieverID
        RecieverDetails
        Purpose
        NotifyMessage
        Subject
        ObjectType
        ReferenceID
        isView
        isRead
        Status
        CreatedDate
        ModifiedDate
        CreatedBy
        ModifiedBy
      }
    }
  }
`;

export const updatePassword = gql`
mutation($UserID: Int, $OldPassword:String, $NewPassword:String) {
  updatePassword(UserID:$UserID, OldPassword:$OldPassword, NewPassword:$NewPassword) {
    Message
  }
}
`;

export const isUserBlocked = gql`
  query($UserID: Int) {
    isUserBlocked(UserID: $UserID) 
  }
`;

export const joinGroup = gql`
mutation($UserID: Int, $GroupID:Int) {
  joinGroup(UserID:$UserID, GroupID:$GroupID) {
    ID
  }
}
`;

export const exitGroup = gql`
mutation($UserID: Int, $GroupID:Int) {
  exitGroup(UserID:$UserID, GroupID:$GroupID) {
    ID
  }
}
`;

export const getAllObjectTypesPopup = gql`
  {
    getAllObjectTypes {
      ID
      ObjectType
      Question
      Description
    }
  }
  `;