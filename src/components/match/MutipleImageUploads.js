import React, { Component } from "react";
import { DropzoneDialog } from "material-ui-dropzone";
import Button from "@material-ui/core/Button";

class ImageUploads extends React.Component {
  constructor() {
    super();
    this.state = {
      images: [],
    };
  }
  updateImage = (imageArray) => {
    this.setState({
      featureImage: imageArray[0].base64,
      matchImages: this.getOnlyBase64(imageArray),
      imageArray: imageArray,
      images: imageArray,
    });
  };
  getOnlyBase64 = (imageArray) => {
    var base64ImageArray = [];
    imageArray.forEach((image) => {
      if (image.base64) {
        base64ImageArray.push(image.base64);
      } else {
        base64ImageArray.push(image.url);
      }
    });
    this.props.callback(base64ImageArray, imageArray);
    return base64ImageArray;
  };
  onDropFiles = (acceptedFiles) => {
    acceptedFiles.forEach((file, index, array) => {
      const reader = new FileReader();
      // reader.onabort = () => console.log("file reading was aborted");
      // reader.onerror = () => console.log("file reading has failed");
      reader.readAsDataURL(file);
      reader.onload = () => {
        const base64 = reader.result;
        let imgArr = this.state.images;
        imgArr.push({
          url: URL.createObjectURL(file),
          base64: base64,
        });
        this.setState({ images: imgArr });
        if (index === array.length - 1) {
          this.updateImage(imgArr);
        }
      };
    });
  };

      handleClose() {
        this.setState({
            open: false
        });
    }
 
    handleSave(files) {
        //Saving files to state for further use and closing Modal.
        this.setState({
            files: files,
            open: false
        });
    }
 
    handleOpen() {
        this.setState({
            open: true,
        });
    }
render (){
return(
    <React.Fragment>
    <Button onClick={this.handleOpen.bind(this)} className="form-control"
     style={{
        border: "3px dashed #888",
        borderRadius: '4px',
        color: '#aaa',
        textAlign: 'center',
        fontSize: '15px',
        fontWeight: 'bold',
        padding: '3.5em',
        cursor: 'pointer'
                        }}
    >
                  Click Here to Add Pictures 
                </Button>            
                <DropzoneDialog
                    open={this.state.open}
                    onSave={this.onDropFiles.bind(this)}
                    filesLimit={50}
                    acceptedFiles={['image/jpeg', 'image/png', 'image/bmp']}
                    showPreviews={true}
                    maxFileSize={500000}
                    onClose={this.handleClose.bind(this)}
                    dropzoneText={"Drop pictures here.."}
                />
    </React.Fragment>
)
}
}

export default ImageUploads;
