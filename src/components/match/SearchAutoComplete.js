import React from "react";
import useAutocomplete from "@material-ui/lab/useAutocomplete";
import NoSsr from "@material-ui/core/NoSsr";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";
import styled from "styled-components";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { connect } from "react-redux";
import { createCrewAction } from "../../data/actions/CrewAction";
import { getSailMatchMemberList } from "../../data/query";

const Label = styled("label")`
  padding: 0 0 4px;
  line-height: 1.5;
  display: block;
`;

const InputWrapper = styled("div")`
  width: 100%;
  background-color: #fff;
  border: 1px solid #ced4da;
  border-radius: 15px;
  padding: 1px;
  display: flex;
  flex-wrap: wrap;

  &:hover {
    border-color: #40a9ff;
  }

  &.focused {
    border-color: #40a9ff;
    box-shadow: 0 0 0 2px rgba(24, 144, 255, 0.2);
  }

  & input {
    font-size: 14px;
    height: 30px;
    box-sizing: border-box;
    padding: 4px 6px;
    width: 0;
    min-width: 30px;
    flex-grow: 1;
    border: 0;
    margin: 0;
    outline: 0;

    border-radius: 15px;
  }
`;

const Tag = styled(({ label, img, ls, onDelete, ...props }) => (
  <div {...props}>
    <img src={img} style={{ width: 40, height: 40, paddingRight: 10 }} />
    <span>
      {label} {ls}
    </span>
    <CloseIcon onClick={onDelete} />
  </div>
))`
  display: flex;
  align-items: center;
  height: 24px;
  margin: 2px;
  line-height: 22px;
  background-color: #fafafa;
  border: 1px solid #e8e8e8;
  border-radius: 15px;
  box-sizing: content-box;
  padding: 5px 4px 5px 10px;
  outline: 0;
  overflow: hidden;

  &:focus {
    border-color: #40a9ff;
    background-color: #e6f7ff;
  }

  & span {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
  }

  & svg {
    font-size: 12px;
    cursor: pointer;
    padding: 0px;
    margin: 4px;
  }
`;

const Listbox = styled("ul")`
  width: 95%;
  margin: 2px 0 0;
  padding: 0;
  position: absolute;
  list-style: none;
  background-color: #fff;
  overflow: auto;
  max-height: 250px;
  border-radius: 4px;
  box-shadow: 0 2px 8px rgba(0, 0, 0, 0.15);
  z-index: 1;

  & li {
    padding: 5px 12px;
    display: flex;

    & span {
      flex-grow: 1;
    }

    & svg {
      color: transparent;
    }
  }

  & li[aria-selected="true"] {
    background-color: #fafafa;
    font-weight: 600;

    & svg {
      color: #1890ff;
    }
  }

  & li[data-focus="true"] {
    background-color: #e6f7ff;
    cursor: pointer;

    & svg {
      color: #000;
    }
  }
`;

function SearchAutoComplete(props) {
  const [keyValue, setValue] = React.useState("");
  const [membersList, setMembersList] = React.useState([]);
  const [selectedMember, setSelectedMembers] = React.useState([]);

  var userData = [];
  const getAllMatchMembers = (name) => {
    props.client
      .query({
        query: getSailMatchMemberList,
        variables: { Name: name },
      })
      .then((res) => {
        userData = res.data.getSailMatchMemberList;
        setMembersList(res.data.getSailMatchMemberList);
      });
  };

  const getKeys = (e) => {
    setValue({ keyValue: e.target.value });
    if (e.target.value.length > 2) {
      getAllMatchMembers(e.target.value);
    }
  };

  const {
    getRootProps,
    getInputLabelProps,
    getInputProps,
    getTagProps,
    getListboxProps,
    getOptionProps,
    groupedOptions,
    value,
    focused,
    setAnchorEl,
    onChange,
  } = useAutocomplete({
    id: "customized-hook-demo",
    // defaultValue: [top100Films[1]], //[selectedListFromProps]
    multiple: true,
    options: membersList,
    getOptionLabel: (option) => option.FirstName,
    // onChange:(event) => 
  });

  const getSelectedValue = () => {
    props.callback(value);
  };

  return (
    <NoSsr>
      {getSelectedValue()}
      <div>
        <div {...getRootProps()}>
          <Label {...getInputLabelProps()}>{props.label ? props.label : "Who Attended"}</Label>
          <InputWrapper ref={setAnchorEl} className={focused ? "focused" : ""}>
            {value.map((option, index) => (
              <Tag
                label={option.FirstName}
                ls={option.LastName}
                img={
                  option.ProfileImage
                    ? option.ProfileImage
                    : "../../assets/images/post-user-icon.png"
                }
                {...getTagProps({ index })}
              />
            ))}
            <span
              value={getInputProps().value}
              onChange={getKeys}
              style={{ width: "100%", borderRadius: "15px" }}
            >
              {" "}
              <input
                {...getInputProps()}
                value={getInputProps().value}
                style={{ width: "100%" }}
              />
            </span>
          </InputWrapper>
        </div>
        {groupedOptions.length > 0 ? (
          <Listbox {...getListboxProps()}>
            {groupedOptions.map((option, index) => (
              <li {...getOptionProps({ option, index })}>
                <img
                  src={
                    option.ProfileImage
                      ? option.ProfileImage
                      : "../../assets/images/post-user-icon.png"
                  }
                  style={{ width: 45, height: 45, marginRight: 20 }}
                ></img>{" "}
                &nbsp;&nbsp;
                <span style={{ paddingTop: 10 }}>
                  {option.FirstName} {option.LastName}
                </span>
                <CheckIcon fontSize="small" style={{ marginTop: 10 }} />
              </li>
            ))}
          </Listbox>
        ) : null}
      </div>
    </NoSsr>
  );
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { createCrewAction }),
  withApollo
)(SearchAutoComplete);
