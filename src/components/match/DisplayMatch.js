import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getMatchByUrl } from "../../data/query";
import ImagePlaceholder from "../../services/ImagePlaceholder";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";
import MemberContent from "../common/MemberContent";
import StaticMember from "../common/StaticMember";
import { ToastContainer, toast } from "react-toastify";
import { deleteMatch } from "../../data/mutation";
import PhotosContent from "../common/PhotosContent";
import ConfirmationPopup from "../common/ConfirmationPopup";
import VideosContent from "../common/VideosContent";
import PostListParent from "../common/PostListParent";
import {getViewAllPictures} from '../../data/actions/CommonActions'
import ObjectLikeDislike from "../common/ObjectLikeDislike";
import HelmetData from '../common/_helmet' ;
import ShareContent from "../common/ShareModel/shareOption";

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
let Name = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

if (LocalStorageData.ID) {
  let FirstName = LocalStorageData.FirstName;
  let LastName = LocalStorageData.LastName;
  Name = FirstName + " " + LastName;
}
class DisplayMatch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false,
      matchData: [],
      MatchUrl: this.props.match.params.id,
      membersList: [],
      picturesArray: [],
      matchPictures:[],
      userDetailsPic:{},
      totalCount:"",
      metaData:[],
      showShareModel: false,
    };
  }

  getMatchData = () => {
    this.props.client
      .query({
        query: getMatchByUrl,
        variables: {
          MatchUrl: this.state.MatchUrl,
        },
        fetchPolicy: "network-only",
      })
      .then((res) => {

        var metaData = res.data.getMatchByUrl;

        var metaArr = [];
        metaArr.push({
         title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
         keywords : metaData.ObjectType, SITE_STATIC_NAME,
         description : metaData.Description ? metaData.Description : '',
         og_url : window.location.href,
         og_title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
         og_description : metaData.Description ? metaData.Description : '',
         canonical_url : window.location.href,
         og_image : metaData.ProfilePicture ? metaData.ProfilePicture : SITELOGO,
         amp_url : window.location.href,
        })

        this.setState({
          metaData:metaArr[0],
          matchData: res.data.getMatchByUrl,
          renderLike:true,
          matchPictures:res.data.getMatchByUrl.Pictures,
          membersList: res.data.getMatchByUrl.MemberList,
          userDetailsPic: res.data.getMatchByUrl.UserDetails,
          totalCount:res.data.getMatchByUrl.LikeCount
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getMatchData();
  }
  deleteObject = () => {
    this.props.client
      .mutate({
        mutation: deleteMatch,
        variables: {
          MatchID: parseInt(this.state.matchData.ID),
          UserID: ID,
        },
      })
      .then((res) => {
        if (res.data.deleteMatch) {
          toast.success("Match deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
      });
  };
  handleImageLoaded() {
    this.setState({ loaded: true });
  }

  goBack = () => {
    this.props.history.goBack();
  };

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };
  
  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.matchData.ID,this.state.matchData.CreatedBy,this.state.matchData.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
      this.setState({matchPictures:picturesData.Pictures, 
                     userDetailsPic:picturesData.UserDetails,
                    })
        console.log(picturesData,"picturesData");
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    
    });
   }

  viewLessPictures = () =>{
    let queryResult = this.state.matchPictures
    let filterdata = [];
 for (let i = 0; i <  queryResult.length; i++) {         
     filterdata.push(queryResult[i]);
     if (filterdata.length == 4) {
       break;
     }
   }
   this.setState({matchPictures:filterdata})
}
objectLike= (count) =>{
  console.log(count);
this.setState({totalCount:count})
}
  goToEditPage = (url) => {
    this.props.history.push('/edit-match/'+this.state.MatchUrl)
  }

  render() {
    const { loaded, matchData,matchPictures, totalCount, renderLike} = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};
    return (
      <div className="container">
         <HelmetData metaDetails={this.state.metaData} history={this.props}/>
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a className="desktop_back cursor-pointer" onClick={this.goBack}>
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
        </div>

        <div className="col-md-12">
          <div className="row">
            <div className="col-md-5 col-sm-12 mb-2 mt-2">
              {!loaded && <ImagePlaceholder />}
              <img
                src={
                  matchData.ProfilePicture
                    ? matchData.ProfilePicture
                    : "../../assets/images/no_image.png"
                }
                className="match-img"
                alt="Back"
                style={imageStyle}
                onLoad={this.handleImageLoaded.bind(this)}
              />
            </div>

            <div className="col-md-7 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {matchData.Title ? matchData.Title : ""}
                {ID && matchData.CreatedBy === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img">
                <img src="../../assets/images/match-icon.png" alt="Back" />
              </div>
              <p className="date-time-text">
                {" "}
                <img
                  src="../../assets/images/date-icon.png"
                  alt="location-icon"
                  className="pr-2"
                />{" "}
                <span> {_formatDate(matchData.CreatedDate)}</span>{" "}
              </p>
              <p className="date-time-text">
                {" "}
                <img
                  src="../../assets/images/location-icon.png"
                  alt="location-icon"
                  className="pr-2"
                />{" "}
                <span>
                  {matchData.Location ? matchData.Location.Address : ""}
                </span>{" "}
              </p>
              <p className="date-time-text">
                {" "}
                <img
                  src="../../assets/images/sail-icon.png"
                  alt="location-icon"
                  className="pr-2"
                />{" "}
                <span>Sail</span>
                {ID && ID == matchData.CreatedBy && (
                  <ConfirmationPopup
                    callBack={this.deleteObject}
                    object={"Match"}
                  />
                )}
              </p>
              <hr />
              <div className="like">
              {ID && renderLike && <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={matchData.ObjectType} 
                ReferenceID={matchData.ID} 
                status={matchData.isLike} 
                ReferenceUserID={matchData.CreatedBy} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
              </div>
              <div className="comment">
                <img
                  src="../../assets/images/comment-icon.png"
                  alt="comment-icon"
                  className="pl-6"
                />
                <span>
                  {" "}
                  {matchData.CommentCount ? matchData.CommentCount : 0}
                </span>
              </div>
              <div className="forward" onClick={this.openShareModal}>
                <img
                  src="../../assets/images/forward-icon.png"
                  alt="forward-icon"
                  className="pl-6"
                />
                <span> {matchData.ShareCount ? matchData.ShareCount : 0}</span>
              </div>
              { ID && this.state.showShareModel &&
                <ShareContent 
                  ObjectType="Matches"
                  ReferenceID={parseInt(this.state.matchData.ID)}
                  ReferenceUserID={ID}
                  PostUrl={this.state.MatchUrl}
                  closeHandler={this.openShareModal}
                  shareUrl={url.concat(this.props.match.url)}
                  title={matchData.Title}
                  quote={matchData.Title} />
              }

              <select className="form-control col-12" id="membersOnly">
                <option>Members Only</option>
                <option>New York</option>
                <option>New York</option>
              </select>
              <select className="form-control col-12" id="membersOnlyMobile">
                <option>Members Only</option>
                <option>New York</option>
                <option>New York</option>
              </select>
              <img
                src="../../assets/images/white-drop-down-arrow.png"
                className="matchDownArrow"
                alt="white-drop-down-arrow"
              />
            </div>
          </div>
        </div>

        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">About this match</h5>
            <p className="about-event-info">
              {matchData.Description ? matchData.Description : ""}
            </p>
          </div>
        </div>

        {typeof this.state.membersList !== "undefined" &&
          this.state.membersList.length > 0 && (
            <MemberContent MemberList={this.state.membersList} forMatch />
          )}

        {typeof this.state.membersList == "undefined" ||
          (this.state.membersList.length == 0 && <StaticMember />)}
        <PhotosContent Pictures={matchPictures} Details={matchData} postPictures={matchData.PostPictures} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getMatchData} />

        <VideosContent videos={matchData.Videos} />
        
        {matchData.ObjectType &&
        <PostListParent ReferenceID={matchData.ID} ObjectType={matchData.ObjectType} updatePhotos={this.getMatchData} />}
        <ToastContainer autoClose={1000}/>
      </div>
    );
  }
}

export default compose(connect(null, {getViewAllPictures}), withApollo)(DisplayMatch);
