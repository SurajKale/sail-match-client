import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import {
  createMatchAction,
  createPublicMatchAction,
} from "../../data/actions/MatchActions";
import {
  getAllEventTypes,
  myBackdatedEventList,
  myBackdatedCoursesList,
  isMatchAlreadyExists,
  getEventCourseDataByID,
} from "../../data/query";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import Component from "../location/LocationContent";
import { StyledDropZone } from "react-drop-zone";
import Dropzone from "react-dropzone";
import ReorderImages from "react-reorder-images";
import SearchAutoComplete from "./SearchAutoComplete";
import ImageUploads from "./MutipleImageUploads";
import Clone from "./Clone";
import AddVideo from "../common/Video/AddVideo";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class CreateMatchPub extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dec: "No",
      eventType: [],
      skillLevel: "",
      startDate: new Date(),
      endDate: new Date(),
      // startTime: "",
      // endTime: "",
      streetName: "",
      city: "",
      latitude: "",
      longitude: "",
      address: "",
      state: "",
      matchTitle: "",
      description: "",
      eventsArray: [],
      profilePic: "",
      file: [null],
      images: [],
      isPublic: false,
      whoAttendedList: [],
      baseImageArray: [],
      otherWhoAttended: [],
      publicDisable: false,
      publicEventsArray: [],
      publicCoursesArray: [],
      matchExistError: "",
      selectedEventCourse: {},
      button: true,
      videos: []
    };
    this.toBase64 = this.toBase64.bind(this);
    this._getProps = this._getProps.bind(this);
  }
  componentDidMount = () => {
    this.getAllEventsTypes();
  };

  _getProps(data) {
    this.setState({ otherWhoAttended: data });
  }

  getvideoUrls = (data) => {
    this.setState({ videos: data });
  }

  getAllEventsTypes = () => {
    this.props.client
      .query({
        query: getAllEventTypes,
      })
      .then((res) => {
        res.data.getAllEventTypes.forEach((item) => {
          item.isSelected = false;
        });
        this.setState({
          eventsArray: res.data.getAllEventTypes,
        });
      });
  };
  goBack = () => {
    this.props.history.goBack();
  };
  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };
  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  handleEventsSelectNew = (id, title) => {
    this.setState({
      eventType: {
        ID: id,
        Name: title,
      },
    });
  };
  handleStartChange = async (startDate) => {
    this.setState({
      startDate: startDate,
    });
  };
  handleEndChange = async (e) => {
    this.setState({
      endDate: e,
    });
  };
  handleChanges = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };
  getLocationDetails = (location) => {
    let address =
      location.mainPlace +
      "," +
      "" +
      location.city +
      "," +
      "" +
      location.zipCode +
      "," +
      "" +
      location.state +
      "," +
      "" +
      location.country;
    this.setState({
      city: location.city,
      latitude: location.latitude,
      longitude: location.longitude,
      address: address,
      state: location.state,
    });
  };
  handleStartTimeChange = (e) => {
    this.setState({ startTime: e });
  };
  handleEndTimeChange = (e) => {
    this.setState({ endTime: e });
  };
  getWhoAttendedList = (whoAttended) => {
    this.setState({ whoAttendedList: whoAttended });
  };
  getImagesArray = (baseArray, imagesArray) => {
    this.setState({ images: imagesArray, baseImageArray: baseArray });
  };

  createMatch = (e) => {
    this.setState({ button : !this.state.button })
    const {
      eventType,
      finalStartDate,
      finalEndDate,
      startTime,
      endTime,
      latitude,
      longitude,
      address,
      objectType,
      referenceID,
      startDate,
      endDate,
      eventsArray,
      matchTitle,
      profilePic,
      description,
      baseImageArray,
      whoAttendedList,
      otherWhoAttended,
      videos
    } = this.state;

    let sdate = startDate.toISOString().substring(0, 10);
    let edate = endDate.toISOString().substring(0, 10);

    this.setState({ finalStartDate: sdate, finalEndDate: edate });

    let lat = latitude.toString();
    let long = longitude.toString();

    var options = eventsArray.filter(function (item) {
      return item.isSelected;
    });

    const Location = { Latitude: lat, Longitude: long, Address: address };
    const EventType = options.map(function (item) {
      return { ID: item.ID, Name: item.EventTitle };
    });

    var AttendedBy = whoAttendedList.map(function (item) {
      return item["ID"];
    });
    let Pictures = baseImageArray;

    if (this.state.isPublic == true) {
      this.props
        .createPublicMatchAction(
          this.props.client,
          eventType,
          objectType,
          referenceID,
          matchTitle,
          profilePic,
          ID,
          AttendedBy,
          otherWhoAttended,
          description,
          finalStartDate,
          finalEndDate,
          startTime,
          this.state.isPublic,
          endTime,
          Location,
          Pictures,
          videos
        )
        .then((res) => {
          toast.success("Match created successfully", {
            onClose: () => this.props.history.push("/"),
          });
        })
        .catch(function (error) {
          toast.error("Something went wrong", {});
        });
    } else {
      this.props
        .createMatchAction(
          this.props.client,
          eventType,
          matchTitle,
          profilePic,
          ID,
          AttendedBy,
          otherWhoAttended,
          description,
          sdate,
          edate,
          startTime,
          endTime,
          Location,
          Pictures,
          videos
        )
        .then((res) => {
          toast.success("Match created successfully", {
            onClose: () => this.props.history.push("/"),
          });
        })
        .catch(function (error) {
          toast.error("Something went wrong", {});
        });
    }
  };
  goBack = () => {
    this.props.history.goBack();
  };
  getEventsList = (isPublic) => {
    this.props.client
      .query({
        query: myBackdatedEventList,
        variables: { UserID: ID },
      })
      .then((res) => {
        this.setState({
          publicEventsArray: res.data.myBackdatedEventList,
        });
      });
  };
  getCoursesList = (isPublic) => {
    this.props.client
      .query({
        query: myBackdatedCoursesList,
        variables: { UserID: ID },
      })
      .then((res) => {
        this.setState({
          publicCoursesArray: res.data.myBackdatedCoursesList,
        });
      });
  };
  handleDecision = (e) => {
    if (e.target.value == "Yes") {
      this.setState({ publicDisable: true, isPublic: true, dec: "Yes" });
      this.getEventsList(e.target.value);
      this.getCoursesList(e.target.value);
    } else {
      //  Private Match
      this.setState({
        publicDisable: false,
        isPublic: false,
        dec: "No",
        startDate: new Date(),
        endDate: new Date(),
        startTime: "",
        endTime: "",
        streetName: "",
        city: "",
        latitude: "",
        longitude: "",
        address: "",
        state: "",
        matchTitle: "",
      });
    }
  };
  getEventOrCourseByID = (eventdata) => {
    this.setState({
      objectType: eventdata.ObjectType,
      referenceID: eventdata.ID,
    });
    this.props.client
      .query({
        query: getEventCourseDataByID,
        variables: {
          ObjectType: eventdata.ObjectType,
          ReferenceID: eventdata.ID,
        },
      })
      .then((res) => {
        var sdate = new Date(
          res.data.getEventCourseDataByID.DateAndTime.StartDate
        );
        var edate = new Date(
          res.data.getEventCourseDataByID.DateAndTime.EndDate
        );
        let stime = moment(
          res.data.getEventCourseDataByID.DateAndTime.StartTime
        );
        let etime = moment(res.data.getEventCourseDataByID.DateAndTime.EndTime);
        let ssdate = sdate.toISOString().substring(0, 10);
        let eedate = edate.toISOString().substring(0, 10);
        this.setState({
          finalEndDate: eedate,
          finalStartDate: ssdate,
          startDate: sdate,
          endDate: edate,
          startTime: stime,
          endTime: etime,
          streetName: res.data.getEventCourseDataByID.Location.StreetName,
          city: res.data.getEventCourseDataByID.Location.City,
          latitude: res.data.getEventCourseDataByID.Location.Latitude,
          longitude: res.data.getEventCourseDataByID.Location.Longitude,
          address: res.data.getEventCourseDataByID.Location.Address,
          state: res.data.getEventCourseDataByID.Location.State,
          matchTitle: res.data.getEventCourseDataByID.Title,
        });
      });
  };
  selectEventOrCourse = (eventdata) => {
    this.setState({ selectedEventCourse: eventdata });
    this.props.client
      .query({
        query: isMatchAlreadyExists,
        variables: {
          ObjectType: eventdata.ObjectType,
          ReferenceID: eventdata.ID,
        },
      })
      .then((res) => {
        if (res.data.isMatchAlreadyExists.IsExists == false) {
          this.getEventOrCourseByID(eventdata);
          this.setState({ matchExistError: "" });
        } else {
          this.setState({});
          this.setState({
            matchExistError: "Match already exists for your selection",
            startDate: new Date(),
            endDate: new Date(),
            startTime: "",
            endTime: "",
            streetName: "",
            city: "",
            latitude: "",
            longitude: "",
            address: "",
            state: "",
            matchTitle: "",
          });
        }
      });
  };

  removeImage = (name,base64) => {
    const { images, baseImageArray } = this.state
    const updatedImages = images.filter(img => img.url != name);
    while (baseImageArray.indexOf(base64) !== -1) {
      baseImageArray.splice(baseImageArray.indexOf(base64), 1);
    }
    this.setState({
      images : updatedImages,
      baseImageArray
    });
    
  }

  setProfilePic = (pic) => {
    this.setState({ profilePic: pic})
  }

  render() {
    const {
      eventsArray,
      selectedEventCourse,
      eventType,
      isPublic,
      isCourse,
      dec,
      publicEventsArray,
      publicCoursesArray,
      eventsFlag,
      publicDisable,
      startDate,
      endDate,
      startTime,
      endTime,
      city,
      latitude,
      longitude,
      state,
      matchTitle,
      description,
      profilePic,
      baseImageArray,
      whoAttendedList,
      address,
      button,
      video
    } = this.state;
    const format = "h:mm a";
    const label = profilePic
      ? "Image successfully added"
      : "Click or drop your image here";
    const enabled =
      eventType &&
      startDate &&
      endDate &&
      startTime &&
      endTime &&
      city &&
      state &&
      latitude &&
      longitude &&
      matchTitle &&
      description &&
      profilePic &&
      baseImageArray.length > 0 &&
      whoAttendedList.length > 0;
    const publicenabled =
      eventType &&
      startDate &&
      endDate &&
      startTime &&
      endTime &&
      city &&
      state &&
      latitude &&
      longitude &&
      matchTitle &&
      description &&
      profilePic &&
      button &&
      baseImageArray &&
      baseImageArray.length > 0;
    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-2 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-8 ">
            <div className="text-left">
              <a href="#" className="float-left mobile_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Match</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label >
                      Would you like to create Match for latest event/courses
                      attended?
                    </label>
                    <div>
                      <label
                        className={
                          dec == "Yes"
                            ? "selectedEventTypeLabel"
                            : "eventTypeLabel"
                        }
                      >
                        Yes
                        <input
                          type="radio"
                          name="typeOfEvent"
                          style={{ opacity: 0 }}
                          name="decision"
                          value={"Yes"}
                          onChange={this.handleDecision}
                        />
                      </label>
                      <label
                        className={
                          dec == "No"
                            ? "selectedEventTypeLabel"
                            : "eventTypeLabel"
                        }
                      >
                        No
                        <input
                          type="radio"
                          name="typeOfEvent"
                          style={{ opacity: 0 }}
                          name="decision"
                          value={"No"}
                          onChange={this.handleDecision}
                        />
                      </label>
                    </div>
                  </div>
                </div>
                {isPublic && (
                  <label>Please select Event/Course to create match</label>
                )}
                <div className="row">
                  {isPublic &&
                    publicEventsArray &&
                    publicEventsArray.map((type) => {
                      return (
                        <div
                        className="col-sm-3 mb-2"
                          onClick={() => this.selectEventOrCourse(type)}
                        >
                          <div className="post_single">
                            <img
                              src={type.ProfilePicture}
                              alt=""
                              style={{ borderRadius: 15, height: "100px" }}
                            />
                            <h6 style={{ textAlign: "center" }}>
                              <a>
                                {type.EventTitle.length > 15
                                  ? type.EventTitle.substring(0, 15) + "..."
                                  : type.EventTitle}
                              </a>
                              {selectedEventCourse &&
                              selectedEventCourse.ID == type.ID &&
                              selectedEventCourse.ObjectType ==
                                type.ObjectType ? (
                                <img
                                  src="../../assets/images/green-tick.png"
                                  style={{ width: "23px", float: "right" }}
                                />
                              ) : (
                                ""
                              )}{" "}
                            </h6>
                          </div>
                        </div>
                      );
                    })}
                </div>
                {isPublic && (
                  <div className="row">
                    {isPublic &&
                      publicCoursesArray &&
                      publicCoursesArray.map((type) => {
                        return (
                          <div
                          className="col-sm-3 mb-2"
                            onClick={() => this.selectEventOrCourse(type)}
                          >
                            <div className="post_single">
                              <img
                                src={type.ProfilePicture}
                                alt=""
                                style={{ borderRadius: 15, height: "100px" }}
                              />
                              <h6 style={{ textAlign: "center" }}>
                                <a>
                                  {type.Title && type.Title.length > 15
                                    ? type.Title.substring(0, 15) + "..."
                                    : type.Title}
                                </a>
                                {selectedEventCourse &&
                                selectedEventCourse.ID == type.ID &&
                                selectedEventCourse.ObjectType ==
                                  type.ObjectType ? (
                                  <img
                                    src="../../assets/images/green-tick.png"
                                    style={{ width: "23px", float: "right" }}
                                  />
                                ) : (
                                  ""
                                )}
                              </h6>
                            </div>
                          </div>
                        );
                      })}
                  </div>
                )}
                {isPublic && (
                  <label style={{ color: "red" }}>
                    {this.state.matchExistError}{" "}
                  </label>
                )}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="typeOfEvent">Type of Match</label>
                    <div>
                      {this.state.eventsArray &&
                        this.state.eventsArray.map((type) => {
                          return (
                            <input
                              type="button"
                              key={type.ID}
                              className={
                                eventType.Name == type.EventTitle
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }
                              onClick={(e) =>
                                this.handleEventsSelectNew(
                                  type.ID,
                                  type.EventTitle
                                )
                              }
                              value={type.EventTitle}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="matchStartDate">Match Start Date</label>
                    <div className="input-group date">
                      <DatePicker
                        id="eventStartDate"
                        dateFormat="yyyy-MM-dd"
                        className="form-control"
                        maxDate={startDate}
                        selected={startDate}
                        onChange={this.handleStartChange}
                        placeholderText="Match Start Date"
                        disabled={publicDisable}
                      />
                    </div>
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="matchEndDate">
                      Match End Date{" "}
                      <span className="input-group-addon">
                        <i className="glyphicon glyphicon-th"></i>
                      </span>
                    </label>
                    <DatePicker
                      dateFormat="yyyy-MM-dd"
                      maxDate={startDate}
                      className="form-control"
                      selected={endDate}
                      onChange={this.handleEndChange}
                      placeholderText="Match End Date"
                      disabled={publicDisable}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="matchStartTime">Match Start Time</label>
                    <TimePicker
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleStartTimeChange}
                      value={startTime}
                      use12Hours
                      inputReadOnly
                      disabled={publicDisable}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="matchEndTime">Match End Time</label>
                    <TimePicker
                      showSecond={false}
                      className="form-control"
                      onChange={this.handleEndTimeChange}
                      value={endTime}
                      format={format}
                      use12Hours
                      inputReadOnly
                      disabled={publicDisable}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <Component
                      callback={this.getLocationDetails}
                      isDisabled={publicDisable}
                      initial={address}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>City </label>
                    <input
                      type="text"
                      className="form-control"
                      name="city"
                      placeholder="City"
                      disabled
                      value={city || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label>State </label>
                    <input
                      type="text"
                      className="form-control"
                      name="state"
                      placeholder="State"
                      disabled
                      value={state || ""}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>Latitude </label>
                    <input
                      type="text"
                      className="form-control"
                      name="latitude"
                      placeholder="Latitude"
                      disabled
                      value={latitude || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label>Longitude </label>
                    <input
                      type="text"
                      className="form-control"
                      name="longitude"
                      placeholder="Longitude"
                      disabled
                      value={longitude || ""}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    {isPublic ? <label>Who Attended</label> : ""}
                    {isPublic ? (
                      <input
                        type="text"
                        className="form-control"
                        disabled
                        value={"All the members of selected event/course"}
                      />
                    ) : (
                      <SearchAutoComplete callback={this.getWhoAttendedList} />
                    )}
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    {!publicDisable && <Clone getProps={this._getProps} />}
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="matchTitle">Match Title</label>
                    <input
                      type="text"
                      className="form-control"
                      name="matchTitle"
                      placeholder="Match Title"
                      value={matchTitle}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      name="description"
                      rows="3"
                      value={description}
                      onChange={this.handleChanges}
                    >
                      Lorem ipsum dolor sit amet, posse timeam necessitatibus
                      per an.{" "}
                    </textarea>
                  </div>
                </div>
                <hr />
                <AddVideo getvideoUrls={this.getvideoUrls} />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="matchPicture">Match Picture</label>
                    <br />
                    <ImageUploads callback={this.getImagesArray} />
                    <br />
                  </div>
                </div>

                <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                  {
                    this.state.images && this.state.images.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                  </div>
                  {
                    this.state.images && this.state.images.length > 0 && !profilePic &&
                    <div className="col-sm-12 my-2 pt-2 text-center">
                      <i className="text-danger h6">Please select profile picture from above images</i>
                    </div>
                  }
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  {profilePic && <label htmlFor="profilePicture">Profile Picture</label> }
                    {/* <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    /> */}
                  </div>
                </div>
              
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        disabled={isPublic ? !publicenabled : !enabled}
                        onClick={this.createMatch}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-2 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { createMatchAction, createPublicMatchAction }),
  withApollo
)(CreateMatchPub);
