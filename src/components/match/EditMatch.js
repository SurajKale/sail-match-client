import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { updatePublicMatchAction } from "../../data/actions/MatchActions";
import { getMatchByUrl } from "../../data/query";
import { StyledDropZone } from "react-drop-zone";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class EditMatch extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
			MatchID: 0,
			MatchUrl: this.props.match.params.url,
			title: "",
			description: "",
			profilePic: ""
    };
	}
	
	getMatchData = () => {
    this.props.client
      .query({
        query: getMatchByUrl,
        variables: {
          MatchUrl: this.state.MatchUrl,
        },
        fetchPolicy: "network-only",
      })
      .then((res) => {
        var matchData = res.data.getMatchByUrl;
        this.setState({
					MatchID: matchData.ID,
					title: matchData.Title,
					description: matchData.Description,
					profilePic: matchData.ProfilePicture
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  componentDidMount() {
    window.scrollTo(0, 0);
    this.getMatchData();
  }
  
	goBack = () => {
			this.props.history.goBack();
	};

	setFile = (file) => {
		this.toBase64(file).then((result) => {
			this.setState({
					profilePic: result,
			});
		});
	};

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
  });

	handleChanges = async (e) => {
			await this.setState({
			[e.target.name]: e.target.value,
			});
	};

  createMatch = (e) => {
		const { title, description, profilePic, MatchID } = this.state;
		
		const matchObject = { title, description, profilePic, MatchID }
    this.props
			.updatePublicMatchAction(
				this.props.client,
				matchObject
			)
			.then((res) => {
				toast.success("Match updated successfully", {
					onClose: () => this.props.history.push("/"),
				});
			})
			.catch(function (error) {
				toast.error("Something went wrong", {});
		});
  }

  goBack = () => {
    this.props.history.goBack();
  };
  
  render() {
    const { title, description, profilePic } = this.state;
    const label = profilePic
      ? "Image successfully added"
      : "Click or drop your image here";
		const enabled = title && description && profilePic
		
    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-2 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-8 ">
            <div className="text-left">
              <a href="#" className="float-left mobile_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Edit Match</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="matchTitle">Match Title</label>
                    <input
                      type="text"
                      className="form-control"
                      name="title"
                      placeholder="Match Title"
                      value={title}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      name="description"
                      rows="3"
                      value={description}
                      onChange={this.handleChanges}
                    >
                      Lorem ipsum dolor sit amet, posse timeam necessitatibus
                      per an.{" "}
                    </textarea>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="profilePicture">Profile Picture</label>
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
              
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        disabled={!enabled}
                        onClick={this.createMatch}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-2 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { updatePublicMatchAction }),
  withApollo
)(EditMatch);
