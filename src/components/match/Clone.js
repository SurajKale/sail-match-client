import React, { Component } from "react";
import { validEmailRegex } from "../../services/CommonServices";

export default class ListOfresponsibility extends Component {
  state = {
    responsibility: [""],
    validMail: false,
    disableCross: [false],
  };
  errorEmail = "";
  enabled = false;
  handleText = (i) => (e) => {
    const { value } = e.target;
    const checkEmail = validEmailRegex.test(value);
    if (checkEmail === true) {
      this.errorEmail = "";
      this.setState({ validMail: false });
    } else {
      this.errorEmail = "Please enter valid email";
      this.setState({ validMail: true });
    }

    let responsibility = [...this.state.responsibility];
    this.disableDelete(responsibility);
    responsibility[i] = e.target.value;
    this.setState({
      responsibility: responsibility,
    });
    this.props.getProps(responsibility);
  };
  checkEmailValidation = (i) => (e) => {
    const { value } = e.target;
    const checkEmail = validEmailRegex.test(value);
    if (checkEmail === true) {
      this.errorEmail = "";
      this.setState({ validMail: false });
    } else {
      this.errorEmail = "Please enter valid email";
      this.setState({ validMail: true });
    }
  };
  disableDelete = (responsibility) => {
    if (responsibility.length <= 1) {
      this.setState({ disableCross: true });
    } else {
      this.setState({ disableCross: false });
    }
  };
  handleDelete = (i) => (e) => {
    e.preventDefault();
    let responsibility = [
      ...this.state.responsibility.slice(0, i),
      ...this.state.responsibility.slice(i + 1),
    ];
    this.disableDelete(responsibility);
    this.setState({
      responsibility: responsibility,
    });
    var validityChecker = responsibility.filter(function (item) {
      return validEmailRegex.test(item);
    });
    if (validityChecker.length >= 1) {
      this.errorEmail = "";
      this.setState({ validMail: false });
    }
    this.props.getProps(responsibility);
  };

  addQuestion = (e) => {
    e.preventDefault();
    let responsibility = this.state.responsibility.concat([""]);
    this.setState({
      responsibility: responsibility,
    });
    this.disableDelete(responsibility);
    this.props.getProps(responsibility);
  };

  render() {
    this.enabled = this.state.validMail;
    const { label } = this.props;
    return (
      <React.Fragment>
        <div className="input-group">
          {
            label ?
            <label>
              Add Emails{" "}
              <span title={label}>
                <i className="fa fa-info-circle" aria-hidden="true"></i>
              </span>
            </label>
            :
            <label>
              Others{" "}
              <span title="Add email of your friends who is not preset in above dropdown, we will send the mail invite to your friends">
                <i className="fa fa-info-circle" aria-hidden="true"></i>
              </span>
            </label>
          }
        </div>
        <div className="row">
          {this.state.responsibility.map((question, index) => (
            <React.Fragment key={index}>
              <div className="col-md-5">
                <input 
                  type="email"
                  className="form-control"
                  name="location_area"
                  style={{ margin: "3px" }}
                  value={question}
                  onBlur={this.checkEmailValidation(index)}
                  onChange={this.handleText(index)}
                />
              </div>
              <div className="col-md-1">
                <button
                  type="button"
                  disabled={this.state.disableCross}
                  className="btn btn-primary btn-sm mt-2"
                  onClick={this.handleDelete(index)}
                >
                  X
                </button>
              </div>
            </React.Fragment>
          ))}
        </div>
        <div>{this.errorEmail}</div>
        <button
          type="button"
          disabled={this.state.validMail}
          className="btn btn-primary btn-sm m-2"
          onClick={this.addQuestion}
        >
          Add
        </button>
      </React.Fragment>
    );
  }
}
