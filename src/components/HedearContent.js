import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import logo from "../assets/images/logo.png";
import post from "../assets/images/post-icon.svg";
import favourite from "../assets/images/Path_3.png";
import notification from "../assets/images/Path_4.png";
import favourite1 from "../assets/images/path_31.png";
import notification1 from "../assets/images/path_41.png";

import PostPopup from "./PostPopup";
import SearchInput from "./search/SearchInput";
import ChatNotification from './chat-app/ChatNotification';
import Notification from './common/Notification/notification';

import { viewChatNotification, isUserBlocked } from "../data/query";


const LocalStorageData = require("../services/LocalStorageData");
let FirstName = null;
let UserName = null;
let ID = null;
let ProfileImage = null;
let SchoolAdmin = null;
let tmp_img = "../assets/images/profile_pic.png";

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.FirstName) {
  FirstName = LocalStorageData.FirstName;
}
if (LocalStorageData.UserName) {
  UserName = LocalStorageData.UserName;
}
if (LocalStorageData.SchoolAdmin) {
  SchoolAdmin = LocalStorageData.SchoolAdmin;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
} else {
  ProfileImage = tmp_img;
}

class HeaderContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      toogleDropdown: false,
      doShowPostPopup: false,
      update_profile: ProfileImage,
      showMessageNotification: false,
      showNotification: false,
      notificationCount: 0,
      isBlock: false
    };
  }
  
  componentDidMount () {
    if(ID) {
      this.isUserBlocked()
      this.viewChatNotification()
      // setInterval(() => {this.viewChatNotification()}, 30000);
    }
  }

  viewChatNotification = () => {
    this.props.client
      .query({
          query: viewChatNotification,
          variables: {
            UserID: parseInt(ID),
          },
          fetchPolicy: "network-only"
      })
      .then(res => {
          this.setState({
              notificationCount: res.data.viewChatNotification.Count
          })
      });
  }

  isUserBlocked = () => {
    this.props.client
      .query({
          query: isUserBlocked,
          variables: {
            UserID: parseInt(ID),
          },
          fetchPolicy: "network-only"
      })
      .then(async res => {
          await this.setState({
            isBlock: res.data.isUserBlocked
          })
          if(this.state.isBlock) {
            this.logoutAndUpdatePage();
          }
      });
  }

  logoutAndUpdatePage = () => {
    localStorage.clear();
    window.location.reload();
  }

  toggleDown = () => {
    let toggle = this.state.toogleDropdown;
    this.setState({ toogleDropdown: !toggle });
  };

  goToLogin = (e) => {
    this.props.history.push("/login");
  };

  goToSignUp = (e) => {
    this.props.history.push("/signup");
  };
  gotoHome = (e) => {
    this.props.history.push("/");
  };
  logout = (e) => {
    this.setState({ user: {} });
    // dispatch(clearUserData());
    localStorage.clear();
    global.token = "";
    global.UserName = "";
    window.location.href = "/";
  };
  openPostPopup = () => {
    document.body.style.overflowY = "hidden";
    this.setState({ doShowPostPopup: true });
  };

  closePostPopup = () => {
    document.body.style.overflowY = "auto";

    this.setState({ doShowPostPopup: false });
  };

  showMessageNotifications = async () => {
    if(ID) {
      await this.setState({ showMessageNotification: !this.state.showMessageNotification })
      this.viewChatNotification();
      this.closeNotificationPopup();
    } else {
      toast.error("Please login first")
    }
  }

  showNNotifications = async () => {
    if(ID) {
      await this.setState({ showNotification: !this.state.showNotification })
      this.closeMessageNotificationPopup();
    } else {
      toast.error("Please login first")
    }
  }

  closeNotificationPopup = () => {
    this.setState({ showNotification: false })
  }

  closeMessageNotificationPopup = () => {
    this.setState({ showMessageNotification: false })
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({
        update_profile: nextProps.user.update_profile
          ? nextProps.user.update_profile
          : ProfileImage,
      });
    }
  }
  render() {
    const { doShowPostPopup, showMessageNotification, showNotification, notificationCount } = this.state;
    return (
      <div className="d-flex flex-column flex-md-row align-items-center p-3  mb-3 bg-white border-bottom shadow-sm">
        <div className="container">
          <div className="row">
            <div
              className="mr-md-auto sailMatch-logo"
              onClick={(e) => this.gotoHome()}
            >
              <a>
                <img src={logo} alt="SailMatch" title="Sail Match" />
              </a>
            </div>

            <SearchInput />
            <nav className="my-2 my-md-0 ">
              {" "}
              <a
                href="#"
                className="text-dark add_post nav-el cursor-pointer"
                onClick={this.openPostPopup}
              >
                <img src={post} alt="Post" /> <span>Post</span>
              </a>{" "}
              {
                !ID &&
                  <Fragment>
                  <a className="alert-notification nav-el" href="javascript:void(0);">
                  <img src={favourite1} alt="Favourite" onClick={this.showMessageNotifications} />
                </a>{" "}
                <a className="alert-notification_bell" href="javascript:void(0);">
                  <img src={notification1} alt="Notification" onClick={this.showNNotifications} />
                </a>{" "}
                </Fragment>
              }
              {
                ID &&
                <Fragment>
              
              <a className="alert-notification nav-el" href="javascript:void(0);">
                <img src={favourite} alt="Favourite" onClick={this.showMessageNotifications} />
                { ID ? notificationCount : null  }
              </a>{" "}
              <a className="alert-notification_bell" href="javascript:void(0);">
                <img src={notification} alt="Notification" onClick={this.showNNotifications} />
              </a>{" "}
              </Fragment>
              }
              {ID && (
                <React.Fragment>
                  <a
                    className="dropdown dropdown-toggle nav-el"
                    href=""
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      src={this.state.update_profile}
                      alt="Frank Hail"
                      className="header-user-icon"
                    />
                    <span className="profile_name">
                      {FirstName
                        ? FirstName
                        : UserName.length > 7
                        ? UserName.substring(0, 7)
                        : UserName}
                    </span>
                    <img src="../assets/images/dropdown.svg" alt="Dropdown" />
                  </a>

                  <div
                    className="dropdown-menu"
                    id="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <Link
                      className="dropdown-item"
                      to={"/edit-profile/" + UserName}
                    >
                      Edit profile
                    </Link>
                    {/* { SchoolAdmin === true && <Link className="dropdown-item" to={'/add-course'}>Course Management</Link>} */}
                    <a
                      className="dropdown-item cursor-pointer"
                      href="/change-password"
                    > Change Password</a>
                    <a
                      className="dropdown-item cursor-pointer"
                      // href="javascript:void(0);"
                      onClick={(e) => this.logout(e)}
                    >
                      Logout
                    </a>
                  </div>
                </React.Fragment>
              )}
              {!ID && (
                <span>
                  <button
                    type="button"
                    className="btn btn-sm btn-primary "
                    style={{
                      borderRadius: "20px",
                      marginRight: "10px",
                      marginLeft: "10px",
                    }}
                    onClick={(e) => this.goToLogin()}
                  >
                    {" "}
                    Sign In
                  </button>
                </span>
              )}
              <PostPopup
                doShow={doShowPostPopup}
                closePopup={this.closePostPopup}
              ></PostPopup>
            </nav>
          </div>
          { showMessageNotification && ID &&
            <ChatNotification 
              notificationCount={notificationCount}
              user={ID}
              closeMessageNotification={this.showMessageNotifications}
            />
          }
          { showNotification && ID &&
            <Notification/>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.userDetails,
  update_profile: state.update_profile,
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withApollo
)(HeaderContent);
