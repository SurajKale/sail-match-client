import React, { Component, Fragment } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { getGroupCreateAction, getGroupDetailsByID } from "../../data/actions";
import LocationComponent from '../location/LocationContent';
import SearchAutoComplete from "../match/SearchAutoComplete";
import Clone from "../match/Clone";
import ImageUploads from '../match/MutipleImageUploads';

const LocalStorageData = require('../../services/LocalStorageData');
let UID = null;

 if(LocalStorageData.ID){
  UID = LocalStorageData.ID;
}

class EditGroupContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: this.props.match.params.url,
      groupName: "",
      groupType: "",
      groupTopic: "",
      groupDescription: "",
      groupFacebookUrl: "",
      groupLinkdInUrl: "",
      coOrgnisers: [],
      groupOrganizer: [],
      otherEmailList: [],
      profilePic: "",
      locationObject: {},
      images: [],
      imagesRender: [],
      inviteUser: [],
      mailList: []
    }
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    await this.props.getGroupDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getGroupDetailsByID !== null ) {
        let groupData = res.data.getGroupByUrl;
        this.setState({
            groupID: groupData.ID,
            groupName: groupData.Title,
            groupTopic: groupData.Topic,
            groupDescription: groupData.Description,
            groupOrganizer: groupData.CoOrgnizers,
            imagesRender1: groupData.Pictures,
            groupFacebookUrl:groupData.FacebookUrl,
            groupLinkdInUrl:groupData.LinkedInUrl,
            groupAddress: groupData.Location.Address,
            locationObject: {
              Latitude: groupData.Location.Latitude,
              Longitude: groupData.Location.Longitude,
              Address: groupData.Location.Address,
              City: groupData.Location.City,
              State: groupData.Location.State,
              Country: groupData.Location.Country,
              ZipCode: groupData.Location.ZipCode,
              StreetName: groupData.Location.StreetName
            },
            userID:groupData.UserID,
            profilePic: groupData.ProfilePicture,
            groupType: groupData.GroupType
        })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
    // console.log(error);
    })
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  getImagesArray = (baseArray,imagesArray) =>{
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray })
  }

  getCoorgniser = (whoAttended) => {
    this.setState({ coOrgnisers : whoAttended });
  };

  getInviteUserList = (invite) => {
    this.setState({ inviteUser : invite });
  };

  getotherEmailList = (data) => {
    this.setState({ otherEmailList: data });
  }

  callback = (data) => {
    let address = data.mainPlace.concat(data.city, "," , data.state, ",", data.zipCode)
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
        City: data.city,
        State: data.state,
        Country: data.country,
        ZipCode: data.zipCode,
        StreetName: data.mainPlace
      }
    })
  }

  onDrop(picture) {
    this.setState({
        pictures: this.state.pictures.concat(picture),
    });
  }

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
    
  }

  removeCoorganiserImage = (name) => {
    const { groupOrganizer } = this.state
    while (groupOrganizer.indexOf(name) !== -1) {
      groupOrganizer.splice(groupOrganizer.indexOf(name), 1);
    }
    this.setState({
      groupOrganizer
    })
  }

  removeExistingImage = (name) => {
    const { imagesRender1, images } = this.state
    const updatedImages = imagesRender1.filter(img => img != name);
    this.setState({
      imagesRender1 : updatedImages
    }); 
  }

  setProfilePic = (pic) => {
    this.setState({ profilePic: pic})
  }

  createGroup = async (e) => {
    e.preventDefault();
    
    const { groupName, groupType, groupTopic, groupDescription, groupFacebookUrl, groupLinkdInUrl,
      coOrgnisers, otherEmailList, profilePic, locationObject, images, imagesRender1, url, groupID,
      inviteUser, groupOrganizer } =this.state

    let updatedImages = imagesRender1.concat(images);

    var organizerList =
      coOrgnisers.map(({ID, FirstName, LastName, Email, Phone}) => ({ID: ID, Name: FirstName + " " + LastName, Email: Email, Phone: Phone}));

    var organizerList1 =
      groupOrganizer.map(({ID, Name, Email, Phone}) => ({ID: ID, Name: Name, Email: Email, Phone: Phone}));

    let UpdatedCoList = organizerList.concat(organizerList1);

    var inviteUserList =
      inviteUser.map(({ID, FirstName, LastName}) => ({ Name: FirstName + " " + LastName, UserID: ID }));

    let groupObject = {
      groupName, groupType, groupTopic, groupDescription, groupFacebookUrl, groupLinkdInUrl,
      coOrgnisers, otherEmailList, profilePic, locationObject, images, updatedImages, url, 
      groupID, organizerList, inviteUserList, UpdatedCoList
    }
    
    await this.props.getGroupCreateAction(this.props.client, groupObject, UID).then(res => {
      toast.success("Group updated successfully", {
            onClose: () => this.props.history.push('/')
        });
      }).catch(function (error) {
          console.log(error);
      }
    )
  }

  goBack = () => {
    this.props.history.goBack();
  }

  render() {
    const { groupName, groupTopic, groupDescription, groupType, profilePic, images,
      groupFacebookUrl, groupLinkdInUrl, groupOrganizer, groupAddress } = this.state;

    const enabled = groupName && groupType && groupDescription && profilePic && images &&
    groupTopic

    return (
      <div className="container">
      <div className="row pt-4">
        <div className="col-md-3 mb-2"> <a className="desktop_back" href="#"><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
        <div className="col-md-6 mb-2"><div className="text-left">
          <a href="#" className="float-left mobile_back"><img src="../../assets/images/back_icon.png" alt="Back"/></a>
          <h1>Update Group</h1>
          <form className="mt-4">
    
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="courseName">Group Name</label>
                <input type="text" className="form-control" id="groupName" placeholder="Group Name" name="groupName" defaultValue={groupName} onChange={this.handleChange}/>
              </div>
            </div>
    
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="groupType">Group Type</label>
                <div className="">
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Local" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Local" />
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Global" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Global" />
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Sailing Club" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Sailing Club" />
                </div>
              </div>
            </div>
    
            { groupType === "Local" && <LocationComponent callback={this.callback} renderingValue={groupAddress} /> }
            { groupType === "Sailing Club" && <LocationComponent callback={this.callback} renderingValue={groupAddress} /> }
            <hr />

            <SearchAutoComplete label="Add Co-Organizers" callback={this.getCoorgniser} />
            <div className="row pt-3">
              {
                groupOrganizer && groupOrganizer.length > 0 && groupOrganizer.map((img) => {
                  return <div className="col-sm-4 my-2 multiple-image-preview" key={img.ID}>
                      <img src={ img.ProfilePicture.ProfileImage ? img.ProfilePicture.ProfileImage : "../../assets/images/4.png"} class="group-image-imp UserPhoto1" />
                      <div className="close-button" style={{ right: "10px", top: "0px" }} onClick={() => {this.removeCoorganiserImage(img)}}>X</div>
                  </div>
                })
              }
            </div>  
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="topic">Topic</label>
                <input type="text" className="form-control" id="topic" placeholder="Topic" name="groupTopic" defaultValue={groupTopic} onChange={this.handleChange} />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-6 my-2">
                <label htmlFor="facebook">Facebook</label>
                <input type="text" className="form-control" id="facebook" placeholder="Facebook" name="groupFacebookUrl" onChange={this.handleChange} defaultValue={groupFacebookUrl} />
              </div>
              <div className="col-sm-6 my-2">
                <label htmlFor="linkedin">LinkedIn</label>
                <input type="text" className="form-control" id="linkedin" placeholder="LinkedIn" name="groupLinkdInUrl" onChange={this.handleChange} defaultValue={groupLinkdInUrl} />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="description">Description/Purpose</label>
                <textarea className="form-control" id="description" rows="3" name="groupDescription" onChange={this.handleChange} defaultValue={groupDescription} />
              </div>
            </div>
            <hr />

            <SearchAutoComplete label="Invite Members" callback={this.getInviteUserList} />
            <br />
            <Clone getProps={this.getotherEmailList} />
            <div className="row">
                <div className="col-sm-12 my-2">
                  <label htmlFor="photo">Photo</label>
                  <ImageUploads callback={this.getImagesArray}/>
                </div>   
              </div>

              <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                {
                    this.state.imagesRender1 && this.state.imagesRender1.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeExistingImage(item)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                  {
                    this.state.imagesRender && this.state.imagesRender.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                </div>
              </div>
              <div className="row">
                <div className="col-sm-12 my-2">
                {profilePic && <label htmlFor="profilePicture">Group Profile Picture</label> }
                  {/* <StyledDropZone 
                    className="form-control"
                    onDrop={this.setFile}
                    label={label}
                  /> */}
                </div>
              </div>
              {profilePic &&
              <div className="row">
                <div className="col-sm-12 my-2 text-center">
                      <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile"  />
                </div>
              </div>}
              <div className="row">
                <div className="col-sm-12 my-2">
                    <p className="text-center"><button disabled={!enabled} type="button" className="btn btn-primary save_profile" onClick={this.createGroup}>Save</button></p>
                </div>
              </div>
            </form>
          </div>
          <div className="col-md-3 mb-2"></div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    </div>
    );
  }
}

export default compose(
  connect(null, { getGroupCreateAction, getGroupDetailsByID }
  ),
  withApollo
)(EditGroupContent);

