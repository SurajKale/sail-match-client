import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { getGroupCreateAction } from "../../data/actions";

import LocationComponent from '../location/LocationContent';
import SearchAutoComplete from "../match/SearchAutoComplete";
import Clone from "../match/Clone";
import ImageUploads from '../match/MutipleImageUploads';
import AddVideo from "../common/Video/AddVideo";

const LocalStorageData = require('../../services/LocalStorageData');
let UID = null;

if(LocalStorageData.ID){
  UID = LocalStorageData.ID;
}

class CreateGroupContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      groupName: "",
      groupType: "",
      groupTopic: "",
      groupDescription: "",
      groupFacebookUrl: "",
      groupLinkdInUrl: "",
      coOrgnisers: [],
      otherEmailList: [],
      profilePic: "",
      locationObject: {},
      images: [],
      imagesRender: [],
      inviteUser: [],
      videos: []
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name] : e.target.value })
  }

  getImagesArray = (baseArray,imagesArray) =>{
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray })
  }

  getCoorgniser = (whoAttended) => {
    this.setState({ coOrgnisers : whoAttended });
  };

  getInviteUserList = (invite) => {
    this.setState({ inviteUser : invite });
  };

  getotherEmailList = (data) => {
    this.setState({ otherEmailList: data });
  }

  callback = (data) => {
    let address = data.mainPlace.concat(data.city, "," , data.state, ",", data.zipCode)
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
        City: data.city,
        State: data.state,
        Country: data.country,
        ZipCode: data.zipCode,
        StreetName: data.mainPlace
      }
    })
  }

  onDrop(picture) {
    this.setState({
        pictures: this.state.pictures.concat(picture),
    });
  }

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
  }

  setProfilePic = (pic) => {
    this.setState({ profilePic: pic})
  }

  getvideoUrls = (data) => {
    this.setState({ videos: data })
  }

  createGroup = async (e) => {
    e.preventDefault();
    
    const { groupName, groupType, groupTopic, groupDescription, groupFacebookUrl, groupLinkdInUrl,
      coOrgnisers, otherEmailList, profilePic, locationObject, images, inviteUser, videos } =this.state

    var organizerList =
      coOrgnisers.map(({ID, FirstName, LastName, Email, Phone}) => ({ID: ID, Name: FirstName + " " + LastName, Email: Email, Phone: Phone}));

    var inviteUserList =
      inviteUser.map(({ID, FirstName, LastName}) => ({ Name: FirstName + " " + LastName, UserID: ID }));

    let groupObject = {
      groupName, groupType, groupTopic, groupDescription, groupFacebookUrl, groupLinkdInUrl,
      coOrgnisers, otherEmailList, profilePic, locationObject, images, inviteUserList,
      organizerList, videos
    }
    
    await this.props.getGroupCreateAction(this.props.client, groupObject, UID).then(res => {
      toast.success("Group added successfully", {
            onClose: () => this.props.history.push('/')
        });
      }).catch(function (error) {
          console.log(error);
      }
    )
  }

  goBack = ()=> {
    this.props.history.goBack();
  }

  render() {
    const { groupName, groupTopic, groupDescription,groupType, profilePic, images, imagesRender} = this.state;

    const enabled = groupName && groupType && groupDescription && profilePic && images &&
    groupTopic

    return (
      <div className="container">
      <div className="row pt-4">
        <div className="col-md-3 mb-2"> <a className="desktop_back" href="#" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
        <div className="col-md-6 mb-2"><div className="text-left">
          <a href="#" onClick={this.goBack} className="float-left mobile_back"><img src="../../assets/images/back_icon.png" alt="Back"/></a>
          <h1>Create Group</h1>
          <form className="mt-4">
    
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="courseName">Group Name</label>
                <input type="text" className="form-control" id="groupName" placeholder="Group Name" name="groupName" onChange={this.handleChange}/>
              </div>
            </div>
    
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="groupType">Group Type</label>
                <div className="">
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Local" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Local" />
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Global" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Global" />
                  <input type="button" name="groupType" onClick={this.handleChange} className={groupType==="Sailing Club" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Sailing Club" />
                </div>
              </div>
            </div>
            { groupType === "Local" && <LocationComponent callback={this.callback} /> }
            { groupType === "Sailing Club" && <LocationComponent callback={this.callback} /> }
            <hr />

            <SearchAutoComplete label="Add Co-Organizers" callback={this.getCoorgniser} />
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="topic">Topic</label>
                <input type="text" className="form-control" id="topic" placeholder="Topic" name="groupTopic" onChange={this.handleChange} />
              </div>
            </div>
    
            <div className="row">
              <div className="col-sm-6 my-2">
                <label htmlFor="facebook">Facebook</label>
                <input type="text" className="form-control" id="facebook" placeholder="Facebook" name="groupFacebookUrl" onChange={this.handleChange} />
              </div>
              <div className="col-sm-6 my-2">
                <label htmlFor="linkedin">LinkedIn</label>
                <input type="text" className="form-control" id="linkedin" placeholder="LinkedIn" name="groupLinkdInUrl" onChange={this.handleChange} />
              </div>
            </div>
            <div className="row">
              <div className="col-sm-12 my-2">
                <label htmlFor="description">Description/Purpose</label>
                <textarea className="form-control" id="description" rows="3" name="groupDescription" onChange={this.handleChange} />
              </div>
            </div>
            <hr />

            <SearchAutoComplete label="Invite Members" callback={this.getInviteUserList} />
            <br />
            <Clone getProps={this.getotherEmailList} />
            <hr/>

            <AddVideo getvideoUrls={this.getvideoUrls} />
            <div className="row">
                <div className="col-sm-12 my-2">
                  <label htmlFor="photo">Photo</label>
                  <ImageUploads callback={this.getImagesArray}/>
                </div>   
              </div>

              <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                  {
                    imagesRender && imagesRender.length > 0 && this.state.imagesRender.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div>
                      </div>
                    })
                  }
                </div>
                {
                  imagesRender && imagesRender.length > 0 && !profilePic &&
                  <div className="col-sm-12 my-2 pt-2 text-center">
                    <i className="text-danger h6">Please select profile picture from above images</i>
                  </div>
                }
              </div>
              <div className="row">
                <div className="col-sm-12 my-2">
                {profilePic && <label htmlFor="profilePicture">Group Profile Picture</label> }
                </div>
              </div>
              {profilePic &&
              <div className="row">
                <div className="col-sm-12 my-2 text-center">
                      <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile"  />
                </div>
              </div>}
              <hr />
              <div className="row">
                <div className="col-sm-12 my-2">
                    <p className="text-center"><button disabled={!enabled} type="button" className="btn btn-primary save_profile" onClick={this.createGroup}>Save</button></p>
                </div>
              </div>
            </form>
          </div>
          <div className="col-md-3 mb-2"></div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    </div>
    );
  }
}

export default compose(
  connect(null, { getGroupCreateAction }
  ),
  withApollo
)(CreateGroupContent);

