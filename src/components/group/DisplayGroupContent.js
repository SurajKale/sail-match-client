import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { getGroupDetailsByID } from "../../data/actions";
import { url } from "../../services/CommonServices";
import {deleteGroup} from '../../data/mutation'
import {joinGroup,exitGroup} from '../../data/query'

import MemberContent from '../common/MemberContent';
import PhotosContent from '../common/PhotosContent';
import VideoContent from '../common/VideosContent';
import PostListParent from '../common/PostListParent';
import ConfirmationPopup from '../common/ConfirmationPopup';
import ObjectLikeDislike from '../common/ObjectLikeDislike';
import {getViewAllPictures} from '../../data/actions/CommonActions';
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class DisplayGroupContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: this.props.match.params.url,
      grpData: [],
      groupID: null,
      groupName: "",
      groupTopic: "",
      groupDescription: "",
      groupOrganizer: [],
      groupPictures: [],
      groupFacebook: "",
      groupLinkdin:"",
      groupAddress: "",
      groupProfile: "",
      groupType: "",
      userID: null,
      loaded: false,
      totalCount:"",
      groupDetails:{},
      cCount: 0,
      metaData:[],
      showChatPopup: false,
      showShareModel: false,
      isJoinGroupMember: false,
      JoinGroupMember: []
    }
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    await this.props.getGroupDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getGroupDetailsByID !== null ) {
        let groupData = res.data.getGroupByUrl;

        var metaArr = [];
        metaArr.push({
          title : groupData.Title ?  groupData.Title : SITE_STATIC_NAME,
          keywords : groupData.ObjectType, SITE_STATIC_NAME,
          description : groupData.Description ? groupData.Description : '',
          og_url : window.location.href,
          og_title : groupData.Title ?  groupData.Title : SITE_STATIC_NAME,
          og_description : groupData.Description ? groupData.Description : '',
          canonical_url : window.location.href,
          og_image : groupData.ProfilePicture ? groupData.ProfilePicture : SITELOGO,
          amp_url : window.location.href,
        })

        this.setState({
            metaData:metaArr[0],
            groupDetails:groupData,
            isLike:groupData.isLike,
            renderLike:true,
            groupID:groupData.ID,
            groupName: groupData.Title,
            groupTopic: groupData.Topic,
            groupDescription: groupData.Description,
            groupOrganizer: groupData.CoOrgnizers,
            groupPictures: groupData.Pictures,
            groupFacebook: groupData.FacebookUrl,
            groupLinkdin: groupData.LinkedInUrl,
            groupAddress: groupData.Location.Address,
            userID:groupData.UserID,
            groupProfile: groupData.ProfilePicture,
            groupType: groupData.GroupType,
            totalCount:groupData.LikeCount,
            userDetailsPic:groupData.UserDetails,
            cCount: groupData.CommentCount,
            isJoinGroupMember: groupData.isJoinGroupMember,
            JoinGroupMember: groupData.JoinGroupMember
        })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
        console.log(error);
    })
  }

  goBack = ()=> {
      this.props.history.goBack();
  }

  openShareModal = () => {
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  handleImageLoaded = () => {
    this.setState({ loaded: true });
  }

  deleteObject=()=>{
    this.props.client
    .mutate({
      mutation: deleteGroup,
      variables: {
        GroupID: parseInt(this.state.groupID),
        UserID: ID
      },
    })
    .then(res => { 
      if(res.data.deleteGroup){
        toast.success("Group deleted successfully", {
          onClose: () => this.props.history.push("/"),
        });
      }
    });  
  }

  joinGroup=()=>{
    this.props.client
    .mutate({
      mutation: joinGroup,
      variables: {
        GroupID: parseInt(this.state.groupID),
        UserID: ID
      },
    })
    .then(res => { 
      if(res.data.joinGroup.ID){
        toast.success("Group Joined successfully");
        this.componentDidMount();
      }
    });  
  }

  exitGroup=()=>{
    this.props.client
    .mutate({
      mutation: exitGroup,
      variables: {
        GroupID: parseInt(this.state.groupID),
        UserID: ID
      },
    })
    .then(res => { 
      if(res.data.exitGroup.ID){
        toast.success("Group left successfully");
        this.componentDidMount();
      }
    });  
  }

  objectLike = (count) => {
    this.setState({ totalCount:count })
  }

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.groupDetails.ID,this.state.groupDetails.UserID,this.state.groupDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({
          groupPictures:picturesData.Pictures, 
          userDetailsPic:picturesData.UserDetails,
        })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    });
  }
    
  viewLessPictures = () =>{
    let queryResult = this.state.groupPictures
    let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
      filterdata.push(queryResult[i]);
      if (filterdata.length == 4) {
        break;
      }
    }
    this.setState({ groupPictures:filterdata })
  }

  openChatPopup = () => {
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  render() {
    const { groupName, groupTopic, groupDescription, groupOrganizer, groupPictures,
      groupFacebook, groupLinkdin, groupAddress, userID, groupProfile, groupType,
      totalCount, groupDetails,renderLike,isLike, cCount, isJoinGroupMember, groupID,
      JoinGroupMember } = this.state;          
            
    return (
      <div className="container">
        <HelmetData metaDetails={this.state.metaData} />
        <div className="row">
          <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2"> <a className="desktop_back cursor-pointer" href="#" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
          </div>
        </div>

        <div className="col-md-12 col-12">
          <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2"> 
              <img src={ groupProfile ? groupProfile : "../../assets/images/default.jpg"} className="profImg sideImg" alt="Back"/> 
              <div className="userSectionSocialSection">
                <div className="like">
                  {ID && renderLike && <ObjectLikeDislike
                  LikeCount={totalCount} 
                  ObjectType={groupDetails.ObjectType} 
                  ReferenceID={groupDetails.ID} 
                  status={groupDetails.isLike} 
                  ReferenceUserID={groupDetails.UserID} 
                  updateOnParent={this.objectLike} 
                  isDetail={true}/>}
                  { ID == null && <> 
                    <img
                      src="../../assets/images/heart-icon.png"
                      alt="heart-icon"
                      className="pr-2"
                    />
                    <span>{totalCount}</span> 
                    </>
                  }
                </div>
                <div class="bar">|</div>
                <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {cCount}</span></div>
                <div class="bar">|</div>
                <div className="forward" onClick={this.openShareModal}><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> {groupDetails.ShareCount || 0}</span></div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                    ObjectType="Groups"
                    ReferenceID={parseInt(groupDetails.ID)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={groupName}
                    quote={groupName} />
                }
              </div>    
            </div>
            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">    
              <h2>{groupName}</h2>
              <div className="date-time-img"><img src="../../assets/images/Crew.svg" alt="Back"/></div>
              <table className="addressTable">
                <tbody>
                  { groupType !== "Global" && 
                    <tr>
                        <td className="add-icon"><img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2"/></td>
                        <td className="add">{groupAddress}</td>
                    </tr>
                  }
                  { groupFacebook && <tr>
                    <td className="add">
                      <img 
                        src="../../assets/images/facebook-icon.png"
                        alt="position-icon"
                        className="pr-2"
                      />
                    </td>
                    <td className="add">{groupFacebook}</td>
                  </tr>}
                  { groupLinkdin && <tr>
                    <td className="add-icon">
                      <img 
                        src="../../assets/images/linkedin.png"
                        alt="position-icon"
                        className="pr-2"
                      />
                    </td>
                    <td className="add">{groupLinkdin}</td>
                  </tr>}
                </tbody>                 
              </table>    
              <br />
              <p className="date-time-text">  
                { this.state.userID !== ID && <button className="crewBtn" onClick={this.openChatPopup}>Contact Organizer</button>}
                { ID && groupID && !isJoinGroupMember && <button className="crewBtn" onClick={this.joinGroup}>Join Group</button>}
                { ID && groupID && isJoinGroupMember && <button className="crewBtn" onClick={this.exitGroup}>Leave Group</button>}
                {ID && this.state.userID === ID && <ConfirmationPopup callBack={this.deleteObject} object={"Group"}/>}
              </p> 
            </div>        
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">{groupTopic}</h5>
            <p className="about-event-info">{groupDescription}</p>    
          </div>
        </div>
        <div className="col-md-12"> 
          <div className="row">
              { groupOrganizer && groupOrganizer.length > 0 && <h5 className="about-event">Group Co-Organizers</h5> }
              { groupOrganizer && groupOrganizer.length > 0 && groupOrganizer.map((usr) => { 
                return <div className="col-12 col-sm-3 col-md-4 col-lg-3" key={usr.ID}>                
                  <img src={ usr.ProfilePicture.ProfileImage ? usr.ProfilePicture.ProfileImage : "../../assets/images/4.png"} className="img-fluid center-block mx-auto d-flex justify-content-center flex-wrap" alt={usr.Name}/>  
                  <p className="groupUserName">{usr.Name}</p>                                                  
                </div> 
              })}      
          </div>         
        </div>
        { JoinGroupMember && <MemberContent MemberList={JoinGroupMember} forGroup />}
        <PhotosContent Pictures={groupPictures} postPictures={groupDetails.PostPictures} Details={groupDetails} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} />
        <VideoContent videos={groupDetails.Videos} />
        {groupDetails.ObjectType &&
        <PostListParent ReferenceID={parseInt(this.state.groupID)}  ObjectType={groupDetails.ObjectType}   />}
        <ToastContainer autoClose={1000} />
        { ID && this.state.showChatPopup && 
          <ChatApp
          sender={ID}
          receiver={this.state.userID}
          profilePic={groupProfile}
          title={groupName}
          objectType="Groups"
          referenceID={parseInt(this.state.groupID)}
          popupHandler={this.openChatPopup}
          myID={ID}
          />
        }
      </div>
    )}
  }

export default compose(
  connect(null, { getGroupDetailsByID, getViewAllPictures }
  ),
  withApollo
)(DisplayGroupContent);
