import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import Autosuggest from "react-autosuggest";
import "react-toastify/dist/ReactToastify.css";
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
import ReactTooltip from "react-tooltip";

import { getProfileUpdateAction, getPrivateProfileAction } from "../data/actions";
import { getSignInAction } from "../data/actions/UserActions";
import { isPhoneExists, getAllStatesCities, getStateByID, getAllMasterClubs } from "../data/query";

import UploadProfile from './profile/UploadProfile';
import LocationComponent from './location/LocationContent';
import {_decryptPwd} from '../services/PwdConversion';

class EditProfileContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.match.params.username,
      id: "",
      boatOwner: false,
      certifications: [
        { id: 1, Type: "Basic Keelboat" },
        { id: 2, Type: "Basic Coastal Cruising" },
        { id: 3, Type: "Bareboat Cruising" },
        { id: 4, Type: "Other" }
      ],
      city: "",
      club: false,
      drinker: "",
      experience: "",
      gender: "",
      interests: [
        { id: 1, Type: "Recreational" },
        { id: 2, Type: "Racing" },
        { id: 3, Type: "Crew" },
        { id: 4, Type: "Seeking Crew" },
        { id: 5, Type: "Volunteering" },
      ],
      marijuana: "",
      firstName: "",
      lastName: "",
      bio: "",
      smoker: "",
      state: "",
      mobileNo: "",
      stateArray: [],
      cityArray: [],
      value: "",
      suggestions: [],
      clubName: "",
      clubArray: [],
      locationObject: {},
      email: '',
      password: '',
      boatList: [],
      openPopup:false,
      profilePic: ""
    };
  }

  async componentDidMount() {
    await this.props
      .getPrivateProfileAction(this.props.client)
      .then((res) => {
        let userData = res.data.getUserProfile;
        let userLocation = userData.Location;
        delete userLocation.__typename;
          this.setState({
          id: userData.ID,
          firstName: userData.FirstName,
          lastName: userData.LastName,
          city: userData.City,
          state: userData.State,
          gender: userData.Gender,
          bio: userData.Bio,
          experience: userData.Experience,
          certifications: [
            { id: 1, Type: "Basic Keelboat", isSelected: userData.Certifications.includes("Basic Keelboat") ? true: false },
            { id: 2, Type: "Basic Coastal Cruising", isSelected: userData.Certifications.includes("Basic Coastal Cruising") ? true: false  },
            { id: 3, Type: "Bareboat Cruising", isSelected: userData.Certifications.includes("Bareboat Cruising") ? true: false  },
            { id: 4, Type: "Other", isSelected: userData.Certifications.includes("Other") ? true: false  }
          ],
          interests: [
            { id: 1, Type: "Recreational", isSelected: userData.Desire.includes("Recreational") ? true: false },
            { id: 2, Type: "Racing", isSelected: userData.Desire.includes("Racing") ? true: false },
            { id: 3, Type: "Crew", isSelected: userData.Desire.includes("Crew") ? true: false  },
            { id: 4, Type: "Seeking Crew", isSelected: userData.Desire.includes("Seeking Crew") ? true: false },
            { id: 5, Type: "Volunteering", isSelected: userData.Desire.includes("Volunteering") ? true: false  }
          ],
          drinker: userData.Drink,
          smoker: userData.Smoke,
          marijuana: userData.Pot,
          mobileNo: userData.Phone,
          boatOwner: userData.isBoatOwner,
          club: userData.isClubMember,
          clubName: userData.ClubName,
          userAddress: userData.Location.Address,
          locationObject: userLocation,
          email: userData.Email,
          password: userData.Password,
          boatList: userData.BoatProfileList,
          profilePic: userData.ProfileImage
        });
      })
      .catch(function (error) {
        throw error;
      });
  }

  getAllMasterClubs = () => {
    this.props.client
      .query({
        query: getAllMasterClubs,
        variables: {
          Name: this.state.value,
        },
        fetchPolicy: "network-only",
      })
      .then((res) => {
        this.setState({
          clubArray: res.data.getAllMasterClubs.map(
            ({ ID, Name, City, State }) => ({
              id: ID,
              name: Name,
              city: City,
              state: State,
            })
          ),
        });
      });
  };

  getSuggestionValue = (suggestion) => {
    this.setState({
      selectedClubID: suggestion.id,
    });
    return suggestion.name;
  };

  renderSuggestion = (suggestion) => {
    return (
      <span>
        {suggestion.name}
        <i style={{ color: "grey", fontSize: "13px" }}>
          &nbsp;{suggestion.city},{suggestion.state}
        </i>
      </span>
    );
  };

  onChange = async (event, { newValue, method }) => {
    await this.setState({ value: newValue });
    if (this.state.value.length > 2) {
      await this.getAllMasterClubs();
      if (this.state.clubArray) {
        await this.onSuggestionsFetchRequested();
      }
    }
  };

  onSuggestionsFetchRequested = () => {
    if (this.state.value.length > 2) {
      this.setState({
        suggestions: this.state.clubArray,
      });
    }
  };

  onSuggestionsClearRequested = () => {
    this.setState({
      suggestions: [],
    });
  };

  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleInterestChange = async (category) => {
    let oldEvents = this.state.interests.slice();
    oldEvents.forEach((item) => {
      if (item.id === category.id) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      interests: oldEvents
    });
  };

  handleCertificationChange = async (category) => {
    let oldEvents = this.state.certifications.slice();
    oldEvents.forEach((item) => {
      if (item.id === category.id) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      certifications: oldEvents
    });
  };

  handleChangeOwner = (e, flag, value) => {
    if (flag === "owner") { 
      this.setState({ boatOwner: value }); 
    } else if (flag === "club") {
      if(value === true) {
        this.setState({ club: value });
      } else {
        this.setState({ club: value, selectedClubID: null, value: "" });
      }
    }
  };

  handlePhoneChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  goToBoatProfilePage = () => {
    this.props.history.push("/create-boat");
  };

  goToBoatProfileDetailsPage = (url) => {
    this.props.history.push('/boat-details/'+url)
  }

  callback = (data) => {     
    let address = data.mainPlace.concat(data.city, "," , data.state, ",", data.zipCode)
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
        City: data.city,
        State: data.state,
        Country: data.country,
        ZipCode: data.zipCode,
        StreetName: data.mainPlace
      }
    })
  }

  handleLocationChnage = (e) => {
    this.setState({
      locationObject: { ...this.state.locationObject, ZipCode : e.target.value } 
    })
  }

  openProfileModal = () =>{
    this.setState({openPopup:true});
  }

  popupToggle = () => {
    this.state.openPopup ? this.setState({openPopup: false}) : this.setState({openPopup: true}); 
  };

  getImage = async (image) =>{
    await this.setState({ profilePic: image })
    await this.setState({ openPopup: false })
  }

  updateProfile = async (e) => {
    e.preventDefault();
    const { boatOwner, certifications, city, club, drinker, experience, gender, interests,
      marijuana, firstName, lastName, bio, smoker, state, id, mobileNo, selectedClubID,
      locationObject, profilePic } = this.state;

    var options = interests.filter(function (item) { return item.isSelected });
    var coptions = certifications.filter(function (item) { return item.isSelected });

    let profileObject = { id, boatOwner, coptions, city, club, drinker, experience,
      gender, options, marijuana, firstName, lastName, bio, smoker, state, mobileNo,
      selectedClubID, locationObject, profilePic };

    this.props.client
      .query({
        query: isPhoneExists,
        variables: {
          Phone: mobileNo,
          UserID: parseInt(id),
        },
        fetchPolicy: "network-only",
      })
      .then(async (res) => {
        if (res.data.isEmailPhoneExist.Message === "true") {
          toast.error("Phone number already exsists");
        } else {
          this.props
          .getProfileUpdateAction(this.props.client, profileObject)
          .then(async (res) => {
              const userData = res.data.editProfile;
              if (userData.ID) {
                this.loginForm()
              }
          })
          .catch(function (error) {
            throw error;
          });
        }
      });
  };

  loginForm = (e) => {
    const { email, password } = this.state;
     this.props.getSignInAction(this.props.client, 
            email, 
            password,
            ).then(response => {  
                if (response.data.signin.ID) {                
                    localStorage.setItem("sessionUser", JSON.stringify(response.data.signin));
                    toast.success("Profile updated successfully", {
                      onClose: () => window.location.href = '/',
                    });
                } else {
                  toast.error("Something wents wrong");
            }
        }).catch(function (error) {
            toast.error(error.message);
		});
    };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {
    const { boatOwner, certifications, city, club, drinker, experience, gender,
      interests, marijuana, firstName, lastName, bio, smoker, state, mobileNo,
      cityArray, stateArray, clubName, locationCity, locationState, locationObject,
      userAddress, boatList, profilePic, value, suggestions, openPopup } = this.state;

    const inputProps = {
      placeholder: clubName ? clubName : "Enter your club Name",
      value,
      onChange: this.onChange,
    };

    const enabled = certifications && drinker && experience && gender && interests &&
      marijuana && firstName && lastName && bio && smoker && locationObject.Address &&
      mobileNo && profilePic;

    const buttonCheck = boatOwner ? true : false;

    let stateOption = stateArray.map((state) => (
      <option key={state.StateID} value={state.StateID}>
        {state.StateName}
      </option>
    ));

    let cityOption = cityArray.map((city, index) => (
      <option key={index} value={city.Name}>
        {city.Name}
      </option>
    ));

    return (
      <Fragment>
        <div className="container">
          <div className="row pt-4">
            <div className="col-md-3 mb-2">
              <a
                className="desktop_back"
                href="javaascript:void(0);"
                onClick={this.goBack}
              >
                <img src="../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
            <div className="col-md-6 mb-2">
              <div>
                <a href="/" className="float-left mobile_back">
                  <img src="../assets/images/back_icon.png" alt="Back" />
                </a>
                <h1>Edit Profile</h1>
              </div>
              <br />
              <form>
                <div className="row justify-content-center">
                  <div className="col-md-3 col-12 mb-2 mt-2 profile-photo-section">      
                    <img src={profilePic ? profilePic : '../../assets/images/no_image.jpg'} alt="Back" onClick={this.openProfileModal} data-tip="Update Profile" place="bottom" type="info"/> 
                    <ReactTooltip />
                    {openPopup &&
                    <UploadProfile popupToggle={this.popupToggle} openPopup={this.state.openPopup} callback={this.getImage} />}                         
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="firstname">First Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="fullname"
                      name="firstName"
                      defaultValue={firstName}
                      onChange={this.handleChange}
                      placeholder="First Name"
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="lastname">Last Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="lastname"
                      name="lastName"
                      defaultValue={lastName}
                      onChange={this.handleChange}
                      placeholder="Last name"
                    />
                  </div>

                  {/* <div className="col-sm-6 my-2">
                    <label htmlFor="state">State</label>
                    <select
                      className="form-control"
                      id="state"
                      name="state"
                      value={state}
                      onChange={this.handleChange}
                    >
                      {stateOption}
                    </select>
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="city">City</label>
                    <select
                      className="form-control"
                      id="city"
                      name="city"
                      value={city}
                      onChange={this.handleChange}
                    >
                      {cityOption}
                    </select>
                  </div> */}
                </div>

                <LocationComponent callback={this.callback} renderingValue={userAddress} />
                <div class="row">
                  <div class="col-sm-6 my-2">
                    <label for="city">City</label>
                    <input readOnly type="text" class="form-control" id="city" placeholder="City" name="city" value={locationObject.City} />
                  </div>       
                  <div class="col-sm-6 my-2">
                    <label for="state">State</label>
                    <input readOnly type="text" class="form-control" id="state" placeholder="State" name="state" value={locationObject.State} />
                  </div>       
                </div>
                <div class="row">
                  <div class="col-sm-6 my-2">
                    <label for="zipCode">Country</label>
                    <input readOnly type="text" class="form-control" id="zipCode" placeholder="Country" name="zipCode" value={locationObject.Country}  />
                  </div>       
            
                  <div class="col-sm-6 my-2">
                    <label for="gpsCoordinate">Zip Code</label>
                    <input readOnly type="text" class="form-control" id="gpsCoordinate" placeholder="Zipcode" name="gpsCoordinate" defaultValue={locationObject.ZipCode} onChange={this.handleLocationChnage} />
                  </div>       
                </div>

                <div className="row">
                <div className="col-sm-6 my-2">
                    <label htmlFor="lastname">Mobile Number</label>
                    <PhoneInput
                      country={'us'}
                      value={this.state.mobileNo}
                      onChange={mobileNo => this.setState({ mobileNo })}
                      onlyCountries={['us']}
                      disableCountryCode
                      placeholder="Phone"
                    />
                    {/* <input
                      type="tel"
                      className="form-control"
                      id="mobileno"
                      name="mobileNo"
                      maxLength={10}
                      defaultValue={mobileNo}
                      onChange={this.handlePhoneChange}
                      placeholder="Mobile No"
                    /> */}
                </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="gender">Gender</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        className={
                          gender === "Male"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="gender"
                        value="Male"
                        onClick={this.handleChange}
                      />
                      <input
                        type="button"
                        className={
                          gender === "Female"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="gender"
                        value="Female"
                        onClick={this.handleChange}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="bio">Bio</label>
                    <textarea
                      className="form-control"
                      id="bio"
                      rows="3"
                      name="bio"
                      defaultValue={bio}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="experience">Experience</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="experience"
                        onClick={this.handleChange}
                        className={
                          experience === "Want to learn"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Want to learn"
                      />
                      <input
                        type="button"
                        name="experience"
                        onClick={this.handleChange}
                        className={
                          experience === "Beginner"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Beginner"
                      />
                      <input
                        type="button"
                        name="experience"
                        onClick={this.handleChange}
                        className={
                          experience === "Intermediate"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Intermediate"
                      />
                      <input
                        type="button"
                        name="experience"
                        onClick={this.handleChange}
                        className={
                          experience === "Competent"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Competent"
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="experience">Interests</label>
                    <div className="" role="group" aria-label="Basic example">
                      {interests &&
                        interests.map((sail) => {
                          return (
                            <input
                              type="button"
                              key={sail.id}
                              className={`${
                                sail.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleInterestChange(sail)}
                              value={sail.Type}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="experience">Certifications</label>
                    <div className="" role="group" aria-label="Basic example">
                      {certifications &&
                        certifications.map((sail) => {
                          return (
                            <input
                              type="button"
                              key={sail.id}
                              className={`${
                                sail.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleCertificationChange(sail)}
                              value={sail.Type}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="customSwitch1">Are you a boat owner?</label>
                    <div className="row">
                    <div className="col-6" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="boatOwner"
                        onClick={(e) => this.handleChangeOwner(e, "owner", false)}
                        className={
                          boatOwner === false
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="No"
                      />
                      <input
                        type="button"
                        name="boatOwner"
                        onClick={(e) => this.handleChangeOwner(e, "owner", true)}
                        className={
                          boatOwner === true
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Yes"
                      />
                    </div>
                    <div className="col-6 text-right" role="group" aria-label="Basic example">
                      <button
                        disabled={!buttonCheck}
                        type="button"
                        onClick={this.goToBoatProfilePage}
                        className="btn btn-outline-secondary gender"
                      >
                        Create/Edit Boat
                      </button>
                    </div>
                    </div>
                    {/* <div className="custom-control custom-switch">
                      <input
                        type="checkbox"
                        name="boatOwner"
                        checked={boatOwner ? true : false}
                        className="custom-control-input"
                        id="customSwitch1"
                        onChange={(e) => this.handleChangeOwner(e, "owner")}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customSwitch1"
                      ></label>
                    </div> */}
                  </div>
                </div>
                {
                  boatList && boatList.length > 0 && boatList.map( boat => ( 
                  <div className="row">
                  <div className="col-sm-12 my-2 boatListEditProfilePage">
                    <div className="">
                      <span>
                        <img className="header-user-icon" src={boat.ProfilePicture} />
                      </span>
                      <span>
                        &nbsp;&nbsp;{boat.Title}
                      </span>
                    </div>
                    <div>
                      <button
                        type="button"
                        onClick={() => this.goToBoatProfileDetailsPage(boat.Url)}
                        className="btn btn-outline-secondary gender"
                      >
                        View Boat Profile
                      </button>
                    </div>
                  </div>
                </div>))
                }
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="customSwitch2">
                      Are you affiliated with any club?
                    </label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="club"
                        onClick={(e) => this.handleChangeOwner(e, "club", false)}
                        className={
                          club === false
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="No"
                      />
                      <input
                        type="button"
                        name="club"
                        onClick={(e) => this.handleChangeOwner(e, "club", true)}
                        className={
                          club === true
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Yes"
                      />
                    </div>
                    {/* <div className="custom-control custom-switch">
                      <input
                        type="checkbox"
                        name="club"
                        checked={club ? true : false}
                        className="custom-control-input"
                        id="customSwitc2"
                        onChange={(e) => this.handleChangeOwner(e, "club")}
                      />
                      <label
                        className="custom-control-label"
                        htmlFor="customSwitc2"
                      ></label>
                    </div> */}
                  </div>
                  {
                    club &&
                    <div className="col-sm-12 my-2">
                      <label htmlFor="clubname">Enter your club name</label>
                      <Autosuggest
                        suggestions={suggestions}
                        onSuggestionsFetchRequested={
                          this.onSuggestionsFetchRequested
                        }
                        onSuggestionsClearRequested={
                          this.onSuggestionsClearRequested
                        }
                        getSuggestionValue={this.getSuggestionValue}
                        renderSuggestion={this.renderSuggestion}
                        inputProps={inputProps}
                      />
                    </div>
                  }
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-1">
                    <label htmlFor="permissions">Permissions</label>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-1">
                    <label htmlFor="experience">Smoker?</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="smoker"
                        onClick={this.handleChange}
                        className={
                          smoker === "No"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="No"
                      />
                      <input
                        type="button"
                        name="smoker"
                        onClick={this.handleChange}
                        className={
                          smoker === "Yes"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Yes"
                      />
                      {/* <input
                        type="button"
                        name="smoker"
                        onClick={this.handleChange}
                        className={
                          smoker === "Moderate"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Moderate"
                      /> */}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-1">
                    <label htmlFor="experience">Drinker?</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="drinker"
                        onClick={this.handleChange}
                        className={
                          drinker === "No"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="No"
                      />
                      <input
                        type="button"
                        name="drinker"
                        onClick={this.handleChange}
                        className={
                          drinker === "Yes"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Yes"
                      />
                      {/* <input
                        type="button"
                        name="drinker"
                        onClick={this.handleChange}
                        className={
                          drinker === "Moderate"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Moderate"
                      /> */}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-1">
                    <label htmlFor="experience">Marijuana?</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        name="marijuana"
                        onClick={this.handleChange}
                        className={
                          marijuana === "No"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="No"
                      />
                      <input
                        type="button"
                        name="marijuana"
                        onClick={this.handleChange}
                        className={
                          marijuana === "Yes"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Yes"
                      />
                      {/* <input
                        type="button"
                        name="marijuana"
                        onClick={this.handleChange}
                        className={
                          marijuana === "Moderate"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        value="Moderate"
                      /> */}
                    </div>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <input
                        type="button"
                        disabled={!enabled}
                        onClick={this.updateProfile}
                        className="btn btn-primary save_profile"
                        value="Save Profile"
                      />
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  myProfile: state.profiles,
});

export default compose(
  connect(mapStateToProps, { getPrivateProfileAction, getProfileUpdateAction, getSignInAction }),
  withApollo
)(EditProfileContent);
