import React, { Component, Fragment } from "react";
import ReactPlayer from 'react-player';
import VideoPopup from './Video/VideoModal';

class VideosContent extends Component {
  constructor(props) {
  super(props);
  this.state = {
      doShow: false,
      videoURLs: [],
      playVideo: ""
    }
  }

  static getDerivedStateFromProps(props, state) {
    if (props !== state) {
      return {
        videoURLs: props.videos,
      }     
    }
    return null;
  }

  closePostPopup = () => {
    document.body.style.overflowY = "auto";
    this.setState({ doShow: false })
  }

  openPopup = (url) => {
    document.body.style.overflowY = "hidden";
    this.setState({ doShow: true, playVideo: url })
  }

  render() {
    const { doShow, videoURLs, playVideo } = this.state;
    return (
      <Fragment>
        { videoURLs && videoURLs.length > 0 &&
        <Fragment>
          <div className="col-md-12">
            <div className="row">
              <h5 className="about-event">Videos 
                <span>({videoURLs && videoURLs.length || 0})</span>
                { videoURLs && videoURLs.length > 4 && <a href="#!" className="view-all">View All</a>}
              </h5>
            </div>
          </div>
          <div className="col-md-12"> 
            <div className="row"> {
              videoURLs && videoURLs.length > 0 ?
              videoURLs.map((vid, index) => (
                  <div className="col-md-3 col-12 mb-4" key={index}  onClick={() => {this.openPopup(vid)}}  >  
                  <div className="videos">
                      {/* <img src="../../assets/images/white-right-arrow.png" className="whiteRightImg" alt="Back"/>               */}
                      {/* <img src="../../assets/images/video-1.png" className="img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/>  */}
                      <div className='player-wrapper'>
                          <ReactPlayer
                          url={vid}
                          className='videoReact'
                          style={{ borderRadius: '20px'}}
                          width='253px'
                          height='248px'
                          />
                      </div>
                      <a href="#!">{vid}</a>                                                   
                  </div>    
              </div>
              ))
              : null
            }                
            </div>         
          </div>
          <VideoPopup doShow={doShow} closePopup={this.closePostPopup} videoLink={playVideo} />
        </Fragment>
        }
      </Fragment>
      );
    }
  }

export default VideosContent;