import React from 'react';
import Helmet from 'react-helmet';

const CONFIG = require('../config');
var SITEURL = CONFIG.SITEURL;
var SITELOGO = CONFIG.SITELOGO;
var FB_APP_ID = CONFIG.FB_APP_ID;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

class HelmetData extends React.Component{
    constructor(props) {
		super(props);

		//  this.title = document.createElement('meta');
		//  this.keywords = document.createElement('meta');
		//  this.canonical_url = document.createElement('meta');
		//  this.description = document.createElement('meta');
		//  this.og_description = document.createElement('meta');
		//  this.og_image = document.createElement('meta');
		//  this.image = document.createElement('meta');
		//  this.og_url = document.createElement('meta');
		//  this.og_site_name = document.createElement('meta');
		//  this.og_type = document.createElement('meta');
		//  this.og_title = document.createElement('meta');


		this.state = {
			
		}
	}


	componentWillReceiveProps(nextProps){
		var metaData =  nextProps.metaDetails;

		document.querySelector('meta[name="title"]').setAttribute("content", SITE_STATIC_NAME +' | '+metaData.title);
		document.querySelector('meta[name="description"]').setAttribute("content", metaData.description);
		document.querySelector('meta[name="keywords"]').setAttribute("content", metaData.keywords);
		document.querySelector('meta[property="og:description"]').setAttribute("content", metaData.description);
		document.querySelector('meta[property="og:image"]').setAttribute("content", metaData.og_image);
		document.querySelector('meta[property="og:title"]').setAttribute("content", SITE_STATIC_NAME +' | '+metaData.og_title);
		document.querySelector('meta[property="og:url"]').setAttribute("content", metaData.og_url);
		// document.querySelector("link[rel='canonical']").setAttribute(metaData.canonical_url);
		}

	render() {

		const metaDetails = this.props.metaDetails;
	return(
		       <Helmet>

		        <script type="application/ld+json">
		        	{`
					    { 
						"@context" : "http://schema.org", 
						"@type" : "Organization", 
						"name" : "SAILMATCH™",
						"url" : "http://ec2-54-236-24-40.compute-1.amazonaws.com/",
						"logo": "http://socialenginespecialist.com/PuneDC/sailmatch/images/logo.png",
							}
					  `}
				</script> 

	              <title>{SITE_STATIC_NAME +' | '+metaDetails.title}</title>

	              <meta name="title" content={SITE_STATIC_NAME +' | '+metaDetails.title} />

	             {metaDetails.keywords &&
	              <meta name="keywords" content={metaDetails.keywords} />}
	              	             	  
	              <meta name="description" content={metaDetails.description} />

				  <link rel="canonical" href={metaDetails.canonical_url}/>

	          	  <meta property="og:site_name" content="SAILMATCH™"/>

				{metaDetails.og_type &&  
				 <meta property="og:type" content={metaDetails.og_type} />}
				 
				 <meta property="og:title" content={SITE_STATIC_NAME +' | '+metaDetails.title} />

				 <meta property="og:description" content={metaDetails.description} />

				 <link rel="icon" type="image/png" href={metaDetails.og_image} sizes="16x16" />


				  {metaDetails.og_image && 
				  	<meta property="og:image" content={metaDetails.og_image} />}

				  {metaDetails.og_image && 
				    <meta itemProp="image" content={metaDetails.og_image} />}

	            <meta property="og:url" content={metaDetails.og_url} />}

	            <meta property="fb:app_id" content={FB_APP_ID} />

    	    </Helmet>	 
		)
	}
}
export default HelmetData;
