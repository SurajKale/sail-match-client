import React, { Component, Fragment } from "react";
import { _formatDate, _formatTime } from "../../services/CommonServices";

class MatchesCommon extends Component {
    constructor(props) {
    super(props);
    this.state = {
        eventMatch:this.props.dataList,
        startDate: this.props.startDate,
        // startTime: this.props.startTime
      }
      console.log(this.props,"props");
    }

render(){
    const {startDate,startTime} = this.state
    return(
        <div className="row">
        <div className="col-md-12">
          <h5 className="about-event">
            Match for event{" "}
            <span>
              {/* {this.state.eventMatch && this.state.eventMatch.length} */}
            </span>
          </h5>
        </div>
        {this.state.eventMatch && (
          <div className="col-sm-3 mb-5">
            <div className="post_single">
              <img
                src={this.state.eventMatch.ProfilePicture}
                alt=""
                style={{ borderRadius: 15 }}
              />
              <h5>
                <a href="#!">
                  {this.state.eventMatch.Title &&
                  this.state.eventMatch.Title.length > 15
                    ? this.state.eventMatch.Title.substring(0, 15) + "..."
                    : this.state.eventMatch.Title}
                </a>
              </h5>
              <p>
                {_formatDate(startDate)}, 
                {/* {_formatTime(startTime)} */}
              </p>
              {/* <div className="author"><span className="author_name"><img src="../../assets/images/4.png" alt=""/> Frank Hail</span><span className="category"><a href="#"><img src="../../assets/images/Crew.svg" alt="home"/></a></span></div> */}
              <div className="divider">
                <hr />
              </div>
              <div className="post_footer">
                <span className="col text-left">
                  <i className="far fa-heart"></i> 0
                </span>
                <span className="col text-center">
                  <i className="far fa-comment-alt"></i> 0
                </span>
                <span className="col text-right">
                  <i className="fas fa-share"></i> 0
                </span>
              </div>
            </div>
          </div>
        )}
      </div>
    )
}
}

export default MatchesCommon;