import React, { Component, useState, useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { PropTypes } from "prop-types";
import Popup from "reactjs-popup";
// import CropImage from './CropImage';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"

import { createPostAction } from "../../data/actions/PostAction";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;


class EditPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
        Comment:"",
        postFile:[],
        PostID:'',
        loading: false,
    };
  }

  addFiles = (evt) =>{
    let that = this;
    if (evt.target.files.length > 0) {
      if(evt.target.files.length <=4){
        this.setState({loading:true});
        const files = Array.from(evt.target.files);
        Promise.all(files.map(file => {
            return (new Promise((resolve,reject) => {
                const reader = new FileReader();
                reader.addEventListener('load', (ev) => {
                    resolve(ev.target.result);
                });
                reader.addEventListener('error', reject);
                reader.readAsDataURL(file);
            }));
        }))
        .then(images => {
            this.setState({ postFile :this.state.postFile.concat(images),loading:false })
        }, error => {        
            console.error(error);
        });
      }else{
        toast.error("Maximum 4 files allowed to upload");
      }
    }
}

  _handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  deletePhoto = (e, day) => {
    e.preventDefault();
    this.setState({ loading:true })
    let postFile = this.state.postFile;
    var index = postFile.findIndex((item) => item == day);
    
    postFile.splice(index, 1);
    this.setState({ postFile: postFile,loading:false });
  };

  _returnFile(file,index){
    if(file.startsWith("data:image/")){
      return (
        <div className="multiple-image-preview">
          <img src={file} className="img-fluid post_select_images multiple-image-render" />
          <div className="multiple-middle-text">
            <div className="post-close-button" onClick={(e) => this.deletePhoto(e, file)} >X</div>
          </div>
       </div>
       );
    }
    if(file.startsWith("data:video/")){
      return (
        <div className="multiple-image-preview">
          <video width="320" height="240" className="img-fluid post_select_images multiple-image-render" controls="controls" preload="metadata">
            <source src={file} type="video/mp4" />
          </video>
           <div className="multiple-middle-text">
              <div className="post-close-button" onClick={(e) => this.deletePhoto(e, file)}>X</div>
             </div>
          </div>
          );
  }

  if(!file.includes("data:image/")){
    return (
      <div className="multiple-image-preview">
        <img src={file} className="img-fluid post_select_images multiple-image-render" />
        <div className="multiple-middle-text">
          <div className="post-close-button" onClick={(e) => this.deletePhoto(e, file)} >X</div>
        </div>
     </div>
     );
  }

}

  _postData = () =>{
    const {postFile,Comment,PostID}  = this.state;
    const {UserID,ReferenceID, ObjectType}  = this.props;
    let that = this;
    const Pictures = [];
    const Videos = [];

//     if(postFile.length > 0){
//       postFile.map(function(obj) {
//         if(obj.startsWith("data:image/")){
//           Pictures.push(obj)
//         }
//         if(obj.startsWith("data:video/")){
//           Videos.push(obj)
//           }
//       });
//   }

if(postFile.length > 0){
    postFile.map(function(obj) {
        Pictures.push(obj)
    });
}

    this.setState({loading:true});
    this.props
      .createPostAction(
        this.props.client,
        PostID,
        UserID,
        ReferenceID,
        ObjectType,
        Comment,
        Pictures,
        Videos,
      )
      .then((res) => {
          that.setState({loading:true});
          that._closeModal();
          that.props.callPostListEdit();
          this.props.updatePhotos();
      }).catch(function (error) {
        that.setState({loading:false});
        console.log(error);
      });
  }

  _closeModal = () =>{
    this.setState({ Comment:"", postFile:[],loading:false });
    this.props.closePopupEdit();
  }

  componentDidMount(){
let editData = this.props.editData;
this.setState({Comment:editData.Comment,postFile:editData.Pictures,PostID:editData.ID})
  }

  render() {
    const { Comment,postFile,loading } = this.state;
    const { blog } = this.props;
    const enabled = Comment || postFile.length > 0 || loading;
    return (
      <Popup open={this.props.doShowEdit} onClose={this._closeModal}>
        <div id="notificationModal" style={{ paddingLeft: 17 }}>
          <div className="modal-dialog create_post_modal">
            <div className="modal-content">

    <div className="modal-header postModelHeader">
        <h6 className="modal-title postText" >{ blog ? 'Edit Comment' : 'Edit Post'}</h6>
        <button type="button" className="close closeBtn" onClick={this._closeModal}>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div className="createPostModel">
   <div className="modelPostContainer">
        <div className="postUserImgContainer">
            <img src={this.props.ProfileImage ? this.props.ProfileImage: '../../assets/images/profile_pic.png' } className="postUserImg" alt="Back" />
        </div>
        <div className="postInputBox">
                <textarea name="Comment" 
                          value={Comment} 
                          onChange={this._handleChange} 
                          rows="3"
                          className="postInput" 
                          placeholder="Write something here...">
               </textarea>
            </div>
        <hr />
        <div className="postUpload">
                <label className="">
                   <div className="uploadImg"> <img src="../../assets/images/multimedia.png" /> Photo/Video </div>
                    <input type="file"
                           multiple
                          //  accept="images/*, videos/*"
                          accept="images/*"
                           name="postFile" 
                           onChange={this.addFiles}
                           style={{opacity: '0'}} />
                            {this.state.postFileCount &&
                            <span style={{color:'green',fontSize:'14px',textTransform:'lowercase'}}>{this.state.postFileCount} </span>}

                </label>
                </div>
                <div className="row">
                        {postFile && postFile.length > 0 && postFile.map((file,index)=>(
                            <div key={index} className="col-md-3">
                            {this._returnFile(file,index)}
                            </div>
                        ))}
                    </div>
                    <div className="sweet-loading">
                          <PuffLoader
                          css={override}
                          size={60}
                          color={"#123abc"}
                          loading={loading}/>
                      </div>

                      <button type="button" disabled={!enabled} className="btn btn-primary postBtn" onClick={this._postData}>Update</button>
   </div>
  
  </div>

             
                </div>
            </div>
          </div>
          <ToastContainer autoClose={1000} />
      </Popup>
    );
  }
}

EditPost.propTypes = {
    doShowEdit: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
});

export default compose(
  connect(mapStateToProps,{createPostAction}),
  withRouter,
  withApollo
)(EditPost);
