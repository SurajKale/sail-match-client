import React, { Component, Fragment } from "react";
import InfiniteCarousel from 'react-leaf-carousel';

class MemberContent extends Component {

    groupMember = () => {
        return this.props.MemberList.map((grp,index) => (
            <div className="" key={index}>
                <a className=""> 
                    <img title={grp.UserData.FullName} key={index} src={grp.UserData && grp.UserData.ProfileImage ? grp.UserData.ProfileImage : "../../assets/images/profile_pic.png"} 
                        className="img-fluid  smallListImg" alt="Profile" />
                </a>
            </div> 
        ));
    }

    matchMember = () => {
        return this.props.MemberList.map((mat,index) => (
            <div className="" key={index}>
                <a className=""> 
                    <img key={index} src={mat.ProfileImage ? mat.ProfileImage : "../../assets/images/profile_pic.png"} 
                        className="img-fluid  smallListImg" alt="Profile" />
                </a>
            </div> 
        ));
    } 

    render() {
        const {MemberList, forGroup, forMatch} = this.props;
        if (MemberList.length < 1) {return null;}
    return (
       <Fragment>
            <div className="col-md-12">
                <div className="row">
                    <h5 className="about-event">Members <span>({MemberList && MemberList.length || 0})</span><a href="#" className="view-all"></a></h5>
                </div>
            </div>
            {
                forGroup &&
                <div className="col-md-12">
                    <div className="row"> 
                        <div className="col-md-12 mb-2 photoSection">
                            <InfiniteCarousel
                                breakpoints={[
                                {
                                    breakpoint: 0,
                                    settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    },
                                },
                                {
                                    breakpoint: 576,
                                    settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    },
                                },
                                ]}
                                scrollOnDevice
                                slidesToShow={10}
                                slidesToScroll={3}
                            >
                                {this.groupMember()}
                            </InfiniteCarousel>
                        </div>
                    </div>           
                </div>
            }
            {
                forMatch &&
                <div className="col-md-12">
                    <div className="row"> 
                        <div className="col-md-12 mb-2 photoSection">
                            <InfiniteCarousel
                                breakpoints={[
                                {
                                    breakpoint: 0,
                                    settings: {
                                    slidesToShow: 1,
                                    slidesToScroll: 1,
                                    },
                                },
                                {
                                    breakpoint: 576,
                                    settings: {
                                    slidesToShow: 3,
                                    slidesToScroll: 3,
                                    },
                                },
                                ]}
                                scrollOnDevice
                                slidesToShow={10}
                                slidesToScroll={3}
                            >
                                {this.matchMember()}
                            </InfiniteCarousel>
                        </div>
                    </div>           
                </div>
            }  
       </Fragment>
      );
    }
  }
  
  export default MemberContent;