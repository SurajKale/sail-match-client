import React, { Component, useState, useEffect, Fragment } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { PropTypes } from "prop-types";
import Popup from "reactjs-popup";
// import CropImage from './CropImage';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"
import SearchAutoComplete from "../../match/SearchAutoComplete";
import Clone from "../../match/Clone";
import { sharePostAction } from "../../../data/actions";
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon } from 'react-share'

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class SharePost extends Component {
  constructor(props) {
    super(props);
    this.state = {
        shareContent : "",
        userList : [],
        otherEmailList: [],
        loading: true
    };
  }

  handleChange = async (e) => {
    await this.setState({
      shareContent: e.target.value,
    });
  };

  getWhoAttendedList = (whoAttended) => {
    this.setState({ userList: whoAttended.map((usr) => usr.ID) });
  };

  getotherEmailList = (data) => {
    this.setState({ otherEmailList: data });
  }

  sharePost = async (e) => {
    this.setState({ loading: false })
   
    const { shareContent, userList, otherEmailList } = this.state;
    const { ObjectType, ReferenceID, ReferenceUserID, PostUrl, ShareType } = this.props;

    let shareObject = {
      shareContent, userList, ObjectType, ReferenceID, ReferenceUserID, PostUrl, ShareType,
      otherEmailList
    }
    
    await this.props.sharePostAction(this.props.client, shareObject).then(res => {
      toast.success("Post shared successfully", {
          onClose: () => window.location.reload()
      });
    }).catch(function (error) {
        console.log(error);
    })
  }

  closeModal = () =>{
    this.setState({ postContent:"", userList:[] });
    this.props.closePopup();
  }

  render() {
    const { doShow, ShareType, shareUrl, title, quote } = this.props;
    const { shareContent, userList, otherEmailList, loading } = this.state;
    const enabled = (userList && userList.length > 0 && loading) || ( otherEmailList && otherEmailList.length > 0 && loading)  ;

    return (
      <Popup open={doShow} onClose={this.closeModal}>
        <div id="notificationModal" style={{ paddingLeft: 17 }}>
          <div className="modal-dialog create_post_modal">
            <div className="modal-content">
                <div className="modal-header postModelHeader">
                    { ShareType === "Member" &&
                        <h6 className="modal-title postText" >
                          Share to Sailmatch Member
                        </h6>
                    }
                    { ShareType === "Email" &&
                        <h6 className="modal-title postText" >
                          Share via Email
                        </h6>
                    }
                    { ShareType === "Facebook" &&
                        <h6 className="modal-title postText" >
                          Share on Facebook
                        </h6>
                    }
                    { ShareType === "Twitter" &&
                        <h6 className="modal-title postText" >
                          Share on Twitter
                        </h6>
                    }
                    <button type="button" className="close closeBtn" onClick={this.closeModal}>
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div className="createPostModel">
                    <div className="postContainer postContainer_Modal">
                       { ShareType === "Member" && <SearchAutoComplete label="Add User to Share this Post" callback={this.getWhoAttendedList} />}
                       { ShareType === "Email" && <Clone label="Add emails hers" getProps={this.getotherEmailList} />}
                
                       <br />
                       {
                          ShareType === "Member" &&
                          <Fragment>
                            <div className="postInputBox">
                              <textarea name="postContent" 
                                  value={shareContent} 
                                  onChange={this.handleChange} 
                                  rows="3"
                                  className="postInput pt-3" 
                                  placeholder="Write something here...">
                              </textarea>
                            </div>
                            <hr />
                          </Fragment>
                       }
                       {
                          ShareType === "Email" &&
                          <Fragment>
                            <div className="postInputBox">
                              <textarea name="postContent" 
                                  value={shareContent} 
                                  onChange={this.handleChange} 
                                  rows="3"
                                  className="postInput pt-3" 
                                  placeholder="Write something here...">
                              </textarea>
                            </div>
                            <hr />
                          </Fragment>
                       }  
                    {
                       ShareType === "Member" &&
                       <button type="button" disabled={!enabled} className="btn btn-primary postBtn" onClick={this.sharePost}>Share</button>
                    }
                    {
                       ShareType === "Email" &&
                       <button type="button" disabled={!enabled} className="btn btn-primary postBtn" onClick={this.sharePost}>Share</button>
                    }  
                    </div>
                  </div>
            </div>
        </div>
        </div>
        <ToastContainer autoClose={1000} />
      </Popup>
    );
  }
}

SharePost.propTypes = {
  doShow: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
});

export default compose(
  connect(mapStateToProps, { sharePostAction }),
  withRouter,
  withApollo
)(SharePost);
