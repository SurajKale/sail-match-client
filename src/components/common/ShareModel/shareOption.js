import React, { Component, Fragment } from 'react';
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import SharePopup from './sharePopup';
import { sharePostAction } from "../../../data/actions";
import { FacebookShareButton, FacebookIcon, TwitterShareButton, TwitterIcon } from 'react-share'

class shareOption extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showShareModel: false,
            shareType: ""
        }
    }

    closeSharePopup = () => {
        document.body.style.overflowY = "auto";
        this.setState({ showShareModel: false });
        this.props.closeHandler()
    };

    openSharePopup = (type) => {
        document.body.style.overflowY = "auto";
        this.setState({ showShareModel: true, shareType: type });
    };

    sharePost = async (ShareType) => {
        const { ObjectType, ReferenceID, ReferenceUserID, PostUrl } = this.props;
    
        let shareObject = {
          ObjectType, ReferenceID, ReferenceUserID, PostUrl, ShareType }
        
        await this.props.sharePostAction(this.props.client, shareObject).then(res => {
          toast.success("Post shared successfully", {
              onClose: () => window.location.reload()
          });
        }).catch(function (error) {
            console.log(error);
        })
    }

    render() {
        const { showShareModel } = this.state;
        return (
            <Fragment>
                <div className="shareSection" style={{ display: 'unset'}}>
                    <div id="share-triangle-up"></div>
                    <p className="where-to-share">Select Where to Share:</p>
                    <ul>
                        <li onClick={() => this.openSharePopup("Member")}><img src="../../assets/images/Matches.svg"/>Share to Sailmatch Member</li>
                        <li data-toggle="modal" data-target="#ShareModel">
                            <FacebookShareButton 
                                url="https://www.wikipedia.org/"
                                quote={this.props.quote}
                                onShareWindowClose={() => this.sharePost("Facebook")}
                            >
                                <FacebookIcon size={32} round={true} />
                                &nbsp;&nbsp;Share on Facebook
                            </FacebookShareButton>
                        </li>
                        <li>
                            <TwitterShareButton 
                                url="https://www.wikipedia.org/"
                                title={this.props.title}
                                onShareWindowClose={() => this.sharePost("Twitter")}
                            >
                                <TwitterIcon size={32} round={true} />
                                &nbsp;&nbsp;Share on Twitter
                            </TwitterShareButton>
                        </li>
                        <li onClick={() => this.openSharePopup("Email")}><img src="../../assets/images/Mail-at-icon.png" className="sharFB"/>Share via Email</li>
                    </ul>
                </div>
                <SharePopup
                    doShow={showShareModel}
                    closePopup={this.closeSharePopup}
                    ObjectType={this.props.ObjectType}
                    ReferenceID={this.props.ReferenceID}
                    ReferenceUserID={this.props.ReferenceUserID}
                    PostUrl={this.props.PostUrl}
                    ShareType={this.state.shareType}
                    shareUrl="https://www.wikipedia.org/"
                    title={this.props.title}
                    quote={this.props.quote}
                />
                <ToastContainer autoClose={1000}/>
            </Fragment>
        )
    }
}

export default compose(
    connect(null, { sharePostAction }),
    withApollo
  )(shareOption);
