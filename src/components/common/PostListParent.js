import React, { Component } from "react";
import CreatePost from "./CreatePost";
import { getPostList } from '../../data/query';

import PostListContent from './PostListContent';

import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import EditPost from "./EditPost";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
let Name = null;
let ProfileImage = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}


class PostListParent extends Component {
    constructor(props) {
    super(props);
    this.state = {
        doShowPostPopup: false,
        doShowPostPopupEdit:false,
        postListData:[],
        loading:false,
        editData:[],

        UserID:ID,
        ProfileImage:ProfileImage,
        

        limit:5,
        offset: 0,
        totalCount: 0,
        isScroll:true,

      }
    }

    callPostList =() => {
       this.setState({isScroll:true,postListData:[],limit:5,offset:0,totalCount:100})
      this.getPostListData(0,true);
    }

    callPostListEdit =() => {
      this.setState({isScroll:true,postListData:[],limit:5,offset:0,totalCount:100})
     this.getPostListData(0,true);
   }

   callPostListEDelete =() => {
     this.setState({isScroll:true,postListData:[],limit:5,offset:0,totalCount:100})
     this.getPostListData(0,true);
 }

    openPostPopup = (evt) => {
      evt.preventDefault();
      document.body.style.overflowY = "hidden";
      this.setState({ doShowPostPopup: true });
    };

    editPost = (evt,data) => {
      evt.preventDefault();
      document.body.style.overflowY = "hidden";
      this.setState({ doShowPostPopupEdit: true,editData:data });
    };

    closePostPopup = () => {
      document.body.style.overflowY = "auto";
      this.setState({ doShowPostPopup: false });
    };
    
    closePopupEdit = () => {
      document.body.style.overflowY = "auto";
      this.setState({ doShowPostPopupEdit: false, editData:[]});
    };


    getPostListData = (offsetValue,prop)=> {
      let that = this;
      if(!prop){
         window.addEventListener("scroll", this.handleScroll);
      }
      this.setState({loading:true});
      this.props.client
      .query({
          query: getPostList,
          variables: {
            ObjectType:this.props.ObjectType,
            ReferenceID: this.props.ReferenceID,
            limit: this.state.limit,
            offset: offsetValue
          },
         fetchPolicy: "network-only"
       }) .then(res => {  
        const data = res.data.getPostList.AllPosts;
        console.log(data)
        var joined = that.state.postListData.concat(data);
        if(data.length == 0){
          that.setState({isScroll:false, loading:false,});
          } 

          if(joined.length > 0){
            console.log(joined)
            that.setState({
              postListData: joined,
               totalCount: res.data.getPostList.TotalPostCount, //joined[0].TotalArticleCount
               loading:false,
             });
           }
        })
        .catch(function (error) {
          console.log(error);
        });
    }

    componentDidMount(){
      this.getPostListData(this.state.offset,false);
    }

    componentWillUnmount = () => {
      window.removeEventListener("scroll", this.handleScroll);
    };

    handleScroll = event => {
      // console.log('XXXXXXXXXXXXXXXXXX');
      // console.log(this.state.isScroll);
      if(this.state.isScroll){
        // console.log('trueeeee')
       if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {

        // console.log('vvvvvvvvvvvvvvvvvvvvv')
        //  console.log('postListData.length:'+this.state.postListData.length)
        //  console.log('totalCount:'+this.state.totalCount)
        //  console.log('offset:'+this.state.offset)
        //  console.log('totalCount:'+this.state.totalCount);

         // console.log('yessssssssssssssss')
         //you're at the bottom of the page
         //show loading spinner and make fetch request to api
         //will update offset
    

         this.setState({ offset:this.state.offset + 1 });

         // this.setState({
         //   offset:
         //     this.state.offset === 0
         //       ? this.state.limit + this.state.offset + 1
         //       : this.state.limit + this.state.offset
         // });
         if (
           this.state.postListData.length < this.state.totalCount &&
           this.state.offset < this.state.totalCount
         ) {

          // console.log('TTTTTTTTTTTTTTTTTTTTTTTTTTTT')
           
           this.getPostListData(this.state.offset,false);
         }
         //need to call the function which will add the records in state
         //and thus again call the getArticle function
       }
      }
     };

render() {
  const {ReferenceID,ObjectType,updatePhotos} = this.props;
  const {doShowPostPopup,postListData,loading,editData,doShowPostPopupEdit,UserID,ProfileImage,totalCount} = this.state;

  // console.log(postListData)

  return (
       <React.Fragment>
          <div className="col-md-12">
          
          { postListData.length > 0 &&
            <div className="row">
                <h5 className="about-event">Comments <span>({totalCount})</span>
                <a href="#!" className="view-all">View All</a></h5>
            </div>}
    </div>

    <div className="col-12 col-md-12 col-xl-12"> 
    <div className="row">

        <div className="col-12 col-md-2 col-xl-2"> </div>
        <div className="col-12 col-md-8 col-xl-8">  

           {UserID &&
            <div className="postContainer ">
                <div className="postText" id="postText">{ this.props.blog ? 'Add Comment' : 'Create Post'}</div>
                <div className="postSection">
                  <div className="postUserImgContainer">
                    <img src={ProfileImage ? ProfileImage : "../../assets/images/profile_pic.png"} className="postUserImg" alt="DP" />
                  </div>
                  <div className="postInputBox">
                      <textarea type="text" rows="2" id="input-text" readOnly className="postInput" placeholder="Write something here..." onClick={this.openPostPopup}></textarea>
                  </div>
                </div> 
            </div>}

              {postListData && <PostListContent postListData = {postListData} loading={loading} ObjectType={ObjectType} editPost={this.editPost} UserID={UserID} callPostListEDelete={this.callPostListEDelete}/>  }
        </div>   
        <div className="col-12 col-md-2 col-xl-2"> </div>
            
    </div>  

    {doShowPostPopup &&
           <CreatePost
                doShow={doShowPostPopup}
                closePopup={this.closePostPopup}
                UserID={UserID} 
                ReferenceID={ReferenceID}
                ObjectType={ObjectType} 
                ProfileImage={ProfileImage}
                callPostList={this.callPostList}
                updatePhotos={updatePhotos}
                blog={this.props.blog ? true : false }
                />}

{doShowPostPopupEdit &&
           <EditPost
                doShowEdit={doShowPostPopupEdit}
                closePopupEdit={this.closePopupEdit}
                UserID={UserID} 
                ReferenceID={ReferenceID}
                ObjectType={ObjectType} 
                ProfileImage={ProfileImage}
                editData={editData}
                callPostListEdit={this.callPostListEdit} 
                updatePhotos={updatePhotos}
                blog={this.props.blog ? true : false }
                />}
</div>

        
       </React.Fragment>
      );
    }
  }
  

  const mapStateToProps = (state) => ({
  });
  
  export default compose(
    connect(mapStateToProps,{}),
    withApollo
  )(PostListParent);
  