import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import {objectLikeDisLike} from '../../data/mutation'
import{_getLikeCount} from '../../data/actions/LikeActions'

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class ObjectLikeDislike extends Component {
    constructor(props) {
    super(props);
    this.state = {
        iconState:'',
        showCount:this.props.LikeCount,
        status:this.props.status
      }
    }

    static getDerivedStateFromProps(props, state) {
      if (props !== state) {
        return {
          showCount: props.LikeCount,
          status:props.status
        }     
    }
    return null;
  }

    _handleAction = (evt) =>{
      this.props.client
        .mutate({
          mutation: objectLikeDisLike,
          variables: {
                UserID:ID,
                ReferenceID:this.props.ReferenceID,
                ReferenceUserID:this.props.ReferenceUserID,
                ObjectType:this.props.ObjectType
          }
        })
        .then(res => {
          this.props.updateOnParent(res.data.objectLikeDisLike.TotalCount)
        if(this.state.iconState == 'fa fa-heart liked' || this.state.imgSrc == "../../assets/images/heart-icon.png"){
          //dislike code
          this.setState({iconState:'far fa-heart',imgSrc:"../../assets/images/dislike.png"});
          this.props._getLikeCount(5); //--> Here we have to pass count of thaat post after response call
      }else{
          //like code
             this.setState({iconState:'fa fa-heart liked',imgSrc:"../../assets/images/heart-icon.png"});
          this.props._getLikeCount(10); //--> Here we have to pass count of thaat post after response call
      }
          return this.setState({
            showCount:
              res.data.objectLikeDisLike.TotalCount
          });       
        });
    }

    componentDidMount(){
        this.setState({showCount:this.props.LikeCount,status:this.props.status});
        if(this.state.status && this.props.isDetail == false){
            this.setState({iconState: 'fa fa-heart liked'});
        }else{
            this.setState({iconState: 'far fa-heart'});
        }
        if(this.state.status && this.props.isDetail == true){
          this.setState({imgSrc:"../../assets/images/heart-icon.png"});
       }
       else{
          this.setState({imgSrc:"../../assets/images/dislike.png"});
      }
    }

render() {
const {LikeCount,ObjectType,ReferenceID,status,isDetail} = this.props;
const {iconState,showCount} = this.state;

const tmplikeCount = this.props.likeCount.likeCount;
// console.log(tmplikeCount)

  return (
       <Fragment>
         {isDetail == false ?
             <span className="col text-left">
                <i className={iconState} onClick={(evt) => this._handleAction(evt)}></i> 
                &nbsp;{showCount ? showCount : 0}
            </span>  
:
 <><img
  src={this.state.imgSrc}
  alt="heart-icon"
  className="pr-2"
  onClick={this._handleAction}
/> 
 <span>&nbsp;{showCount ? showCount :0}</span></>
}
       </Fragment>
      );
    }
  }
    
  const mapStateToProps = (state) => ({
    likeCount: state.likeCount
  })


export default compose(
  connect(mapStateToProps, {_getLikeCount}),
  withApollo
)(ObjectLikeDislike);
