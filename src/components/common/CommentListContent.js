import React, { Component } from "react";
import ShowCommentList from './ShowCommentList';

class CommentListContent extends Component {
    constructor(props) {
    super(props);
    this.state = {
      showItems:2,
      showMoreText:'View more',
      };
    }

    showAllComment = (total) =>{
      console.log(total);
      if(this.state.showMoreText == 'View more'){
        this.setState({showItems:total,showMoreText:'View less'})
      }else{
        this.setState({showItems:2,showMoreText:'View more'})
      }
    }


render() {
  const {showMoreText,showItems} = this.state;

    const {commentData,CommentCount,UserID,ObjectType} = this.props;
  return (
      <React.Fragment>
          {commentData && commentData.length > 0 && commentData.slice(0,showItems).map((comment,index)=>(
            <ShowCommentList comment={comment} key={index} callCommentListEdit={this.props.callCommentListEdit} editComment={this.props.editComment} UserID={UserID} ObjectType={ObjectType}/> ))}

{CommentCount > 2 &&
                <div className="loadCommentText">
                    <span onClick={(evt)=>this.showAllComment(CommentCount)}>{showMoreText}</span>
                </div>}

        </React.Fragment>
      );
    }
  }
  
export default CommentListContent;
  