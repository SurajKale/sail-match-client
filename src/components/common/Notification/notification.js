import React, { Component, Fragment } from "react";
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";

import { viewNotificationList } from '../../../data/query';

const LocalStorageData = require("../../../services/LocalStorageData");

let ID = null;
let ProfileImage = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationArray:[],
      total: 0,
      loading: true
    }
  }

  componentDidMount() {
    this.viewNotificationList();
  }

  viewNotificationList = () => {
    this.props.client
    .query({
        query: viewNotificationList,
        variables: {
          UserID: ID,
          limit: 5,
          offset: 0
        },
        fetchPolicy: "network-only"
     }) .then(res => {  
          const data = res.data.viewNotificationList.AllNotifications;
          const total = res.data.viewNotificationList.TotalCount;
          this.setState({ notificationArray: data, total: total, loading: false })
     });   
  };

  render() {
    const { notificationArray, total, loading } = this.state;
    return (
        <Fragment>
            <div class="notificationBox">
                <div id="triangle-up"></div>
                    <ul class="notificationUl">
                        <li class="notificationText">
                            <div><strong>Notifications</strong></div>
                            {/* <a href="#" class="markAllAsRead">Mark All as Read</a> */}
                        </li>
                        {
                          !loading && total === 0 &&
                          <li>
                            No Notification
                          </li>
                        }
                        { notificationArray && notificationArray.length > 0 && notificationArray.map( not => { 
                          return <Fragment>
                          {
                            not.Purpose === "Reported Post" &&
                            <li key={not.ID}>
                              <img src="../../assets/images/blue-folder.png" alt="Notification Photo" width="30" height="25" />
                              Your {not.Subject}
                              <span class="notificationTime">{not.CreatedDate}</span>
                            </li>
                          }
                          {
                            not.Subject === "post like" && not.ObjectType !== "Users" &&
                            <li key={not.ID}>
                              <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} alt="Notification Photo" width="30" height="25" />
                              {not.SenderDetails.FullName} likes your {not.ObjectType} - {not.Purpose}
                              <span class="notificationTime">{not.CreatedDate}</span>
                            </li>
                          }
                          {
                            not.Subject === "post like" && not.ObjectType === "Users" &&
                            <li key={not.ID}>
                              <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} alt="Notification Photo" width="30" height="25" />
                              {not.SenderDetails.FullName} likes your profile
                              <span class="notificationTime">{not.CreatedDate}</span>
                            </li>
                          }
                          {
                            not.Subject === "Leave" && not.ObjectType === "Groups" &&
                            <li key={not.ID}>
                              <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} alt="Notification Photo" width="30" height="25" />
                              {not.SenderDetails.FullName} leaves your {not.ObjectType} - {not.Purpose}
                              <span class="notificationTime">{not.CreatedDate}</span>
                            </li>
                          }
                          {
                            not.Subject === "Join" && not.ObjectType === "Groups" &&
                            <li key={not.ID}>
                              <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} alt="Notification Photo" width="30" height="25" />
                              {not.SenderDetails.FullName} join your {not.ObjectType} - {not.Purpose}
                              <span class="notificationTime">{not.CreatedDate}</span>
                            </li>
                          }
                          </Fragment>
                        })}
                        { notificationArray && total > 5 && <li><a href="/all-notification" class="seeAll">See All</a></li>}
                    </ul>      
              </div> 
        </Fragment>
    );}
  }

  export default compose(
    connect(null),
    withApollo
  )(Notification);