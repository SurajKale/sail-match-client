import React, { Component, Fragment } from "react";
import { css } from "@emotion/core";
import ReactPaginate from 'react-paginate';
import PuffLoader from "react-spinners/PuffLoader";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import "react-alert-confirm/dist/index.css";
import confirm, { Button, alert } from "react-alert-confirm";

import { viewNotificationList } from '../../../data/query';
import { deleteNotification } from '../../../data/mutation';

const LocalStorageData = require("../../../services/LocalStorageData");

let ID = null;
let ProfileImage = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class AllNotification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notificationArray:[],
      total: 0,
      limit: 10,
      offset: 0,
      loading: true
    }
  }

  componentDidMount() {
    this.viewNotificationList(this.state.offset);
  }

  handlePageClick = (data) => {
    let selected = data.selected;
    this.viewNotificationList(selected);
  };

  viewNotificationList = (offset) => {
    this.props.client
    .query({
        query: viewNotificationList,
        variables: {
          UserID: ID,
          limit: this.state.limit,
          offset: offset
        },
        fetchPolicy: "network-only"
     }) .then(res => {
          const data = res.data.viewNotificationList;
          const total = data.TotalCount;
          const pageCount = Math.ceil(total / this.state.limit);

          if(data.AllNotifications.length > 0){
              this.setState({ 
                notificationArray: data.AllNotifications,
                total:pageCount,
                loading:false
              });
          }else{
              this.setState({ 
                notificationArray: [],
                total:0,
                loading:false
              });
          }
     });   
  };

  deleteNotification = (id, index) => {
    this.props.client
      .mutate({
          mutation: deleteNotification,
          variables: {
            NotificationID: parseInt(id),
          }
      })
      .then(res => {
        if(res.data.deleteNotification.ID){
          let data = this.state.notificationArray.slice();
          data.splice(index, 1);
          this.setState({ notificationArray: data })
          toast.success("Notification deleted successfully");
        }
    });
  }

  handleClickConfirm =  (id, index) => {
    const instance = confirm({
      title: `Hello`,
      content: `Are you sure you want to delete this notification !`,
      lang: "en",
      onOk: async () => {
        await this.deleteNotification(id, index);
      },
      onCancel: () => {
        console.log("cancel");
      },
    });
  };

  render() {
    const { notificationArray, total, loading } = this.state;
    return (
        <Fragment>
            <div class="container TopSpace">
                <div class="row nomargin mb-4">
                    <div class="col-sm-1 col-lg-1 col-md-12 col-xs-12 col-xl-1 nopadding"></div>
                    <div class="col-sm-10 col-lg-10 col-md-12 col-xs-12 col-xl-10 nopadding">
                        <div class="row nomargin ">
                            <h1 class="notiTitle">Notifications</h1>
                        </div>
                        <div className="sweet-loading">
                          <PuffLoader
                            css={override}
                            size={60}
                            color={"#123abc"}
                            loading={loading}
                          />
                        </div>
                        {
                            !loading && notificationArray.length === 0 &&
                            <div class="row nomargin notiList">
                                &nbsp;&nbsp;NO RECORD FOUND
                            </div>
                        }
                        {
                          notificationArray && notificationArray.length > 0 && notificationArray.map((not,index) => (
                            <div class="row nomargin notiList">
                                <div class=" col-lg-1 col-md-12 col-12 col-xl-1 nopadding">
                                    <div class="notiSideImg">
                                        {  not.Purpose === "Reported Post" &&    
                                            <div class="">
                                                <img src="../../assets/images/blue-folder.png" class="img-fluid "/>
                                            </div>
                                        }
                                        {  not.Subject === "post like" &&     
                                            <div class="">
                                                <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} class="img-fluid "/>
                                            </div>
                                        }
                                        {  not.Subject === "Leave" && not.ObjectType === "Groups" &&    
                                            <div class="">
                                                <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} class="img-fluid "/>
                                            </div>
                                        }
                                        {  not.Subject === "Join" && not.ObjectType === "Groups" &&    
                                            <div class="">
                                                <img src={not.SenderDetails && not.SenderDetails.ProfileImage ? not.SenderDetails.ProfileImage : "../../assets/images/blue-folder.png"} class="img-fluid "/>
                                            </div>
                                        }
                                    </div>
                                </div>
                                <div class="col-sm-10 col-lg-10 col-md-12 col-xs-12 col-xl-10 nopadding">
                                    <div class="notiContent">
                                        { not.Purpose === "Reported Post" &&
                                            <p class="meText">
                                                Your {not.Subject}
                                            </p>
                                        }
                                        { not.Subject === "post like" && not.ObjectType !== "Users" && 
                                            <p class="meText">
                                              {not.SenderDetails.FullName} likes your {not.ObjectType} - {not.Purpose}
                                            </p>
                                        }
                                        { not.Subject === "post like" && not.ObjectType === "Users" &&
                                            <p class="meText">
                                              {not.SenderDetails.FullName} likes your profile
                                            </p>
                                        }
                                        { not.Subject === "Leave" && not.ObjectType === "Groups" &&
                                            <p class="meText">
                                              {not.SenderDetails.FullName} leaves your {not.ObjectType} - {not.Purpose}
                                            </p>
                                        }
                                        { not.Subject === "Join" && not.ObjectType === "Groups" &&
                                            <p class="meText">
                                              {not.SenderDetails.FullName} join your {not.ObjectType} - {not.Purpose}
                                            </p>
                                        }
                                    </div>
                                </div>
                                <div class="col-lg-1 col-md-12 col-12 col-xl-1 nopadding">
                                    <div class="notiMore" onClick={()=>{this.handleClickConfirm(not.ID, index)}}>
                                        <span><img src="../../assets/images/icon-delete.png"/></span>
                                    </div>
                                </div>            
                            </div>
                          ))
                        }
                    </div>
                    <div class="col-sm-1 col-lg-1 col-md-12 col-xs-12 col-xl-1 nopadding"></div>
                </div>
                <div className="row nomargin">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-12 col-sm-12">
                     <nav aria-label="Page navigation example">
                        <ReactPaginate
                                previousLabel={""}
                                nextLabel={""}
                                breakLabel={'...'}
                                breakClassName={'break-me'}
                                pageCount={total}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                pageLinkClassName={'page-link'}
                                onPageChange={this.handlePageClick}
                                containerClassName={'pagination paginationBottom'}
                                subContainerClassName={'pages pagination'}
                                activeClassName={'page-item active'} 
                                disableInitialCallback={true}
                                previousClassName={'backArrow'}  
                                nextClassName={'nextArrow'}/>
                                  </nav> 
                    </div>
                </div>
            </div>
            <ToastContainer autoClose={1000} />
        </Fragment>
    )}
  }

  export default compose(
    connect(null),
    withApollo
  )(AllNotification);