import React, { Component, useState, useEffect } from "react";
import { connect, useDispatch, useSelector } from "react-redux";
import { withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { PropTypes } from "prop-types";
import Popup from "reactjs-popup";
// import CropImage from './CropImage';
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"

import { createCommentAction } from "../../data/actions/PostAction";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;


class EditReply extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CommentText:"",
      CommentPhoto:[],
        ID:'',
        loading: false,
        ProfileImage:'',
    };
  }

  toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  addFile = (evt) =>{
    let file = evt.target.files[0];
    this.setState({fileName:file.name+' selected'})
    this.toBase64(file).then((result) => {
     this.setState({ CommentPhoto: result });
    });
}

  _handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  deletePhoto = (e, day) => {
    this.setState({ CommentPhoto:'' });
  };

  _postData = () =>{
    const {CommentPhoto,CommentText,ID,ParentCommentID}  = this.state;
    const editCommentData = this.props.editCommentData;
    const UserID = editCommentData.UserID;
    const ReferenceID= editCommentData.ReferenceID;
    const Reply = undefined;
    const type = "CommentPosts" 


    let that = this;

    this.setState({loading:true});
    this.props
      .createCommentAction(
        this.props.client,
        ParentCommentID,
        CommentText,
        ID,
        UserID,
        ReferenceID,
        Reply,
        CommentPhoto,
        type
      )
      .then((res) => {
        // const ReferenceID = res.data.addComment.ReferenceID;
          that.setState({loading:true});
          that._closeModal();
          that.props.callCommentListEdit(ParentCommentID);
      }).catch(function (error) {
        that.setState({loading:false});
        console.log(error);
      });
  }

  _closeModal = () =>{
    this.setState({ Comment:"", CommentPhoto:'',loading:false });
    this.props.closePopupCommentEdit();
  }

  componentDidMount(){
    let editCommentData = this.props.editCommentData;
    this.setState({CommentText:editCommentData.Reply,CommentPhoto:editCommentData.Pictures,ID:editCommentData.ID,
        ProfileImage:editCommentData.UserDetails.ProfileImage,ParentCommentID:this.props.ParentCommentID})
  }

  render() {
    const { CommentText,CommentPhoto,loading,ProfileImage } = this.state;
    const enabled = CommentText || CommentPhoto || loading;
    return (
      <Popup open={this.props.doShowCommentEdit} onClose={this._closeModal}>
        <div id="notificationModal" style={{ paddingLeft: 17 }}>
          <div className="modal-dialog create_post_modal">
            <div className="modal-content">

    <div className="modal-header postModelHeader">
        <h6 className="modal-title postText" >Edit Reply</h6>
        <button type="button" className="close closeBtn" onClick={this._closeModal}>
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div className="createPostModel">
   <div className="modelPostContainer">
        <div className="postUserImgContainer">
            <img src={ProfileImage} className="postUserImg" alt="Back" />
        </div>
        <div className="postInputBox">
                <textarea name="CommentText" 
                          value={CommentText} 
                          onChange={this._handleChange} 
                          rows="3"
                          className="postInput" 
                          placeholder="Write something here...">
               </textarea>
            </div>
        <hr />
        <div className="postUpload">
                <label className="">
                   <div className="uploadImg"> <img src="../../assets/images/multimedia.png" /> Photo/Video </div>
                    <input type="file"
                           multiple
                          //  accept="images/*, videos/*"
                          accept="images/*"
                           name="CommentPhoto" 
                           onChange={this.addFile}
                           style={{opacity: '0'}} />
                           

                </label>
                </div>
                    {CommentPhoto &&
                      <div className="row">
                            <div className="col-md-3">
                                <div className="multiple-image-preview">
                                    <img src={CommentPhoto} className="img-fluid post_select_images multiple-image-render" />
                                    <div className="multiple-middle-text">
                                        <div className="post-close-button" onClick={(e) => this.deletePhoto(e, CommentPhoto)} >X</div>
                                    </div>
                                </div>
                            </div>
                         </div>}
                    <div className="sweet-loading">
                          <PuffLoader
                          css={override}
                          size={60}
                          color={"#123abc"}
                          loading={loading}/>
                      </div>

                      <button type="button" disabled={!enabled} className="btn btn-primary postBtn" onClick={this._postData}>Update</button>
   </div>
  
  </div>

             
                </div>
            </div>
          </div>
          <ToastContainer autoClose={1000} />
      </Popup>
    );
  }
}

EditReply.propTypes = {
    doShowCommentEdit: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
});

export default compose(
  connect(mapStateToProps,{createCommentAction}),
  withRouter,
  withApollo
)(EditReply);
