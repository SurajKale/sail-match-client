import React, { Component } from "react";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import ConfirmDeleteComment from './ConfirmDeleteComment';
import ConfirmReportComment from './Comment/reportPostComment';
import { deleteCommentReply } from "../../data/mutation";
import CommentLikeDislike from '../common/CommentLikeDislike';
import ReplyComment from './ReplyComment';
import { getListofReplies, reportPost } from '../../data/query';
import EditReply from './EditReply';
import WriteReply from './WriteReply';

import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";

class CommentListContent extends Component {
    constructor(props) {
    super(props);
    this.state = {
       totalCount:'',
       replyData:[],
       ReplyCount:'',

       doShowReplyPopupEdit:false,
       editCommentData:[],
       CommentID:'',


      }
    }

    callCommentListEdit =(CommentID) => {
      this.getCommentReply(CommentID);
    }

    editReply = (data) => {
      document.body.style.overflowY = "hidden";
      this.setState({ doShowReplyPopupEdit: true,editCommentData:data });
    };

    closePopupReplyEdit = () => {
      document.body.style.overflowY = "auto";
      this.setState({ doShowReplyPopupEdit: false, editCommentData:[]});
    };

    objectLike = (count) =>{
        this.setState({totalCount:count})
      }

   
    deleteObject = (evt,UserID,PostID) => {
      this.props.client
        .mutate({
          mutation: deleteCommentReply,
          variables: {
              ID: PostID,
              UserID: UserID,
          },
        })
        .then((res) => {
          if (res.data.deleteCommentReply) {
            const ReferenceID = res.data.deleteCommentReply.ReferenceID;
                  this.props.callCommentListEdit(ReferenceID);
          }
        });
    };

    reportComment = (evt, UserID, PostID, reason, ObjectType) => {
      this.props.client
        .mutate({
          mutation: reportPost,
          variables: {
              ReferenceType: "Comments",
              ReferenceID: parseInt(PostID),
              ReferenceUserID: parseInt(UserID),
              ObjectType: ObjectType,
              ReporterDetails: {
                UserID: parseInt(this.props.UserID),
                Reason: reason
              },
          }
        })
        .then((res) => {
          if(res.data.reportPost) {
            toast.success("Comment reported successfully");
          }
        });
    };
    
    componentDidMount = () => {
        this.setState({totalCount:this.props.comment.LikeCount,ReplyCount:this.props.comment.ReplyCount})
    }

    getCommentReply = (CommentID) => {
      this.props.client
      .query({
          query: getListofReplies,
          variables: {
            CommentID:CommentID,
          },
          fetchPolicy: "network-only"
       }) .then(res => {  

            const data = res.data.getListofReplies.AllReply;
            if(data.length > 0){
              this.setState({replyData:data,showComment:true,ReplyCount:res.data.getListofReplies.TotalReplCount,CommentID:CommentID})
            }else{
              this.setState({replyData:data,showComment:false,CommentID:'' })
            }
       });         
    };


render() {
    const {comment,UserID,ObjectType} = this.props;
    const {totalCount,ReplyCount,replyData,doShowReplyPopupEdit,editCommentData,CommentID} = this.state;
   

  return (
      <React.Fragment>
         {comment &&
            <div className="commentSection">    
                <div className="postCommentBySection">
                    <img src={comment.UserDetails ? comment.UserDetails.ProfileImage : ''} className="commentUserImg" />
                </div>

                <div className="postCommentsByUser">
                    { UserID && 
                    <React.Fragment>
                      <div className="commentDot" onClick={(evt)=>window.cmntActClick(evt,comment.ID)}></div>
                      <div className="commentDotModel" id={'cmntAction'+comment.ID} style={{display:'none'}}>
                        <ul>
                          {UserID && comment.UserID == UserID && <li onClick={(evt)=>this.props.editComment(comment)}>Edit</li>}
                          {UserID && comment.UserID == UserID && 
                          <li>
                            <ConfirmDeleteComment
                              UserID={comment.UserID}
                              PostID={comment.ID}
                              callBack={this.deleteObject}
                              object={"comment"}/>
                          </li>}
                          {UserID && comment.UserID !== UserID && 
                          <li>
                            <ConfirmReportComment
                              UserID={comment.UserID}
                              PostID={comment.ID}
                              callBack={this.reportComment}
                              object={"comment"}
                              ObjectType={ObjectType}
                            />
                          </li>}
                        </ul>
                      </div>
                    </React.Fragment>}

                        <p className="commentsByUserName">
                          {comment.UserDetails ? comment.UserDetails.FullName : ''}  <span>{comment.CreatedDate}</span></p>
                        <p className="postUserComments">{comment.Comment}
                            {comment.Pictures &&
                            <img src={comment.Pictures} className="commentUploadImg" alt="Back" />}
                        </p>
                    </div>

                {/* <div className="replyUserComments"><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="" /><span> 10</span></div> */}

                {!UserID && 
                    <div className="replyUserComments">
                         <span className="col text-left">
                              <i className="far fa-heart" style={{fontSize:'18px'}}></i> 
                             {totalCount}
                          </span>
                    </div>}

                      {UserID && <CommentLikeDislike LikeCount={totalCount} ObjectType={'Comments'} ReferenceID={comment.ID} status={comment.isLike} ReferenceUserID={comment.UserID} updateOnParent={this.objectLike} isDetail={false} from="comment"/> }
                
                <div className="like" onClick={(evt)=>this.getCommentReply(comment.ID)}>
                    <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" />
                    <span> {ReplyCount}</span>

                   

                </div>
                <div className="replyToCommentSection">
                {UserID &&  <WriteReply post={comment} getCommentReply={this.getCommentReply} />}

                     <ReplyComment replyData={replyData} callCommentListEdit={this.callCommentListEdit} editReply={this.editReply} CommentID={CommentID} UserID={UserID}/>
               </div>
            </div>
            
          }

{doShowReplyPopupEdit &&
           <EditReply
                doShowCommentEdit={doShowReplyPopupEdit}
                closePopupCommentEdit={this.closePopupReplyEdit}
                UserID={comment.UserID} 
                ParentCommentID={CommentID}
                editCommentData={editCommentData}
                callCommentListEdit={this.callCommentListEdit} 
                />}
          <ToastContainer autoClose={1000} />
        </React.Fragment>
      );
    }
  }
  
export default compose(connect(null, {}), withApollo)(CommentListContent);
  