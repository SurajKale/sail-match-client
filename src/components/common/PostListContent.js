import React, { Component } from "react";
import CreatePost from "./CreatePost";
import { getPostList } from '../../data/query';

import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"
import ShowPostList from './ShowPostList';

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PostListContent extends Component {
    constructor(props) {
    super(props);
    this.state = {
        postListData:[],

      }
    }

render() {
  const {postListData,loading,ObjectType,UserID} = this.props;


  return (
      <React.Fragment> 
          {postListData.map((post,index)=>(
                 <ShowPostList post={post} key={index} ObjectType={ObjectType} editPost={this.props.editPost} UserID={UserID} callPostListEDelete={this.props.callPostListEDelete} />
                 ))}
            </React.Fragment>
         );
    }
  }
  

  const mapStateToProps = (state) => ({
  });
  
  export default compose(
    connect(mapStateToProps,{}),
    withApollo
  )(PostListContent);
  