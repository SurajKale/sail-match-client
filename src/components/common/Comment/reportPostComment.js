import React, { Component, Fragment } from "react";
import "react-alert-confirm/dist/index.css";
import confirm, { Button, alert } from "react-alert-confirm";
const LocalStorageData = require("../../../services/LocalStorageData");
let ID = null;
let Name = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ID) {
  let FirstName = LocalStorageData.FirstName;
  let LastName = LocalStorageData.LastName;
  Name = FirstName + " " + LastName;
}

class ConfirmReportComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reason: ""
    };
  }

  handleChange = (e) => {
    this.setState({ reason: e.target.value})
  }

  data = () => {
    return <Fragment> 
      <p>Are you sure you want to report this {this.props.object} ?</p>
      <textarea className="form-control" row="2" name="reason" onChange={this.handleChange}/>
    </Fragment>
    
  }

  handleClickConfirm = (evt) => {
    const instance = confirm({
      title: `Hello ${Name} `,
      content: this.data(),
      lang: "en",
      onOk: () => {
        this.props.callBack(evt,this.props.UserID,this.props.PostID, this.state.reason, this.props.ObjectType);
      },
      onCancel: () => {
        // console.log("cancel", instance);
      },
    });

    
  };
  render() {
    return (
      <Fragment >
        {/* <button className="deleteBtn" onClick={(evt)=>this.handleClickConfirm(evt)}> */}
          {/* <img
            src="../../assets/images/trash.png"
            className="pr-1"
            alt="tick-icon"
          />{" "} */}
          <span onClick={(evt)=>this.handleClickConfirm(evt)}>Report</span>
          
        {/* </button>{" "} */}
      </Fragment>
    );
  }
}

export default ConfirmReportComment;
