import React, { useState } from 'react';
import MultiImageInput from 'react-multiple-image-input';
 
function App() {
  const crop = {
    unit: '%',
    aspect: 4 / 3,
    width: '100'
  };
 
  const [images, setImages] = useState({});
 
  console.log(images)
  return (
    <MultiImageInput
      images={images}
      setImages={setImages}
      theme="light "
      cropConfig={{ crop, ruleOfThirds: true }}
    />
  );
}
 
export default App;