import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { deleteCommentReply } from "../../data/mutation";
import { reportPost } from '../../data/query';
import CommentLikeDislike from './CommentLikeDislike';
import ConfirmDeleteComment from './ConfirmDeleteComment';
import ConfirmReportComment from './Comment/reportPostComment';

class ShowReplyList extends Component {
    constructor(props) {
    super(props);
    this.state = {
      totalCount:'',
      }
    }

    objectLike = (count) =>{
      this.setState({totalCount:count})
    }

       
    deleteObject = (evt,UserID,PostID) => {
      this.props.client
        .mutate({
          mutation: deleteCommentReply,
          variables: {
              ID: PostID,
              UserID: UserID,
          },
        })
        .then((res) => {
          if (res.data.deleteCommentReply) {
            // const ReferenceID = res.data.deleteCommentReply.ReferenceID;
                  this.props.callCommentListEdit(this.props.CommentID);
          }
        });
    };

    reportComment = (evt, UserID, PostID, reason, ObjectType) => {
      this.props.client
        .mutate({
          mutation: reportPost,
          variables: {
              ReferenceType: "Comments",
              ReferenceID: parseInt(PostID),
              ReferenceUserID: parseInt(UserID),
              ObjectType: ObjectType,
              ReporterDetails: {
                UserID: parseInt(this.props.UserID),
                Reason: reason
              },
          }
        })
        .then((res) => {
          if(res.data.reportPost) {
            toast.success("Reply reported successfully");
          }
        });
    };

    componentDidMount() {
      this.setState({totalCount:this.props.reply.LikeCount})
  }

render() {
    const {reply,CommentID,UserID,ObjectType} = this.props;
    const {totalCount} = this.state;
  return (
        <React.Fragment>
            <div className="postCommentBySection">
                <img src={reply.UserDetails ? reply.UserDetails.ProfileImage : ''} className="commentUserImg" />
            </div>
            <div className="postCommentsByUserReply">
            {UserID &&
            <React.Fragment>
              <div className="replyDot" onClick={(evt)=>window.replyClick(evt,reply.ID)}></div>
                <div className="replyDotModel" id={'replyAction'+reply.ID} style={{display: 'none'}}>
                  <ul>
                    { UserID && reply.UserID == UserID && <li onClick={(evt)=>this.props.editReply(reply)}>Edit</li>}
                    { UserID && reply.UserID == UserID && <li>
                        <ConfirmDeleteComment
                            UserID={reply.UserID}
                            PostID={reply.ID}
                            callBack={this.deleteObject}
                            object={"comment"}/>
                    </li>}
                    {UserID && reply.UserID !== UserID && <li>
                      <ConfirmReportComment
                        UserID={reply.UserID}
                        PostID={reply.ID}
                        callBack={this.reportComment}
                        object={"reply"}
                        ObjectType={ObjectType}
                      />
                    </li>}
                  </ul>
                </div>
            </React.Fragment>}
                <p className="commentsByUserName">{reply.UserDetails.FullName} <span>{reply.CreatedDate}</span></p>

                <p className="postUserComments"> {reply.Reply}
                {reply.Pictures &&
                    <img src={reply.Pictures} className="commentUploadImg" alt="Back" />}
                </p>
            </div>

            {!UserID  && 
                    <div className="replyUserComments">
                         <span className="col text-left">
                              <i className="far fa-heart" style={{fontSize:'18px'}}></i> 
                             {totalCount}
                          </span>
                    </div>}

                      {UserID && <CommentLikeDislike LikeCount={totalCount} ObjectType={'Comments'} ReferenceID={reply.ID} status={reply.isLike} ReferenceUserID={reply.UserID} updateOnParent={this.objectLike} isDetail={false} from="comment"/> }

            {/* <div className="replyUserComments">
              <img src="../../assets/images/heart-icon.png" alt="heart-icon" className="" /><span> 10</span>
          </div> */}
<br />
        </React.Fragment>
      );
    }
  }
  
export default compose(connect(null, {}), withApollo)(ShowReplyList);
  