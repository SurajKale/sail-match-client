import React, { Component, Fragment } from 'react';
import { withGoogleMap, GoogleMap,Circle, withScriptjs, InfoWindow, Marker } from "react-google-maps";
import Geocode from "react-geocode";
import Autocomplete from 'react-google-autocomplete';
import { GoogleMapsAPI } from '../../../services/CommonServices';
import Range from './range'

Geocode.setApiKey( GoogleMapsAPI );
Geocode.enableDebug();

class Map extends Component{
	constructor( props ){
		super( props );	
		this.state = {
			address: '',
			city: '',
			area: '',
			state: '',
			mapPosition: {
				lat: this.props.center.lat,
				lng: this.props.center.lng
			},
			markerPosition: {
				lat: this.props.center.lat,
				lng: this.props.center.lng
			},
			mapRadius:5000,
			getRangeVisible:false
		}			
	}
	
	/**
	 * 
	 * Get the current address from the default map position and set those values in the state
	 */	  
	 async componentDidMount() {		  
		 await Geocode.fromLatLng ( this.state.mapPosition.lat , this.state.mapPosition.lng ).then(
			async response => {
				const address = response.results[0].formatted_address,
				      addressArray =  response.results[0].address_components,
				      city = this.getCity( addressArray ),
				      area = this.getArea( addressArray ),
				      state = this.getState( addressArray ),
					  latValue = response.results[0].geometry.location.lat,
					  lngValue = response.results[0].geometry.location.lng;

				await this.setState( {
					address: ( address ) ? address : '',
					area: ( area ) ? area : '',
					city: ( city ) ? city : '',
					state: ( state ) ? state : '',
					markerPosition: {
						lat: latValue,
						lng: lngValue
					},
					getRangeVisible:true
				} )
				await this.getDataFromLocalStorage()
			},
			error => {
				console.error( error );
			}
		);
		this.props.callback({
			area: this.state.area,
			city: this.state.city,
			radius: this.state.mapRadius,
			coordinate: this.state.mapPosition
	    })
	};

	getDataFromLocalStorage = async () => {
		const locationData = JSON.parse(localStorage.getItem('locationData'));
		{
			locationData !== null && locationData && 
				 await this.setState({
					mapPosition: {
						lat: locationData.mapPosition.lat,
						lng: locationData.mapPosition.lng
					},
					markerPosition: {
						lat: locationData.mapPosition.lat,
						lng: locationData.mapPosition.lng
					},
					area: locationData.area,
					// mapRadius: locationData.radius
				})
		}
	}
    


	handleOnChange = (value) => {
		this.setState({
		  volume: value
		})
	  }

	/**
	 * Component should only update ( meaning re-render ), when the user selects the address, or drags the pin
	 *
	 * @param nextProps
	 * @param nextState
	 * @return {boolean}
	 */
	shouldComponentUpdate( nextProps, nextState ){
		if (
			this.state.markerPosition.lat !== this.props.center.lat ||
			this.state.address !== nextState.address ||
			this.state.city !== nextState.city ||
			this.state.area !== nextState.area ||
			this.state.state !== nextState.state
			) {
			return true
		} else if ( this.props.center.lat === nextProps.center.lat ){
			return false
		}
	}

	/**
	 * Get the city and set the city input value to the one selected
	 *
	 * @param addressArray
	 * @return {string}
	 */
	getCity = ( addressArray ) => {
		let city = '';
		for( let i = 0; i < addressArray.length; i++ ) {
			if ( addressArray[ i ].types[0] && 'administrative_area_level_2' === addressArray[ i ].types[0] ) {
				city = addressArray[ i ].long_name;
				return city;
			}
		}
	};

	/**
	 * Get the area and set the area input value to the one selected
	 *
	 * @param addressArray
	 * @return {string}
	 */
	getArea = ( addressArray ) => {
		let area = '';
		for( let i = 0; i < addressArray.length; i++ ) {
			if ( addressArray[ i ].types[0]  ) {
				for ( let j = 0; j < addressArray[ i ].types.length; j++ ) {
					if ( 'sublocality_level_1' === addressArray[ i ].types[j] || 'locality' === addressArray[ i ].types[j] ) {
						area = addressArray[ i ].long_name;
						return area;
					}
				}
			}
		}
	};

	/**
	 * Get the address and set the address input value to the one selected
	 *
	 * @param addressArray
	 * @return {string}
	 */
	getState = ( addressArray ) => {
		let state = '';
		for( let i = 0; i < addressArray.length; i++ ) {
			for( let i = 0; i < addressArray.length; i++ ) {
				if ( addressArray[ i ].types[0] && 'administrative_area_level_1' === addressArray[ i ].types[0] ) {
					state = addressArray[ i ].long_name;
					return state;
				}
			}
		}
	};

	/**
	 * And function for city,state and address input
	 * @param event
	 */
	onChange = ( event ) => {
		this.setState({ [event.target.name]: event.target.value });
	};
	/**
	 * This Event triggers when the marker window is closed
	 *
	 * @param event
	 */
	onInfoWindowClose = ( event ) => {

	};

	/**
	 *
	 * When the marker is dragged you get the lat and long using the functions available from event object.
	 * Use geocode to get the address, city, area and state from the lat and lng positions.
	 * And then set those values in the state.	 *
	 * @param event
	 */
	onMarkerDragEnd = async ( event ) => {
		let newLat = event.latLng.lat(),
		    newLng = event.latLng.lng();

		await Geocode.fromLatLng( newLat , newLng ).then(
			async response => {
				const address = response.results[0].formatted_address,
				      addressArray =  response.results[0].address_components,
				      city = this.getCity( addressArray ),
				      area = this.getArea( addressArray ),
				      state = this.getState( addressArray );
				await this.setState( {
					address: ( address ) ? address : '',
					area: ( area ) ? area : '',
					city: ( city ) ? city : '',
					state: ( state ) ? state : '',
					markerPosition: {
						lat: newLat,
						lng: newLng
					},
					mapPosition: {
						lat: newLat,
						lng: newLng
					},
					getRangeVisible:true
				} )
			},
			error => {
				console.error(error);
			}
		);
		this.props.callback({
			area: this.state.area,
			city: this.state.city,
			radius: this.state.mapRadius,
			coordinate: this.state.mapPosition
	    })
	};

	/**
	 * When the user types an address in the search box
	 * @param place
	 */
	onPlaceSelected = async( place ) => {
		const address = place.formatted_address,
		      addressArray =  place.address_components,
		      city = this.getCity( addressArray ),
		      area = this.getArea( addressArray ),
		      state = this.getState( addressArray ),
		      latValue = place.geometry.location.lat(),
		      lngValue = place.geometry.location.lng();
		// Set these values in the state.
		await this.setState({
			address: ( address ) ? address : '',
			area: ( area ) ? area : '',
			city: ( city ) ? city : '',
			state: ( state ) ? state : '',
			markerPosition: {
				lat: latValue,
				lng: lngValue
			},
			mapPosition: {
				lat: latValue,
				lng: lngValue
			},
			getRangeVisible:true
			
		})
		this.props.callback({
			area: this.state.area,
			city: this.state.city,
			radius: this.state.mapRadius,
			coordinate: this.state.mapPosition
	    })
	};

	updateRadius() {	
		// const updatedRadius = this.getRadius();
		// this.setState({mapRadius:updatedRadius})
		// console.log(updatedRadius,"updatedRadius");		
	}

	getSelectedRadius = async (data) => {
	  	await this.setState({mapRadius:data})
		this.props.callback({
			area: this.state.area,
			city: this.state.city,
			radius: this.state.mapRadius,
			coordinate: this.state.mapPosition
		})
	}

	render(){
		const AsyncMap = withScriptjs(
			withGoogleMap(
				props => (
					<Fragment>
					<GoogleMap 
						google = { this.props.google }
						defaultZoom = { 
							this.state.mapRadius <= "15000" ? this.props.zoom : 8 
						}
						defaultCenter={{ lat: this.state.mapPosition.lat, lng: this.state.mapPosition.lng }}
						defaultOptions={{ disableDefaultUI: true }}
					>  
						<Marker 
							google={this.props.google}
							name={'Dolores park'}
							draggable={true}
							onDragEnd={ this.onMarkerDragEnd }
							position={{ lat: this.state.markerPosition.lat, lng: this.state.markerPosition.lng }}
						/>
						<Autocomplete
							style={{
								width: '100%',
								height: '40px',
								paddingLeft: '16px',
								marginTop: '5px'
								// marginTop: '2px',
								// marginBottom: '500px'
							}}
							className={'form-control'}
							onPlaceSelected={ this.onPlaceSelected }
							types={['(regions)']}
							defaultValue={this.state.area ? this.state.area : this.state.city}
						/>
						<Circle
							defaultCenter={{
								lat: parseFloat(this.state.markerPosition.lat),
								lng: parseFloat(this.state.markerPosition.lng )
							}}
							radius={this.state.mapRadius}
							options={{
								strokeColor:'#ECE7E7',
								fillColor:'#2D6BA4',
								fillOpacity:0.1
							}}
						/> 
					</GoogleMap>
					</Fragment>	
				)
			)
		);
		let map;
		if( this.props.center.lat !== undefined ) {
			map = <div>
				<AsyncMap
					googleMapURL={`https://maps.googleapis.com/maps/api/js?key=${GoogleMapsAPI}&libraries=places`}
					loadingElement={
						<div style={{ height: `100%` }} />
					}
					containerElement={
						<div style={{ height: this.props.height }} />
					}
					mapElement={
						<div style={{ height: `100%` }} />
					}
				/>
				<br/>
				<div style={{marginTop:30}}>
                     <Range callBack={this.getSelectedRadius} firstValue={this.state.mapRadius}></Range>
                              
				</div>
				<br/>
			</div>
		} else {
			map = <div style={{height: this.props.height}} />
		}
		return( map )
	}
}

export default Map;