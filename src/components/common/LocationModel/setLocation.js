import React, { Component, Fragment } from 'react';
import Popup from "reactjs-popup";
import Map from './map';
const LocalStorageData = require("../../../services/LocalStorageData");
let ID = null
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class setLocation extends Component {
    constructor( props ){
		super( props );
		this.state = {
			markerPosition: {
				lat: this.props.userLocation.Latitude,
                lng: this.props.userLocation.Longitude
			},
            isMarkerShown:true,
            locationObject: {},
            radius: "",
            place: ""
		}
    }
    
    static getDerivedStateFromProps(props, state) {
        if (props.userLocation.Latitude !== state.markerPosition.lat) {
            console.log("here");
          return {
            markerPosition: {
				lat: props.userLocation.Latitude,
                lng: props.userLocation.Longitude
			},
          };
      }
      return null;
    }
	
	componentDidMount =()=> {
        // if(ID) {
        //     this.showCurrentLocation()
        // }
	}

	showCurrentLocation = () => {
		if (navigator.geolocation) {
		  navigator.geolocation.getCurrentPosition(
			 position =>{			
	         this.setState({
				markerPosition: {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				},
				isMarkerShown: true
			})
			}
		  )
		} else {
		 return error => console.log(error)
		}
	}

    closeModal = () => {
        this.props.closePopup();
    }

    getMapData = (data) => {
        this.setState({
            locationObject: {
                Lattitude: data.coordinate.lat,
                Longitude: data.coordinate.lng
            },
            radius: data.radius,
            place: data.area ? data.area : data.city
        })
    }

    setLocationClick = async () => {
        await this.props.getPositionData({
            locationObject: this.state.locationObject,
            radius: this.state.radius,
            place: this.state.place
        })
        const locationData = {
            mapPosition: {
              lat: this.state.locationObject.Lattitude,
              lng: this.state.locationObject.Longitude,
            },
            // radius: this.state.radius,
            area: this.state.place
          }
        await localStorage.setItem("locationData", JSON.stringify(locationData));
        this.props.closePopup();
    }

    render() {
        const { doShow } = this.props;
        return (
            <Fragment>
            <Popup open={doShow} onClose={this.closeModal}>
                <div className="modal location-modal" id="locationModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog location-modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">Change Location</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.closeModal}>
                                    <span aria-hidden="false" className="close-icon">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div className="col-xs-12 col-lg-12 col-md-12 col-xl-12 locationContainer">
                                    <div className="mapouter">  
                                        <div className="gmap_canvas">
                                        {this.state.isMarkerShown === true ? 
                                            <Map
                                                google={this.props.google}
                                                center={{lat:this.state.markerPosition.lat, lng: this.state.markerPosition.lng}}
                                                height='300px'
                                                zoom={11}
                                                callback={this.getMapData}
                                            />			
                                        :
                                            <Map
                                                google={this.props.google}
                                                height='300px'
                                                zoom={11}
                                                callback={this.getMapData}
                                            />
                                        }
                                        </div>
                                    </div>
                                </div>
                                <button disabled={!this.state.place ? true : false} className="set_location_btn set-location-btn-margin" onClick={this.setLocationClick}>Set Location</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Popup>
            </Fragment>
        )
    }
}

export default setLocation;