import React, { Component } from 'react';

class Range extends Component {
    constructor(props) {
        super(props); 
        this.state = {
            minValue: 5,
            maxValue: 50,
            step: 5,
            firstValue: null,
            secondValue: null
        }; 
        this.handleChange = this.handleChange.bind(this);
    }
    
    componentWillMount(){
        this.setState({firstValue: this.state.minValue, secondValue: this.state.maxValue});
    }
    
    handleChange(name, event){
        let value = event.target.value;
        if(name === "second"){
                if(parseInt(this.state.firstValue) < parseInt(value)){
                this.setState({secondValue:value});
            }
        }
        else {
                if(parseInt(value) <= parseInt(this.state.secondValue)){
                this.setState({firstValue: value}); 
                let radius = parseInt(value) * 1000
                this.props.callBack(radius)            
            }
        }
    }
    
    render(){
        return (
            <div>
                {/* <h5>Change Radius</h5> */}
                <div className="rangeValues"> {this.state.firstValue} km</div> 
                    <input type="range" value={this.state.firstValue} min={this.state.minValue} max={this.state.maxValue} step={this.state.step}  onChange={this.handleChange.bind(this, "first")} 
                    style={{'width': '-webkit-fill-available'}}
                    />
            </div>
        );
    }
    }
    
export default Range;