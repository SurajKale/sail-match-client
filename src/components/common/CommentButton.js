import React, { Component } from "react";
import { deletePost } from "../../data/mutation";
import { getListofComments } from '../../data/query';

import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";

class CommentButton extends Component {
    constructor(props) {
    super(props);
    this.state = {
        isLoaded:true
        
      }
    }

    getPostComment = () => {
        if(this.state.isLoaded){
        this.props.client
        .query({
            query: getListofComments,
            variables: {
                ReferenceID:this.props.PostID,
                ReferenceType: "CommentPosts"
            },
            fetchPolicy: "network-only"
         }) .then(res => {  
              const data = res.data.getListofComments.AllComments;
              if(data.length > 0){
                this.setState({isLoaded:false})
                this.props.callbackComment(data,true);
              }else{
                this.setState({isLoaded:true})
                this.props.callbackComment(data,false);        
              }
         });   
        }       
      };

render() {
  return (
        <div className="comment" onClick={(evt)=>this.getPostComment(evt)}>
            <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" />
            <span> 39</span>
        </div>
      );
    }
  }

export default compose(connect(null, {}), withApollo)(CommentButton);


  