import React, { Component, Fragment } from "react";
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader";
import { _formatDate, _formatDateUserdetails} from "../../services/CommonServices";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getViewAllPictures } from '../../data/actions/CommonActions'
import { createCommentActionForPhotos } from "../../data/actions/PostAction";
import { ToastContainer, toast } from "react-toastify";
import { objectLikeDisLike } from '../../data/mutation';
import { getListofComments } from '../../data/query';
import { assertNonNullType } from "graphql";
const LocalStorageData = require("../../services/LocalStorageData");

let ID = null;
let ProfileImage = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PhotosContent extends Component {
    constructor(props) {
    super(props);
    this.state = {
         count : 0,
         pictures: this.props.Pictures,
         postPictures: this.props.postPictures,
         loader:  false,
         slideIndex: 1,
         details: this.props.Details,
         userDetails: this.props.UserDetails,
         viewAllFlag: true,
         sliceCount: 0,
         picID: 0,
         picOwnerID: 0,
         likeCount: 0,
         commentCount: 0,
         ProfileImage: ProfileImage,
         comment: "",
         commentList: []
      }
    }

    static getDerivedStateFromProps(props, state) {
      if (props !== state) {
        return {
          pictures: props.Pictures,
          userDetails:props.UserDetails,
          details:props.Details,
          postPictures: props.postPictures
        }     
    }
    return null;
    }

    openModal() {
      document.getElementById("photoModal").style.display = "block";
    }
    
    closeModal = () => {
      this.setState({ commentList: [] })
      document.getElementById("photoModal").style.display = "none";
    }
      
    plusSlides = (n, like, comment, uid,  pid, pic) => {
      console.log(pic,"-->");
      this.showSlides(this.state.slideIndex += n);
      this.setState({
        likeCount: like,
        commentCount: comment,
        picOwnerID: uid,
        picID: pid
      })
      
    }
    
    currentSlide(n) {
      this.showSlides(this.state.slideIndex = n);
    }
    
    showSlides =(n)=> {
      var i;
      var slides = document.getElementsByClassName("mySlides")
      if (n > slides.length) {this.state.slideIndex = 1}
      if (n < 1) {this.state.slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      slides[this.state.slideIndex-1].style.display = "block";
    }

    openpoup = async (index, like, comment, uid, pid) =>{
      this.openModal()
      this.currentSlide(index)
      await this.setState({
        likeCount: like,
        commentCount: comment,
        picOwnerID: uid,
        picID: pid
      })
      this.getPostComment()
    }

    viewAllPictures= () =>{
      this.setState({viewAllFlag:false})
      this.props.callBack()
    }

    viewLessPictures =() =>{
      this.setState({viewAllFlag:true})
      this.props.viewLess()
    }

    handleComment = (e) => {
      this.setState({ comment : e.target.value })
    }

    objectLikeDisLike = () => {
      this.props.client
      .mutate({
          mutation: objectLikeDisLike,
          variables: {
            ObjectType: "Pictures",
            UserID: parseInt(ID),
            ReferenceID: parseInt(this.state.picID),
            ReferenceUserID: parseInt(this.state.picOwnerID)
        },
       })
        .then(res => { 
          if(res.data.objectLikeDisLike){

            this.props.updatePhotos();
            this.setState({ likeCount: res.data.objectLikeDisLike.TotalCount})
          }
        });  
    }

    pictureComment = () => {
      const { comment, picID } = this.state;
      const uid = parseInt(ID);
      this.props
        .createCommentActionForPhotos(
          this.props.client,
          picID,
          comment,
          uid
        )
        .then((res) => {
            this.props.updatePhotos();
            this.setState({ commentCount: this.state.commentCount + 1, comment: "" })
            this.getPostComment();
        }).catch(function (error) {
              console.log(error);
      });
    }

    getPostComment = (PostID) => {
      this.props.client
      .query({
          query: getListofComments,
          variables: {
            ReferenceID: parseInt(this.state.picID),
            ReferenceType:"Pictures"
          },
          fetchPolicy: "network-only"
       }) .then(res => {  
         if(res.data.getListofComments) {
           this.setState({
             commentList: res.data.getListofComments.AllComments
           })
         }
       });         
    };
    
    

  render() {
    const {Pictures,Details,objectImage,fromBoat, boatPictures} = this.props;
    const {loader,pictures,userDetails,viewAllFlag,details,postPictures,likeCount,commentCount,ProfileImage,comment,commentList} = this.state;
    let sliceCount = details && details.TotalPictureCount < 4 && 4 - details.TotalPictureCount;
  return (
    <Fragment>
    {
      fromBoat ?
      <Fragment>
        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">Photos 
              <span>({boatPictures && boatPictures.length > 0 ? boatPictures.length : 0 })</span>
              
            </h5>
          </div>
        </div>
        <div className="row" > 
        {boatPictures && boatPictures.map((pic,index)=>(
              <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">                  
                <div className="match-photo" style={{ padding: 'unset'}}>                                          
                  <img src={pic} className="videoSection img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/> 
                                                                    
                </div>    
              </div>      
            ))}  
            </div>
      </Fragment>
      :
      <Fragment>
      { (pictures || postPictures)  &&
      <Fragment> 
        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">Photos 
              <span>({details && (details.TotalPictureCount || details.TotalPostPictureCount) ? details.TotalPictureCount + details.TotalPostPictureCount : 0 })</span>
              {viewAllFlag && details && details.TotalPictureCount + details.TotalPostPictureCount > 4 && <a className="view-all cursor-pointer" onClick={this.viewAllPictures}>
              View All</a>}
              {!viewAllFlag && <a className="view-all cursor-pointer" onClick={this.viewLessPictures}>View Less</a>}
            </h5>
          </div>
        </div>

        <div className="col-md-12"> 

          <div id="photoModal" className="modalContainer">
            <span className="close cursor" onClick={this.closeModal}>&times;</span>
              <div className="modal-body">
  	            <div className="row">	  	
                  <Fragment>
                    <div className="modelphotoContainer">	
                    {pictures && pictures.length > 0 &&  pictures.map((pic,index)=>(
	    	            <Fragment>
                      <div className="mySlides" >
                        <img src={pic.Url} className="enlargePhoto"/> 
                        <div className="modelPhotoSocialIcons">
                          <div className="like"><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" onClick={this.objectLikeDisLike}/><span> {pic.LikeCount}</span></div>
                          <div className="bar">|</div>
                          <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {pic.CommentCount}</span></div>
                          {/* <div className="bar">|</div> */}
                          {/* <div className="forward"><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> 6</span></div> */}
                        </div>
                      </div>	  
                      <a className="prev" onClick={()=>{this.plusSlides(-1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID, pic)}}><img src="../../assets/images/left-arrow-angle-big-gross-symbol.png" width="50" alt="Back"/> </a>
                      <a className="next" onClick={()=>{this.plusSlides(1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID)}}><img src="../../assets/images/arrow-angle-pointing-to-right.png" width="50" alt="Back"/> </a>
                    </Fragment>
                    ))} 

                    { postPictures && postPictures.length > 0 && postPictures.map((pic,index) => (
                    <Fragment>
                      <div className="mySlides" >
                        <img src={pic.Pictures.Url} className="enlargePhoto"/> 
                        <div className="modelPhotoSocialIcons">
                          <div className="like"><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" onClick={this.objectLikeDisLike} /><span> {pic.Pictures.LikeCount}</span></div>
                          <div className="bar">|</div>
                          <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {pic.Pictures.CommentCount}</span></div>
                          {/* <div className="bar">|</div> */}
                          {/* <div className="forward"><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> 6</span></div> */}
                        </div>
                      </div>	  
                      <a className="prev" onClick={()=>{this.plusSlides(-1,pic.Pictures.LikeCount,pic.Pictures.CommentCount, pic.UserID,pic.Pictures.ID)}}><img src="../../assets/images/left-arrow-angle-big-gross-symbol.png" width="50" alt="Back"/> </a>
                      <a className="next" onClick={()=>{this.plusSlides(1,pic.Pictures.LikeCount,pic.Pictures.CommentCount, pic.UserID,pic.Pictures.ID)}}><img src="../../assets/images/arrow-angle-pointing-to-right.png" width="50" alt="Back"/> </a>
                    </Fragment>
                    ))} 
                    </div> 

                    <div className="modelInfoContainer">
                      { Details &&	<div className="basicInfo">	
                        <img src="../../assets/images/match-icon.png" alt="Back"/>
                        <p className="title">{Details.Title}</p>
                        <div className="col-12 " style={{paddingLeft: 6}}>  
                        <div className="match-photo" style={{border:'none'}}> 
                          <div className="author_name comment-by">
                            { userDetails && userDetails.ProfileImage ? <img src={userDetails.ProfileImage} style={{marginLeft:0}}/> : <img src="../../assets/images/small-photo-1.png" alt="Back" style={{marginLeft:0}}/>  }
                            <div className="photo-by-name" style={{left: '36px'}}>{userDetails && userDetails.FullName}</div>  
                            <div className="photo-time" style={{width:"auto",left:'36px'}}>{_formatDateUserdetails(Details.CreatedDate)}</div> 
                          </div> 
                        </div>
                      </div>
                      <div className="modelSocialIcons">
                        <div className="like"><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" onClick={this.objectLikeDisLike} /><span> {likeCount}</span></div>
                        <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {commentCount}</span></div>
                        {/* <div className="forward"><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> 6</span></div> */}
                      </div>



                      <div class="commentContainer">
					<img src={ ProfileImage ? ProfileImage : "../../assets/images/1.png"} class="commentUserImg"/>
                	<div class="inputSection" >
    					
              { comment && <img src="../../assets/images/send_2.png" alt="send" onClick={this.pictureComment} />}
    					<input type="text" value={comment} class="commentInput slider-comment-input"  placeholder="Add a comment..." onChange={this.handleComment} />
					</div>

					
                    <div class="commentBox"> 
                        {/* <div class="row">
        					<div class="commentSection">	
    							<div class="commentBySection"><img src="../../assets/images/1.png" class="commentUserImg"/></div>
    							<div class="commentsByUser">
    								<p class="commentsByUserName">Jennifer W. <span>3h</span></p>
    								<p class="userComments">lorem ipsum akismat lo ispita ye houe frtjn lorem ipsum akismat lo ispita ye houe frtjn</p>
    							</div>
                               
                                <div class="commentSocial">    
                                    <div class="commentsmall"><img src="../../assets/images/heart-icon.png" alt="heart-icon" class=""/><span> 10</span></div>
                                    <div class="like"><img src="../../assets/images/comment-icon.png" alt="comment-icon" class="pl-6"/><span> 65</span></div>
                                </div>
    						</div>
        					
    						<div class="commentReply">
    							<div class="commentBySection"><img src="../../assets/images/5.png" class="commentUserImg"/></div>
    						</div>	
    						<div class="commentReplyByUser">
    								<p class="commentsByUserName">Katty L. <span>59m</span></p>
    								<p class="userCommentsReply">lorem ipsum akismat lo ispita ye </p>
    						</div>
        				</div> */}

              { commentList && commentList.length > 0 && commentList.map((cmt) => (
                  <div class="row" key={cmt.ID}>
                  <div class="commentSection">	
                    <div class="commentBySection"><img src={cmt.UserDetails.ProfileImage ? cmt.UserDetails.ProfileImage : "../../assets/images/5.png"} class="commentUserImg"/></div>
                    <div class="commentsByUser">
                      <p class="commentsByUserName">{cmt.UserDetails.FullName || ""} <span>{cmt.CreatedDate}</span></p>
                      <p class="userComments">{cmt.Comment}  </p>
                    </div>
                  </div>	
                </div>
              )) }
    					
              
                        
                    </div>

                </div>







                </div>}

                    </div>
                  </Fragment>
	              </div>    
            </div>
          </div>

          <div className="row" > 
            {pictures && pictures.map((pic,index)=>(
              <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">                  
                <div className="match-photo">  
                  <div className="photoHover">
                    <div className="hoverEffect">Likes <span>({pic.LikeCount})</span></div> 
                    <div className="hoverEffect">Comments<span>({pic.CommentCount})</span></div>
                  </div>                                          
                  <img src={pic.Url} onClick={(e)=>this.openpoup(index+1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID)} className="videoSection img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/> 
                  <div className="author_name comment-by">
                    { userDetails && userDetails.ProfileImage ? <img src={userDetails.ProfileImage} /> : <img src="../../assets/images/small-photo-1.png" alt="Back"/>  }
                      <div className="photo-by-name">{userDetails && userDetails.FullName}</div>
                      <div className="photo-time" style={{'padding-left': '16px' , width: 'auto'}}>{_formatDateUserdetails(Details.CreatedDate)}</div>   
                  </div>                                                   
                </div>    
              </div>      
            ))}   

            { details && details.TotalPictureCount < 4 &&
            postPictures && postPictures.slice(0, sliceCount).map((pic,index)=> (
              <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">                  
                <div className="match-photo">  
                  <div className="photoHover">
                    <div className="hoverEffect">Likes <span>({pic.Pictures.LikeCount})</span></div> 
                    <div className="hoverEffect">Comments<span>({pic.Pictures.CommentCount})</span></div>

                  </div> 
                  <img src={pic.Pictures.Url} onClick={(e)=>this.openpoup(details.TotalPictureCount+index+1,pic.Pictures.LikeCount,pic.Pictures.CommentCount,pic.UserID,pic.Pictures.ID)} className="videoSection img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/> 
                  <div className="author_name comment-by">
                    { pic && pic.OwnerDetails ? <img src={pic.OwnerDetails.ProfileImage} /> : <img src="../../assets/images/small-photo-1.png" alt="Back"/>  }
                    <div className="photo-by-name">{pic.OwnerDetails && pic.OwnerDetails.FullName}</div>
                    <div className="photo-time" style={{'padding-left': '16px' , width: 'auto'}}>{_formatDateUserdetails(Details.CreatedDate)}</div>   
                  </div>                                                                                      
                </div>    
              </div>
            ))
            }

            { !viewAllFlag  &&
            postPictures && postPictures.map((pic,index) => (
              <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4" key={index}>                  
                <div className="match-photo">  
                  <div className="photoHover">
                    <div className="hoverEffect">Likes <span>({pic.Pictures.LikeCount})</span></div> 
                    <div className="hoverEffect">Comments<span>({pic.Pictures.CommentCount})</span></div>
                  </div> 
                  <img src={pic.Pictures.Url} onClick={(e)=>this.openpoup(details.TotalPictureCount+index+1,pic.Pictures.LikeCount,pic.Pictures.CommentCount,pic.UserID,pic.Pictures.ID)} className="videoSection img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/> 
                  <div className="author_name comment-by">
                    { pic && pic.OwnerDetails ? <img src={pic.OwnerDetails.ProfileImage} /> : <img src="../../assets/images/small-photo-1.png" alt="Back"/>  }
                    <div className="photo-by-name">{pic.OwnerDetails && pic.OwnerDetails.FullName}</div>
                    <div className="photo-time" style={{'padding-left': '16px' , width: 'auto'}}>{_formatDateUserdetails(Details.CreatedDate)}</div>   
                  </div>                                                                                      
                </div>    
              </div>
            ))
            }
          </div>             
        </div>
      </Fragment>}
    </Fragment>
      
    }
    </Fragment>
    
    );}
  }

  export default compose(
    connect(null, { getViewAllPictures, createCommentActionForPhotos }),
    withApollo
  )(PhotosContent);