import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { createCommentAction } from "../../data/actions/PostAction";


const LocalStorageData = require("../../services/LocalStorageData");
let ProfileImage = null;

if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}


class WriteReply extends Component {
    constructor(props) {
    super(props);
    this.state = {
        CommentText:"",
        CommentPhoto:"",
      }
    }

    toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

    addFile = (evt) =>{
        let file = evt.target.files[0];
        this.setState({fileName:file.name+' selected'})
        this.toBase64(file).then((result) => {
            console.log(result);
         this.setState({ CommentPhoto: result });
        });
    }
    
    _handleChange = async (e) => {
        await this.setState({
        [e.target.name]: e.target.value,
        });
    };


    _handleEnter = (event,post) =>{
        if(event.key=='Enter'){
            if(this.state.CommentText){
            this._postComment(event,post); 
            }
          }      
    }

    _postComment = (evt,post) =>{
        if(this.state.CommentText || this.state.CommentPhoto !== ''){
        console.log(post);
        let that = this;
        const ParentCommentID = post.ID;
        const Reply = '';

        const {CommentPhoto,CommentText}  = this.state;
        const PostID = post.ReferenceID;
        const UserID = post.UserID;
        const ID = undefined;
        const type = "CommentPosts" 

        this.props
          .createCommentAction(
            this.props.client,
            ParentCommentID,
            CommentText,
            ID,
            UserID,
            PostID,
            Reply,
            CommentPhoto,
            type
          )
          .then((res) => {
              console.log(res)
              const ParentCommentID = res.data.addComment.ParentCommentID;
              that.setState({fileName:'', CommentText:'',CommentPhoto:''});
              that.props.getCommentReply(ParentCommentID);
          }).catch(function (error) {
                console.log(error);
          });
        }
      }

render() {
    const {post } = this.props;
    console.log(post);
    const {CommentText,CommentPhoto,fileName} = this.state;
    const enabled = CommentText || CommentPhoto !== '';


  return (
              <div className="replyPostInputSection">
                <img src={ProfileImage ? ProfileImage : '../../assets/images/profile_pic.png'} className="commentUserImg" />
                <div className="inputSection" id="postInputSection">
                
            {/* {enabled &&
            <img src={"../../assets/images/camera.png"} className="post-comment" onClick={(evt)=>this._postComment(evt,post)}/>} */}

            <img src={"../../assets/images/camera.png"} className="post-comment" onClick={(evt)=>this._postComment(evt,post)}/>

                    <label htmlFor={'file-input'+post.ID} className="post-cmnt-icon">
                        
                         <img src={"../../assets/images/upload.png"} />
                    </label>

                         <input id={'file-input'+post.ID} type="file"
                                accept="images/*"
                                name="CommentPhoto"
                                style={{display:'none'}}
                                onChange={(evt)=>this.addFile(evt)} />

                        <input type="text" 
                            className="commentInput postCommentInput" 
                            name="CommentText" 
                            value={CommentText} 
                            onChange={this._handleChange}
                            placeholder="Add a comment..." 
                            onKeyPress={ (evt) => this._handleEnter(evt,post) } />

                    {fileName && <small className="text-success comment-file-name">{fileName}</small>}
                </div>
            </div>  
      );
    }
  }

export default compose(connect(null, {createCommentAction}), withApollo)(WriteReply);