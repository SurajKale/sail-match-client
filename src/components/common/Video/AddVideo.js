import React, { Component, Fragment } from "react";
import youtubeUrl from "youtube-url";

export default class AddVideo extends Component {
  state = {
    responsibility: [""],
    validUrl: false,
    disableCross: [false],
  };

  errorUrl = "";
  enabled = false;
  
  handleText = (i) => (e) => {
    const { value } = e.target;
    const checkUrl = youtubeUrl.valid(value);

    if (checkUrl === true) {
      this.errorUrl = "";
      this.setState({ validUrl: false });
    } else {
      this.errorUrl = "Please enter valid youtube url";
      this.setState({ validUrl: true });
    }

    let responsibility = [...this.state.responsibility];
    this.disableDelete(responsibility);
    responsibility[i] = e.target.value;

    this.setState({
      responsibility: responsibility,
    });

    this.props.getvideoUrls(responsibility);
  };

  checkUrlValidation = (i) => (e) => {
    const { value } = e.target;
    const checkUrl = youtubeUrl.valid(value);

    if (checkUrl === true) {
      this.errorUrl = "";
      this.setState({ validUrl: false });
    } else {
      this.errorUrl = "Please enter valid youtube url";
      this.setState({ validUrl: true });
    }
  };

  disableDelete = (responsibility) => {
    if (responsibility.length <= 1) {
      this.setState({ disableCross: true });
    } else {
      this.setState({ disableCross: false });
    }
  };

  handleDelete = (i) => (e) => {
    e.preventDefault();
    let responsibility = [
      ...this.state.responsibility.slice(0, i),
      ...this.state.responsibility.slice(i + 1),
    ];

    this.disableDelete(responsibility);
    this.setState({
      responsibility: responsibility,
    });

    var validityChecker = responsibility.filter(function (item) {
      return youtubeUrl.valid(item);
    });

    if (validityChecker.length >= 1) {
      this.errorUrl = "";
      this.setState({ validUrl: false });
    }
    this.props.getvideoUrls(responsibility);
  };

  addQuestion = (e) => {
    e.preventDefault();
    let responsibility = this.state.responsibility.concat([""]);
    this.setState({ responsibility: responsibility });
    this.disableDelete(responsibility);
    this.props.getvideoUrls(responsibility);
  };

  render() {
    this.enabled = this.state.validUrl;
    return (
      <Fragment>
        <div className="input-group">
          <label>Add Videos&nbsp;
            <span title="we only accepting youtube video url">
              <i className="fa fa-info-circle" aria-hidden="true"></i>
            </span>
          </label>
        </div>
        <div className="row">
          {this.state.responsibility.map((question, index) => (
            <Fragment key={index}>
              <div className="col-md-5">
                <input 
                  type="url"
                  className="form-control"
                  name="location_area"
                  style={{ margin: "3px" }}
                  value={question}
                  onBlur={this.checkUrlValidation(index)}
                  onChange={this.handleText(index)}
                />
              </div>
              <div className="col-md-1">
                <button
                  type="button"
                  disabled={this.state.disableCross}
                  className="btn btn-primary btn-sm mt-2"
                  onClick={this.handleDelete(index)}
                >
                  X
                </button>
              </div>
            </Fragment>
          ))}
        </div>
        <div className="video-error">{this.errorUrl}</div>
        <button
          type="button"
          disabled={this.state.validUrl}
          className="btn btn-primary btn-sm m-2"
          onClick={this.addQuestion}
        >
          Add
        </button>
      </Fragment>
    );
  }
}