import React, { Component } from "react";
import Popup from "reactjs-popup";
import ReactPlayer from 'react-player';

class VideoPlayer extends Component {
  render() {
    const { doShow, closePopup, videoLink } = this.props;
    return (
      <Popup open={doShow} onClose={closePopup} position="right center">
        <div id="notificationModal">
          <div className="videoModel">
            <div className="modal-content">
              <div className="closeModel" onClick={closePopup}>
                <img
                  src="../assets/images/close-icone.png"
                  width="50"
                  height="50"
                  className="postImg img-fluid close1"
                  data-dismiss="modal"
                  aria-label="Close"
                  alt="Matches"
                />
              </div>
              <div className="modal-body1">
                <div className="col-md-12">
                  {/* <h1 className="postTitle">What do you want to post?</h1> */}
                  <div className='player-popup'>
                    <ReactPlayer
                      url={videoLink}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Popup>
    );
  }
}

export default VideoPlayer;
