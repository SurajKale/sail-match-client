import React, { Component, Fragment } from "react";
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader";
import { _formatDate, _formatDateUserdetails} from "../../services/CommonServices";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getViewAllPictures } from '../../data/actions/CommonActions'
import { createCommentActionForPhotos } from "../../data/actions/PostAction";
import { ToastContainer, toast } from "react-toastify";
import { objectLikeDisLike } from '../../data/mutation';
import { getListofComments } from '../../data/query';
import { assertNonNullType } from "graphql";
const LocalStorageData = require("../../services/LocalStorageData");

let ID = null;
let ProfileImage = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ProfileImage) {
  ProfileImage = LocalStorageData.ProfileImage;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class PhotosContent extends Component {
    constructor(props) {
    super(props);
    this.state = {
         count : 0,
         pictures: this.props.Pictures,
         postPictures: this.props.postPictures,
         loader:  false,
         slideIndex: 1,
         details: this.props.Details,
         userDetails: this.props.UserDetails,
         viewAllFlag: true,
         sliceCount: 0,
         picID: 0,
         picOwnerID: 0,
         likeCount: 0,
         commentCount: 0,
         ProfileImage: ProfileImage,
         comment: "",
         commentList: []
      }
    }

    static getDerivedStateFromProps(props, state) {
      if (props !== state) {
        return {
          pictures: props.Pictures,
          userDetails:props.UserDetails,
          details:props.Details,
          postPictures: props.postPictures
        }     
    }
    return null;
    }

    openModal() {
      document.getElementById("photoModal").style.display = "block";
    }
    
    closeModal = () => {
      this.setState({ commentList: [] })
      document.getElementById("photoModal").style.display = "none";
    }
      
    plusSlides = (n, like, comment, uid,  pid, pic) => {
      console.log(pic,"-->");
      this.showSlides(this.state.slideIndex += n);
      
    }
    
    currentSlide(n) {
      this.showSlides(this.state.slideIndex = n);
    }
    
    showSlides =(n)=> {
      var i;
      var slides = document.getElementsByClassName("mySlides")
      if (n > slides.length) {this.state.slideIndex = 1}
      if (n < 1) {this.state.slideIndex = slides.length}
      for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
      }
      slides[this.state.slideIndex-1].style.display = "block";
    }

    openpoup = async (index, like, comment, uid, pid) =>{
      this.openModal()
      this.currentSlide(index)
    }
    
    

  render() {
    const { pictures } = this.state;
  return (
    <Fragment>
      { (pictures) &&
      <Fragment> 
        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">Photos 
              <span>({pictures.length})</span>
            </h5>
          </div>
        </div>

        <div className="col-md-12"> 

          <div id="photoModal" className="modalContainer">
            <span className="close cursor" onClick={this.closeModal}>&times;</span>
              <div className="modal-body">
  	            <div className="row">	  	
                  <Fragment>
                    <div className="modelphotoContainer">	
                    {pictures && pictures.length > 0 &&  pictures.map((pic,index)=>(
	    	            <Fragment>
                      <div className="mySlides" >
                        <img src={pic} className="enlargePhoto"/>
                      </div>	  
                      <a className="prev" onClick={()=>{this.plusSlides(-1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID, pic)}}><img src="../../assets/images/left-arrow-angle-big-gross-symbol.png" width="50" alt="Back"/> </a>
                      <a className="next" onClick={()=>{this.plusSlides(1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID)}}><img src="../../assets/images/arrow-angle-pointing-to-right.png" width="50" alt="Back"/> </a>
                    </Fragment>
                    ))} 
                    </div>
                  </Fragment>
	              </div>    
            </div>
          </div>

          <div className="row" > 
            {pictures && pictures.map((pic,index)=>(
              <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">                  
                <div className="match-photo">                                      
                  <img src={pic} onClick={(e)=>this.openpoup(index+1,pic.LikeCount,pic.CommentCount, pic.UserID, pic.ID)} className="videoSection img-fluid center-block mx-auto d-flex justify-content-center flex-wrap photosVideosImg" alt="Back"/> 
                </div>    
              </div>      
            ))}
          </div>             
        </div>
      </Fragment>}
    </Fragment>
    );}
  }

  export default compose(
    connect(null, { getViewAllPictures, createCommentActionForPhotos }),
    withApollo
  )(PhotosContent);