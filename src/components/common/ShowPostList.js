import React, { Component } from "react";
import PostLikeDislike from '../common/PostLikeDislike';
import CommentListContent from './CommentListContent';
import ConfirmDeletePost from "../common/ConfirmDeletePost";
import ConfirmReportPost from './Comment/reportPostComment';
import { deletePost } from "../../data/mutation";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
// import CommentButton from './CommentButton';
import WriteComment from './WriteComment';
import EditComment from './EditComment';

import { getListofComments, reportPost } from '../../data/query';


import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";

class ShowPostList extends Component {
    constructor(props) {
    super(props);
    this.state = {
        totalCount:'',
        commentData:[],
        showComment:false,

        doShowCommentPopupEdit:false,
        editCommentData:[],

        CommentCount:'',

      }
    }

    componentDidMount = () => {
        this.setState({totalCount:this.props.post.LikeCount,CommentCount:this.props.post.CommentCount})
    }

    objectLike = (count) =>{
        this.setState({totalCount:count})
      }

      deleteObject = (evt,UserID,PostID) => {
        this.props.client
          .mutate({
            mutation: deletePost,
            variables: {
                PostID: PostID,
                UserID: UserID,
            },
          })
          .then((res) => {
            if (res.data.deletePost) {
                    this.props.callPostListEDelete();
            }
          });
      };

      
    getPostComment = (PostID) => {
      this.props.client
      .query({
          query: getListofComments,
          variables: {
              ReferenceID: PostID,
              ReferenceType: "CommentPosts"
          },
          fetchPolicy: "network-only"
       }) .then(res => {  

            const data = res.data.getListofComments.AllComments;
            if(data.length > 0){
              this.setState({commentData:data,showComment:true,CommentCount:res.data.getListofComments.TotalCmmtCount})
            }else{
              this.setState({commentData:data,showComment:false })
            }
       });         
    };

    editComment = (data) => {
      document.body.style.overflowY = "hidden";
      this.setState({ doShowCommentPopupEdit: true,editCommentData:data });
    };

    closePopupCommentEdit = () => {
      document.body.style.overflowY = "auto";
      this.setState({ doShowCommentPopupEdit: false, editCommentData:[]});
    };

    callCommentListEdit =(PostID) => {
     this.getPostComment(PostID);
   }

  //  getCommentCount = (count) =>{
  //    this.setState({CommentCount:count})
  //  }

  flagPost = (evt, UserID, PostID, reason) => {
    this.props.client
      .mutate({
        mutation: reportPost,
        variables: {
            ReferenceType: "CommentPosts",
            ReferenceID: parseInt(PostID),
            ReferenceUserID: parseInt(UserID),
            ObjectType: this.props.ObjectType,
            ReporterDetails: {
              UserID: parseInt(this.props.UserID),
              Reason: reason
            },
        }
      })
      .then((res) => {
        if(res.data.reportPost) {
          toast.success("Post reported successfully");
        }
      });
  };


render() {
    const { totalCount,commentData,showComment,doShowCommentPopupEdit,editCommentData,CommentCount } = this.state;
    const {post,ObjectType,UserID} = this.props;

  return (
      <React.Fragment>
          {post &&
            <div className="dynamicPostContainer">
                  <div className="postUserImgContainer">
                    <img src={post.UserDetails && post.UserDetails.ProfileImage !== "" ? post.UserDetails.ProfileImage : '../../assets/images/profile_pic.png'} className="postUserImg" alt="Back" />

                    {UserID &&
                     <React.Fragment>
                    <div className="dot" onClick={(evt)=>window.dotClick(evt,post.ID)}></div>
                      <div className="dotModel" id={'openAction'+post.ID} style={{display:'none'}}>
                          <ul>
                            {UserID && post.UserID == UserID && <li onClick={(evt)=> this.props.editPost(evt,post)}>Edit</li>}
                            {UserID && post.UserID == UserID && 
                              <li>
                                  <ConfirmDeletePost
                                      UserID={UserID}
                                      PostID={post.ID}
                                      callBack={this.deleteObject}
                                      object={"post"}/>
                            </li>}
                            {UserID && post.UserID !== UserID && 
                            <li>
                              <ConfirmReportPost
                                UserID={post.UserID}
                                PostID={post.ID}
                                callBack={this.flagPost}
                                object={"post"}
                                ObjectType={ObjectType}
                              />
                          </li>}
                          </ul>
                        </div>
                        </React.Fragment>}

                    <p className="postUserName">{post.UserDetails ? post.UserDetails.FullName : ''}</p>
                    <span className="postTime">{post.CreatedDate ? post.CreatedDate : ''}</span>

                    {post.Comment &&
                        <p className="postContent">
                       {post.Comment ? post.Comment : ''}
                        </p>}

                      <div className="col-md-12">
                            <div className="row">
                            {post.Pictures && post.Pictures .length > 0 && post.Pictures.map((picture)=>{ 
                                if(post.Pictures.length == 1){
                                    return  <div className={post.Videos.length > 1  ? 'col-md-6 p-2' : 'col-md-12 p-2'} >
                                    <img src={picture ? picture : '../../assets/images/no_image.png'} className="sideImg" alt="Back" />
                                </div>}
                                if(post.Pictures.length > 1){
                                    return  <div className="col-md-6 p-2">
                                        <img src={picture ? picture : '../../assets/images/no_image.png'} className="sideImg" alt="Back" />
                                    </div>}
                                })}

                        {post.Videos && post.Videos .length > 0 && post.Videos.map((videos)=>{ 
                        if(post.Videos.length == 1){
                            return  <div className={post.Pictures.length > 1  ? 'col-md-6 p-2' : 'col-md-12 p-2'}>
                                        <video controls className="w-100">
                                            <source  src={videos} />
                                        </video>
                                </div>}
                                if(post.Videos.length > 1){
                                    return <div className="col-md-6 p-2">
                                            <video controls className="w-100">
                                            <source  src={videos} />
                                        </video>
                                    </div>}
                                })}
                            </div>
                        </div>
                  </div>
                  <div className="postSocialSection">
                    <div className="like">

                    {!UserID &&  <span className="col text-left">
                          <i className="far fa-heart" style={{fontSize: '18px'}}></i> 
                         &nbsp;{totalCount}
                    </span>}
                      {UserID && post.ID && <PostLikeDislike LikeCount={totalCount} ObjectType={'CommentPosts'} ReferenceID={post.ID} status={post.isLike} ReferenceUserID={post.UserID} updateOnParent={this.objectLike} isDetail={false}/> }
               
                        {/* <img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" />
                        <span> 140</span> */}
                    </div>


                    {CommentCount > 0 ?
                      <div className="comment" onClick={(evt)=>this.getPostComment(post.ID)}>
                        <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" />
                        <span> {CommentCount}</span>
                  </div> :  <div className="comment">
                        <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" />
                        <span> {CommentCount}</span>
                  </div>}

                  {/* <CommentButton PostID={post.ID} callbackComment={this.callbackComment}/> */}

                    {/* <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" /><span> 39</span></div> */}

                    {/* <div className="forward"><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6" /><span> 12</span></div> */}
                  </div> 
                 <div className="postCommentSection">
                   
                   {UserID && <WriteComment post={post} getPostComment={this.getPostComment}/>}

                  {showComment && <CommentListContent ObjectType={ObjectType} commentData={commentData} callCommentListEdit={this.callCommentListEdit} editComment={this.editComment} getCommentCount={this.getCommentCount} CommentCount={CommentCount} UserID={UserID}/>}
                </div>
            </div>}
        <ToastContainer autoClose={1000}/>

        {doShowCommentPopupEdit &&
           <EditComment
                doShowCommentEdit={doShowCommentPopupEdit}
                closePopupCommentEdit={this.closePopupCommentEdit}
                UserID={UserID} 
                editCommentData={editCommentData}
                callCommentListEdit={this.callCommentListEdit} 
                />}

            </React.Fragment>
      );
    }
  }

export default compose(connect(null, {}), withApollo)(ShowPostList);


  