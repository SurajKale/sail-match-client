import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import ShowReplyList from './ShowReplyList';

class ReplyComment extends Component {
    constructor(props) {
    super(props);
    this.state = {
      }
    }


render() {
    const {replyData,UserID} = this.props;
    
  return (
           <React.Fragment>
               {replyData && replyData.length > 0 && replyData.map((reply,index)=>(
                   <ShowReplyList reply={reply} key={index} callCommentListEdit={this.props.callCommentListEdit} editReply={this.props.editReply} CommentID={this.props.CommentID} UserID={UserID} />))}
           </React.Fragment>
      );
    }
  }
  
export default compose(connect(null, {}), withApollo)(ReplyComment);
  