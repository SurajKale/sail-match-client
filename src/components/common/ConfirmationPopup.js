import React, { Component } from "react";
import "react-alert-confirm/dist/index.css";
import confirm, { Button, alert } from "react-alert-confirm";
import { Fragment } from "react";
const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
let Name = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.ID) {
  let FirstName = LocalStorageData.FirstName;
  let LastName = LocalStorageData.LastName;
  Name = FirstName + " " + LastName;
}

class ConfirmationPopup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  handleClickConfirm = () => {
    const instance = confirm({
      title: `Hello ${Name} `,
      content: `Are you sure you want to delete this ${this.props.object} !`,
      lang: "en",
      onOk: () => {
        this.props.callBack();
      },
      onCancel: () => {
        // console.log("cancel", instance);
      },
    });
  };
  render() {
    return (
      <>
        <button className="deleteBtn" onClick={this.handleClickConfirm}>
          <img
            src="../../assets/images/trash.png"
            className="pr-1"
            alt="tick-icon"
          />{" "}
          Delete
        </button>{" "}
      </>
    );
  }
}

export default ConfirmationPopup;
