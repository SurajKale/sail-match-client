import React, { Component } from "react";
import { Link } from "react-router-dom";
// import { compose } from "recompose";
// import { connect } from "react-redux";

class RenderContent extends Component {
    constructor(props) {
    super(props)
    }

render() {
  return (
  <div class="container">
  <div class="row">
    <div class="col-md-12 mb-2 text-center">
      <h1>Matching Local Sailing Enthusiast </h1>
      <h6>with Day sailors and Weekend Cruising in the US.</h6>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12 mb-2 text-right"><a href="#">View Blog</a></div>
    <div class="col-md-4 mb-2 text-center"><a href="#"><img src="./assets/images/Home_img01.png" alt="home"/> </a></div>
    <div class="col-md-4 mb-2 text-center"><a href="#"><img src="./assets/images/Home_img02.png" alt="home"/></a> </div>
    <div class="col-md-4 mb-2 text-center"><a href="#"><img src="./assets/images/Home_img03.png" alt="home"/> </a></div>
  </div>
  <div class="row">
    <div class="col-sm-12 mt-3 mb-5 text-center categories">
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Matches.svg" alt="home"/>Matches </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Crew.svg" alt="home"/>Crew </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Rides.svg" alt="home"/>Rides </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Events.svg" alt="home"/>Events </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Courses.svg" alt="home"/>Courses </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Skills.svg" alt="home"/>Skills </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Clubs.svg" alt="home"/>Clubs </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Service.svg" alt="home"/>Service </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Rentals.svg" alt="home"/>Rentals </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Photos.svg" alt="home"/>Photos </a></div>
      <div class="col-sm-1"><a href="#"><img src="./assets/images/Videos.svg" alt="home"/>Videos </a></div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 mb-4">
      <div class="location_left"><span class="mobile_hide">Location:</span> <span class="boxed"><img src="./assets/images/ic_location.svg" alt="Dropdown"/> Astoria <a href="#">Edit</a> </span></div>
      <div class="location_right"> <span class="mobile_hide"> Allow your location to see posts near you.
        <button type="button" class="btn btn-primary button-rounded">Allow</button>
        </span> <span class="desktop_hide">
        <button type="button" class="btn btn-primary button-rounded">Allow location</button>
        </span></div>
    </div>
  </div>
  <div class="row">
  <div class="col-sm-3 mb-5">
    <div class="post_single"><img src="../assets/images/4.png" alt=""/>
  <h5><a href="#"> I want a baot to sail on</a></h5>
      <p>8:30 PM, Today</p>
      <div class="author"><span class="author_name"><img src="./assets/images/4.png" alt=""/> Frank Hail</span><span class="category"><a href="#"><img src="./assets/images/Crew.svg" alt="home"/></a></span></div>
      <div class="divider">
        <hr />
      </div>
      <div class="post_footer"><span class="col text-left"><i class="far fa-heart"></i> 320</span><span class="col text-center"><i class="far fa-comment-alt"></i> 52</span><span class="col text-right"><i class="fas fa-share"></i> 12</span></div>
    </div>
  </div>
    <div class="col-sm-3 mb-5">
      <div class="post_single"><img src="./assets/images/photo-1535024966840-e7424dc2635b.png" alt=""/>
        <h5><a href="#">I need crew for day sailing</a></h5>
        <p>4:00 PM, May 16th</p>
        <div class="author"><span class="author_name"><img src="./assets/images/istockphoto-544358212-612x612.png" alt=""/> Aaron Rbel</span><span class="category"><a href="#"><img src="./assets/images/Rides.svg" alt="home"/></a></span></div>
        <div class="divider">
          <hr />
        </div>
        <div class="post_footer"><span class="col text-left"><i class="far fa-heart selected"></i> 320</span><span class="col text-center"><i class="far fa-comment-alt"></i> 52</span><span class="col text-right"><i class="fas fa-share"></i> 12</span></div>
      </div>
    </div>
    <div class="col-sm-3 mb-5">
      <div class="post_single"><img src="./assets/images/photo-1515169067868-5387ec356754.png" alt=""/>
        <h5><a href="#">Networking on cruise</a></h5>
        <p>5:00 PM, May 16th</p>
        <div class="author"><span class="author_name"><img src="./assets/images/HaroldGabe.png" alt=""/> Harold Gabe</span><span class="category"><a href="#"><img src="./assets/images/Events.svg" alt="home"/></a></span></div>
        <div class="divider">
          <hr />
        </div>
        <div class="post_footer"><span class="col text-left"><i class="far fa-heart"></i> 320</span><span class="col text-center"><i class="far fa-comment-alt"></i> 52</span><span class="col text-right"><i class="fas fa-share"></i> 12</span></div>
      </div>
    </div>
    <div class="col-sm-3 mb-5">
      <div class="post_single"><img src="./assets/images/SailingMatch.png" alt=""/>
        <h5><a href="#">Sailing Match</a></h5>
        <p>4 Members</p>
        <div class="author"><span class="author_name members"><img src="./assets/images/photo-1548372290-8d01b6c8e78c.png" alt=""/><img src="./assets/images/2.png" alt=""/><img src="./assets/images/photo-1494790108377-be9c29b29330.png" alt=""/><img src="./assets/images/4.png" alt=""/></span><span class="category"><a href="#"><img src="./assets/images/Matches.svg" alt="home"/></a></span></div>
        <div class="divider">
          <hr />
        </div>
        <div class="post_footer"><span class="col text-left"><i class="far fa-heart"></i> 320</span><span class="col text-center"><i class="far fa-comment-alt"></i> 52</span><span class="col text-right"><i class="fas fa-share"></i> 12</span></div>
      </div>
    </div>
  </div>
</div>
      );
    }
  }
  

  
  export default RenderContent;