import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from "react-drop-zone";
import "react-drop-zone/dist/styles.css";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { getBoatCreateAction, getBoatDetailsByID } from "../../data/actions";
var formatcoords = require('formatcoords');

const LocalStorageData = require("../../services/LocalStorageData");
let UID = null;

if (LocalStorageData.ID) {
  UID = LocalStorageData.ID;
}

class EditBoatContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.url,
      boatID: null,
      boatType: "",
      boatName: "",
      boatMake: "",
      boatLength: "",
      sailArray: [
        { ID: 1, Type: "Day Sailing", isSelected: false },
        { ID: 2, Type: "Crusing", isSelected: false  },
        { ID: 3, Type: "Racing", isSelected: false  },
      ],
      boatProfilePic: "",
      locationObject: {},
      buttonCheck: false,
      city: "",
      state: "",
      zipCode: "",
      gpsCoordinate: "",
      showUpdated: null,
      images: [],
      imagesRender: [],
      boatYear: "",
      boatModal: ""
    };
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    await this.props.getBoatDetailsByID(this.props.client, this.state.url ).then( async res => { 
      if(res.data.getBoatProfileByUrl !== null ) {
        let boatData = res.data.getBoatProfileByUrl;
        await this.setState({
          boatType: boatData.BoatType,
          boatName: boatData.Title,
          boatLength: boatData.Length,
          boatMake: boatData.Make,
          locationObject: boatData.Location,
          boatDescription: boatData.Description,
          boatProfilePic: boatData.ProfilePicture,
          boatID:boatData.ID,
          createdBy:boatData.CreatedBy,
          boatYear: boatData.ManufactureYear,
          boatModal: boatData.BoatModel
        })
        await this.handleEventsSelect(boatData.SailingType[0])
      } else {
        toast.error("Something wents wrong")
      }
    }).catch(function (error) {
      //   console.log(error);
    })
  }

  handleEventsSelect = async (category) => {
    let oldEvents = this.state.sailArray.slice();
    oldEvents.forEach((item) => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      sailArray: oldEvents,
      buttonCheck: true,
    });
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({ boatProfilePic: result });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  createBoat = async (e) => {
    e.preventDefault();
    const { boatName, boatLength, boatMake, boatDescription, boatProfilePic, boatType,
      sailArray, boatID, boatYear, boatModal } = this.state;

    let boatObject = { boatName, boatLength, boatMake, boatDescription, boatProfilePic,
      boatType, sailArray, boatID, boatYear, boatModal };

    var options = sailArray.filter(function (item) {
      return item.isSelected;
    });

    const SailType = options.map(function (item) {
      return { ID: item.ID, Name: item.Type };
    });

    await this.props
    .getBoatCreateAction(this.props.client, boatObject, SailType, UID)
    .then((res) => {
      toast.success("Boat updated successfully", {
        onClose: () => this.props.history.goBack(),
      });
    })
    .catch(function (error) {
      // console.log(error);
    });
  };

  render() {
    const { boatName, boatLength, boatMake, boatDescription, boatProfilePic,
      locationObject, boatType, sailArray, buttonCheck, boatYear, boatModal } = this.state;

    const enabled = boatType && boatName && boatLength && boatMake && boatDescription &&
      locationObject && boatProfilePic && buttonCheck;

    const label = boatProfilePic ? "Image successfully added" : "Click or drop your image here";

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" href="/">
              <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="/" className="float-left mobile_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Edit Boat Profile</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="typeOfBoat">Type of Boat</label>
                    <div className="">
                      <input
                        type="button"
                        className={
                          boatType === "Mono"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="boatType"
                        value="Mono"
                        onClick={this.handleChange}
                      />
                      <input
                        type="button"
                        className={
                          boatType === "Cat"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="boatType"
                        value="Cat"
                        onClick={this.handleChange}
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="typeOfSail">Sailing Type</label>
                    <div className="">
                      {sailArray &&
                        sailArray.map((sail) => {
                          return (
                            <input
                              type="button"
                              key={sail.id}
                              className={`${
                                sail.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleEventsSelect(sail)}
                              value={sail.Type}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatName">Boat Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatName"
                      defaultValue={boatName}
                      placeholder="Boat Name"
                      name="boatName"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label for="boatLength">Length of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatLength"
                      defaultValue={boatLength}
                      placeholder="Length of Boat"
                      name="boatLength"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Make of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatMake"
                      defaultValue={boatMake}
                      placeholder="Make of Boat"
                      name="boatMake"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Model of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatModal"
                      defaultValue={boatModal}
                      placeholder="Model of Boat"
                      name="boatModal"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Year of Manufacture</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatYear"
                      defaultValue={boatYear}
                      placeholder="Year of Manufacture"
                      name="boatYear"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      defaultValue={boatDescription}
                      rows="3"
                      name="boatDescription"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  {boatProfilePic && <label for="profilePicture">Profile Picture</label> }
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                
                {boatProfilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={boatProfilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        disabled={!enabled}
                        type="button"
                        className="btn btn-primary save_profile"
                        onClick={this.createBoat}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { getBoatCreateAction, getBoatDetailsByID }),
  withApollo
)(EditBoatContent);
