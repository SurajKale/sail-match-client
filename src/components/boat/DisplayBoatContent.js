import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import  formatcoords from 'formatcoords';

import { getBoatDetailsByID } from "../../data/actions";
import {deleteBoatProfile} from '../../data/mutation'

import PhotosContent from '../common/PhotosContent';
import VideoContent from '../common/VideosContent';
import ConfirmationPopup from '../common/ConfirmationPopup';
import ObjectLikeDislike from '../common/ObjectLikeDislike';
import HelmetData from '../common/_helmet' ;

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class DisplayBoatContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: this.props.match.params.url,
      boatType: "",
      boatName: "",
      sailType: [],
      boatDescription: "",
      boatMake: "",
      boatLength: "",
      boatProfilePic: "",
      address: "",
      Latitude: "",
      Longitude: "",
      boatDetails:{},
      totalCount:"",
      CommentCount: 0,
      metaData:[],
      boatModal: "",
      boatYear: ""
    }
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    await this.props.getBoatDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getBoatProfileByUrl !== null ) {
          let boatData = res.data.getBoatProfileByUrl;
          var metaArr = [];
          metaArr.push({
            title : boatData.Title ?  boatData.Title : SITE_STATIC_NAME,
            keywords : boatData.ObjectType, SITE_STATIC_NAME,
            description : boatData.Description ? boatData.Description : '',
            og_url : window.location.href,
            og_title : boatData.Title ?  boatData.Title : SITE_STATIC_NAME,
            og_description : boatData.Description ? boatData.Description : '',
            canonical_url : window.location.href,
            og_image : boatData.ProfilePicture ? boatData.ProfilePicture : SITELOGO,
            amp_url : window.location.href,
          })
          this.setState({
            metaData:metaArr[0],
            boatDetails:boatData,
            isLike:boatData.isLike,
            renderLike:true,
            boatID:boatData.ID,
            boatName: boatData.Title,
            boatType: boatData.BoatType,
            boatName: boatData.Title,
            boatMake: boatData.Make,
            boatModal: boatData.BoatModel,
            boatYear: boatData.ManufactureYear,
            boatLength: boatData.Length,
            boatProfilePic: boatData.ProfilePicture,
            location: boatData.Location.MapLocation,
            Latitude:boatData.Location.Latitude,
            Longitude:boatData.Location.Longitude,
            address: boatData.Location.Address,
            boatDescription: boatData.Description,
            sailType: boatData.SailingType.map((item) => item.Name),
            createdBy:boatData.CreatedBy,
            totalCount:boatData.LikeCount,
            CommentCount: boatData.CommentCount
          })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
    //   console.log(error);
    })
  }

  objectLike= (count) => { this.setState({totalCount:count})}
  goBack = ()=> { this.props.history.goBack() }
  
  deleteObject = () => {
    this.props.client
    .mutate({
        mutation: deleteBoatProfile,
        variables: {
          BoatID:parseInt(this.state.boatID),
          UserID:ID
        },
      })
      .then(res => { 
        if(res.data.deleteBoatProfile){
          toast.success("Boat Profile deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
    });  
  }

  goToEditPage = () => {
    this.props.history.push("/edit-boat-details/"+this.state.url)
  }

  render() {
    const { boatType, boatName, boatMake, boatLength, boatProfilePic, address,totalCount,
      Latitude, Longitude, boatDescription, sailType,boatDetails, CommentCount, renderLike,
      boatModal, boatYear } = this.state;
    
    var point = formatcoords(parseFloat(Latitude), parseFloat(Longitude)).format();

    return (
      <div class="container">
        <HelmetData metaDetails={this.state.metaData} />
        <div class="row pt-4">
          <div class="col-md-12">
            <div class="col-md-3 mb-2 mt-2"> <a class="desktop_back" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
          </div>
        </div>

        <div class="col-md-12 col-12">
          <div class="row">
            <div class="col-md-3 col-sm-12 mb-2 mt-2"> <img src={boatProfilePic ? boatProfilePic : "../../assets/images/photo-1535024966840-e7424dc2635b.png"} className="sideImg" alt={boatName}/> </div>
            <div class="col-md-9 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {boatName}
                {ID && this.state.boatDetails.CreatedBy === ID &&
                <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                />
                }
              </h2>
              <div class="date-time-img"><img src="../../assets/images/Rides.svg" alt="Back"/></div>
              <p class="boatSection"> <img src="../../assets/images/location-icon.png" alt="location-icon" class="pr-2"/> <span>{address}</span> </p>
              <p class=""> <img src="../../assets/images/position-icon.png" alt="position-icon" class="pr-2"/> <span> {point}</span> </p>
              <table class="boatAttSection">
                <tr>
                  <th>Length</th>
                  <td>{boatLength}'</td>
                </tr>
                <tr>
                  <th>Boat Type </th>
                  <td>{boatType}</td>
                </tr>
                <tr>
                  <th>Make </th>
                  <td>{boatMake}</td>
                </tr>
                <tr>
                  <th>Boat Modal </th>
                  <td>{boatModal}</td>
                </tr>
                <tr>
                  <th>Year of Manufacture </th>
                  <td>{boatYear}</td>
                </tr>
                <tr>
                  <th>Type of Sail </th>
                  <td>{sailType.join(", ")}</td>
                </tr>
              </table>
              <div class="date-time-text">  
                  {ID && this.state.createdBy == ID && <ConfirmationPopup callBack={this.deleteObject} object={"Boat profile"}/>}
              </div> 
              <hr/>  
              <div class="like">
                {   ID && boatDetails && renderLike && <ObjectLikeDislike
                  LikeCount={totalCount} 
                  ObjectType={boatDetails.ObjectType} 
                  ReferenceID={boatDetails.ID} 
                  status={boatDetails.isLike} 
                  ReferenceUserID={boatDetails.CreatedBy} 
                  updateOnParent={this.objectLike} 
                  isDetail={true}/>
                }
                { ID == null && <> <img src="../../assets/images/heart-icon.png" alt="heart-icon" class="pr-2"/><span> {totalCount}</span> </>}
              </div>
              <div class="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" class="pl-6"/><span> {CommentCount}</span></div>
            </div>        
          </div>
        </div>
        <div class="col-md-12">
          <div class="row">
            <p class="about-event-info">{boatDescription}</p>    
          </div>
        </div>
        <PhotosContent boatPictures={boatDetails.Pictures} fromBoat />
        <VideoContent videos={boatDetails.Videos} />
        <ToastContainer autoClose={1000}/>
      </div>
   )}
  }

export default compose(
  connect(null, { getBoatDetailsByID }
  ),
  withApollo
)(DisplayBoatContent);
