import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { getBoatCreateAction } from "../../data/actions";
import { getAllEventTypes, getUsersBoatCount } from "../../data/query";
import LocationComponent from "../location/LocationContent";
import ImageUploads from "../match/MutipleImageUploads";
import AddVideo from "../common/Video/AddVideo";

var formatcoords = require('formatcoords');

const LocalStorageData = require("../../services/LocalStorageData");
let UID = null;

if (LocalStorageData.ID) {
  UID = LocalStorageData.ID;
}

class CreateBoatContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boatType: "",
      boatName: "",
      boatMake: "",
      boatLength: "",
      sailArray: [
        { id: 1, Type: "Day Sailing" },
        { id: 2, Type: "Crusing" },
        { id: 3, Type: "Racing" },
      ],
      boatProfilePic: "",
      locationObject: {},
      buttonCheck: false,
      city: "",
      state: "",
      zipCode: "",
      gpsCoordinate: "",
      showUpdated: null,
      images: [],
      imagesRender: [],
      boatCount: 0,
      boatYear: "",
      boatModal: "",
      videos: []
    };
  }

  componentDidMount() {
    this.getUsersBoatCount();
  }

  getUsersBoatCount = () => {
    this.props.client
      .query({
        query: getUsersBoatCount,
        variable: {
          UserID: UID
        },
        fetchPolicy: "network-only"
      })
      .then((res) => {
        if(res.data.getUsersBoatCount.BoatCount !== null){
          this.setState({ boatCount: res.data.getUsersBoatCount.BoatCount })
      }
    });
  };

  handleEventsSelect = async (category) => {
    let oldEvents = this.state.sailArray.slice();
    oldEvents.forEach((item) => {
      if (item.id === category.id) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      sailArray: oldEvents,
      buttonCheck: true,
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleStartDateChange = (date) => {
    this.setState({ startDate: date, endDate: date });
  };

  handleEndDateChange = (date) => {
    this.setState({ endDate: date });
  };

  handleStartTimeChange = (value) => {
    this.setState({ startTime: value });
  };

  handleEndTimeChange = (value) => {
    this.setState({ endTime: value });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({ boatProfilePic: result })
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  callback = (data) => {
    var point = formatcoords(data.latitude, data.longitude).format();
    this.setState({
      gpsCoordinate: data.latitude + data.longitude,
      city: data.city,
      state: data.state,
      zipCode: data.zipCode,
      showUpdated: point
    })
    let address = data.mainPlace.concat(
      data.city,
      ",",
      data.state,
      ",",
      this.state.zipCode
    );
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
      },
    });
  };

  handleLocationChnage = (e) => {
    this.setState({ zipCode : e.target.value })
  }

  getImagesArray = (baseArray, imagesArray) => {
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray,
    });
  };

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
  }

  setProfilePic = (pic) => {
    this.setState({ boatProfilePic: pic})
  }

  getvideoUrls = (data) => { this.setState({ videos: data }) }

  createBoat = async (e) => {
    e.preventDefault();
    const { boatName, boatLength, boatMake, boatDescription, boatProfilePic, images,
      locationObject, boatType, sailArray, videos, boatYear, boatModal } = this.state;

    let boatObject = { boatName, boatLength, boatMake, boatDescription, boatProfilePic,
      locationObject, boatType, sailArray, images, videos, boatYear, boatModal };

    var options = sailArray.filter(function (item) {
      return item.isSelected;
    });

    const SailType = options.map(function (item) {
      return { ID: item.id, Name: item.Type };
    });

    await this.props
    .getBoatCreateAction(this.props.client, boatObject, SailType, UID)
    .then((res) => {
      toast.success("Boat Added successfully", {
        onClose: () => this.props.history.push("/"),
      });
    })
    .catch(function (error) {
      // console.log(error);
    });
  };

  render() {
    const { boatName, boatLength, boatMake, boatDescription, boatProfilePic, locationObject,
      boatType, sailArray, buttonCheck, city, state, zipCode, gpsCoordinate, showUpdated,
      boatCount } = this.state;

    const enabled = boatType && boatCount < 5 && boatName && boatLength && boatMake &&
      boatDescription && locationObject && boatProfilePic && buttonCheck;

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" href="/">
              <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="/" className="float-left mobile_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Boat Profile</h1>
              { boatCount >= 5 &&
                <form className="mt-4">
                  <div className="row">
                    <div className="col-sm-12 my-2">
                      <p>You have exceeded the limit. Max limit is 5</p>
                    </div>
                  </div>
                </form>
              }
              <form className={ boatCount >= 5 ? "blurEffect mt-4": "mt-4"}>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="typeOfBoat">Type of Boat</label>
                    <div className="">
                      <input
                        type="button"
                        className={
                          boatType === "Mono"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="boatType"
                        value="Mono"
                        onClick={this.handleChange}
                      />
                      <input
                        type="button"
                        className={
                          boatType === "Cat"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="boatType"
                        value="Cat"
                        onClick={this.handleChange}
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="typeOfSail">Sailing Type</label>
                    <div className="">
                      {sailArray &&
                        sailArray.map((sail) => {
                          return (
                            <input
                              type="button"
                              key={sail.id}
                              className={`${
                                sail.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleEventsSelect(sail)}
                              value={sail.Type}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatName">Boat Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatName"
                      placeholder="Boat Name"
                      name="boatName"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label for="boatLength">Length of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatLength"
                      placeholder="Length of Boat"
                      name="boatLength"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Make of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatMake"
                      placeholder="Make of Boat"
                      name="boatMake"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Model of Boat</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatModal"
                      placeholder="Model of Boat"
                      name="boatModal"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="boatMake">Year of Manufacture</label>
                    <input
                      type="text"
                      className="form-control"
                      id="boatYear"
                      placeholder="Year of Manufacture"
                      name="boatYear"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <hr />
                <LocationComponent callback={this.callback} />
                <div class="row">
                  <div class="col-sm-6 my-2">
                    <label for="city">City</label>
                    <input
                      readOnly
                      type="text"
                      class="form-control"
                      id="city"
                      placeholder="City"
                      name="city"
                      value={city}
                    />
                  </div>

                  <div class="col-sm-6 my-2">
                    <label for="state">State</label>
                    <input
                      readOnly
                      type="text"
                      class="form-control"
                      id="state"
                      placeholder="State"
                      name="state"
                      value={state}
                    />
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6 my-2">
                    <label for="zipCode">Zipcode</label>
                    <input
                      type="text"
                      class="form-control"
                      id="zipCode"
                      placeholder="Zipcode"
                      name="zipCode"
                      value={zipCode}
                      onChange={this.handleLocationChnage}
                    />
                  </div>

                  <div class="col-sm-6 my-2">
                    <label for="gpsCoordinate">GPS Coordinates</label>
                    <input
                      readOnly
                      type="text"
                      class="form-control"
                      id="gpsCoordinate"
                      placeholder="GPS Coordinates"
                      name="gpsCoordinate"
                      value={showUpdated}
                    />
                  </div>
                </div>
                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="boatDescription"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <hr />
                <AddVideo getvideoUrls={this.getvideoUrls} />   
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="profilePicture">Pictures</label>
                    <ImageUploads callback={this.getImagesArray}/>
                  </div>
                </div>
                <div className="row" style={{ justifyContent: "center"}}>
                  <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                    {
                      this.state.imagesRender && this.state.imagesRender.map((item,index) => {
                        return <div className="multiple-image-preview">
                          <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                          <div class="multiple-middle-text">
                            <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                            <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                          </div></div>
                      })
                    }
                  </div>
                  {
                    this.state.imagesRender && this.state.imagesRender.length > 0 && !boatProfilePic &&
                    <div className="col-sm-12 my-2 pt-2 text-center">
                      <i className="text-danger h6">Please select profile picture from above images</i>
                    </div>
                  }
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                  {boatProfilePic && <label for="profilePicture">Profile Picture</label> }
                  </div>
                </div>
                {boatProfilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={boatProfilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        disabled={!enabled}
                        type="button"
                        className="btn btn-primary save_profile"
                        onClick={this.createBoat}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { getBoatCreateAction }),
  withApollo
)(CreateBoatContent);
