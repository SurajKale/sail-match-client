import React from "react";
// import {FacebookProvider, Login } from 'react-facebook';
import fb_logo from "../../assets/images/fb-icon.png";
import axios from "axios";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";

import { facebookSignIn } from "../../data/query";
import { facebookSignInSignUp } from "../../data/actions/UserActions";

import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";

import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const fbappId = "740523353377876";

class FacebookLoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: "",
    };
  }

  responseFacebook = async (response) => {
    let FirstName = null;
    let LastName = null;
    let Email = null;
    let ExternalSiteID = null;
    let ExternalProfileImage = null;
    if (response.status !== "unknown" || response.status !== "undefined") {
      if (response.name !== "") {
        FirstName = response.name.split(" ").slice(0, -1).join(" ");
        LastName = response.name.split(" ").slice(-1).join(" ");
        Email = response.email;
        ExternalSiteID = response.id;
        if (response.picture.data) {
          ExternalProfileImage = response.picture.data.url;
        }

        await this.props
          .facebookSignInSignUp(
            this.props.client,
            FirstName,
            LastName,
            Email,
            ExternalSiteID,
            ExternalProfileImage
          )
          .then((result) => {
            localStorage.setItem(
              "sessionUser",
              JSON.stringify(result.data.facebookSignInSignUp)
            );
            window.location.href = "./";
          })
          .catch(function (error) {
            toast.error(error.message);
          });
      }
    }
  };

  render() {
    return (
      <React.Fragment>
        <ToastContainer autoClose={1000} />
        <FacebookLogin
          appId={fbappId}
          size="small"
          fields="name,email,picture"
          callback={this.responseFacebook}
          textButton="Signup with Facebook"
          render={(renderProps) => (
            <button className="social fbSignUp" onClick={renderProps.onClick}>
              <img src={fb_logo} style={{ paddingRight: "10px" }} />
              {
                this.props.show ? 'Sign in with Facebook' : 'Sign up with Facebook'
              }
            </button>
          )}
        />
      </React.Fragment>
    );
  }
}

export default compose(
  connect(null, { facebookSignInSignUp }),
  withApollo
)(FacebookLoginPage);
