import React, { Component } from "react";
import { Link,withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { validEmailRegex } from '../../services/CommonServices'
import { getSignUpAction } from "../../data/actions/UserActions";
import { withApollo } from "react-apollo";
import {isEmailExists,isPhoneExists} from '../../data/query';

import logo from '../../assets/images/logo.png';
import fb_logo from '../../assets/images/fb-icon.png';
import google_logo from '../../assets/images/google-icon.png';
import {_encryptPwd} from '../../services/PwdConversion';
import FacebookLogin from './FacebookLogin';
import GoogleLoginPage from './GoogleLoginPage';
const LocalStorageData = require("../../services/LocalStorageData");
let ID = null
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email: "",
        phone: "",
        password: "",
        confirmpPassword:"",
        firstName: "",
        lastName: "",
        userType: "",
        gender: "",
        dateOfBirth: "",
        ipAddress: "",
        signUpMethod: "",
        successMsg: "",
        emailExists:false,
        phoneExists:false,
        sucessfullMessage:false,
        errorEmail:"",
        errorPhone:"",
        errorCFPassword:"",
        errorPwd: "",
    
    };
  }
componentDidMount = () =>{
  if(ID != null){
   this.props.history.push('/')
  }
}
  getEmailValidate = (Email) =>{
    this.props.client
    .query({
        query:isEmailExists,
        variables: {
            Email:Email
        },
        fetchPolicy: "network-only"
    })
    .then(async (res) => { 
      await res.data.isEmailPhoneExist.Message == "true" ? this.setState({emailExists:true,errorEmail:'Email already exist'}):  this.setState({emailExists:false,errorEmail:''})      
    })   
   };

   getPhoneValidate = (Phone) =>{
    this.props.client
    .query({
        query:isPhoneExists,
        variables: {
            Phone:Phone
        },
        fetchPolicy: "network-only"
    })
    .then(async (res) => {    
        await res.data.isEmailPhoneExist.Message == "true" ? this.setState({phoneExists:true,errorPhone:'Phone number already exist'}):  this.setState({phoneExists:false,errorPhone:''})
    })   
   }
   checkEmails = (e) => {
    const { value } = e.target;
    const checkEmail = validEmailRegex.test(value);
    checkEmail
      ? this.setState({ errorEmail: "" })
      : this.setState({ errorEmail: "Please enter valid email" });
  };

  handleChange = (e) => {
    if(e.target.name === "phone"){
       if(e.target.value.length ==  10){
        this.getPhoneValidate(e.target.value)
      }
    }
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  _validMobile = (e) => {
    const re = /[0-9]+/g;
    if (!re.test(e.key)) {
    e.preventDefault();
    }
  }


  handleEmailChange = (e) => {
    const { value } = e.target;
    // const checkEmail;
    const checkEmail = validEmailRegex.test(value);
    checkEmail ? this.setState({ email: value }) : this.setState({ email: "" });
    if(checkEmail === true){
      this.getEmailValidate(value)
    }
  };

  handleCfPasswordChange = (event) =>{
    this.setState({ confirmpPassword: event.target.value });
    event.target.value !== this.state.password
      ? this.setState({
          errorCFPassword: (
            <span className="text-danger">Passwords do not match.</span>
          )
        })
      : this.setState({ errorCFPassword: null });
  }

  
  goDashboard = (e)=>{
    //   Router.push("/");
            this.props.history.push('./home');
      }

      checkEmail = (e) => {
        const { value } = e.target;
        const checkEmail = validEmailRegex.test(value);
        checkEmail
          ? this.setState({ errorEmail: "" })
          : this.setState({ errorEmail: "Please enter valid email" });
      };
      
      checkPassword = (e) => {
        const { value } = e.target;
        const checkpwd = value.length >= 6;
        checkpwd
          ? this.setState({ errorPwd: "" })
          : this.setState({
              errorPwd: "Password should be minimum 6 characters",
            });
      };
      submitForm = async () => {
        let that = this;
        const { 
          email,
          password
        } = this.state;
        await this.props.getSignUpAction(
            this.props.client,  
            email,
            _encryptPwd(password),
            "Site",
            ""
        ).then(response => { 
          if (response.data.userSignup.ID) {
            toast.success("Registered successfully ! Please check your mail");
              that.setState({successMsg : 'Sucessfully SignUp ! Please check your mail for Account Activation !',sucessfullMessage : true});
          } else {
            toast.error("Something wents wrong");
            that.setState({sucessfullMessage : true});
            }
        });  
      };
      goToHome = (e) =>{
        this.props.history.push('/')
      }
      checkPassword = (e) => {
        const { value } = e.target;
        const checkpwd = value.length >= 6;
        checkpwd
          ? this.setState({ errorPwd: "" })
          : this.setState({
              errorPwd: "Please enter valid password ! Password length should be minimum 6 characters",
            });
      };
  render() {
    const { email, password,confirmpPassword,emailExists} = this.state;
    const enabled = email && password && (confirmpPassword == password) && password.length >= 6 && emailExists == false;

    return (
        <React.Fragment>
              { this.state.sucessfullMessage == false &&
        <div className="container">
        <div className="col-md-12">
            <div className="row">
                <div className="login-logo-section" onClick={this.goToHome}><a><img src={logo}alt="SailMatch" /></a></div>
            </div>
        </div>
    
        <div className="col-md-12">
            <div className="row">			
                <div className="col-12 col-md-4"></div>               
                <div className="col-12 col-md-4">  
                    <div className="loginSection">		
                    <h3>Create account</h3>    		
                            {/* <div className="form-group">
                                <label>First Name</label>
                                <input type="text" 
                                       className="form-control"
                                       placeholder="First Name"
                                       name="firstName"
                                       onChange={this.handleChange}  />
                            </div>
                            <div className="form-group fieldSection">
                                <label>Last Name</label>
                                <input type="text" 
                                       className="form-control"
                                       placeholder="Last Name"
                                       name="lastName"
                                       onChange={this.handleChange}  />
                            </div> */}
                            <div className="form-group fieldSection">
                                <label>Email </label>
                                <input type="email"
                                       className="form-control"
                                       placeholder="Email address"
                                       onBlur={this.checkEmail}
                                       name="email"
                                       onBlur={this.checkEmails}
                                       onChange={this.handleEmailChange} /> 
                                <span className="text-danger">{this.state.errorEmail}</span>
                            </div>		
                            {/* <div className="form-group fieldSection">
                                <label>Mobile number</label>
                                    <input type="text" 
                                        className="form-control"
                                        placeholder="Mobile number"
                                        name="phone"
                                        maxLength="10"
                                        onKeyPress={this._validMobile}
                                        onChange={this.handleChange}  />
                                        <span className="text-danger">{this.state.errorPhone}</span>
                            </div> */}

                            <div className="form-group fieldSection">
                                <label>Password</label>
                                <input type="password"
                                       className="form-control" 
                                       placeholder="Password"
                                       onBlur={this.checkPassword}
                                       name="password"
                                       onBlur={this.checkPassword}
                                       onChange={this.handleChange} />

                              <span className="text-danger">{this.state.errorPwd}</span>

                            </div>

                            <div className="form-group fieldSection">
                                <label>Confirm Password</label>
                                <input type="password"
                                       className="form-control" 
                                       placeholder="Password"
                                       name="confirmpPassword"
                                       onChange={this.handleCfPasswordChange} />
                            {this.state.errorCFPassword}
                            </div>

                            <div className="d-flex justify-content-center continueBtnSection">
                                    <button className="continueBtn" 
                                            onClick={this.submitForm}
                                            disabled={!enabled}> Create account</button>
                            </div>
                
                        <ToastContainer autoClose={1000}/>
    
                        <hr />
                        <FacebookLogin />
				                    <GoogleLoginPage />
                            </div>
 
                              <p className="signUpLink">Already have an account?
                                  <Link to={'./login'} style={{marginLeft: 5}}>Sign in</Link>
                              </p>
                
                            <div className="col-12 col-md-4"></div>
                    </div>
               </div> 
            </div>
    
    </div>}

    { this.state.sucessfullMessage &&
        <div className="container">
            <div className="col-md-12">
                <div className="row">
                  <div className="login-logo-section"><a href="javascript:void(0);">
                    <img src={logo} alt="SailMatch" /></a></div>
              </div>
            </div>

          <div className="col-md-12">
            <div className="row">			
              <div className="col-12 col-md-4col-12 col-sm-4 col-md-3 col-lg-3"></div>
                
                <div className="col-12 col-sm-4 col-md-6 col-lg-6">
                  <div className="loginSection signupSuccess">		
                  <h4>Please check your inbox</h4>  
                  <div className="success-text">We just emailed a magic link to</div>
                  <div className="success-text">{email}</div>
                  <div className="success-text">Click the link to complete your account set-up.</div>  
                  <div className="d-flex justify-content-center continueBtnSection">
                    <button className="okBtn" onClick={this.goToHome}>Ok</button></div>		            
                </div>           
              <div className="col-12 col-md-4"></div>
            </div>
          </div>
        </div>
        </div>
  }   

 </React.Fragment>
    
    );
  }
}

const mapStateToProps = (state) => ({
    userData: state.userDetails.userDetails
  })
  
  
  export default compose(
    connect(mapStateToProps, {getSignUpAction}
    ),
    withApollo
  )(Registration);
  
