import React, { Component } from 'react';
import { withApollo } from 'react-apollo';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { validEmailRegex } from '../../services/CommonServices';
import { forgotPassword } from '../../data/query';
import logo from '../../assets/images/logo.png';

class ForgotPassword extends Component {

    state = {
        email: "",
        errorEmail: "",
    }

    checkEmail = (e) => {
        const { value } = e.target;
        const checkEmail = validEmailRegex.test(value);
        checkEmail
          ? this.setState({ errorEmail: "" })
          : this.setState({ errorEmail: "Please enter valid email" });
      };

    handleEmailChange = (e) => {
        const { value } = e.target;
        const checkEmail = validEmailRegex.test(value);
        checkEmail ? this.setState({ email : value }) : this.setState({ email : ""})
    }

    handleSubmit = (e) => {
        e.preventDefault()
        this.props.client
            .mutate({
                mutation: forgotPassword,
                variables: {
                    Email: this.state.email,
                }
            })
            .then(res => {
                toast.success(res.data.forgotPassword.Message)
            });
    }
    goToHome = (e) =>{
        this.props.history.push('/')
      }
      checkEmail = (e) => {
        const { value } = e.target;
        const checkEmail = validEmailRegex.test(value);
        checkEmail
          ? this.setState({ errorEmail: "" })
          : this.setState({ errorEmail: "Please enter valid email" });
      };
    render() {
        const { email } = this.state;
        const enabled = email 
        
        return (
            <div className="container">
                <div className="col-md-12">
                    <div className="row">
                        <div className="login-logo-section" onClick={this.goToHome}><a><img src={logo} alt="SailMatch"/></a></div>
                    </div>
                </div>
                <div className="col-md-12">
                    <div className="row">			
                        <div className="col-12 col-md-4"></div>
                        <div className="col-12 col-md-4">
                            {/* <p className="success-msg">Recovery email sent successfully !!!</p> */}
                            <div className="loginSection">		
                                <h3>Forgot your password?</h3>  
                                <p>Forgot your account’s password, enter your email address and we’ll send you a recovery link.</p>  		
                                <form action="#" id="">
                                    <div className="form-group">
                                        <label htmlFor="email">Email</label>
                                        <input 
                                            type="email"
                                            className="form-control"
                                            id="email"
                                            placeholder="enter your email"
                                            name="email"
                                            onBlur={this.checkEmail}
                                            onChange={this.handleEmailChange}/>
                                             <span className="text-danger">{this.state.errorEmail}</span>

                                    </div>
                                    <div className="d-flex justify-content-center continueBtnSection">
                                        <button className="continueBtn" onClick={this.handleSubmit} disabled={!enabled}> Submit</button>
                                    </div>
                                </form>
                            </div>
                            <p className="signUpLink">Already have an account? <a href="/login">Sign in</a></p>
                            <div className="col-12 col-md-4"></div>
                        </div>
                    </div>
                </div>
                <ToastContainer />
            </div>
        )
    }
}

export default compose(connect(), withApollo)(ForgotPassword);