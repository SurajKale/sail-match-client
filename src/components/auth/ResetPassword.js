import React, { Component, Fragment } from 'react';
import { withApollo } from 'react-apollo';
import { compose } from 'redux';
import { connect } from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader";
import { resetPassword, isResetURLValid } from '../../data/query';
import logo from '../../assets/images/logo.png';
import {_encryptPwd} from '../../services/PwdConversion';
import { getSignInAction } from "../../data/actions/UserActions";

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class ResetPassword extends Component {

    state = {
        link: this.props.match.params.link,
        loading: true,
        message: false,
        password: "",
        confirmPassword: ""
    }

    handleChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value    
        })
    }

    componentDidMount() {
        this.checkLink()
    }

    checkLink = () => {
        this.props.client
            .query({
                query: isResetURLValid,
                variables: {
                    ActivationLink: this.state.link,
                },
                fetchPolicy: "network-only"
            })
            .then(res => {
                this.setState({
                    message: res.data.isResetURLValid.Message,
                    loading: false
                })
            });
    }


    loginForm = (email, password) => {      
        this.props.getSignInAction(this.props.client, 
               email, 
              password,
               ).then(response => {  
                   if (response.data.signin.ID) {                
                    localStorage.setItem("sessionUser", JSON.stringify(response.data.signin));
                      toast.success("Password Reset successfully", {
                       onClose: () => window.location.href = '/'
                   });
                   } else {
                     toast.error("Something wents wrong");
               }
           }).catch(function (error) {
               toast.error(error.message);
           });
       };

    handleSubmit = (e) => {
        e.preventDefault()
        const { password, confirmPassword, link } = this.state;
        if (  password === confirmPassword ) {
            this.props.client
                .mutate({
                    mutation: resetPassword,
                    variables: {
                        ActivationLink: link,
                        Password: _encryptPwd(this.state.password)
                    }
                })
                .then(res => {
                    console.log(res,"res");
                    this.loginForm(res.data.resetPassword.Email,res.data.resetPassword.Password )
                    // localStorage.setItem("sessionUser", JSON.stringify(res.data.resetPassword));
                    // toast.success("Password Reset successfully", {
                    //     onClose: () => window.location.href = '/'
                    // });
                });
        } else {
            toast.error("Password and confirm password should not match")
        }
    }


    goToHome = (e) =>{
        this.props.history.push('/')
      }

    render() {
        const { password, confirmPassword, message, loading } = this.state;
        const enabled = password && password.length > 5 && confirmPassword
        
        return (
            <div className="container">
                <div className="col-md-12">
                    <div className="row">
                        <div className="login-logo-section" onClick={this.goToHome}><a><img src={logo}alt="SailMatch" /></a></div>
                    </div>
                </div>
                <div className="col-md-12">
                    <div className="row">			
                        <div className="col-12 col-md-4"></div>
                        <div className="col-12 col-md-4">
                            <div className="loginSection">		
                                <h3>Reset Password</h3>
                                <div className="sweet-loading">
                                    <PuffLoader
                                    css={override}
                                    size={60}
                                    color={"#123abc"}
                                    loading={this.state.loading}
                                    />
                                </div>
                                { message === "true" &&
                                 <Fragment>
                                    <div className="form-group fieldSection">
                                        <label>Password</label>
                                        <input type="password"
                                            className="form-control" 
                                            placeholder="Password"
                                            name="password"
                                            onChange={this.handleChange} />
                                    </div>
                                    <div className="form-group fieldSection">
                                        <label>Confirm Password</label>
                                        <input type="password"
                                            className="form-control" 
                                            placeholder="Password"
                                            name="confirmPassword"
                                            onChange={this.handleChange} />
                                    </div>
                                    <div className="d-flex justify-content-center continueBtnSection">
                                        <button className="continueBtn" disabled={!enabled} onClick={this.handleSubmit} >Submit</button>
                                    </div>
                                 </Fragment>
                                }
                                { message === "false" && <p>Link is expired</p> }
                                <hr />
                                <div className="col-12 col-md-4"></div>
                            </div>
                        </div> 
                    </div>
                </div>
                <ToastContainer  autoClose={1000}  />
            </div>
        )
    }
}


export default compose(connect(null,{getSignInAction}), withApollo)(ResetPassword);