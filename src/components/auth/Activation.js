import React, { Component } from "react";
// import Router from 'next/router';
import { connect, useSelector,useDispatch } from 'react-redux';
import { useEffect,useState } from 'react';
import { withApollo } from 'react-apollo';
import { compose } from 'redux';
import { Link,withRouter } from "react-router-dom";
import {isAcccontActivationLinkValid} from '../../data/query'
import {ActivationsignInAction} from '../../data/actions'
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import logo from '../../assets/images/logo.png';
const LocalStorageData = require("../../services/LocalStorageData");
let ID = null
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class UserActivation extends Component {
    constructor(props) {
      super(props);
      this.state = {
        validTokenMsg:"Please wait verification is going on..!",
        validTokenSubMsg:'',
        email:'',
        uniqueLink:'',
        isValid:false,
        isLoading:false,
        user:{}
      }    
    }

componentDidMount=() => {
let parseQueryString = require('query-string');
let queryString = this.props.location.search;
let queryParams = parseQueryString.parse(queryString);
            if(queryParams){
                this.getTokenValidate(queryParams.link)
            }   
            else{
                this.setState({validTokenMsg:"Something went wrong!"})
            }    
}
 
       getTokenValidate = async (token) =>{
        this.props.client
        .query({
            query:isAcccontActivationLinkValid,
            variables: {
                ActivationLink:token
            }
        })
        .then(async (res )=> {          
           if(res.data.isAcccontActivationLinkValid.Email && res.data.isAcccontActivationLinkValid.UniqueAutoSinginKey){
                    await  this.setState({uniqueLink:res.data.isAcccontActivationLinkValid.UniqueAutoSinginKey,
                        email:res.data.isAcccontActivationLinkValid.Email})
                    if(res.data.isAcccontActivationLinkValid.Email != null){
                        this.setState({validTokenMsg:"Your e-mail address has been verified.",
                        isValid:true,
                        isLoading:true
                    });
                        // setTimeout(function(){ Router.push('/login') },10000);
                    }                 
           }    
           else{
            this.setState({validTokenMsg:"This verification link is expired or broken.",
            isValid:false,
            isLoading:true
        });
    }       
    })   
    }
     redirectToDestination = (e)=>{
        if(this.state.isValid){
          this.props.ActivationsignInAction(this.props.client,this.state.email,this.state.uniqueLink)
          .then(async res => {
            const userData = res.data.signin;
                if (userData.ID) {
                  await localStorage.setItem("sessionUser", JSON.stringify(userData));
                    window.location.href = '/';                
                     
                } else {
                  toast.error("Something went wrong");
                }
            })
          }
        else{
          this.props.history.push('/')
          window.location.href = '/'
        }
    }

    goHome = () =>{
      this.props.history.push('/');
    }

render(){
    const {isValid,isLoading,email,uniqueLink,user,validTokenMsg} = this.state
    return (
      <React.Fragment>
      { isValid && isLoading &&
      <div className="container">
        <div className="col-md-12">
          <div className="row">
            <div className="login-logo-section">
                <a href="javascript:void(0);">
                  <img src={logo} alt="SailMatch" /></a>
            </div>
        </div>
      </div>
      <div className="col-md-12">
      <div className="row">			
        <div className="col-12 col-md-4"></div>
          <div className="col-12 col-md-4">
            <div className="loginSection signupSuccess">		
            <h4>Your email address has been verified</h4>  
            <div className="success-text"> Welcome to SailMatch and share your Love of Sailing. Click the button to set sail.</div>
            <div className="success-text">Thank you !!! </div> 
            <div className="d-flex justify-content-center continueBtnSection">
            <button className="continueBtn" onClick={(e)=>this.redirectToDestination()}>Click to continue</button>
          </div>  
        </div>
        <div className="col-12 col-md-4"></div>
      </div>
      </div>
      </div>
    </div>}
   
       { !isValid &&  isLoading &&
          <div className="container">
           <div className="col-md-12">
              <div className="row">
                <div className="login-logo-section">
                  <a href="javascript:void(0);">
                    <img src={logo} alt="SailMatch" /></a>
                </div>
            </div>
          </div>

          <div className="col-md-12">
          <div className="row">			
            <div className="col-12 col-md-4"></div>  
              <div className="col-12 col-md-4">
                <div className="loginSection signupSuccess">		
                <h4>This verification link is expired or broken</h4>  
                <div className="success-text"> This link doesn't seem to be working, most likely due to it's expiration. Note that the verication link is valid only for 48 hours.</div>
                <div className="d-flex justify-content-center continueBtnSection">
                <button className="continueBtn" onClick={this.goHome}> Go to homepage</button>
              </div>    
            </div>
            <div className="col-12 col-md-4"></div>
          </div>
          </div>
        </div>
      </div>}

    {/* <h1 class="display-5">{validTokenMsg}</h1>     */}
  </React.Fragment>
         )
    }
}

const mapStateToProps = (state) => ({
    user: state.userDetails
  }) 

  export default compose(
    connect(mapStateToProps, {ActivationsignInAction}),
    withRouter,
    withApollo
  )(UserActivation);