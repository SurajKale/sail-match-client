import React from 'react';
import google_logo from '../../assets/images/google-icon.png';
import GoogleLogin from "react-google-login";
import {facebookSignIn,googleSignIn} from '../../data/query';
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { connect } from "react-redux";
import { googleSignInSignUp } from "../../data/actions/UserActions";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

class GoogleLoginPage extends React.Component{

 constructor(props) {
        super(props);
        this.state = {
             error:''
        };    
  }

  responseGoogle = async (response) =>{
    let FirstName = null;
    let LastName = null;
    let Email = null;
    let ExternalSiteID = null;
    let ExternalProfileImage = null;
    if(response.profileObj){
        let tmpRes = response.profileObj;
        FirstName = tmpRes.name.split(' ').slice(0,-1).join(' ');
        LastName = tmpRes.name.split(' ').slice(-1).join(' ');
        Email = tmpRes.email;
        ExternalSiteID = tmpRes.googleId;
        if(tmpRes.imageUrl){
            ExternalProfileImage = tmpRes.imageUrl;
        }
        
      await this.props.googleSignInSignUp(
        this.props.client,  
        FirstName,
        LastName,
        Email,
        ExternalSiteID,
        ExternalProfileImage,
    ).then(result => { 
      localStorage.setItem("sessionUser", JSON.stringify(result.data.googleSignInSignUp));
      window.location.href = './'

    }).catch(function (error) {
      toast.error(error.message);
      });
    } else{
        // console.log('zzzzzzzzzzzzzzzzzzzzz')
    }
}
 

render() {
	return(
          <React.Fragment>
             <ToastContainer autoClose={1000} />
                <GoogleLogin
                clientId="1016361037270-rlum43coj6h8k4bjl6imtbm5m75ikvqg.apps.googleusercontent.com" 
                buttonText="Google"
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
                render={renderProps => (
                    <button className="social googleSignUp" onClick={renderProps.onClick}>
                            <img src={google_logo} style={{paddingRight: '10px'}} />
                    { this.props.show ? 'Sign in with Google' : 'Sign up with Google'}</button> )} /> 
            </React.Fragment>
		);
   }
}

export default compose(
    connect(null, {googleSignInSignUp}
    ),
    withApollo
  )(GoogleLoginPage);

