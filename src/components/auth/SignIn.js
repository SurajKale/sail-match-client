import React, { Component } from "react";
import { Link,withRouter } from "react-router-dom";
import { compose } from "redux";
import { connect } from "react-redux";
import { ToastContainer, toast } from "react-toastify";
import { Base64 } from 'js-base64';
import 'react-toastify/dist/ReactToastify.css';

import { validEmailRegex } from '../../services/CommonServices'
import { getSignInAction } from "../../data/actions/UserActions";

import { withApollo } from "react-apollo";
import {_encryptPwd} from '../../services/PwdConversion';
import logo from '../../assets/images/logo.png';
import FacebookLogin from './FacebookLogin';
import GoogleLoginPage from './GoogleLoginPage';
import Cookies from 'universal-cookie';
const LocalStorageData = require("../../services/LocalStorageData");
let ID = null
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

const cookies = new Cookies();

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email: "",
        mobileNo: "",
        password: "",
        errorEmail: "",
        errorPwd: "",
        isChecked: false,
    };
  }

  componentDidMount () {   
      if(ID != null){
       this.props.history.push('/')
      }   
    const userData = JSON.parse(localStorage.getItem('sessionUser'));
    const check = localStorage.getItem('sailRemember')

    if (check === "true" && userData.Email !== "") {
        this.setState({
            isChecked: true,
            email: userData.Email,
            password: Base64.decode(userData.Password)
        })
    }
  }


  checkEmail = (e) => {
    const { value } = e.target;
    const checkEmail = validEmailRegex.test(value);
    checkEmail
      ? this.setState({ errorEmail: "" })
      : this.setState({ errorEmail: "Please enter valid email" });
  };

  checkPassword = (e) => {
    const { value } = e.target;
    const checkpwd = value.length >= 6;
    checkpwd
      ? this.setState({ errorPwd: "" })
      : this.setState({
          errorPwd: "Please enter valid password",
        });
  };

  onChangeCheckbox = (event) => {
    this.setState({
      isChecked: event.target.checked,
    });
  };

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  loginForm = (e) => {
    e.preventDefault();
    let that = this;
    const { email, password, isChecked } = this.state;
     this.props.getSignInAction(this.props.client, 
            email, 
            _encryptPwd(password),
            ).then(response => {  
                if (response.data.signin.ID) {                
                    localStorage.setItem("sessionUser", JSON.stringify(response.data.signin));
                    // localStorage.setItem("rememberLogin", JSON.stringify(response.data.signin));
                    localStorage.setItem("sailRemember", isChecked);
                    toast.success("Login successfully", {

                   onClose: () => {                   
                    let parseRoute = localStorage.getItem('currentRoute');
                    if(parseRoute){
                      let route = JSON.parse(parseRoute)
                      if(route === "Crew") { window.location.href = '/add-crew'}
                      if(route === "Event") { window.location.href = '/create-event'}
                      if(route === "Ride") { window.location.href = '/add-ride'}   
                      if(route === "Match") { window.location.href = '/create-match'}
                      if(route === "Boat") { window.location.href = '/create-boat'}
                      if(route === "Service") { window.location.href = '/create-service'}            
                    }  
                    else{
                      window.location.href = '/'
                    }                
                    }
                  });
                } else {
                  toast.error("Something wents wrong");
            }
        }).catch(function (error) {
          const errorMsg = error.graphQLErrors.map(x => x.message);
          toast.error(errorMsg[0]);
		});
    };

    handleEnter = (event) => {
      if(event.key=='Enter'){
        if(this.state.email && this.state.password !== ''){
           this.loginForm(event); 
          }
      }      
    }

    goToHome = (e) =>{
      this.props.history.push('/')
    }

  render() {
    const { email, password, isChecked } = this.state;
    const enabled = email && password !== '';
    return (
    <div className="container">
     <div className="col-md-12">
        <div className="row">
        	<div className="login-logo-section" onClick={this.goToHome}><a>
                <img src={logo}alt="SailMatch" /></a>
            </div>
    	</div>
    </div>

	<div className="col-md-12">
		<div className="row">			
			<div className="col-12 col-md-4"></div>
		    <div className="col-12 col-md-4">
		    	<div className="loginSection">		
		    	<h3>Login</h3>    		
						<div className="form-group">
							<label>Email</label>
                             <input type="email" 
                                    placeholder="Enter your email"
                                    name="email"
                                    value={email}
                                    onChange={this.handleChange}
                                    onBlur={this.checkEmail}
                                    onKeyPress={ (evt) => this.handleEnter(evt) }
                                    autoComplete="off"
                                    className="form-control" />
                                    <span className="text-danger">{this.state.errorEmail}</span>
						</div>                      
						<div className="form-group fieldSection">
							<label>Password</label>
                            <input type="password"
                                   className="form-control"
                                   placeholder="Password"
                                   value={password}
                                   name="password"
                                   onChange={this.handleChange}
                                   onKeyPress={ (evt) => this.handleEnter(evt) }
                                   onBlur={this.checkPassword} />
                        <span className="text-danger">{this.state.errorPwd}</span>
						</div>

                         <input
                            type="checkbox"
                            checked={isChecked}
                            name="lsRememberMe"
                            onChange={this.onChangeCheckbox}
                            />
                        &nbsp;<label>Remember me</label> 

<ToastContainer autoClose={1000} />
						<div className="d-flex justify-content-center continueBtnSection">
							<button type="button" className="continueBtn" disabled={!enabled} onClick={this.loginForm}> Continue</button>
						</div>
						<Link className="forgotPassLink" to={'./forgot-password'} >Forgot password?</Link>
				    	<hr className="hr-line" />
                            <FacebookLogin show />
				                    <GoogleLoginPage show />
                        </div>
			              <p className="signUpLink">Don't have an account?
                     <Link to={'./signup'} style={{marginLeft: 5}}>Sign up</Link></p>
		            	<div className="col-12 col-md-4"></div>
		                </div>
                	</div>
                </div>
           
        </div>
    );
  }
}

  export default compose(
    connect(null, {getSignInAction}
    ),
    withApollo,
    withRouter
  )(Login);
  

