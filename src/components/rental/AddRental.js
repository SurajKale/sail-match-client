import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getListOfBoatTypes } from "../../data/query";
import LocationContent from "../location/LocationContent";
import { ToastContainer, toast } from "react-toastify";
import { createRentalAction } from "../../data/actions/RentalAction";
import SelectHours from "./SelectHours";
import { StyledDropZone } from "react-drop-zone";
import "react-drop-zone/dist/styles.css";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class AddRental extends Component {
  constructor(props) {
    super(props);
    this.state = {
      boatArr: [],
      boatID: "",

      city: "",
      latitude: "",
      longitude: "",
      address: "",
      state: "",
      gpsCoordinate: "",
      zipCode: "",
      country: "",

      profilePic: "",
      description: "",
      website: "",
      business: "",
      Hours: "Mon, 10-6",

      dayName: "",
      startTime: "",
      endTime: "",

      daysFlag: true,
      startTimeFlag: true,
      getHourData: [],
      button: true
    };
  }

  getAllBoat = () => {
    this.props.client
      .query({
        query: getListOfBoatTypes,
        fetchPolicy: "network-only",
      })
      .then((res) => {
        res.data.getListOfBoatTypes.forEach((item) => {
          item.isSelected = false;
        });
        this.setState({ boatArr: res.data.getListOfBoatTypes });
      });
  };

  componentDidMount() {
    this.getAllBoat();
  }

  handleEventsSelect = async (category) => {
    this.setState({ boatID: category.ID });
    let oldEvents = this.state.boatArr.slice();
    oldEvents.forEach((item) => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      boatArr: oldEvents,
    });
  };

  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  getLocationDetails = (location) => {
    let address =
      location.mainPlace +
      "," +
      "" +
      location.city +
      "," +
      "" +
      location.zipCode +
      "," +
      "" +
      location.state +
      "," +
      "" +
      location.country;
    this.setState({
      city: location.city,
      latitude: location.latitude,
      longitude: location.longitude,
      address: address,
      state: location.state,
      country: location.country,
      zipCode: location.zipCode,
      gpsCoordinate: location.latitude + '"N' + "-" + location.longitude + '"E',
    });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  createRental = (e) => {
    this.setState({ button : !this.state.button })
    const {
      boatID,
      latitude,
      longitude,
      address,
      profilePic,
      description,
      website,
      business,
      getHourData,
    } = this.state;
    const CreatedBy = ID;

    let lat = latitude.toString();
    let long = longitude.toString();
    const Location = { Latitude: lat, Longitude: long, Address: address };

    var options = this.state.boatArr.filter(function (item) {
      return item.isSelected;
    });

    const BoatType = options.map(function (item) {
      return { ID: item.ID, Name: item.Name };
    });

    this.props
      .createRentalAction(
        this.props.client,
        boatID,
        BoatType,
        description,
        profilePic,
        Location,
        CreatedBy,
        website,
        business,
        getHourData
      )
      .then((res) => {
        toast.success("Rental added successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  _callbackHours = (data) => {
    this.setState({ getHourData: data });
  };

  render() {
    const {
      boatArr,
      boatID,
      gpsCoordinate,
      zipCode,
      state,
      city,
      country,
      website,
      business,
      description,
      profilePic,
      button
    } = this.state;

    const label = profilePic
      ? "Profile Image successfully added"
      : "Click or drop your image here";

    const enabled = boatID && website && business && description && profilePic && button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            <a className="desktop_back" href="#">
              <img
                src="../../assets/images/back_icon.png"
                onClick={this.goBack}
                alt="Back"
              />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="#" className="float-left mobile_back">
                <img
                  src="../../assets/images/back_icon.png"
                  onClick={this.goBack}
                  alt="Back"
                />
              </a>
              <h1>Create Rental</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="ObjectType">Types of Boats</label>
                    <div>
                      {boatArr &&
                        boatArr.map((type) => {
                          return (
                            <input
                              type="button"
                              key={type.ID}
                              className={`${
                                type.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleEventsSelect(type)}
                              value={type.Name}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>

                <LocationContent callback={this.getLocationDetails} />

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="city">City</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="city"
                      placeholder="City"
                      name="city"
                      value={city}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="state">State</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="state"
                      placeholder="State"
                      name="state"
                      value={state}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="zipCode">Zipcode</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="zipCode"
                      placeholder="Zipcode"
                      name="zipCode"
                      value={zipCode}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="country">Country</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="country"
                      placeholder="country"
                      name="country"
                      value={country}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="gpsCoordinate">GPS Coordinates</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="gpsCoordinate"
                      placeholder="GPS Coordinates"
                      name="gpsCoordinate"
                      value={gpsCoordinate}
                    />
                  </div>
                  <div className="col-sm-6 my-2"></div>
                </div>
                <hr />
                <SelectHours callbackHours={this._callbackHours} />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="website">Website</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Website"
                      name="website"
                      value={website}
                      onChange={this.handleChange}
                    />
                  </div>

                  <div className="col-sm-6 my-2">
                    <label htmlFor="businessName">Business Name</label>
                    <input
                      type="text"
                      className="form-control"
                      placeholder="Business Name"
                      name="business"
                      value={business}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      rows="3"
                      name="description"
                      value={description}
                      onChange={this.handleChange}
                    ></textarea>
                  </div>
                </div>

                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="profilePicture">Profile Picture</label>
                    <StyledDropZone
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                {this.state.profilePic && (
                  <div className="row">
                    <div className="col-sm-12 my-2 text-center">
                      <img
                        src={this.state.profilePic}
                        className="img-fluid mb-3 member_pic_preview"
                        alt="Profile"
                      />
                    </div>
                  </div>
                )}

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        disabled={!enabled}
                        className="btn btn-primary save_profile"
                        onClick={this.createRental}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

export default compose(
  connect(null, { createRentalAction }),
  withApollo
)(AddRental);
