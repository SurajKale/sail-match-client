import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getBoatRentalProfileByUrl } from '../../data/query';
import {_formatDate,_formatTime, url} from '../../services/CommonServices';
import ImagePlaceholder from '../../services/ImagePlaceholder';
import ReactStars from "react-rating-stars-component";
import {deleteBoatRentalProfile} from '../../data/mutation';
import { ToastContainer, toast } from "react-toastify";
import ConfirmationPopup from "../common/ConfirmationPopup";
import PhotosContent from "../common/PhotosContent";
import ObjectLikeDislike from "../common/ObjectLikeDislike";
import PostListParent from "../common/PostListParent";


import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";
import  formatcoords from 'formatcoords';

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class DisplayRental extends Component {
    constructor(props) {   
    super(props);
    this.state = {
       rentalData:[],
       BoatRentalUrl:this.props.match.params.id,
    loaded: false,
    totalCount:"",
    metaData:[],
    showChatPopup: false,
    showShareModel: false,
    userDetailsPic:{}
      }
    }
    
    getBoatRentaalDetails = () => {
        this.props.client
        .query({
            query: getBoatRentalProfileByUrl,
            variables: {
                BoatRentalUrl:this.state.BoatRentalUrl
            },
            fetchPolicy: "network-only"
         }) .then(res => {  
             this.setState({ 
                isLike:res.data.getBoatRentalProfileByUrl.isLike,
                renderLike:true,
                 rentalData : res.data.getBoatRentalProfileByUrl, totalCount:res.data.getBoatRentalProfileByUrl.LikeCount }) 
             const metaData = res.data.getBoatRentalProfileByUrl;
             var metaArr = [];
             metaArr.push({
              title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
              keywords : metaData.ObjectType, SITE_STATIC_NAME,
              description : metaData.Description ? metaData.Description : '',
              og_url : window.location.href,
              og_title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
              og_description : metaData.Description ? metaData.Description : '',
              canonical_url : window.location.href,
              og_image : metaData.ProfilePicture ? metaData.ProfilePicture : SITELOGO,
              amp_url : window.location.href,
             })

             this.setState({  metaData:metaArr[0], rentalData : res.data.getBoatRentalProfileByUrl, totalCount:res.data.getBoatRentalProfileByUrl.LikeCount, rentalPictures: res.data.getBoatRentalProfileByUrl.Pictures }) 
          });          
      };

      componentDidMount(){
          this.getBoatRentaalDetails();
       }

       objectLike= (count) =>{
        console.log(count);
      this.setState({totalCount:count})
      }

       goBack = () =>{
        this.props.history.goBack();
    }

    openChatPopup = () => {
        // document.body.style.overflowY = "hidden";
        this.setState({ showChatPopup: !this.state.showChatPopup });
    };

    openShareModal = () => {
        // document.body.style.overflowY = "hidden";
        this.setState({ showShareModel: !this.state.showShareModel });
    };

    handleImageLoaded() {
        this.setState({ loaded: true });
    }
    deleteObject=()=>{
        this.props.client
        .mutate({
            mutation: deleteBoatRentalProfile,
            variables: {
              BoatRentalID:parseInt(this.state.rentalData.ID),
              UserID:ID
          },
         })
          .then(res => { 
            if(res.data.deleteBoatRentalProfile){
              toast.success("Rental boat profile deleted successfully", {
                onClose: () => this.props.history.push("/"),
              });
            }
          });  
      }

    viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.rentalData.ID,this.state.rentalData.UserID,this.state.rentalData.ObjectType)
    .then((res) => {
        if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({rentalPictures:picturesData.Pictures, 
                        userDetailsPic:picturesData.UserDetails,
                    })
        } else {
        toast.error("Something wents wrong");
        }
    })
    .catch(function (error) {
    
    });
    }
    
    viewLessPictures = () =>{
        let queryResult = this.state.eventPictures
        let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
        filterdata.push(queryResult[i]);
        if (filterdata.length == 4) {
            break;
        }
        }
        this.setState({eventPictures:filterdata})
    }

render() {
    const {rentalData,loaded,totalCount,renderLike,rentalPictures} = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};
    var point = rentalData.Location && formatcoords(parseFloat(rentalData.Location.Latitude), parseFloat(rentalData.Location.Longitude)).format();

  return (
       
    <div className="container">
         <HelmetData metaDetails={this.state.metaData} history={this.props}/>
    
    <div className="row">
        <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2"> 
                <a className="desktop_back" href="javascript:void(0);" onClick={this.goBack}>
                    <img src="../../assets/images/back_icon.png" alt="Back" />
                </a>
             </div>
        </div>
    </div>

    <div className="col-md-12 col-12">
        <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2"> 
            {!loaded && <ImagePlaceholder/> }
                <img src={rentalData.ProfilePicture ? rentalData.ProfilePicture : '../../assets/images/no_image.png'} style={imageStyle} onLoad={this.handleImageLoaded.bind(this)} className="profImg" alt="Back" /> 
                <div className="userSectionSocialSection">
                <div className="like">
                { ID && renderLike &&   <ObjectLikeDislike
                        LikeCount={totalCount} 
                        ObjectType={rentalData.ObjectType} 
                        ReferenceID={rentalData.ID} 
                        status={rentalData.isLike} 
                        ReferenceUserID={rentalData.CreatedBy} 
                        updateOnParent={this.objectLike} 
                        isDetail={true}/>}
                         { ID == null && <> <img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" />  <span> {totalCount}</span> </>}
                </div>
                <div class="bar">|</div>
                                   
                    <div className="comment">
                        <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" />
                            <span> {rentalData.CommentCount ? rentalData.CommentCount : 0}</span>
                        </div>
                <div class="bar">|</div>
                    <div className="forward" onClick={this.openShareModal}>
                        <img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6" />
                            <span> {rentalData.ShareCount ? rentalData.ShareCount : 0}</span>
                    </div>
                    { ID && this.state.showShareModel &&
                        <ShareContent 
                            ObjectType="Rentals"
                            ReferenceID={parseInt(this.state.rentalData.ID)}
                            ReferenceUserID={ID}
                            PostUrl={this.state.BoatRentalUrl}
                            closeHandler={this.openShareModal}
                            shareUrl={url.concat(this.props.match.url)}
                            title={rentalData.Title}
                            quote={rentalData.Title} />
                    }
                </div>    
            </div>

            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
                
                <h2>{rentalData.Title ? rentalData.Title : ''}</h2>
                <div className="date-time-img">
                    <img src="../../assets/images/Rentals.svg" alt="Back" />
                </div>
               
                 <table className="addressTable">
                    <tbody>
                        <tr>
                            <td className="add-icon">
                                <img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2" />
                           </td>
                        <td className="add">{rentalData.Location ? rentalData.Location.Address : ''}</td>
                    </tr>
                    <tr>
                        <td className="add-icon">
                            <img src="../../assets/images/position-icon.png" alt="position-icon" className="pr-2" />
                       </td>
                        <td className="add">{point}</td>
                        <td class="add-icon"><a href="#"><img src="../../assets/images/rating-icon.png" alt="position-icon" className="pr-2" style={{width:"36px"}}/></a></td>
                                <td class="add">
                                <ReactStars
                                    size={15}
                                    onChange={newRating => {
                                    // console.log(newRating);
                                    }}
                                    value={rentalData.Rating}
                                    emptyIcon={<i className="far fa-star" />}
                                    halfIcon={<i className="fa fa-star-half-alt" />}
                                    filledIcon={<i className="fa fa-star" />}
                                    edit={false}
                                    />     
                                </td>
                    </tr>
                    <tr>
                        <td className="add-icon">
                            <img src="../../assets/images/internet-icon.png" alt="position-icon" className="pr-2" />
                    </td>
                            <td className="add">
                                {rentalData.WebsiteUrl}
                            </td>

                        <td className="add">
                            <img src="../../assets/images/facebook-icon.png" alt="position-icon" className="pr-2" />
                        </td>
                    </tr>                  
                </tbody>
                </table>    
                
                <p className="date-time-text">  
                    { rentalData.CreatedBy !== ID && <button className="crewBtn" onClick={this.openChatPopup}>contact Us</button>}
                    {ID && rentalData.CreatedBy == ID && <ConfirmationPopup callBack={this.deleteObject} object={"Rental profile"}/>}
                </p> 
            </div>        
        </div>
    </div>

    <div className="col-md-12">
            <div className="row">
                <p className="about-event-info">
                  {rentalData.Description ? rentalData.Description : ''}
                </p>    
            </div>
        </div>

        {/* <PhotosContent Pictures={rentalPictures} postPictures={rentalData.PostPictures} Details={rentalData} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getBoatRentaalDetails} /> */}

        {rentalData.ObjectType &&
        <PostListParent ReferenceID={rentalData.ID}  ObjectType={rentalData.ObjectType} updatePhotos={this.getBoatRentaalDetails}  />}
        
        <ToastContainer autoClose={1000} />
        { ID && this.state.showChatPopup && 
            <ChatApp
            sender={ID}
            receiver={rentalData.CreatedBy}
            profilePic={rentalData.ProfilePicture}
            title={rentalData.Title}
            objectType="Rentals"
            referenceID={parseInt(this.state.rentalData.ID)}
            popupHandler={this.openChatPopup}
            myID={ID}
            />
        }
     </div>
      );
    }
  }
  

  export default compose(
    connect(null,{}),
    withApollo
)(DisplayRental);
