import React, { Component } from "react";
import { DayName, _formatRentalTime } from "../../services/CommonServices";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";

class SelectHours extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dayName: "",
      startTime: "",
      endTime: "",
      daysFlag: true,
      startTimeFlag: true,
      hourData: [],
    };
  }

  handleChangeDay = async (e) => {
    if (e.target.value == "") {
      await this.setState({
        dayName: "",
        daysFlag: true,
        startTimeFlag: true,
        startTime: "",
        endTime: "",
      });
    } else {
      await this.setState({ dayName: e.target.value, daysFlag: false });
    }
  };

  handleStartTimeChange = (e) => {
    if (e == "") {
      this.setState({ startTime: "", startTimeFlag: true });
    } else {
      this.setState({ startTime: e, startTimeFlag: false });
    }
  };

  handleEndTimeChange = (e) => {
    this.setState({ endTime: e });
  };

  addMore = () => {
    let hourData = this.state.hourData;
    const d_name = this.state.dayName;
    const s_time = _formatRentalTime(this.state.startTime);
    const e_time = _formatRentalTime(this.state.endTime);
    const hrs_data = { Day: d_name, StartTime: s_time, EndTime: e_time };
    var exist = false;
    for (var i = 0; i < hourData.length; i++)
      if (hourData[i].Day == d_name) {
        exist = true;
        break;
      }
    if (!exist) {
      hourData.push(hrs_data);
    } else {
      var index = hourData.findIndex((item) => item.Day == d_name);
      hourData.splice(index, 1, hrs_data);
    }
    this.setState({
      hourData: hourData,
      dayName: "",
      startTime: "",
      endTime: "",
    });
    this.props.callbackHours(hourData);
  };

  deleteTime = (e, day) => {
    e.preventDefault();
    let hourData = this.state.hourData;
    var index = hourData.findIndex((item) => item.Day == day);
    hourData.splice(index, 1);
    this.setState({ hourData: hourData });
    this.props.callbackHours(hourData);
  };

  viewDet = () => {
    // console.log(this.state.hourData);
  };

  render() {
    const {
      dayName,
      endTime,
      startTime,
      daysFlag,
      startTimeFlag,
      hourData,
    } = this.state;

    const format = "h a";
    const days = DayName.map((data, index) => (
      <option
        value={data.name}
        selected={this.state.dayName == data.name}
        key={index}
      >
        {data.name}
      </option>
    ));

    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-4 my-3 selectdiv fas">
            <label htmlFor="hours">Day</label>
            <select
              className="form-control minimal"
              name="dayName"
              onChange={this.handleChangeDay}
            >
              <option value="">Select day </option>
              {days}
            </select>
          </div>

          <div className="col-sm-4 my-2 ">
            <label htmlFor="hours">From</label>
            <TimePicker
              showSecond={false}
              className="form-control"
              format={format}
              onChange={this.handleStartTimeChange}
              value={startTime}
              use12Hours
              showMinute={false}
              disabled={daysFlag}
              placeholder="select start time"
              inputReadOnly
            />
          </div>

          <div className="col-sm-4 my-2 ">
            <label htmlFor="hours">To</label>
            <TimePicker
              className="form-control"
              onChange={this.handleEndTimeChange}
              value={endTime}
              format={format}
              showMinute={false}
              showSecond={false}
              disabled={startTimeFlag}
              placeholder="select end time"
              use12Hours
              inputReadOnly
            />
          </div>
        </div>

        <div className="row">
          <div className="col-sm-2 my-2">
            <p className="text-center">
              <button
                type="button"
                className="btn btn-primary"
                disabled={this.state.endTime == ""}
                onClick={this.addMore}
              >
                Add
              </button>
            </p>
          </div>
        </div>

        <div className="row">
          <div className="col-sm-12 my-3 ">
            {hourData && hourData.length > 0 && (
              <table className="table table-bordered hour_table">
                <thead>
                  <tr>
                    <th scope="col">Day</th>
                    <th scope="col">Start Time</th>
                    <th scope="col">End Time</th>
                    <th scope="col">Action</th>
                  </tr>
                </thead>
                <tbody>
                  {hourData &&
                    hourData.length > 0 &&
                    hourData.map((dt, index) => (
                      <tr key={index}>
                        <td>{dt.Day}</td>
                        <td>{dt.StartTime}</td>
                        <td>{dt.EndTime}</td>
                        <td style={{ textAlign: "center" }}>
                          <i
                            className="fa fa-trash"
                            aria-hidden="true"
                            style={{ cursor: "pointer", color: "#ef5050" }}
                            onClick={(e) => this.deleteTime(e, dt.Day)}
                          ></i>
                        </td>
                      </tr>
                    ))}
                </tbody>
              </table>
            )}
          </div>
          {/* <button type="button" className="btn btn-primary" disabled={this.state.hourData.length == 0} onClick={this.viewDet}>View</button> */}
        </div>
      </React.Fragment>
    );
  }
}

export default SelectHours;
