import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createCrewAction, getCrewByIdAction } from "../../data/actions/CrewAction";
import { getAllEventTypes } from "../../data/query";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import Component from "../location/LocationContent";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class EditCrew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.url,
      crewTitle: "",
      description: "",
      crewID: 0
    };
  }
  componentDidMount = () => {
    window.scrollTo(0, 0);
    this.getCrewById();
  };

  getCrewById = () => {
    this.props
      .getCrewByIdAction(this.props.client, this.state.id)
      .then((res) => {
        let crewDetails =  res.data.getCrewByUrl;
        this.setState({
          crewID: crewDetails.ID,
          crewTitle: crewDetails.Title,
          description: crewDetails.Description,
          eventList: crewDetails.EventType,
          locationDetails: crewDetails.Location,
          startDate: crewDetails.StartDate,
          endDate: crewDetails.EndDate,
          startTime: crewDetails.StartTime,
          endTime: crewDetails.EndTime,
          skillLevel: crewDetails.SkillLevel
        });
      })
      .catch(function (error) {});
  };

  handleChanges = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  createCrew = (e) => {
    const {
      skillLevel,
      startDate,
      endDate,
      startTime,
      endTime,
      locationDetails,
      eventList,
      crewTitle,
      description,
      crewID
    } = this.state;

    const EventType = eventList.map(function (item) {
      return { EventTypeID: item.EventTypeID, Name: item.Name };
    });

    delete locationDetails.__typename;

    this.props
      .createCrewAction(
        this.props.client,
        crewTitle,
        ID,
        description,
        EventType,
        skillLevel,
        startDate,
        endDate,
        startTime,
        endTime,
        locationDetails,
        1,
        crewID
      )
      .then((res) => {
        toast.success("Crew updaated successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        toast.error("Something went wrong", {});
      });
  };

  render() {
    const {
      eventsArray,
      eventsFlag,
      skillLevel,
      startDate,
      endDate,
      startTime,
      endTime,
      city,
      latitude,
      longitude,
      state,
      crewTitle,
      description,
      button
    } = this.state;
    const format = "h:mm a";
    const enabled =
      eventsFlag &&
      skillLevel &&
      startDate &&
      endDate &&
      startTime &&
      endTime &&
      city &&
      state &&
      latitude &&
      longitude &&
      crewTitle &&
      description &&
      button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a className="float-left mobile_back">
                <img src="images/back_icon.png" alt="Back" />
              </a>
              <h1>Edit Crew</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="crewTitle">Crew Title</label>
                    <input
                      type="text"
                      className="form-control"
                      id="crewTitle"
                      placeholder="Crew Title"
                      name="crewTitle"
                      value={crewTitle}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="description"
                      value={description}
                      onChange={this.handleChanges}
                    ></textarea>
                  </div>
                </div>
                {/* <hr />        */}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        onClick={this.createCrew}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { createCrewAction, getCrewByIdAction }),
  withApollo
)(EditCrew);
