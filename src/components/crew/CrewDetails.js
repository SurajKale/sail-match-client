import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getCrewByIdAction } from "../../data/actions/CrewAction";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";
import ImagePlaceholder from "../../services/ImagePlaceholder";
import { deleteCrew } from "../../data/mutation";
import { ToastContainer, toast } from "react-toastify";
import ConfirmationPopup from "../common/ConfirmationPopup";
import ObjectLikeDislike from "../common/ObjectLikeDislike";
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";
import PostListParent from "../common/PostListParent";
import  formatcoords from 'formatcoords';

const LocalStorageData = require("../../services/LocalStorageData");
const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class CrewDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: this.props.match.params.id,
      crewDetails: {},
      eventList: [],
      locationDetails: {},
      startTime: "",
      endTime: "",
      ProfileImage: "",
      loaded: false,
      totalCount:"",

      metaData:[],
      showChatPopup: false,
      showShareModel: false,
      cCount: 0
    };
  }

  componentDidMount = () => {
    window.scrollTo(0, 0);
    this.getCrewById();
    var time = new Date("2010-01-13T18:31:16Z").toLocaleString();
  };

  getCrewById = () => {
    let CrewId = this.props.match.params.id;
    // let CrewId = 13
    this.props
      .getCrewByIdAction(this.props.client, CrewId)
      .then((res) => {
        var metaData = res.data.getCrewByUrl;
        var metaArr = [];
        metaArr.push({
         title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
         keywords : metaData.ObjectType, SITE_STATIC_NAME,
         description : metaData.Description ? metaData.Description : '',
         og_url : window.location.href,
         og_title : metaData.Title ?  metaData.Title : SITE_STATIC_NAME,
         og_description : metaData.Description ? metaData.Description : '',
         canonical_url : window.location.href,
         og_image : metaData.ProfileImage.ProfileImage ? metaData.ProfileImage.ProfileImage : SITELOGO,
         amp_url : window.location.href,
        })

        this.setState({
          metaData:metaArr[0],
          crewDetails: res.data.getCrewByUrl,
          isLike:res.data.getCrewByUrl.isLike,
          renderLike:true,
          eventList: res.data.getCrewByUrl.EventType,
          locationDetails: res.data.getCrewByUrl.Location,
          startDate: res.data.getCrewByUrl.StartDate,
          endDate: res.data.getCrewByUrl.EndDate,
          startTime: res.data.getCrewByUrl.StartTime,
          endTime: res.data.getCrewByUrl.EndTime,
          ProfileImage: res.data.getCrewByUrl.ProfileImage.ProfileImage,
          totalCount:res.data.getCrewByUrl.LikeCount,
          cCount: res.data.getCrewByUrl.CommentCount
        });
      })
      .catch(function (error) {});
  };
  deleteObject = () => {
    this.props.client
      .mutate({
        mutation: deleteCrew,
        variables: {
          CrewID: parseInt(this.state.crewDetails.ID),
          UserID: ID,
        },
      })
      .then((res) => {
        if (res.data.deleteCrew) {
          toast.success("Crew deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  handleImageLoaded() {
    this.setState({ loaded: true });
  };

  objectLike= (count) =>{
    console.log(count);
    this.setState({totalCount:count})
  };

  openChatPopup = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  goToEditPage = (url) => {
    this.props.history.push('/edit-crew/'+this.state.id)
  }

  render() {
    const {
      crewDetails,
      eventList,
      locationDetails,
      startTime,
      endTime,
      startDate,
      endDate,
      ProfileImage,
      loaded,
      totalCount,
      renderLike,
      isLike,
      cCount
    } = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};
    var point = formatcoords(parseFloat(locationDetails.Latitude), parseFloat(locationDetails.Longitude)).format();

    return (
      <div className="container">
         <HelmetData metaDetails={this.state.metaData}/>

        <div className="row">
          <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a className="desktop_back cursor-pointer" onClick={this.goBack}>
                <img src="../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
        </div>
        <div className="col-md-12 col-12">
          <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2">
              {!loaded && <ImagePlaceholder />}
              <img
                src={ProfileImage ? ProfileImage : "../../assets/images/4.png"}
                alt="Back"
                style={imageStyle}
                className="profImg"
                onLoad={this.handleImageLoaded.bind(this)}
              />
              <div className="userSectionSocialSection">
                <div className="like">
              {ID && renderLike && <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={crewDetails.ObjectType} 
                ReferenceID={crewDetails.ID} 
                status={crewDetails.isLike} 
                ReferenceUserID={crewDetails.MemberID} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                  { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
                </div>
                <div class="bar">|</div>
                <div className="comment">
                  <img
                    src="../../assets/images/comment-icon.png"
                    alt="comment-icon"
                    className="pl-6"
                  />
                  <span> {cCount}</span>
                </div>
                <div class="bar">|</div>
                <div className="forward" onClick={this.openShareModal}>
                  <img
                    src="../../assets/images/forward-icon.png"
                    alt="forward-icon"
                    className="pl-6"
                  />
                  <span> {crewDetails.ShareCount || 0}</span>
                </div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                  ObjectType="Crews"
                  ReferenceID={parseInt(this.state.crewDetails.ID)}
                  ReferenceUserID={ID}
                  PostUrl={this.props.match.params.id}
                  closeHandler={this.openShareModal}
                  shareUrl={url.concat(this.props.match.url)}
                  title={crewDetails.Title}
                  quote={crewDetails.Title} />
                }
              </div>
            </div>
            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {crewDetails && crewDetails.Title}
                {ID && crewDetails.MemberID === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img">
                <img src="../../assets/images/Crew.svg" alt="Back" />
              </div>
              <p className="" id="date-time-text">
                <span>From :</span>
                <span>
                  {_formatDate(startDate)}, {_formatTime(startTime)}
                </span>{" "}
                - To :
                <span>
                  {" "}
                  {_formatDate(endDate)}, {_formatTime(endTime)}
                </span>
              </p>
              <p className="">
                {" "}
                <img
                  src="../../assets/images/location-icon.png"
                  alt="location-icon"
                  className="pr-2"
                />{" "}
                <span> {crewDetails && locationDetails.Address}</span>{" "}
              </p>
              <p className="">
                {" "}
                <img
                  src="../../assets/images/position-icon.png"
                  alt="position-icon"
                  className="pr-2"
                />{" "}
                <span>
                  {" "}
                  {crewDetails &&
                    point
                    }
                </span>{" "}
              </p>
              <p className="date-time-text">
                <button className="crewBtn">My Profile</button>
                { crewDetails.MemberID !== ID && <button className="crewBtn" onClick={this.openChatPopup}> Contact Me</button>}
                {ID && crewDetails.MemberID === ID && (
                  <ConfirmationPopup
                    callBack={this.deleteObject}
                    object={"Crew"}
                  />
                )}
              </p>
              <hr />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12">
            <h5 className="about-event">{crewDetails && crewDetails.Title} </h5>
            <p className="about-event-info">
              {crewDetails && crewDetails.Description}
            </p>
          </div>
        </div>

        <div className="row crewAttributes">
          <div className="col-md-3">
            <h5 className="about-event">Activity Type</h5>
          </div>
          <div className="col-md-9">
            {crewDetails &&
              eventList.map((type) => {
                return (
                  <button key={type.EventTypeID} className="crewBtn">
                    {type.Name}
                  </button>
                );
              })}
          </div>
        </div>

        <div className="row crewAttributes">
          <div className="col-md-3">
            <h5 className="about-event">My Skill Level</h5>
          </div>
          <div className="col-md-9">
            <button className="crewBtn">
              {crewDetails && crewDetails.SkillLevel}
            </button>
          </div>
        </div>
        <ToastContainer autoClose={1000}/>
        { ID && this.state.showChatPopup && 
          <ChatApp
            sender={ID}
            receiver={crewDetails.MemberID}
            profilePic={ProfileImage}
            title={crewDetails.Title}
            objectType="Crews"
            referenceID={parseInt(this.state.crewDetails.ID)}
            popupHandler={this.openChatPopup}
            myID={ID}
          />
        }

      {crewDetails.ObjectType &&
        <PostListParent ReferenceID={crewDetails.ID} ObjectType={crewDetails.ObjectType} />}

      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { getCrewByIdAction }),
  withApollo
)(CrewDetails);
