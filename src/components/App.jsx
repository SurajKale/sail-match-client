import React from 'react';
import Header from './HedearContent';

const AppLayout = ({children}) => {
        return (
            <div>
                <Header />
                <div className="page-body">
                    {children}
                </div>
            </div>
        );
}

export default AppLayout;