import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

import { getCourseCreateAction } from "../../data/actions";
import { getCourseMasterList } from '../../data/query';

import LocationComponent from '../location/LocationContent'
import ImageUploads from "../match/MutipleImageUploads";
import AddVideo from "../common/Video/AddVideo";

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;
if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class CreateCourseContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      startDate: new Date(),
      endDate: new Date(),
      startTime: "",
      endTime: "",
      city: "",
      state: "",
      zipcode: "",
      description: "",
      gpsCoordinate: "",
      locationObject: {},
      courseName: "",
      courseInstructor: "",
      courseDesription: "",
      nameofCourse: [],
      prerequisite: [],
      profilePic: "",
      selectedC: [],
      selectedP: [],
      allCourse: [],
      preAll: {},
      button: true,
      videos: []
    }
  }

  componentDidMount = () =>{
    this.getAllCourseTypes()
  }

  getAllCourseTypes = () => {
    this.props.client
    .query({
        query: getCourseMasterList,
      })
    .then(res => {
      if(res.data.getCourseMasterList) {
        this.setState({
          allCourse: res.data.getCourseMasterList,
          nameofCourse: res.data.getCourseMasterList.map(({ID, Title}) => ({Id: ID, CourseName: Title}))
        })
      } else {
        toast.error("Something wents wrong")
      } } ) 
  }

  handlePrerequiresticCourse = async (category, i) => {
    let oldEvents = this.state.prerequisite.slice();
    if(category.Title === "None") {
      oldEvents.forEach(item => {
          item.isSelected = false;
      });
      oldEvents[i].isSelected = true;
    } else {
      oldEvents.forEach(item => {
        if (item.Id === category.Id) {
          item.isSelected = !item.isSelected;
        }
        if (item.Title === "None") {
          item.isSelected = false;
        }
      });
    }
    await this.setState({
      prerequisite: oldEvents
    });    
  }

  handleChange = (e) => { this.setState({ [e.target.name] : e.target.value })}
  handleStartDateChange = date => { this.setState({ startDate: date, endDate: date });};
  handleEndDateChange = date => { this.setState({ endDate: date });};
  handleStartTimeChange = (value) => { this.setState({ startTime: value })}
  handleEndTimeChange = (value) => { this.setState({ endTime: value })}
  setFile = (file) => { this.toBase64(file).then(result => {
    this.setState({ profilePic: result})});    
  }
  getvideoUrls = (data) => { this.setState({ videos: data }) }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  callback = (data) => {
    let address = data.mainPlace.concat(data.city, "," , data.state, ",", data.zipCode)
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
        City: data.city,
        State: data.state,
        Country: data.country,
        ZipCode: data.zipCode,
        StreetName: data.mainPlace
      },
      gpsCoordinate:data.latitude+'"N'+data.longitude+'"E',
      city:data.city,
      state:data.state,
      zipCode:data.zipCode,
    })
  } 

  filterValue = async (obj, key, value) => {
    await this.setState({
      preAll: obj.find(function(v){ return v[key] === value})
    })
    this.state.preAll.Prerequisite.forEach(item => {
      item.isSelected = false;
    });
    this.setState({
      prerequisite: this.state.preAll.Prerequisite.map(({ID, Title, isSelected}) => ({Id: ID, Title: Title, isSelected: isSelected}))
    })
  }

  handleNameCourse = async (id, name) => {
    await this.setState({
      selectedC: { ID: id, CourseName: name }
    })
    this.filterValue(this.state.allCourse, "ID", this.state.selectedC.ID);
  }

  getImagesArray = (baseArray, imagesArray) => {
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray,
    });
  };

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
  }

  setProfilePic = (pic) => { this.setState({ profilePic: pic}) }

  createCourse = async (e) => {
    this.setState({ button : !this.state.button })
    
    const { startDate, endDate, startTime, endTime, courseInstructor, videos,
        courseDesription, profilePic, locationObject, selectedC, prerequisite, images } = this.state;

    let courseObject = {
      startDate, endDate, startTime, endTime, courseInstructor, videos,
        courseDesription, profilePic, locationObject, selectedC, images
    }

    var options = prerequisite.filter(function(item) {
      return item.isSelected;
    });
    
    const courseprerequisite = options.map(function(item) {  
      return item.Title
      })

    await this.props.getCourseCreateAction(this.props.client, courseObject, ID, courseprerequisite).then(res => {
      toast.success("Course created successfully", {
          onClose: () => this.props.history.push('/')
      });
    }).catch(function (error) {
        // console.log(error);
    })
  }

  goBack = ()=> { this.props.history.goBack();}

  render() {
    const { startDate, endDate, startTime, endTime, profilePic, city, state,
      zipCode, gpsCoordinate, nameofCourse, selectedC, prerequisite,
      courseDesription, courseInstructor, locationObject, button } = this.state;
    
    const enabled = startDate && endDate && startTime && endTime && profilePic &&
      locationObject && nameofCourse && selectedC &&
      courseDesription && courseInstructor && button
    
    const format = 'h:mm a';
    const label = profilePic ? 'Profile image successfully added' : 'Click or drop your image here'

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2"> <a className="desktop_back" href="#!" style={{ cursor : "pointer" }} onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
            <div className="col-md-6 mb-2"><div className="text-left">
              <a href="#!" onClick={this.goBack} className="float-left mobile_back"><img src="../../assets/images/back_icon.png" alt="Back"/></a>
              <h1>Edit Course</h1>
              <form className="mt-4">

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="courseName">Name of Course</label>
                    <div className="">
                      { nameofCourse && nameofCourse.map(type => {
                        return (                   
                          <input key={type.Id} type="button" className={ selectedC.CourseName === type.CourseName ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} name="boatType" value={type.CourseName} onClick={() => {this.handleNameCourse(type.Id, type.CourseName)}} />
                        );
                      })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="coursePreName">Prerequisite</label>
                    <div className="">
                      { prerequisite && prerequisite.map((type, index) => {
                          return (                   
                              <input
                              type="button"
                              key={type.ID}
                              className={`${type.isSelected ? "btn-primary btn gender" : "btn btn-outline-secondary gender"}`}
                              onClick={(e) => this.handlePrerequiresticCourse(type, index)}
                              value={type.Title} />
                          );
                        })
                      }
                    </div>
                  </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="eventStartDate">Course Start Date</label>
                    {/* <input type="text" className="form-control" id="courseStartDate" placeholder="Course Start Date" /> */}
                    <DatePicker
                      id="startDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{width:"250px"}}
                      selected={startDate}
                      minDate={startDate}
                      onChange={this.handleStartDateChange}
                    />
                  </div>
                  
                  <div className="col-sm-6 my-2">
                    <label htmlFor="courseEndDate">Course End Date</label>
                    {/* <input type="text" className="form-control" id="courseEndDate" placeholder="Course End Date" /> */}
                    <DatePicker
                      id="endDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{width:"250px"}}
                      selected={endDate}
                      minDate={startDate}
                      onChange={this.handleEndDateChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="courseStartTime">Course Start Time</label>
                    {/* <input type="text" className="form-control" id="courseStartTime" placeholder="Course Start Time" /> */}
                    <TimePicker
                      id="eventStartTime"
                      name="startTime"
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleStartTimeChange}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                  
                  <div className="col-sm-6 my-2">
                    <label htmlFor="courseEndTime">Course End Time</label>
                    {/* <input type="text" className="form-control" id="courseEndTime" placeholder="Course End Time" /> */}
                    <TimePicker
                      id="eventEndTime"
                      name="endTime"
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleEndTimeChange}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                </div>

                <LocationComponent callback={this.callback} />
                <div className="row">
                  <div className="col-sm-6 my-2">
                      <label htmlFor="city">City</label>
                      <input readOnly type="text" className="form-control" id="city" placeholder="City" name="city" value={city} />
                  </div>       
                
                  <div className="col-sm-6 my-2">
                      <label htmlFor="state">State</label>
                      <input readOnly type="text" className="form-control" id="state" placeholder="State" name="state" value={state} />
                  </div>       
                </div>
            
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="zipCode">Zipcode</label>
                    <input readOnly type="text" className="form-control" id="zipCode" placeholder="Zipcode" name="zipCode" value={zipCode} onChange={this.handleChanges} />
                  </div>       
            
                  <div className="col-sm-6 my-2">
                      <label htmlFor="gpsCoordinate">GPS Coordinates</label>
                      <input readOnly type="text" className="form-control" id="gpsCoordinate" placeholder="GPS Coordinates" name="gpsCoordinate" value={gpsCoordinate} />
                  </div>       
                </div>

                <div className="row">
                  {/* <div className="col-sm-6 my-2">
                    <label htmlFor="courseName">Course Name</label>
                    <input type="text" className="form-control" id="courseName" name="courseName" placeholder="Course Name" onChange={this.handleChange} />
                  </div>    */}
                  <div className="col-sm my-2">
                    <label htmlFor="instructor">Instructor</label>
                    <input type="text" className="form-control" id="instructor" name="courseInstructor" placeholder="Instructor" onChange={this.handleChange} />
                  </div>       
                  
                </div>
      
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea className="form-control" id="description" rows="3" name="courseDesription" onChange={this.handleChange} />
                  </div>
                </div>
                <hr />

                <AddVideo getvideoUrls={this.getvideoUrls} />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="photo">Photo</label>
                    <ImageUploads callback={this.getImagesArray}/>
                  </div>   
                </div>

                <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                  {
                    this.state.imagesRender && this.state.imagesRender.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                  </div>
                  {
                    this.state.imagesRender && this.state.imagesRender.length > 0 && !profilePic &&
                    <div className="col-sm-12 my-2 pt-2 text-center">
                      <i className="text-danger h6">Please select profile picture from above images</i>
                    </div>
                  }
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  {profilePic && <label for="profilePicture">Profile Picture</label> }
                    {/* <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    /> */}
                  </div>
                </div>
              
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center"><button disabled={!enabled} type="button" onClick={this.createCourse} className="btn btn-primary save_profile">Save</button></p>
                  </div>
                </div>
              </form>
            </div>
          <div className="col-md-3 mb-2"></div>
        </div>
      </div>
      <ToastContainer autoClose={1000} />
    </div>
  )}
}

export default compose(
  connect(null, { getCourseCreateAction }
  ),
  withApollo
)(CreateCourseContent);
