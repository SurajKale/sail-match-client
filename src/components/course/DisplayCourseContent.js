import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getCourseDetailsByID, clearUserData,getViewAllPictures  } from "../../data/actions";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";
import ImagePlaceholder from "../../services/ImagePlaceholder";
import {deleteCourseMaster} from '../../data/mutation'
import ConfirmationPopup from "../common/ConfirmationPopup";
import ObjectLikeDislike from "../common/ObjectLikeDislike";
import PhotosContent from '../common/PhotosContent';
import VideoContent from '../common/VideosContent';
import PostListParent from "../common/PostListParent";
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";
import  formatcoords from 'formatcoords';

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class DisplayCourseContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.url,
      courseTitle: "",
      courseName: "",
      courseDescription: "",
      startTime: "",
      endTime: "",
      profilePic: "",
      address: "",
      location: "",
      Latitude: "",
      Longitude: "",
      prerequisite: "",
      loaded: false,
      eventMatch: {},
      courseDeatils:{},
      totalCount:"",
      images: [],
      userDetailsPic:{},
      metaData:[],
      showChatPopup: false,
      showShareModel: false,
      cCount: 0
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getCourseData()
  }

  getCourseData = async () => {
    await this.props
      .getCourseDetailsByID(this.props.client, this.state.url)
      .then((res) => {
        if (res.data.getCourseByUrl !== null) {
          let courseData = res.data.getCourseByUrl;

          var metaArr = [];
          metaArr.push({
           title : courseData.Title ?  courseData.Title : SITE_STATIC_NAME,
           keywords : courseData.ObjectType, SITE_STATIC_NAME,
           description : courseData.Description ? courseData.Description : '',
           og_url : window.location.href,
           og_title : courseData.Title ?  courseData.Title : SITE_STATIC_NAME,
           og_description : courseData.Description ? courseData.Description : '',
           canonical_url : window.location.href,
           og_image : courseData.ProfilePicture ? courseData.ProfilePicture : SITELOGO,
           amp_url : window.location.href,
          })

          this.setState({metaData:metaArr[0],
            courseDeatils:courseData,
            renderLike:true,
            isLike:courseData.isLike,
            courseID:courseData.ID,
            courseTitle: courseData.Title,
            courseDescription: courseData.Description,
            startDate: courseData.StartDate,
            endDate: courseData.EndDate,
            startTime: courseData.StartTime,
            endTime: courseData.EndTime,
            location: courseData.Location.MapLocation,
            Latitude: courseData.Location.Latitude,
            Longitude: courseData.Location.Longitude,
            address: courseData.Location.Address,
            profilePic: courseData.ProfilePicture,
            prerequisite: courseData.Prerequisite,
            courseName: courseData.NameOfCourse[0].CourseName,
            eventMatch: courseData.CourseMatch,
            userID:courseData.UserID,
            totalCount:courseData.LikeCount,
            images: courseData.Pictures,
            userDetailsPic:courseData.UserDetails,
            cCount: courseData.CommentCount
          });
        } else {
          toast.error("Something wents wrong");
        }
      })
    .catch(function (error) {
      // console.log(error);
    });
  }

  openChatPopup = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  handleImageLoaded = () => {
    this.setState({ loaded: true });
  };
  deleteObject=()=>{
    this.props.client
    .mutate({
        mutation: deleteCourseMaster,
        variables: {
          CourseID:parseInt(this.state.courseID),
          UserID:ID
      },
     })
      .then(res => { 
        if(res.data.deleteCourseMaster){
          toast.success("Course deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
      });  
  }
  objectLike= (count) =>{
    console.log(count);
    this.setState({totalCount:count})
  }
  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.courseID,this.state.userID,this.state.courseDeatils.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
      this.setState({images:picturesData.Pictures, 
                     userDetailsPic:picturesData.UserDetails,
                    })
        console.log(picturesData,"picturesData");
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    
    });
   }
   viewLessPictures = () =>{
    let queryResult = this.state.images
    let filterdata = [];
 for (let i = 0; i <  queryResult.length; i++) {         
     filterdata.push(queryResult[i]);
     if (filterdata.length == 4) {
       break;
     }
   }
   this.setState({images:filterdata})
}

  goToEditPage = (url) => {
    this.props.history.push('/edit-course/'+this.state.url)
  }

  render() {
    const {
      courseTitle,
      courseDescription,
      courseName,
      prerequisite,
      startDate,
      endDate,
      startTime,
      endTime,
      profilePic,
      address,
      Latitude,
      Longitude,
      loaded,
      courseDeatils,
      totalCount,
      courseID,
      userID,
      images,
      renderLike,
      isLike,
      cCount
    } = this.state;

    const imageStyle = !loaded ? { display: "none" } : {};
    var point = formatcoords(parseFloat(Latitude), parseFloat(Longitude)).format();
  
    return (
      <div className="container">
         <HelmetData metaDetails={this.state.metaData} history={this.props}/>

        <div className="row">
          <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a className="desktop_back cursor-pointer" onClick={this.goBack}>
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
        </div>
        <div className="col-md-12 col-12">
          <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2">
              {!loaded && <ImagePlaceholder />}
              <img
                src={
                  profilePic
                    ? profilePic
                    : "../../assets/images/photo-1535024966840-e7424dc2635b.png"
                }
                style={imageStyle}
                className="profImg sideImg"
                alt="Back"
                onLoad={this.handleImageLoaded}
              />
              <div className="userSectionSocialSection">
                <div className="like">
               { ID && renderLike && <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={courseDeatils.ObjectType} 
                ReferenceID={courseID} 
                status={courseDeatils.isLike} 
                ReferenceUserID={userID} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                 { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
                </div>
                <div class="bar">|</div>
                <div className="comment">
                  <img
                    src="../../assets/images/comment-icon.png"
                    alt="comment-icon"
                    className="pl-6"
                  />
                  <span> {cCount}</span>
                </div>
                <div class="bar">|</div>
                <div className="forward" onClick={this.openShareModal}>
                  <img
                    src="../../assets/images/forward-icon.png"
                    alt="forward-icon"
                    className="pl-6"
                  />
                  <span> {courseDeatils.ShareCount || 0}</span>
                </div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                    ObjectType="Courses"
                    ReferenceID={parseInt(this.state.courseID)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={courseName}
                    quote={courseName} />
                }
              </div>
            </div>
            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {courseName}
                {ID && this.state.userID === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img">
                <img src="../../assets/images/Courses.svg" alt="Back" />
              </div>
              <p className="date-time-text" id="date-time-text">
                <span>From :</span>
                <span>
                  {" "}
                  {_formatDate(startDate)}, {_formatTime(startTime)}
                </span>{" "}
                - To :
                <span>
                  {" "}
                  {_formatDate(endDate)}, {_formatTime(endTime)}
                </span>
              </p>
              <table className="addressTable">
                <tr>
                  <td className="add-icon">
                    <img
                      src="../../assets/images/location-icon.png"
                      alt="location-icon"
                      className="pr-2"
                    />
                  </td>
                  <td className="add">{address}</td>
                </tr>
                <tr>
                  <td className="add-icon">
                    <img
                      src="../../assets/images/position-icon.png"
                      alt="position-icon"
                      className="pr-2"
                    />
                  </td>
                  <td className="add">
                    {point}
                  </td>
                </tr>
              </table>
              <div className="date-time-text">
                { this.state.userID !== ID && <button className="crewBtn" onClick={this.openChatPopup}>Contact Organizer</button>}
                {ID && this.state.userID == ID &&<ConfirmationPopup callBack={this.deleteObject} object={"Course"}/>} 
              </div>  
              <hr />
            </div>
          </div>
        </div>
        <div className="row crewAttributes">
          <div className="col-md-2">
            <h5 className="about-event">Prerequisite</h5>
          </div>
          <div className="col-md-10">
            { prerequisite && prerequisite.map((item,index) => {
              return <button key={index} className="crewBtn">{item}</button>
            })}
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">{courseTitle}</h5>
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <p className="about-event-info">{courseDescription}</p>
          </div>
        </div>

        <PhotosContent Pictures={images} Details={this.state.courseDeatils} postPictures={this.state.courseDeatils.PostPictures} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getCourseData} />
        <VideoContent videos={courseDeatils.Videos} />
        {this.state.eventMatch &&  <div>
          <div className="col-md-12">
          <div class="row">
            <h5 className="about-event">
            {this.state.eventMatch && " Match for Courses"}
              <span>
                {this.state.eventMatch && this.state.eventMatch.length}
              </span>
            </h5>
           </div>
          </div>
          {this.state.eventMatch && (
            <div class="col-sm-3 mb-5">
              <div class="post_single">
                <img
                  src={this.state.eventMatch.ProfilePicture}
                  alt=""
                  style={{ borderRadius: 15 }}
                />
                <h5>
                  <a href="#">
                    {this.state.eventMatch.Title &&
                    this.state.eventMatch.Title.length > 15
                      ? this.state.eventMatch.Title.substring(0, 15) + "..."
                      : this.state.eventMatch.Title}
                  </a>
                </h5>
                <p>
                  {_formatDate(startDate)}, {_formatTime(startTime)}
                </p>
                {/* <div class="author"><span class="author_name"><img src="../../assets/images/4.png" alt=""/> Frank Hail</span><span class="category"><a href="#"><img src="../../assets/images/Crew.svg" alt="home"/></a></span></div> */}
                <div class="divider">
                  <hr />
                </div>
                <div class="post_footer">
                  <span class="col text-left">
                    <i class="far fa-heart"></i> 0
                  </span>
                  <span class="col text-center">
                    <i class="far fa-comment-alt"></i> 0
                  </span>
                  <span class="col text-right">
                    <i class="fas fa-share"></i> 0
                  </span>
                </div>
              </div>
            </div>
             
          )}
        </div>}
          {courseDeatils.ObjectType &&
          <PostListParent ReferenceID={parseInt(this.state.courseID)} ObjectType={courseDeatils.ObjectType} updatePhotos={this.getCourseData} />}
        <ToastContainer autoClose={1000} />
        { ID && this.state.showChatPopup && 
          <ChatApp
            sender={ID}
            receiver={this.state.userID}
            profilePic={profilePic}
            title={courseName}
            objectType="Courses"
            referenceID={parseInt(this.state.courseID)}
            popupHandler={this.openChatPopup}
            myID={ID}
          />
        }
      </div>
    );
  }
}

export default compose(
  connect(null, { getCourseDetailsByID , getViewAllPictures }),
  withApollo
)(DisplayCourseContent);
