import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from 'react-drop-zone'
import 'react-drop-zone/dist/styles.css'
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { getCourseCreateAction, getCourseDetailsByID } from "../../data/actions";
import { getCourseMasterList } from '../../data/query';
import LocationComponent from '../location/LocationContent'
import ImageUploads from "../match/MutipleImageUploads";

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class EditCourseContent extends Component {
    constructor(props) {
      super(props)
      this.state = {
        courseID: 0,
        url: this.props.match.params.url,
        startDate: new Date(),
        endDate: new Date(),
        startTime: "",
        endTime: "",
        locationObject: {},
        courseInstructor: "",
        courseDescription: "",
        prerequisite: [],
        profilePic: "",
        selectedC: []
      }
    }

    async componentDidMount() {
      window.scrollTo(0, 0);
      this.getCourseData()
    }
  
    getCourseData = async () => {
      await this.props
        .getCourseDetailsByID(this.props.client, this.state.url)
        .then((res) => {
          if (res.data.getCourseByUrl !== null) {
            let courseData = res.data.getCourseByUrl;
  
            this.setState({
              courseID:courseData.ID,
              startDate: courseData.StartDate,
              endDate: courseData.EndDate,
              startTime: courseData.StartTime,
              endTime: courseData.EndTime,
              courseInstructor: courseData.Instructor.Name,
              courseDescription: courseData.Description,
              profilePic: courseData.ProfilePicture,
              prerequisite: courseData.Prerequisite,
              selectedC: courseData.NameOfCourse,
              locationObject: courseData.Location
            });
          } else {
            toast.error("Something wents wrong");
          }
        })
      .catch(function (error) {
        // console.log(error);
      });
    }

    handleChange = (e) => {
      this.setState({
        [e.target.name] : e.target.value
      })
    }

    setFile = (file) => {
      this.toBase64(file).then(result => {
        this.setState({
          profilePic: result
        })
      });    
    }

    toBase64 = file => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
    });

    createCourse = async (e) => {
     
      const { startDate, endDate, startTime, endTime, courseInstructor,
          courseDesription, profilePic, locationObject, selectedC, prerequisite, images, courseID } = this.state;

      var selctedData = selectedC.map(({CourseName, ID }) => ({CourseName: CourseName, ID: ID }));
      
      let courseObject = {
        startDate, endDate, startTime, endTime, courseInstructor,
          courseDesription, profilePic, locationObject, selectedC, images
      }

      delete locationObject.__typename;

      await this.props.getCourseCreateAction(this.props.client, courseObject, ID, prerequisite, courseID, selctedData).then(res => {
          // console.log(res);
          toast.success("Course updated successfully", {
              onClose: () => this.props.history.push('/')
          });
      }).catch(function (error) {
          // console.log(error);
      })
    }

    goBack = ()=> {
      this.props.history.goBack();
    }

    render() {
      const { profilePic, courseDescription, courseInstructor } = this.state;
      
      const label = profilePic ? 'Profile image successfully added' : 'Click or drop your image here'

      return (
        <div className="container">
          <div className="row pt-4">
            <div className="col-md-3 mb-2"> <a className="desktop_back" href="#!" style={{ cursor : "pointer" }} onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
              <div className="col-md-6 mb-2"><div className="text-left">
                <a href="#!" onClick={this.goBack} className="float-left mobile_back"><img src="../../assets/images/back_icon.png" alt="Back"/></a>
                <h1>Create Course</h1>
                <form className="mt-4">
                  <div className="row">
                    {/* <div className="col-sm-6 my-2">
                      <label htmlFor="courseName">Course Name</label>
                      <input type="text" className="form-control" id="courseName" name="courseName" placeholder="Course Name" onChange={this.handleChange} />
                    </div>    */}
                    <div className="col-sm my-2">
                      <label htmlFor="instructor">Instructor</label>
                      <input type="text" className="form-control" id="instructor" name="courseInstructor" defaultValue={courseInstructor} placeholder="Instructor" onChange={this.handleChange} />
                    </div>       
                    
                  </div>
        
                  <div className="row">
                    <div className="col-sm-12 my-2">
                      <label htmlFor="description">Description</label>
                      <textarea className="form-control" id="description" rows="3" name="courseDesription" defaultValue={courseDescription} onChange={this.handleChange} />
                    </div>
                  </div>

                  <div className="row">
                    <div className="col-sm-12 my-2">
                    {profilePic && <label for="profilePicture">Profile Picture</label> }
                      <StyledDropZone 
                        className="form-control"
                        onDrop={this.setFile}
                        label={label}
                      />
                    </div>
                  </div>
                
                  {profilePic &&
                  <div className="row">
                    <div className="col-sm-12 my-2 text-center">
                          <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                    </div>
                  </div>}

                  <hr />
                  <div className="row">
                    <div className="col-sm-12 my-2">
                      <p className="text-center"><button type="button" onClick={this.createCourse} className="btn btn-primary save_profile">Save</button></p>
                    </div>
                  </div>
                </form>
              </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
  )}
}

export default compose(
  connect(null, { getCourseCreateAction, getCourseDetailsByID }
  ),
  withApollo
)(EditCourseContent);
