import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { PropTypes } from "prop-types";
import Popup from "reactjs-popup";
import ReactTooltip from 'react-tooltip';

import { getAllObjectTypesPopup } from "../data/query";

const LocalStorageData = require("../services/LocalStorageData");
let ID = null;
let SchoolAdmin = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
if (LocalStorageData.SchoolAdmin) {
  SchoolAdmin = LocalStorageData.SchoolAdmin;
}

class PostPopup extends Component {
  state = {
    user: {},
    objectsData: [],
    toogleDropdown: false,
  };
  
  componentDidMount () {
    this.getAllObjectTypesPopup()
  }

  getAllObjectTypesPopup = () => {
    this.props.client
      .query({
          query: getAllObjectTypesPopup,
          fetchPolicy: "network-only"
      })
      .then(res => {
          this.setState({ objectsData: res.data.getAllObjectTypes })
      }
    );
  }

  goToRoute = (path) => {
    this.props.closePopup();
    path == "Crew" && this.props.history.push("/add-crew");
    path == "Event" && this.props.history.push("/create-event");
    path == "Boat" && this.props.history.push("/create-boat");
    path == "Ride" && this.props.history.push("/add-ride");
    path == "Match" && this.props.history.push("/create-match");
    path == "Skill" && this.props.history.push("/add-skill");
    path == "Service" && this.props.history.push("/create-service");
    path == "Course" && this.props.history.push("/add-course");
  };

  handleRedirections = (path) => {
    this.props.history.push("/login");
    localStorage.setItem("currentRoute", JSON.stringify(path));
  };

  render() {
    const { doShow, closePopup} = this.props;
    const { objectsData } = this.state;

    return (
      <Popup open={doShow} onClose={closePopup}>
        {/* id="notificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" */}
        <div id="notificationModal" style={{ paddingLeft: 17 }}>
          <div className="modal-dialog postModel">
            <div className="modal-content">
              <div className="closeModel" onClick={closePopup}>
                <img
                  src="../assets/images/close-icone.png"
                  width="50"
                  height="50"
                  className="postImg img-fluid close1"
                  data-dismiss="modal"
                  aria-label="Close"
                  alt="Matches"
                />
              </div>
              <div className="modal-body1">
                <div className="col-md-12">
                  <h1 className="postTitle">What do you want to post?</h1>
                  <table className="table table-bordered postTable">
                  <tbody>
                    <tr>
                      <td colSpan="2">
                        <div
                          className="postHeaderLeft cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Match")
                              : this.handleRedirections("Match")
                          }
                        >
                          <img
                            src={"../assets/images/post-match-icon.png"}
                            className="postImg img-fluid"
                            alt="Matches"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[0].Description: null}
                          />{" "}
                          I want to post a match
                        </div>
                        <div
                          className="postHeaderRight cursor-pointer"
                          data-tip={objectsData && objectsData.length > 0 ? objectsData[0].Description: null}
                        >
                          <img
                            src={"../assets/images/post-question-icon.png"}
                            className="postImg img-fluid"
                            alt="Question"
                          />
                          What is a match?
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          name="Boat"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Boat")
                              : this.handleRedirections("Boat")
                          }
                        >
                          <img
                            src="../assets/images/post-user-icon.png"
                            className="postImg  img-fluid"
                            alt="post-user-icon"
                          />{" "}
                          I’m looking for a boat to crew on.
                        </div>
                      </td>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          name="Crew"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Crew")
                              : this.handleRedirections("Crew")
                          }
                        >
                          <img
                            src="../assets/images/profile-private-icon2.png"
                            className="postImg  img-fluid"
                            alt="Question"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[1].Description: null}
                          />
                          I need crew for my boat.
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Event")
                              : this.handleRedirections("Event")
                          }
                        >
                          <img
                            src="../assets/images/date-time.png"
                            className="postImg  img-fluid"
                            alt="post-user-icon"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[3].Description: null}
                          />{" "}
                          I want to post a sailing event.
                        </div>
                      </td>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Service")
                              : this.handleRedirections("Service")
                          }
                        >
                          <img
                            src="../assets/images/post-service-icon.png"
                            className="postImg  img-fluid"
                            alt="post-service-icon"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[7].Description: null}
                          />
                          I want to post a service.
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Skill")
                              : this.handleRedirections("Skill")
                          }
                        >
                          <img
                            src="../assets/images/post-skill-icon.png"
                            className="postImg  img-fluid"
                            alt="post-user-icon"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[5].Description: null}
                          />{" "}
                          I want to show a skill.
                        </div>
                      </td>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Ride")
                              : this.handleRedirections("Ride")
                          }
                        >
                          <img
                            src="../assets/images/post-ride-icon_1.png"
                            className="postImg  img-fluid"
                            alt="post-user-icon"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[2].Description: null}
                          />{" "}
                          I want to create a ride.
                        </div>
                      </td>
                    </tr>
                    { SchoolAdmin && <tr>
                      <td>
                        <div
                          className="posts cursor-pointer"
                          onClick={() =>
                            ID
                              ? this.goToRoute("Course")
                              : this.handleRedirections("Course")
                          }
                        >
                          <img
                            src="../assets/images/post-course-icon1.png"
                            className="postImg  img-fluid"
                            alt="post-user-icon"
                            data-tip={objectsData && objectsData.length > 0 ? objectsData[4].Description: null}
                          />{" "}
                          I want to create course.
                        </div>
                      </td>
                    </tr>}
                    </tbody>            
                  </table>   
                </div>
              </div>
            </div>
          </div>
          <ReactTooltip place="top" type="dark" effect="float"/>
        </div>
      </Popup>
    );
  }
}

PostPopup.propTypes = {
  doShow: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  user: state.userDetails,
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withApollo
)(PostPopup);
