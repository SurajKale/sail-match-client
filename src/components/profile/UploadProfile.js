import React, { Component } from "react";
import Avatar from "react-avatar-edit";
import Popup from "reactjs-popup";
import { withStyles, createStyles } from "@material-ui/core";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import compose from "recompose/compose";


class UploadProfile extends Component {
  state = {
    preview: "",
    selectedArea: "",
    errorMessage: ""
  };
  componentDidMount = () => {
    this.setState({ preview: this.props.profilePhoto });
  };

  onCrop = preview => {
    this.setState({ selectedArea: preview, errorMessage: "" });
  };

  handleSelect = () => {
    if (this.state.selectedArea !== "") {
      this.setState({
        preview: this.state.selectedArea,
        errorMessage: ""
      });
      this.props.callback(this.state.selectedArea);
    } else {
      this.setState({
        errorMessage: "Please choose a picture first"
      });
    }
  };

  closePreview = () => {
   this.props.popupToggle();
  };

  onClose = () => {
    this.props.popupToggle();
  };

  

  render() {
    const { classes, isProfilePhoto } = this.props;
    const { preview } = this.state;
    return (
     
        <React.Fragment>
          <Popup
            className="popup-overlay"
            open={this.props.openPopup}
            onClose={this.closePreview}
            modal>
            <div className="profile-popup">
              {/* <div className="text-danger">{this.state.errorMessage}</div> */}
              <div className="profileBlock">
                <Avatar
                backgroundColor="white"
                  className="img-responsive center-block "
                  width={400}
                  height={250}
                  onCrop={this.onCrop}
                  onClose={this.onClose}
                />
              </div>

              <div className="row">
                <div className="col-sm-12">
                    <button  className="btn btn-danger" style={{margin:'5px'}}
                             onClick={() => this.props.popupToggle()}>Cancel</button>
                    <button  className="btn btn-primary" style={{float:'right',margin:'5px'}}
                             onClick={this.handleSelect} disabled={this.state.selectedArea == ""} >Select Photo</button>
                    </div>
                </div>
            </div>
          </Popup>

            {preview &&
              <img
                src={preview}
                className={
                  isProfilePhoto ? classes.profilePhoto : classes.headerPhoto
                }
                alt="Profile" />}
        </React.Fragment>
    );
  }
}

const styles = () =>
  createStyles({
    profilePhoto: {
      width: 80,
      height: "auto",
      display: "block",
      marginRight: "auto",
      marginLeft: "auto",
      paddingLeft: 10
    },
    headerPhoto: {
      width: "35px",
      height: "35px"
    }
  });

  export default compose(
    connect(null, {}),
    withApollo
  )(withStyles(styles)(UploadProfile));


