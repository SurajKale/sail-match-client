import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import { updatePassword } from "../../data/query";
import { getSignInAction } from "../../data/actions/UserActions";
import {_encryptPwd} from '../../services/PwdConversion';

const LocalStorageData = require("../../services/LocalStorageData");
let UID = null;

if (LocalStorageData.ID) {
  UID = LocalStorageData.ID;
}

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
			id: UID,
			oldPassword: "",
			newPassword: "",
			confirmPassword: ""
    };
	}

	handleChange = (e) => {
		this.setState({ [e.target.name] : e.target.value })
	}
	
	updatePassword = () => {
		const { id, oldPassword, newPassword, confirmPassword } = this.state;
		if(newPassword === confirmPassword) {
			this.props.client
			.mutate({
					mutation: updatePassword,
					variables: {
						UserID: id,
						OldPassword: _encryptPwd(oldPassword),
						NewPassword: _encryptPwd(confirmPassword)
				},
			})
      .then(res => { 
        if(res.data.updatePassword){
          toast.success(res.data.updatePassword.Message, {
            onClose: () => this.props.history.push("/"),
          });
        }
      }); 
		} else {
			toast.error("New password and confirm password should match")
		} 
  }

  render() {
		const { oldPassword, newPassword, confirmPassword } = this.state

    const enabled = oldPassword && newPassword && newPassword.length > 5 && confirmPassword

    return (
      <Fragment>
        <div className="container">
          <div className="row pt-4">
            <div className="col-md-3 mb-2">
              <a
                className="desktop_back"
                href="/"
              >
                <img src="../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
            <div className="col-md-6 mb-2">
              <div>
                <a href="/" className="float-left mobile_back">
                  <img src="../assets/images/back_icon.png" alt="Back" />
                </a>
                <h1>Change Password</h1>
              </div>
              <br />
              <form>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="firstname">Old Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="fullname"
                      name="oldPassword"
                      onChange={this.handleChange}
                      placeholder="Old Password"
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="firstname">New Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="fullname"
                      name="newPassword"
                      onChange={this.handleChange}
                      placeholder="New Password"
                    />
										{ newPassword.length < 6 ? <p className="passwordColor">&nbsp;Min password length is 6</p> : <p>&nbsp;</p>}
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="firstname">Confirm Password</label>
                    <input
                      type="password"
                      className="form-control"
                      id="fullname"
                      name="confirmPassword"
                      onChange={this.handleChange}
                      placeholder="Confirm Password"
                    />
                  </div>
								</div>
								<br />
                <div className="row">
                  <div className="col-sm-12 my-2">
										<p className="text-center">
											<input
												type="button"
												disabled={!enabled}
												onClick={this.updatePassword}
												className="btn btn-primary save_profile"
												value="Update Password"
											/>
										</p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </Fragment>
    );
  }
}

export default compose(
  connect(null, { getSignInAction }),
  withApollo
)(ChangePassword);
