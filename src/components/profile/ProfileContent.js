import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getUserDetailsByUserName,getMatchListByUserName ,viewAllForProfile} from '../../data/query';
import ImagePlaceholder from '../../services/ImagePlaceholder';
import {_formatDate,_setStorage} from '../../services/CommonServices';
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { updateProfile,_profileReducer} from '../../data/actions/UserActions'
import { ToastContainer, toast } from "react-toastify";
import {getViewAllPictures} from '../../data/actions/CommonActions'
import ObjectLikeDislike from "../common/ObjectLikeDislike";

// import { ControlLabel } from "react-bootstrap";
import UploadProfile from './UploadProfile';
import PhotosContent from "../common/PhotosContent";

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;
let UserName = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}
if(LocalStorageData.UserName){
    UserName = LocalStorageData.UserName;
  }
  
class ProfileContent extends Component {
    constructor(props) {
    super(props);
    let isPrivate = false;
        if(this.props.match.params.username == UserName){
            isPrivate = true;
        }
    this.state = {
        urlUserName:this.props.match.params.username,
        profileData:[],
        loaded: false,
        isPrivate:isPrivate,
        profilePic:'',
        profilePicError:null,
        openPopup:false,
        getMemberData:[],
        profilePictures:["https://sails-match.s3.amazonaws.com/matches/image/3m1vh6vu8kcvwh8ay.jpeg",
         "https://sails-match.s3.amazonaws.com/matches/image/3m1vh6vu8kcvwh8b2.jpeg",
         "https://sails-match.s3.amazonaws.com/matches/image/3m1vh6vu8kcvwh8b3.jpeg",
         "https://sails-match.s3.amazonaws.com/matches/image/3m1vh6vu8kcvwh8b5.jpeg",
         "https://sails-match.s3.amazonaws.com/matches/image/3m1vh6vu8kcvwh8b5.jpeg"]
      };
    }

    getProfileData = () => {
        const UserName = this.state.urlUserName;
        this.props.client
        .query({
            query: getUserDetailsByUserName,
            variables: {
                UserName
            }
         })
          .then(res => { 
              let UserDetailsObj ={FullName : res.data.getUserDetailsByUserName.FirstName+" "+res.data.getUserDetailsByUserName.LastName,ProfileImage:res.data.getUserDetailsByUserName.ProfileImage}
              this.setState({
                  profileData : res.data.getUserDetailsByUserName,
                  profilePic:res.data.getUserDetailsByUserName.ProfileImage,
                  totalCount: res.data.getUserDetailsByUserName.LikeCount,
                //   profilePictures: res.data.getUserDetailsByUserName.PhotoVideoGallery,
                  userDetailsPic:UserDetailsObj,
                  renderLike:true
                }) 
          });          
      };

      getMemberData = () => {
        const UserName = this.state.urlUserName;
        this.props.client
        .query({
            query: getMatchListByUserName,
            variables: {
                UserName
            }
         })
          .then(res => { 
              this.setState({
                getMemberData : res.data.getMatchListByUserName
                }) 
          });          
      };
   
    componentDidMount(){
        window.scrollTo(0,0);
        this.getProfileData();
        this.getMemberData();
    }
    goBack = () => {
        this.props.history.goBack();
    }

    handleImageLoaded() {
        this.setState({ loaded: true });
      }

      _getImage = (image) =>{
        let that = this;
        this._updateProfile(that,image);
      }

      addFile = (evt) =>{
        let that = this;
        if(evt.target.files[0] !== undefined){
            if (evt.target.files[0].type !== '') {
                this.toBase64(evt.target.files[0]).then(result => {
                    this.setState({profilePicError:'',  profilePic: result });
                    that._updateProfile(that,result);

                    });
                }
                else{
                     this.setState({profilePicError:'Only images are acceptable',profilePic:null });
               }
           }
      }

      _updateProfile = (that, result) =>{
        let UserID = ID;
        let ProfileImage = result;
        that.props.updateProfile(this.props.client,UserID,ProfileImage).then(res => {
        _setStorage(res.data.updateUsersProfilePic.Avatar);
        that.props._profileReducer(res.data.updateUsersProfilePic.Avatar);
            that.setState({profilePic:res.data.updateUsersProfilePic.Avatar,openPopup:false});
          }).catch(function (error) {
            //   console.log(error);
          })
      }

      toBase64 = file => new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
      });

      _openPopup = () =>{
          this.setState({openPopup:true});
      }

      popupToggle = () =>{
        this.state.openPopup
        ? this.setState({
          openPopup: false
        })
        : this.setState({ openPopup: true });
    };

    viewAllPictures= () =>{
        this.props.client
        .query({
            query: viewAllForProfile,
            variables: {
        UserID:this.state.profileData.ID,
        ObjectType:this.state.profileData.ObjectType
            }
         })
          .then(res => { 
               if (res.data.viewAllObjectTypePictures !== null) {
            let picturesData = res.data.viewAllObjectTypePictures;
          this.setState({profilePictures:picturesData.Pictures, 
                         userDetailsPic:picturesData.UserDetails,
                        })
            console.log(picturesData,"picturesData");
          } else {
            toast.error("Something wents wrong");
          }
        })
        .catch(function (error) {
        
        });
       }
    
      viewLessPictures = () =>{
        let queryResult = this.state.profilePictures
        let filterdata = [];
     for (let i = 0; i <  queryResult.length; i++) {         
         filterdata.push(queryResult[i]);
         if (filterdata.length == 4) {
           break;
         }
       }
       this.setState({profilePictures:filterdata})
    }
    objectLike= (count) =>{
      console.log(count);
    this.setState({totalCount:count})
    }

render() {
    const {profileData,isPrivate,profilePic,getMemberData,renderLike,totalCount,profilePictures} = this.state;
    const { loaded } = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};

  return (
        <div className="container">
            <div className="col-md-12">
                <div className="row">
                    <div className="col-md-3 col-12 mb-2 mt-2"> 
                      <a className="desktop_back" href="javaascript:void(0);" onClick={this.goBack}>
                        <img src="../../assets/images/back_icon.png" alt="Back" /></a> 
                     </div>
                </div>
            </div>

            <div className="col-md-12 col-12">
                <div className="row">
                {!isPrivate &&
                    <div className="col-md-3 col-12 mb-2 mt-2 profile-photo-section"> 
                         {!loaded && <ImagePlaceholder/> }
                        <img className="user-profile-image" src={profileData.ProfileImage ? profileData.ProfileImage : './assets/images/no_image.jpg'} alt="Back" style={imageStyle} onLoad={this.handleImageLoaded.bind(this)} /> 
                    </div>}

                    {isPrivate &&
                    <div className="col-md-3 col-12 mb-2 mt-2 profile-photo-section">      
                          {/* <ControlLabel data-tip="Edit Profile" place="bottom" type="info"> */}
                             <img src={profilePic ? profilePic : '../../assets/images/no_image.jpg'} alt="Back" className="user-profile-image" onClick={this._openPopup} data-tip="Edit Profile" place="bottom" type="info"/> 
                             {/* <FormControl type="file"
                                    accept="image/*"
                                    style={{display:'none'}}
                                    name="userProfile"
                                    onChange={this.addFile} />
                                      </ControlLabel> */}
                                      <ReactTooltip />

                {this.state.openPopup &&
                    <UploadProfile popupToggle={this.popupToggle} openPopup={this.state.openPopup} callback={this._getImage}/>}                         
                    </div>}
                     <div className="col-md-9 col-12 mb-2 mt-2 event-info profile-private-info">
                        <h2>{(profileData.FirstName ? profileData.FirstName : '-') +' '+(profileData.LastName ? profileData.LastName : '-')}</h2>
                        <p className="date-time-text">Member since: <span>{_formatDate(profileData.CreatedDate)}<span></span></span></p> 
                        <div className="privateIcons">
                            {profileData.ProfileObjects !== undefined &&
                         <div className="privateProfileImg">
                            <img className="profImg1" src={ profileData.ProfileObjects.ProfileCrew ? profileData.ProfileObjects.ProfileCrew: '../../assets/images/profile-private-icon1.png'} alt="Back" />
                        </div>}
                        {profileData.ProfileObjects !== undefined && 
                        <div className="privateProfileImg">
                            <img  className="profImg2" src={profileData.ProfileObjects.ProfileRide ? profileData.ProfileObjects.ProfileRide : '../../assets/images/profile-private-icon2.png'} style={{marginRight: '10px'}} alt="Back" /> 
                        </div> }
                        </div>
                                               
                        <p className="profileLocation">
                             <img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2" /> <span> {profileData.Location ? profileData.Location.Address : ''}</span>
                         </p>
                         {/* {!isPrivate &&
                        <button className="publicleaveMessageBtn">
                             <img src="../../assets/images/message-icon.png" className="pr-1" alt="tick-icon" />Message</button>} */}
                
                {isPrivate &&
                    <Link to={'/edit-profile/'+UserName} className="publicEditProfileBtn"> 
                    <img src="../../assets/images/edit-icon.png" className="pr-1" alt="tick-icon" />Edit Profile</Link>}                    
                        <div className="privateLikeComment"> 
                        <div className="like">
                {ID && renderLike && !isPrivate && <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={profileData.ObjectType} 
                ReferenceID={profileData.ID} 
                status={profileData.isLike} 
                ReferenceUserID={profileData.ID} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
                  { ID && isPrivate && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
                           </div>
                        {/* <div className="comment">
                        <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" /><span> 39</span>
                        </div> */}
                        </div>                    
                    </div>        
                </div>
            </div>
            
            <div className="col-md-12">
    <div className="row profilePrivateInfoSection">
    	<div className="col-md-12">
    		<div className="row">
    			<div className="col-md-9 col-12">
    				<label>Bio</label>
    			  	<div className="bioDetails">{profileData.Bio ? profileData.Bio : '-'} 
                    </div>
    			</div>
    			<div className="col-md-1 col-4 padTopMob">
    			<label>Smoker</label>
	    			<div className="bioDetails">{profileData.Smoke ? profileData.Smoke : '-'}</div>
    			</div>
    			<div className="col-md-1 col-4 padTopMob">
	    			<label>Drink</label>
	    			<div className="bioDetails">{profileData.Drink ? profileData.Drink : '-'}</div>
    			</div>
    			<div className="col-md-1 col-4 padTopMob">
	    			<label>Marijuana</label>
	    			<div className="bioDetails">{profileData.Pot ? profileData.Pot : '-'}</div>
    			</div>	
    		</div>

    		<div className="row">
    			<div className="col-md-1 col-4 pb-3">
    				<label>Experience</label>
    			  	<div className="bioDetails">{profileData.Experience ? profileData.Experience : '-'}</div>
                </div>
    		
    			<div className="col-md-2 col-4">
	    			<label>Certification</label>
	    			<div className="bioDetails">{profileData.Certifications ? profileData.Certifications.join(", ") : '-'}</div>
    			</div>
    			<div className="col-md-3 col-4">
	    			<label>Interests</label>
                <div className="bioDetails">{profileData.Desire ? profileData.Desire.join(", ") : ''}</div>
    			</div>
    			
    			<div className="col-md-3 col-12">
	    			<div className="labelInfo">
	    			<label>Club Affiliation</label>
                    {profileData.ClubDetails!== null &&
	    			<div className="bioDetails">{profileData.ClubDetails ? profileData.ClubDetails.Title : ''}</div>}
	    		</div>
                {profileData.ClubDetails &&
	    			<img className="club-img" src={profileData.ClubDetails ? profileData.ClubDetails.BurgeeImage : ''} />}
    			</div>
                
                {typeof profileData.BoatProfileList !== 'undefined' && profileData.BoatProfileList.length > 0  &&
    			<div className="col-md-3 col-12">
	    			<div className="labelInfo">
	    				<label>Boat</label>
	    				<div className="bioDetails">{profileData.BoatProfileList ? profileData.BoatProfileList.map( boat => boat.Title).join(', ') : ''}</div>
	    			</div>
	    			{/* <img className="boat-img1" src={profileData.BoatDetails ? profileData.BoatDetails[0].ProfilePicture : ''} /> */}
    			</div>}    
    		</div>
    	</div>
    </div>
</div>
{/* <PhotosContent Pictures={profilePictures} Details={profileData} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures}/>

            <div className="col-md-12"> 
                <div className="row">         
                    <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">  
                        <div className="videos">
                            <img src="../../assets/images/white-right-arrow.png" className="profileWhiteRightImg" alt="Back" />              
                            <img src="../../assets/images/video-1.png" className="videoSection" alt="Back" /> 
                            <a href="#">www.youtube.com/pl...</a>                                                   
                        </div>    
                    </div>
                  <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4">
                        <div className="videos">
                            <img src="../../assets/images/white-right-arrow.png" className="profileWhiteRightImg" alt="Back" />                
                            <img src="../../assets/images/video-2.png" className="videoSection" alt="Back" />  
                            <a href="#">www.youtube.com/pl...</a>                                         
                        </div>    
                    </div>                   
                </div>         
            </div> */}

{typeof getMemberData !== 'undefined' && getMemberData.length > 0  &&
<React.Fragment>
            <div className="col-md-12">
                <div className="row">
                <h5 className="about-event">Matches <span>({getMemberData.length})</span></h5>
                    <a href="#" className="videos-view-all">View All</a>
                </div>
            </div>

            <div className="col-md-12"> 
                <div className="row">        
                {getMemberData && getMemberData.map((match,index)=>(
                    <div className="col-12 col-sm-3 col-md-4 col-lg-3 mb-4" key={index}>  
                        <div className="videos">    
                            <img src={match.ProfilePicture ? match.ProfilePicture : '../../assets/images/no_image.jpg'} className="videoSection" alt="Back" style={{width: '250px',height: '168px',borderRadius: '15px'}}/> 
                            <p className="club-name">{match.Title} </p>
                            <div className="club-members">{match.MemberList.length} Members</div>
                            <div className="club-members-photo-section">
                                {typeof match.MemberList !== 'undefined' && match.MemberList.length > 0 ? match.MemberList.map((user,index1)=>(
                               <img key={index1} src={user.ProfileImage} className="club-member-photo member_photo" alt="Back" />  )) : <span>{"All Members"}</span>}                                           
                                <img src={match.ObjectUrl} className="club-member-flag sail_obj_image" alt="Back" /> 
                            </div>  
                            <hr className="club-line" />                                             
                            <div className="like">
                                <img src="../../assets/images/grey-heart-icon.png" alt="heart-icon" className="pr-2" /><span> {match.Like ? match.Like : 0}</span></div>
                            <div className="comment">
                                <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" /><span> {match.CommentCount ? match.CommentCount : 0}</span>
                            </div>
                            <div className="forward">
                                <img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6" /><span> {match.Shares ? match.Shares : 0}</span>
                            </div>
                        </div>    
                    </div>))} 
                </div> 
            </div>
            </React.Fragment>}
        </div>
      );
    }
  }
  

  export default compose(
    connect(null,{updateProfile,_profileReducer,getViewAllPictures}),
    withApollo
)(ProfileContent);
