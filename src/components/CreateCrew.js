import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { createCrewAction } from "../data/actions/CrewAction";
import { getAllEventTypes } from "../data/query";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import Component from "./location/LocationContent";

const LocalStorageData = require("../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class CreateCrew extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eventType: [],
      skillLevel: "",
      startDate: new Date(),
      endDate: new Date(),
      // startTime: "",
      // endTime: "",
      streetName: "",
      city: "",
      latitude: "",
      longitude: "",
      address: "",
      state: "",
      crewTitle: "",
      description: "",
      eventsArray: [],
      button: true
    };
  }
  componentDidMount = () => {
    this.getAllEventsTypes();
  };

  getAllEventsTypes = () => {
    this.props.client
      .query({
        query: getAllEventTypes,
      })
      .then((res) => {
        res.data.getAllEventTypes.forEach((item) => {
          item.isSelected = false;
        });
        this.setState({
          eventsArray: res.data.getAllEventTypes,
        });
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  handleEventsSelect = (category) => {
    let oldEvents = this.state.eventsArray.slice();
    oldEvents.forEach((item) => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });
    this.setState({
      eventsArray: oldEvents,
    });

    let options = oldEvents.filter(function (item) {
      return item.isSelected;
    });
    if (options.length > 0) {
      this.setState({
        eventsFlag: true,
      });
    } else {
      this.setState({
        eventsFlag: false,
      });
    }
  };

  handleStartChange = async (startDate) => {
    let date = startDate.toISOString().substring(0, 10);
    this.setState({
      finalStartDate: date,
      startDate: startDate,
    });
  };

  handleEndChange = async (e) => {
    let date = e.toISOString().substring(0, 10);
    this.setState({
      finalEndDate: date,
      endDate: e,
    });
  };
  handleChanges = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };
  getLocationDetails = (location) => {
    let address =
      location.mainPlace +
      "," +
      "" +
      location.city +
      "," +
      "" +
      location.zipCode +
      "," +
      "" +
      location.state +
      "," +
      "" +
      location.country;
    this.setState({
      city: location.city,
      latitude: location.latitude,
      longitude: location.longitude,
      address: address,
      state: location.state,
    });
  };
  handleStartTimeChange = (e) => {
    this.setState({ startTime: e });
  };
  handleEndTimeChange = (e) => {
    this.setState({ endTime: e });
  };
  createCrew = (e) => {
    this.setState({ button : !this.state.button })
    const {
      skillLevel,
      startDate,
      endDate,
      finalStartDate,
      finalEndDate,
      startTime,
      endTime,
      latitude,
      longitude,
      address,
      eventsArray,
      crewTitle,
      description,
    } = this.state;
    let lat = latitude.toString();
    let long = longitude.toString();
    const Location = { Latitude: lat, Longitude: long, Address: address };

    let sdate = startDate.toISOString().substring(0, 10);
    let edate = endDate.toISOString().substring(0, 10);

    var options = eventsArray.filter(function (item) {
      return item.isSelected;
    });

    const EventType = options.map(function (item) {
      return { EventTypeID: item.ID, Name: item.EventTitle };
    });
    this.props
      .createCrewAction(
        this.props.client,
        crewTitle,
        ID,
        description,
        EventType,
        skillLevel,
        sdate,
        edate,
        startTime,
        endTime,
        Location,
        1
      )
      .then((res) => {
        toast.success("Crew created successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        toast.error("Something went wrong", {});
      });
  };

  render() {
    const {
      eventsArray,
      eventsFlag,
      skillLevel,
      startDate,
      endDate,
      startTime,
      endTime,
      city,
      latitude,
      longitude,
      state,
      crewTitle,
      description,
      button
    } = this.state;
    const format = "h:mm a";
    const enabled =
      eventsFlag &&
      skillLevel &&
      startDate &&
      endDate &&
      startTime &&
      endTime &&
      city &&
      state &&
      latitude &&
      longitude &&
      crewTitle &&
      description &&
      button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a className="float-left mobile_back">
                <img src="images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Crew</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label>Type of Event</label>
                    <div className="">
                      {this.state.eventsArray &&
                        this.state.eventsArray.map((type) => {
                          return (
                            <input
                              type="button"
                              key={type.ID}
                              className={`${
                                type.isSelected
                                  ? "btn-primary btn gender"
                                  : "btn btn-outline-secondary gender"
                              }`}
                              onClick={(e) => this.handleEventsSelect(type)}
                              value={type.EventTitle}
                            />
                          );
                        })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="skillLevel">Skill Level</label>
                    <div>
                      <label
                        className={
                          skillLevel === "Any"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Any
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Any"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Beginner"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Beginner
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Beginner"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Intermediate"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Intermediate
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Intermediate"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Advanced"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Advanced
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Advanced"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                    </div>
                  </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>Crew Start Date</label>
                    <DatePicker
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      minDate={startDate}
                      selected={startDate}
                      onChange={this.handleStartChange}
                      placeholderText="Crew Start Date"
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="crewEndDate">Crew End Date</label>
                    <DatePicker
                      dateFormat="yyyy-MM-dd"
                      minDate={startDate}
                      className="form-control"
                      selected={endDate}
                      onChange={this.handleEndChange}
                      placeholderText="Crew End Date"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="crewStartTime">Crew Start Time</label>
                    <TimePicker
                      id="crewStartTime"
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleStartTimeChange}
                      value={startTime}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="crewEndTime">Crew End Time</label>
                    <TimePicker
                      id="crewEndTime"
                      showSecond={false}
                      className="form-control"
                      onChange={this.handleEndTimeChange}
                      value={endTime}
                      format={format}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <Component callback={this.getLocationDetails} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>City </label>
                    <input
                      type="text"
                      className="form-control"
                      name="city"
                      placeholder="City"
                      disabled
                      value={city || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label>State </label>
                    <input
                      type="text"
                      className="form-control"
                      name="state"
                      placeholder="State"
                      disabled
                      value={state || ""}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>Latitude </label>
                    <input
                      type="text"
                      className="form-control"
                      name="latitude"
                      placeholder="Latitude"
                      disabled
                      value={latitude || ""}
                    />
                  </div>

                  <div className="col-sm-6 my-2">
                    <label>Longitude </label>
                    <input
                      type="text"
                      className="form-control"
                      name="longitude"
                      placeholder="Longitude"
                      disabled
                      value={longitude || ""}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="crewTitle">Crew Title</label>
                    <input
                      type="text"
                      className="form-control"
                      id="crewTitle"
                      placeholder="Crew Title"
                      name="crewTitle"
                      value={crewTitle}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="description"
                      value={description}
                      onChange={this.handleChanges}
                    ></textarea>
                  </div>
                </div>
                {/* <hr />        */}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        disabled={!enabled}
                        onClick={this.createCrew}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { createCrewAction }),
  withApollo
)(CreateCrew);
