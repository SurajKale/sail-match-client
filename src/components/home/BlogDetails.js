import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getBlogPostByUrl } from "../../data/query";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";

import PostListParent from "../common/PostListParent";
import ShareContent from "../common/ShareModel/shareOption";
import ObjectLikeDislike from "../common/ObjectLikeDislike";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class BlogDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.url,
      id: null,
      profilePic: "",
      title: "",
      subTitle: "",
      tags: [],
      category: [],
      Pictures:  [],
      description: "",
      ObjectType: "BlogPost",
      totalCount: 0,
      CommentCount: 0,
      ShareCount: 0,
      isLike: false,
      createdBy: null
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getBlogPostByUrl()
  }

  getBlogPostByUrl=()=>{
    this.props.client
    .query({
        query: getBlogPostByUrl,
        variables: {
          BlogUrl: this.state.url
      },
     })
      .then(res => { 
        if(res.data.getBlogPostByUrl){
          let data = res.data.getBlogPostByUrl
          this.setState({
            id: data.ID,
            profilePic: data.FeatureImage,
            title: data.Title,
            subTitle: data.SubTitle,
            tags: data.Tags,
            category: data.Categories,
            Pictures:  data.Pictures,
            description: data.Description,
            totalCount: data.LikeCount,
            renderLike:true,
            CommentCount: data.CommentCount,
            ShareCount: data.ShareCount,
            isLike: data.isLike,
            createdBy: data.UserID
          })
        } else {
          toast.error("Something wents wrong")
        }
      }
    );  
  }

  goBack = () => {
    this.props.history.goBack();
  };

  objectLike= (count) =>{
    this.setState({totalCount:count})
  }

  openShareModal = () => {
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.eventDetails.ID,this.state.eventDetails.UserID,this.state.eventDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({
          eventPictures:picturesData.Pictures, 
          userDetailsPic:picturesData.UserDetails,
        })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    });
  }
   
  viewLessPictures = () =>{
    let queryResult = this.state.eventPictures
    let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
      filterdata.push(queryResult[i]);
      if (filterdata.length == 4) {
        break;
      }
    }
    this.setState({eventPictures:filterdata})
  }

  render() {
    const { startDate, endDate, startTime, endTime, title, subTitle, category,
      tags, description, profilePic, Pictures, id, address, Latitude, Longitude,
      eventMatch, eventID, CreatedBy, totalCount, ObjectType, isLike, renderLike,
      eventPictures, eventDetails, createdBy } = this.state;

    return (
      <Fragment>
        <div class="container pt-2">
          <div class="col-md-12 col-xl-12">
            <div class="row">
                <div class="mb-2 mt-2"> <a class="desktop_back" href="#!" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
            </div>
          </div>
          <div class="col-md-12 col-xl-12">
            <div class="row">
              <div class="mb-2 mt-2 event-info"> 
                <h4 class="BlogTitleTag">{title}</h4>
                <h4 class="BlogSubtitle">{subTitle}</h4>
              </div>
              <div class="col-md-12 col-sm-12 mb-2 mt-2 nopadding">  
                <div class="blogImage">
                  <img src={profilePic}  alt={title}  class="blogImg" alt="Back"/>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8 col-xl-8 bx_shadowClass">
                <div class="row">
                  <h5 class="about-event">About this event</h5>
                  <p class="about-event-info"  dangerouslySetInnerHTML={{__html: description }} />
                </div>
                <div class="row">
                    <h5 class="about-event">Photos <span>({Pictures && Pictures.length})</span></h5>
                </div>
                <div class="col-md-12 col-xl-12"> 
                    <div class="row">
                      {
                        Pictures && Pictures.length > 0 && 
                        Pictures.map((pic,index) => (
                          <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4" key={index}> 
                            <div class="blog_image">               
                                <img src={pic} alt="blog image" class="img-fluid center-block mx-auto " alt="Back"/> 
                            </div>                                                   
                        </div>
                        ))
                      } 
                    </div>         
                </div>
              </div>
              <div class="col-md-4 col-xl-4">
                <div class="col-md-12 col-sm-12 mb-2 mt-2 bx_shadowClass">
                  <div class="date-time-img"><img src="../../assets/images/date-time.png" alt="Back"/></div>
                  <p class="date-time-text" id="date-time-text"><span>{title}</span></p>
                  <p class="date-time-text"> <img src="../../assets/images/location-icon.png" alt="location-icon" class="pr-2"/> <span> {category && category[0] && category[0].Name}</span> </p>
                  <p class="date-time-text"> <img src="../../assets/images/internet-icon.png" alt="position-icon" class="pr-2"/> <span>{tags && tags.join(",")}</span> </p>
                  <hr/>
                  <div class="like_blog" style={{ textAlign: 'center' }}><span>{ID && renderLike &&  <ObjectLikeDislike
                    LikeCount={totalCount} 
                    ObjectType={ObjectType} 
                    ReferenceID={id} 
                    status={isLike} 
                    ReferenceUserID={createdBy} 
                    updateOnParent={this.objectLike} 
                    isDetail={true}/>}
                    { ID == null && <> <img
                        src="../../assets/images/heart-icon.png"
                        alt="heart-icon"
                        className="pr-2"
                      />
                    <span>{totalCount}</span></>}</span>
                  </div>
                  <div class="comment_blog"><img src="../../assets/images/comment-icon.png" alt="comment-icon" class="pl-6"/><span>&nbsp;&nbsp;{this.state.CommentCount || 0}</span></div>
                  <div class="forward_blog"><img src="../../assets/images/forward-icon.png" alt="forward-icon" class="pl-6"/><span>&nbsp;&nbsp;{this.state.ShareCount || 0}</span></div>
                  { ID && this.state.showShareModel &&
                    <ShareContent 
                    ObjectType={ObjectType}
                    ReferenceID={parseInt(id)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={title}
                    quote={title} />
                  }
                  </div>  
                </div>
            </div>
          </div>
          <br />
          {ObjectType && id && <PostListParent blog ReferenceID={id} ObjectType={ObjectType} />}
          <ToastContainer autoClose={1000}/>               
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  myProfile: state.profiles,
});

export default compose(
  connect(mapStateToProps),
  withApollo
)(BlogDetails);
