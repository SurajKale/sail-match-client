import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getAllPublishBlogs } from '../../data/query';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class HomeBlog extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: [],
      loading: true
    }
  }

  componentDidMount() {
    this.getAllPublishBlogs()
  }

  getAllPublishBlogs = () => {
    this.props.client
    .query({
        query: getAllPublishBlogs,
        variables: {
          isSeqRequired: true
        },
        fetchPolicy: "network-only"
     }) .then(res => {  
          const data = res.data.getAllPublishBlogs;
          data.length > 3 ? this.setState({ list: data.slice(0,3), loading: false }) : this.setState({ list: data, loading: false })
      });          
  };

  goToDetailsPage = (url) => {
    this.props.history.push('/blog-details/'+url)
  }

  render() {
    const { list } = this.state;
    return (
      <Fragment>
        <div>
          <div className="sweet-loading">
            <PuffLoader
              css={override}
              size={60}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 mb-2 text-right"><a href="/blogs">View Blog</a></div>
          {
            list && list.length > 0 &&
            list.map(blog => (
              <div className="col-md-4 mb-2 text-center" key={blog.ID}>
                <a href={"/blog-details/"+blog.Url} className="card_home">
                  <img className="blogCardsSize" src={ blog.FeatureImage ? blog.FeatureImage : "./assets/images/Home_img01.png"} alt={blog.Title}/> 
                  <div className="Blog_text">
                    <h4>{blog.Title}</h4>
                    <p>{blog.CreatedDate}</p>
                  </div>
                </a>
              </div>
            ))
          }
        </div>
      </Fragment>
    );
  }
}
  
export default compose(connect(null,{}),withApollo)(HomeBlog);