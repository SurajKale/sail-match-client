import React, { Component, Fragment } from "react";
import GoogleMap from "../common/LocationModel/setLocation";
const LocalStorageData = require("../../services/LocalStorageData");
let ID = null
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class MyLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLocationModal : false,
      active: false,
      myPosition: {},
      userLocation: {},
      radius: 5000
    }
  }

  async componentDidMount() {
    await navigator.permissions.query({name:'geolocation'}).then( async result =>
    {
      if (result.state === 'granted') {
        await this.setState({ active: true });
      } else if (result.state === 'prompt') {
        await this.setState({active: false});
      } else if (result.state === 'denied') {
        await this.setState({active: false});
        // localStorage.removeItem("locationData");
      }
    });
    await this.getDataFromLocalStorage();
    // if(!ID) {
    //   await this.handleLocation()
    // }
    this.props.callback({ userLocation: this.state.userLocation, radius: this.state.radius })
  }

  getDataFromLocalStorage = async () => {
    const locationData = JSON.parse(localStorage.getItem('locationData'));
    const userData = JSON.parse(localStorage.getItem('sessionUser'));
		{
      userData && userData.Location ?
      this.setState({ userLocation: userData.Location }) 
      :
			locationData !== null && locationData &&
				 await this.setState({
          userLocation: {
            ...this.state.userLocation,
            City: locationData.area,
            Latitude: locationData.mapPosition.lat,
            Longitude: locationData.mapPosition.lng
         }
					// mapRadius: locationData.radius
        })
					// mapRadius: locationData.radius
       
        this.props.callback({ userLocation: this.state.userLocation, radius: this.state.radius })
		}
  }

  handleLocation = async () => {
      // if (navigator.geolocation) {
      //   await navigator.geolocation.getCurrentPosition( async position => {
      //      await this.setState({
      //       userLocation: {
      //         ...this.state.userLocation,
      //         City: '',
      //         Latitude: position.coords.latitude,
      //         Longitude: position.coords.longitude
      //      }
      //     })
      //     this.props.callback({ userLocation: this.state.userLocation, radius: this.state.radius })
      //   });
      // } else {
      //     console.log("unavailable");
      // }
  };

  // showLocationModalPopup = () => {
  //   this.setState({ showLocationModal : !this.state.showLocationModal })
  // }

  closeLocationPopup = () => {
    document.body.style.overflowY = "auto";
    this.setState({ showLocationModal: false });
  };

  showLocationModalPopup = (type) => {
      document.body.style.overflowY = "auto";
      this.setState({ showLocationModal: true  });
  };

  updateCoordinate = async (data) => {
    await this.setState({
      userLocation: {
         ...this.state.userLocation,
         City: data.place,
         Latitude: data.locationObject.Lattitude,
         Longitude: data.locationObject.Longitude
      },
      radius: data.radius
    })
    this.props.callback({ userLocation: this.state.userLocation, radius: this.state.radius })
  }

  removeData = async () => {
    await this.setState({ userLocation: {} })
    this.props.callback({ userLocation: this.state.userLocation, radius: this.state.radius })
  }

  render() {
    const { userLocation, myPosition, active } = this.state;
    let constLocationObject = {
      Latitude: 37.0902,
      Longitude: 95.7129
    }
    return (
      <Fragment>
        <GoogleMap 
          doShow={this.state.showLocationModal}
          closePopup={this.closeLocationPopup}
          getPositionData={this.updateCoordinate}
          userLocation={constLocationObject}
        />
        <div className="row">
          <div className="col-sm-12 mb-4">
            <div className="location_left">
              <span className="mobile_hide">Location:</span>{" "}
              <span className="boxed">
                <img src="./assets/images/ic_location.svg" alt="Dropdown" />{" "}
                {userLocation ? userLocation.City : ""} <span className="cursor-pointer float-right" onClick={this.showLocationModalPopup}>Edit</span>{" "}
              </span>
              { userLocation.Latitude &&
                <span>
                  &nbsp;&nbsp;<i onClick={this.removeData} className="fa fa-times cursor-pointer" aria-hidden="true" />
                </span>
              }
            </div>
            { !active &&
              <div className="location_right">
                {" "}
                <span className="mobile_hide">
                  {" "}
                  Allow your location to see posts near you.
                  <button type="button" className="btn btn-primary button-rounded" onClick={this.handleLocation}>
                    Allow
                  </button>
                </span>{" "}
                <span className="desktop_hide">
                  <button type="button" className="btn btn-primary button-rounded" onClick={this.handleLocation}>
                    Allow location
                  </button>
                </span>
              </div>
            }
          </div>
        </div>
      </Fragment>
    );
  }
}

export default MyLocation;
