import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getBlogPostByUrl } from "../../data/query";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";
import MemberContent from "../common/StaticMember";
import PhotosContent from "../common/BlogPhotos";
import VideosContent from "../common/VideosContent";
import PostListParent from "../common/PostListParent";
import ShareContent from "../common/ShareModel/shareOption";
import ConfirmationPopup from "../common/ConfirmationPopup";
import MatchesCommon  from '../common/MatchesCommon';
import ObjectLikeDislike from "../common/ObjectLikeDislike";

import CreatePost from "../common/CreatePost";

import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class BlogDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: 'titanic-3m1vhehqgkhqagqgl',
      id: null,
      profilePic: "",
      title: "",
      subTitle: "",
      tags: [],
      category: [],
      Pictures:  [],
      description: "",
      ObjectType: "BlogPost",
      totalCount: 0,
      CommentCount: 0,
      ShareCount: 0,
      isLike: false
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getBlogPostByUrl()
    
  }

  getBlogPostByUrl=()=>{
    this.props.client
    .query({
        query: getBlogPostByUrl,
        variables: {
          BlogUrl: this.state.url
      },
     })
      .then(res => { 

        if(res.data.getBlogPostByUrl){
          let data = res.data.getBlogPostByUrl
          this.setState({
            id: data.ID,
            profilePic: data.FeatureImage,
            title: data.Title,
            subTitle: data.SubTitle,
            tags: data.Tags,
            category: data.Categories,
            Pictures:  data.Pictures,
            description: data.Description,
            totalCount: data.LikeCount,
            renderLike:true,
            CommentCount: data.CommentCount,
            ShareCount: data.ShareCount,
            isLike: data.isLike
          })
        } else {
          toast.error("Something wents wrong")
        }
      });  
  }

  goBack = () => {
    this.props.history.goBack();
  };

  objectLike= (count) =>{
  this.setState({totalCount:count})
  }

  openChatPopup = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.eventDetails.ID,this.state.eventDetails.UserID,this.state.eventDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
      this.setState({eventPictures:picturesData.Pictures, 
                     userDetailsPic:picturesData.UserDetails,
                    })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    
    });
   }
   
   viewLessPictures = () =>{
       let queryResult = this.state.eventPictures
       let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
        filterdata.push(queryResult[i]);
        if (filterdata.length == 4) {
          break;
        }
      }
      this.setState({eventPictures:filterdata})
   }

  render() {
    const {
      startDate,
      endDate,
      startTime,
      endTime,
      title,
      subTitle,
      category,
      tags,
      description,
      profilePic,
      Pictures,
      id,
      address,
      Latitude,
      Longitude,
      eventMatch,
      eventID,
      CreatedBy,
      totalCount,
      ObjectType,
      isLike,
      renderLike,
      eventPictures,
      eventDetails
    } = this.state;

    return (
        <div class="container">

        <div class="col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-3 mb-2 mt-2"> <a class="desktop_back" href="#"><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
            </div>
        </div>
    
        <div class="col-md-12 col-xl-12">
            <div class="row">
                <div class="col-md-12 col-sm-12 mb-2 mt-2 event-info"> 
                    <h4 class="BlogTitleTag">Blog Title</h4>
                    <h4 class="BlogSubtitle">Blog SubTitle</h4>
                    <h4 class="BlogSubtitle">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</h4>
                    <h4 class="BlogSubtitle">Blog Category</h4>
                </div>
                <div class="col-md-12 col-sm-12 mb-2 mt-2 nopadding">  
                    <div class="blogImage">
                     <img src="https://sails-match.s3.amazonaws.com/blog/image/3m1vhehqgkhqagqfl.jpeg" class="blogImg" alt="Back"/>
                    </div>
                </div>
    
            </div>
        <div class="row">
                
    
        <div class="col-md-8 col-xl-8 bx_shadowClass">
            <div class="row">
                <h5 class="about-event">About this event</h5>
                <p class="about-event-info">
                    Lorem ipsum dolor sit amet, at mei prompta reprimique, eu adhuc prompta appareat qui. Ei facilis suscipit vix, vix eu quando tibique ponderum. Cum purto possim ea. Per no omittam interesset temporibus, nullam nostrud deterruisset cu eos, eu simul ponderum cum. Est senserit convenire imperdiet ut, augue simul nostrud pri id, duo interesset signiferumque at.
                </p>    
                <p class="about-event-info">
                    Fugit salutatus definitionem est cu, ad eos vero accusamus omittantur. Pri ex illud accusamus. Sed iudico putant luptatum eu, vix summo cetero gloriatur ea. Ei usu nostrum omittantur, ius ex fabulas mentitum. Qui vide euismod legendos et, zril legere moderatius vix at.
                </p>
            </div>
             <div class="row">
                <h5 class="about-event">Photos <span>(150)</span><a href="#" class="view-all">View All</a></h5>
               
            </div>
        
    
        <div class="col-md-12 col-xl-12"> 
            <div class="row">         
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
    
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                <div class="col-12 col-sm-3 col-md-4 col-lg-3 mb-4"> 
                    <div class="blog_image">               
                        <img src="../../assets/images/photo-1.png"   class="img-fluid center-block mx-auto " alt="Back"/> 
                    </div>                                                   
                </div>
                 
            </div>         
        </div>
        </div>
    
         <div class="col-md-4 col-xl-4">
            <div class="col-md-12 col-sm-12 mb-2 mt-2 bx_shadowClass">
               <div class="date-time-img"><img src="../../assets/images/date-time.png" alt="Back"/></div>
                <p class="date-time-text" id="date-time-text"><span>Testing Blog</span></p>
               <p class="date-time-text"> <img src="../../assets/images/location-icon.png" alt="location-icon" class="pr-2"/> <span> Food</span> </p>
               <p class="date-time-text"> <img src="../../assets/images/internet-icon.png" alt="position-icon" class="pr-2"/> <span>travel</span> </p>
               <hr/>
               <div class="like_blog"> <img src="../../assets/images/heart-icon.png" alt="heart-icon" class="pr-2"/><span>0</span></div>
               <div class="comment_blog"><img src="../../assets/images/comment-icon.png" alt="comment-icon" class="pl-6"/><span> 0</span></div>
               <div class="forward_blog"><img src="../../assets/images/forward-icon.png" alt="forward-icon" class="pl-6"/><span> 0</span></div>
                     
                
            </div>  
                     
                
         </div>
    </div>
        
    
    
         </div>
        
       
    
    
        
    
       
   
                           
    
        </div>
    );
  }
}

const mapStateToProps = (state) => ({
  myProfile: state.profiles,
});

export default compose(
  connect(mapStateToProps),
  withApollo
)(BlogDetails);
