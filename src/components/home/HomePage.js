import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getAllUsersPosts,getAllObjectTypes } from '../../data/query';
import {_formatDate,_formatTime} from '../../services/CommonServices';
import Modal from 'react-bootstrap-modal';

import TopHeader from './TopHeader';
import HomeBlog from './HomeBlog';
import HomeObject from './HomeObject';
import MyLocation from './MyLocation';
import HomeContentList from './HomeContentList';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"
import HelmetData from '../common/_helmet' ;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;
let isProfile = null;
let UserName = null;

const CONFIG = require('../config');
var SITEURL = CONFIG.SITEURL;
var SITELOGO = CONFIG.SITELOGO;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}
if(LocalStorageData.isProfile){
  isProfile = LocalStorageData.isProfile;
}
if(LocalStorageData.UserName){
  UserName = LocalStorageData.UserName;
}

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class HomePage extends Component {
    constructor(props) {
    super(props);
    this.state = {
        postDataArr:[],
        objectArr:[],
        UserID:ID,

        limit:4,
        offset: 0,
        totalCount: 5,
        objectState:'',
        isScroll:true,

        loading: true,
        metaData:[],
        radius: 5000,
        userLocation: {},
        showModal: false
      }
    }

    getAllUsersPostData = (ObjectType,offsetValue) => {
      let tmpObjectType = '';
      let locationData;
      if(ObjectType){
        tmpObjectType = ObjectType;
      }
      this.state.userLocation && this.state.userLocation.Latitude && this.state.userLocation.Longitude ? 
      locationData = {
          Latitude: this.state.userLocation.Latitude.toString(),
          Longitude: this.state.userLocation.Longitude.toString() 
        }
        :
        locationData = null
      
        this.props.client
        .query({
            query: getAllUsersPosts,
            variables: {
                UserID:parseInt(this.state.UserID),
                ObjectType : tmpObjectType,
                limit: this.state.limit,
                offset: offsetValue,
                Location: locationData,
                Radius: this.state.userLocation && this.state.userLocation.Latitude ? this.state.radius/1000 : null
            },
            fetchPolicy: "network-only"
         }) .then(res => {  
              const data = res.data.getAllUsersPosts.AllUserPost;
              
              var joined = this.state.postDataArr.concat(data.filter((item) => this.state.postDataArr.indexOf(item) < 0))
              if(data.length == 0){
                this.setState({ isScroll:false, loading:false, postDataArr: [] });
              }
               if(joined.length > 0){
                 // document.getElementById('post_list').scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
                  this.setState({
                    postDataArr: joined,
                    totalCount: res.data.getAllUsersPosts.TotalCount, //joined[0].TotalArticleCount
                    loading:false,
                  });

                }
          });          
      };



      getAllObjectList = () => {
        this.props.client
        .query({
            query: getAllObjectTypes,
            fetchPolicy: "network-only"
         }) .then(res => {  
              const data = res.data.getAllObjectTypes;
              this.setState({ objectArr : data,loading:true }) ;

              window.addEventListener("scroll", this.handleScroll);

              this.getAllUsersPostData(this.state.objectState,this.state.offset);
              

          });          
      };

      // componentDidUpdate = () => {
      //   // if (this.props.userObject.ID !== this.state.userId) {
      //   //   this.setState({
      //   //     userId: this.props.userObject.ID
      //   //   });
      //     window.addEventListener("scroll", this.handleScroll);
      //     this.getAllUsersPostData('', this.state.offset); //by default the input value for offset is 0
      // };

      componentWillUnmount = () => {
        window.removeEventListener("scroll", this.handleScroll);
      };

      handleScroll = event => {
       if(this.state.isScroll){
        if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {

          // console.log('postDataArr.length:'+this.state.postDataArr.length)
          // console.log('totalCount:'+this.state.totalCount)
          // console.log('offset:'+this.state.offset)
          // console.log('totalCount:'+this.state.totalCount);

          // console.log('yessssssssssssssss')
          //you're at the bottom of the page
          //show loading spinner and make fetch request to api
          //will update offset
     

          this.setState({ offset:this.state.offset + 1 });

          // this.setState({
          //   offset:
          //     this.state.offset === 0
          //       ? this.state.limit + this.state.offset + 1
          //       : this.state.limit + this.state.offset
          // });
          if (
            this.state.postDataArr.length < this.state.totalCount &&
            this.state.offset < this.state.totalCount
          ) {

          this.setState({ loading:true});   
            
            this.getAllUsersPostData(this.state.objectState,this.state.offset);
          }
          //need to call the function which will add the records in state
          //and thus again call the getArticle function
        }
       }
      };

      _passObject = (e,obj) =>{
        e.preventDefault();
        this.setState({isScroll:true,postDataArr:[],limit:4,offset:0,totalCount:100, objectState:obj})
        this.getAllUsersPostData(obj,0);
    }

    //   _passObject = (e,obj,mainRes) =>{
    //     e.preventDefault();
    //     let filteredData = mainRes.filter(res => {
    //         return res.ObjectType == obj;
    //   });
    //   if(filteredData.length > 0){
    //     document.getElementById('post_list').scrollIntoView({behavior: "smooth", block: "end", inline: "nearest"});
    //   }

    //   this.setState({postDataArr:filteredData})
    // }


      componentDidMount(){
        setTimeout(
        this.setState({ showModal: true})
        ,2000)
          this.getAllObjectList();

          var metaArr = [];

              var title = 'Matching Local Sailing Enthusiast';
              var keywords = 'Crews,Rides,Events,Courses,Skills,Clubs,Services,Rentals,Photo,Video,Matches,Crews,Rides,Events,Courses,Skills,Clubs,Services,Rentals,Photo';
              var description = 'Matching Local Sailing Enthusiast with Day sailors and Weekend Cruising in the US';
              var og_url = SITEURL;
              var og_title = title;;
              var og_description = description;
              var canonical_url = SITEURL;
              var og_image = SITELOGO;

              metaArr.push({
                title:title,
                keywords:keywords,
                description:description,
                og_url:og_url,
                og_title:og_title,
                og_description:og_description,
                canonical_url:canonical_url,
                og_image:og_image,
              });

              this.setState({ metaData:metaArr[0] })

       }

    //    handleLocation = async () => {
    //     if (navigator.geolocation) {
    //       await navigator.geolocation.getCurrentPosition( position => {
    //          this.setState({
    //           myPosition:{
    //             Latitude: position.coords.latitude,
    //             Longitude: position.coords.longitude
    //           }
    //         })
    //       });
    //     } else {
    //         console.log("unavailable");
    //     }
    // };

       getPositions = async (data) => {
         await this.setState({
           userLocation: data.userLocation,
           radius: data.radius,
           postDataArr: [],
           offset: 0,
           isScroll: true
         })
         this.getAllUsersPostData(this.state.objectState,this.state.offset);
       }

       goToProfilePage = () => {
         this.props.history.push('/edit-profile/'+UserName)
       }

render() {
    const { postDataArr,objectArr } = this.state;


  return (
    <Fragment>
        <div className={ ID && !isProfile ? "container blurEffect" : "container"}>
        <HelmetData metaDetails={this.state.metaData} history={this.props}/>

            <TopHeader />
            <HomeBlog />
            <HomeObject objectArr={objectArr} passObject={this._passObject} />
            <MyLocation callback={this.getPositions} />
            <HomeContentList postDataArr={postDataArr} />
            <div className="sweet-loading">
                <PuffLoader
                css={override}
                size={60}
                color={"#123abc"}
                loading={this.state.loading}
                />
            </div>
            
        </div>
        {
          ID && !isProfile &&
          <div className="modal fade show in bd-example-modal-lg " tabindex="-1" role="dialog" style={{ display: 'flex', justifyContent: 'center'}}>
            <div className="modal-dialog modal-dialog-centered  modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                  <p><img src="http://socialenginespecialist.com/PuneDC/sailmatch/images/logo.png" className="img-fluid d-block mx-auto"/></p>
                  <p className="modal_blurTitle">Welcome to sailmatch , sailmatch is about having fun sailing whether you are an experienced sallor or are just thinking about putting your toe into the water, No boat is required and sailmatch is FREE.</p>
                  <ul className="ListBlur">
                    <li>Browse profile of local sailing enthuslast and find other who want to sail with you.</li>
                    <li>Find or Create a sailing group or club</li>
                  </ul>
                  <div className="introMOdal">
                  <div style={{ width: '75%' }}>
                    <iframe width="100%" height="250" src="https://www.youtube.com/embed/bmVKaAV_7-A" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  </div>
                  <div style={{ padding: '25px 10px 0px 10px' }}>
                    <p style={{ textAlign: 'center' }} className="interestBlr">Let's get started by sharing who you are and what you sailing interest are:</p>
                    <div><button onClick={this.goToProfilePage} type="button" className="GoTOProfilePageBtn">Goto Profile Page</button></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        }
      </Fragment>
      );
    }
  }  

  export default compose(
    connect(null,{}),
    withApollo
)(HomePage);
