import React, { Component } from "react";

import InfiniteCarousel from 'react-leaf-carousel';


class HomeObject extends Component {
    constructor(props) {
    super(props)
    }

 

render() {
  const {objectArr} = this.props;
    if (objectArr.length < 1) {
      return null;
    }

    const imagesRendered = objectArr.map((object,index) => (
      <div className="col" key={index}>
          <a 
          className="cursor-pointer"
          onClick={(e)=>this.props.passObject(e,object.ObjectType)} title={object.ObjectType}> 
            <img src={object.ProfileImage} alt="home"/>{object.ObjectType} 
          </a>
        </div> 
    ));


  return (
        <div className="row">
            <div className="col-sm-12 mt-3 mb-5 text-center categories">
            <InfiniteCarousel
        breakpoints={[
          {
            breakpoint: 0,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
            },
          },
          {
            breakpoint: 576,
            settings: {
              slidesToShow: 3,
              slidesToScroll: 3,
            },
          },
        ]}
        
        scrollOnDevice
        slidesToShow={10}
        slidesToScroll={3}
      >
        {imagesRendered}
      </InfiniteCarousel>

              {/* {objectArr && objectArr.length > 0 && objectArr.map((object,index)=>(
               <div className="col-sm-1" key={index}>
                 <a 
                  className="cursor-pointer"
                  onClick={(e)=>this.props.passObject(e,object.ObjectType)} title={object.ObjectType}> 
                   <img src={object.ProfileImage} alt="home"/>{object.ObjectType} 
                  </a>
                </div> ))} */}
            </div>
        </div>
      );
    }
  }
  

  
  export default HomeObject;