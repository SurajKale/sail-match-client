import React, { Component } from "react";
import { Link } from "react-router-dom";
import ImagePlaceholder from '../../services/ImagePlaceholder';
import ObjectLikeDislike from '../common/ObjectLikeDislike';

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class HomeContentItem extends Component {
    constructor(props) {
    super(props);
    this.state = {
        loaded: false,
        totalCount:''
      }
    }
    componentDidMount = () => {
        this.setState({totalCount:this.props.data.LikeCount})
    }

    handleImageLoaded() {
        this.setState({ loaded: true });
      }
      
      objectLike = (count) =>{
        this.setState({totalCount:count})
      }

render() {
    const { loaded, totalCount } = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};  
    const {data} = this.props;
  return (
        <React.Fragment>
            {data &&
             <div className="col-sm-3 mb-5">
                <div className="post_single">
                {!loaded && <ImagePlaceholder/> }
                <Link to={'../'+data.ObjectTypeDetails.Url+'/'+data.Url}>
                    <img src={data.ProfileImage ? data.ProfileImage : '../../assets/images/no_image.jpg'} className="no_image" alt={data.Title} title={data.Title} style={imageStyle} onLoad={this.handleImageLoaded.bind(this)} />
                </Link>                   
                 <h5><Link to={'../'+data.ObjectTypeDetails.Url+'/'+data.Url} title={data.Title}> {data.Title && data.Title.length > 15 ? data.Title.substring(0,15)+'...' : data.Title}</Link></h5>
                 <p>{data.CreatedDate}</p>
                 {data.UserDetails &&
                     <div className="author">
                         <Link to={'profile/'+data.UserDetails.UserName} title="view profile">
                            <span className="author_name" title={(data.UserDetails.FirstName? data.UserDetails.FirstName : '')+' '+(data.UserDetails.LastName ? data.UserDetails.LastName : '')}>
                                <img src={data.UserDetails.ProfileImage ? data.UserDetails.ProfileImage : './assets/images/profile_pic.png'} title={data.UserDetails.FirstName+' '+data.UserDetails.LastName} className="user-icon-home"/>
                                    {(data.UserDetails.FirstName ? data.UserDetails.FirstName : '') +' '+(data.UserDetails.LastName ? data.UserDetails.LastName : '')} 
                                </span>
                             </Link>
                             <span className="category">
                                 <a
                                //   href="javascript:void(0);"
                                  >
                                     <img src={data.ObjectTypeDetails.ProfileImage} title={data.ObjectType}/>
                                 </a>
                                 </span>
                                 </div>}
                 <div className="divider">
                    <hr />
                </div>
            <div className="post_footer">
                {ID == null &&  <span className="col text-left">
                    <i className="far fa-heart"></i>&nbsp; 
                   {totalCount}
                </span>}
               {ID && <ObjectLikeDislike LikeCount={totalCount} ObjectType={data.ObjectType} ReferenceID={data.ReferenceID} status={data.isLike} ReferenceUserID={data.CreatedBy} updateOnParent={this.objectLike} isDetail={false}/> }
                <span className="col text-center"><i className="far fa-comment-alt"></i> {data.CommentCount ? data.CommentCount : 0}</span><span className="col text-right"><i className="fas fa-share"></i> {data.ShareCount ? data.ShareCount : 0}</span></div>
            </div>
         </div>} 
         </React.Fragment>
      );
    }
  }
  
  
  export default HomeContentItem;