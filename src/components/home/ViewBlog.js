import React, { Component, Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getAllPublishBlogs } from '../../data/query';
import { css } from "@emotion/core";
import PuffLoader from "react-spinners/PuffLoader"

const override = css`
  display: block;
  margin: 0 auto;
  border-color: red;
`;

class ViewBlog extends Component {
  state = {
    list: [],
    loading: true
  }

  componentDidMount() {
    this.getAllPublishBlogs()
  }

  getAllPublishBlogs = () => {
    this.props.client
    .query({
      query: getAllPublishBlogs,
      variables: {
        isSeqRequired: false
      },
      fetchPolicy: "network-only"
     }) .then(res => {  
        const data = res.data.getAllPublishBlogs;
        this.setState({ list: data, loading: false })
    });          
  };

  goToDetailsPage = (url) => {
    this.props.history.push('/blog-details/'+url)
  }

  render() {
    const { list } = this.state;
    
    return (
      <Fragment>
        <div className="container">
          <div className="row">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a href="/" className="desktop_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
          <div className="sweet-loading">
            <PuffLoader
              css={override}
              size={60}
              color={"#123abc"}
              loading={this.state.loading}
            />
          </div>
          <br />
          <div className="row">
            {
              list && list.length > 0 &&
              list.map(blog => (
                <div className="col-md-4 mb-2 text-center" key={blog.ID} onClick={() => this.goToDetailsPage(blog.Url)}>
                  <a href="#" className="card_home">
                    <img className="blogCardsSize" src={ blog.FeatureImage ? blog.FeatureImage : "./assets/images/Home_img01.png"} alt={blog.Title}/> 
                    <div className="Blog_text">
                      <h4>{blog.Title}</h4>
                      <p>{blog.CreatedDate}</p>
                    </div>
                  </a>
                </div>
              ))
            }
          </div>
        </div>
      </Fragment>
    );
  }
}
  
export default compose(connect(null,{}),withApollo)(ViewBlog);