import React from "react";
import GooglePlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
  geocodeByPlaceId,
} from "react-google-places-autocomplete";
// If you want to use the provided css
import "react-google-places-autocomplete/dist/index.min.css";

class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      country: null,
      state: null,
      city: null,
      mainPlace: null,
      placeId: null,
      latitude: null,
      longitude: null,
      zipCode: null,
    };
    this._getAddress = this._getAddress.bind(this);
  }

  _getAddress(value) {
    let country = null;
    let state = null;
    let city = null;
    let mainPlace = null;
    let placeId = null;
    let placeData = [];

    if (value.place_id) {
      placeId = value.place_id;
    }

    if (value.structured_formatting) {
      mainPlace = value.structured_formatting.main_text;
    }

    if (value.description) {
      placeData = value.description.split(",");
      country = placeData[placeData.length - 1];
      state = placeData[placeData.length - 2];
      city = placeData[placeData.length - 3];

      geocodeByAddress(value.description)
        .then((results) => getLatLng(results[0]))
        .then(({ lat, lng }) =>
          geocodeByPlaceId(placeId).then((results) =>
            this._setAll(
              lat,
              lng,
              results,
              country,
              state,
              city,
              mainPlace,
              placeId
            )
          )
        );
    }
  }

  _setAll = async (lat, lng, results, country, state, city, mainPlace, placeId) => {
    let code = results[0].address_components;
    var filteredObj = code.filter(function(obj){
      return obj.types[0] === "postal_code";
    })
    console.log(code,"code");
    console.log(filteredObj,"9999");
    await this.setState({
      latitude: lat,
      longitude: lng,
      country: country,
      state: state,
      city: city,
      mainPlace: mainPlace,
      placeId: placeId,
      zipCode: filteredObj && filteredObj.length > 0 ? filteredObj[0].long_name : null,
    });
    var tmpRes = {
      latitude: lat,
      longitude: lng,
      country: country,
      state: state,
      city: city,
      mainPlace: mainPlace,
      placeId: placeId,
      zipCode: filteredObj && filteredObj.length > 0 ? filteredObj[0].long_name : null,
    };
    this.props.callback(tmpRes);
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { renderingValue } = this.props;
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-12 my-2">
            <label>Location Street Name </label>
            <GooglePlacesAutocomplete
              placeholder=""
              renderInput={(props) => (
                <div className="">
                  <input 
                    className="form-control search_places"
                    {...props}
                  />
                </div>
              )}
              loader={"Loading..."}
              onSelect={this._getAddress}
              disabled={this.props.isDisabled}
              initialValue={renderingValue ? renderingValue : ""}
            />
            <a href="#!">
              <img
                alt="location"
                src="../assets/images/location.svg"
                className="location-icon"
              />
            </a>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Component;
