import React from 'react'; 
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { getRideByUrl } from '../../data/query';
import { ToastContainer, toast } from "react-toastify";
import {_formatDate,_formatTime, url } from '../../services/CommonServices';
import ImagePlaceholder from '../../services/ImagePlaceholder';
import {deleteRide} from '../../data/mutation'
import ConfirmationPopup from '../common/ConfirmationPopup';
import ObjectLikeDislike from "../common/ObjectLikeDislike";
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";
import PostListParent from "../common/PostListParent";
import  formatcoords from 'formatcoords';

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class RideList extends React.Component {
    constructor(props) {
      super(props);
        this.state = {
            rideListData:[],
            LocationData:[],
            id:this.props.match.params.id,
            loaded: false,
            rideData:{},
            totalCount:"",

            metaData:[],
            showChatPopup: false,
            showShareModel: false,
        };
        this.goBack = this.goBack.bind(this);
    }

    goBack(){
        this.props.history.goBack();
    }

    getRideByIdData = () => {

        this.props.client
        .query({
            query: getRideByUrl,
            variables: {
                RideUrl:this.state.id
            },
            fetchPolicy: "network-only"
         }) .then(res => {  
              const data = res.data.getRideByUrl;
              const location = res.data.getRideByUrl.Location;
              const EventType = res.data.getRideByUrl.EventType;

              this.setState({  
                  isLike:data.isLike,
                  renderLike:true,                            
                  rideData:data, rideListData : data,LocationData:location,EventTypeData:EventType,totalCount:res.data.getRideByUrl.LikeCount }) 
              var metaArr = [];
              metaArr.push({
               title : data.Title ?  data.Title : SITE_STATIC_NAME,
               keywords : data.ObjectType, SITE_STATIC_NAME,
               description : data.Description ? data.Description : '',
               og_url : window.location.href,
               og_title : data.Title ?  data.Title : SITE_STATIC_NAME,
               og_description : data.Description ? data.Description : '',
               canonical_url : window.location.href,
               og_image : data.ProfileImage ? data.ProfileImage : SITELOGO,
               amp_url : window.location.href,
              })

              this.setState({metaData:metaArr[0],rideData:data, rideListData : data,LocationData:location,EventTypeData:EventType,totalCount:res.data.getRideByUrl.LikeCount }) 
          });          
      };

      objectLike= (count) =>{
        console.log(count);
      this.setState({totalCount:count})
      }

    componentDidMount(){
        window.scrollTo(0, 0);
     this.getRideByIdData()
    }

    deleteObject=()=>{
        this.props.client
        .mutate({
            mutation: deleteRide,
            variables: {
              RideID:parseInt(this.state.rideData.ID),
              UserID:ID
          },
         })
          .then(res => { 
            if(res.data.deleteRide){
              toast.success("Ride deleted successfully", {
                onClose: () => this.props.history.push("/"),
              });
            }
          });  
      }
    handleImageLoaded() {
        this.setState({ loaded: true });
      }
    
    openChatPopup = () => {
        // document.body.style.overflowY = "hidden";
        this.setState({ showChatPopup: !this.state.showChatPopup });
    };

    openShareModal = () => {
        // document.body.style.overflowY = "hidden";
        this.setState({ showShareModel: !this.state.showShareModel });
    };

    goToEditPage = (url) => {
        this.props.history.push('/edit-ride/'+this.state.id)
    }

render(){
    const {rideListData,LocationData,EventTypeData,loaded,totalCount,renderLike,isLike} = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};
    var point = formatcoords(parseFloat(LocationData.Latitude), parseFloat(LocationData.Longitude)).format();

    return(   
       
        <div className="container">
         <HelmetData metaDetails={this.state.metaData} history={this.props}/>

            <div className="row">
                <div className="col-md-12">
                    <div className="col-md-3 mb-2 mt-2"> 
                        <a className="desktop_back" href="javascript:void(0);" onClick={this.goBack}>
                            <img src="../../assets/images/back_icon.png" alt="Back" /></a> 
                        </div>
                </div>
            </div>

{rideListData &&
        <React.Fragment>
            <div className="col-md-12 col-12">
                <div className="row">
                    {/* <div className="col-md-3 col-sm-12 mb-2 mt-2"> 
                    {!loaded && <ImagePlaceholder/> }
                        <img src={rideListData.ProfileImage ? rideListData.ProfileImage : '../../assets/images/no_image.jpg'} classNameName="ride_image" alt="Profile" style={imageStyle} onLoad={this.handleImageLoaded.bind(this)}/> </div> */}
                    <div className="col-md-3 col-sm-12 mb-2 mt-2"> 
                <img src={rideListData.ProfileImage ? rideListData.ProfileImage : '../../assets/images/no_image.jpg'} className="profImg sideImg" alt="Back"/> 
                <div className="userSectionSocialSection">

                        {/* <div className="like">
                            <img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2"/>
                         <span> 140</span>
                        </div> */}

                    <div className="like">
                       {ID && renderLike && <ObjectLikeDislike
                                LikeCount={totalCount} 
                                ObjectType={rideListData.ObjectType} 
                                ReferenceID={rideListData.ID} 
                                status={isLike} 
                                ReferenceUserID={rideListData.CreatedBy} 
                                updateOnParent={this.objectLike} 
                                isDetail={true}/>}
                                { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
                    </div>
                    <div class="bar">|</div>
                                <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {rideListData.CommentCount || 0}</span></div>
                    <div class="bar">|</div>
                    <div className="forward" onClick={this.openShareModal}><img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> {rideListData.ShareCount || 0}</span></div>
                    { ID && this.state.showShareModel &&
                        <ShareContent 
                            ObjectType="Rides"
                            ReferenceID={parseInt(this.state.rideData.ID)}
                            ReferenceUserID={ID}
                            PostUrl={this.state.id}
                            closeHandler={this.openShareModal}
                            shareUrl={url.concat(this.props.match.url)}
                            title={rideListData.Title}
                            quote={rideListData.Title} />
                    }
                </div>    
            </div>
                    <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
                        
                        <h2>
                            {rideListData.Title ? rideListData.Title : ''}
                            {ID && this.state.rideData.CreatedBy === ID &&
                                <i 
                                    className="fa fa-pencil cursor-pointer"
                                    title='Edit'
                                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                                    onClick={this.goToEditPage}
                                />
                            }
                        </h2>
                        <div className="date-time-img">
                            <img src="../../assets/images/Rides.svg" alt="Back" /></div>
                        <p className="" id="date-time-text"><span>From :</span><span> {_formatDate(rideListData.StartDate)}, {_formatTime(rideListData.StartTime)}</span> - To :<span> {_formatDate(rideListData.EndDate)}, {_formatTime(rideListData.EndTime)}</span></p>    
                        <p className=""> 
                            <img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2" /> <span> {LocationData.Address}</span> </p>
                        <p className=""> 
                        <img src="../../assets/images/position-icon.png" alt="position-icon" className="pr-2" /> <span> {point}</span> </p>
                        <p className="date-time-text">  
                            <button className="crewBtn">Boat Profile</button>
                            <button className="crewBtn">My Profile</button>
                            { this.state.rideData.CreatedBy !== ID && <button className="crewBtn" onClick={this.openChatPopup}> Contact Me</button>}
                            {ID && this.state.rideData.CreatedBy == ID && <ConfirmationPopup callBack={this.deleteObject} object={"Ride"}/>}    
                        </p> 
                        <hr />  
                        {/* <div className="like">
                            <img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2" /><span> {rideListData.LikeCount ? rideListData.LikeCount : 0}</span></div>
                        <div className="comment">
                            <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" /><span> {rideListData.CommentCount ? rideListData.CommentCount : 0}</span></div>
                        <div className="forward">
                            <img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6" /><span> {rideListData.ShareCount ? rideListData.ShareCount : 0}</span></div>         */}
                    
                    </div>        
                </div>
            </div>

            <div className="row crewAttributes">
                <div className="col-md-3">
                    <h5 className="about-event">Activity Type</h5>
                </div>
                <div className="col-md-9">
                    {EventTypeData && EventTypeData.map((event,index)=>(
                        <button key={index} className="crewBtn">{event.Name}</button>))}
                </div>
            </div>
            <div className="row crewAttributes">
                <div className="col-md-3">
                    <h5 className="about-event">Gender</h5>
                </div>
                <div className="col-md-9">
                    <button className="crewBtn">{rideListData.Gender}</button>
                </div>
            </div>
            <div className="row crewAttributes">
                <div className="col-md-3">
                    <h5 className="about-event">Prefered Skill Level</h5>
                </div>
                <div className="col-md-9">
                    <button className="crewBtn">{rideListData.SkillLevel}</button>
                </div>
            </div>
            <div class="col-md-12">
                <div class="row">
                    <h5 class="about-event">Description</h5>
                    <p class="about-event-info">
                        {rideListData.Description}
                    </p>    
                </div>
            </div>
            </React.Fragment>}

    {rideListData.ObjectType &&
        <PostListParent ReferenceID={parseInt(this.state.rideData.ID)} ObjectType={rideListData.ObjectType} />}

            <ToastContainer autoClose={1000}/>
            { ID && this.state.showChatPopup && 
                <ChatApp
                sender={ID}
                receiver={this.state.rideData.CreatedBy}
                profilePic={rideListData.ProfileImage}
                title={rideListData.Title}
                objectType="Rides"
                referenceID={parseInt(this.state.rideData.ID)}
                popupHandler={this.openChatPopup}
                myID={ID}
                />
            }
            
        </div>
        );
    }
}


export default compose(
    connect(null,{}),
    withApollo
)(RideList);
