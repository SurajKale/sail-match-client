import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import TimePicker from "rc-time-picker";
import "rc-time-picker/assets/index.css";
import { StyledDropZone } from "react-drop-zone";
import "react-drop-zone/dist/styles.css";
import Select from "react-select";

import { createRideAction } from "../../data/actions/RideAction";
import { getAllEventTypes, myBoatProfileList } from "../../data/query";

import LocationContent from "../location/LocationContent";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class AddRide extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      eventsArray: [],
      skillLevel: "",
      startDate: new Date(),
      endDate: new Date(),
      startTime: "",
      endTime: "",
      rideTitle: "",
      description: "",
      city: "",
      latitude: "",
      longitude: "",
      address: "",
      state: "",
      gpsCoordinate: "",
      zipCode: "",
      country: "",
      boatProfile: [],
      button: true
    };
  }

  getBoatProfileList = () => {
    const UserID = ID;
    this.props.client
      .query({
        query: myBoatProfileList,
        variables: {
          UserID,
        },
      })
      .then((res) => {
        this.setState({
          boatProfile: res.data.myBoatProfileList,
        });
      });
  };

  getAllEventsTypes = () => {
    this.props.client
      .query({
        query: getAllEventTypes,
      })
      .then((res) => {
        res.data.getAllEventTypes.forEach((item) => {
          item.isSelected = false;
        });
        this.setState({
          eventsArray: res.data.getAllEventTypes,
        });
      });
  };

  handleStartChange = async (startDate) => {
    let date = startDate.toISOString().substring(0, 10);
    this.setState({
      finalStartDate: date,
      startDate: startDate,
    });
  };

  handleEndChange = async (e) => {
    let date = e.toISOString().substring(0, 10);
    this.setState({
      finalEndDate: date,
      endDate: e,
    });
  };

  handleStartTimeChange = (e) => {
    this.setState({ startTime: e });
  };
  handleEndTimeChange = (e) => {
    this.setState({ endTime: e });
  };

  handleStartDateChange = (date) => {
    this.setState({
      startDate: date,
      endDate: date,
    });
  };

  handleEndDateChange = (date) => {
    this.setState({
      endDate: date,
    });
  };

  handleChanges = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  handleEventsSelect = (category) => {
    let oldEvents = this.state.eventsArray.slice();
    oldEvents.forEach((item) => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });
    this.setState({
      eventsArray: oldEvents,
    });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  getLocationDetails = (location) => {
    let address =
      location.mainPlace +
      "," +
      "" +
      location.city +
      "," +
      "" +
      location.zipCode +
      "," +
      "" +
      location.state +
      "," +
      "" +
      location.country;
    this.setState({
      city: location.city,
      latitude: location.latitude,
      longitude: location.longitude,
      address: address,
      state: location.state,
      zipCode: location.zipCode,
      gpsCoordinate: location.latitude + '"N' + "-" + location.longitude + '"E',
    });
  };

  createRide = (e) => {
    this.setState({ button : !this.state.button })
    const {
      skillLevel,
      startDate,
      endDate,
      startTime,
      endTime,
      eventsArray,
      rideTitle,
      description,
      latitude,
      longitude,
      address,
      profilePic,
      gender
    } = this.state;
    const CreatedBy = ID;
    const boat = this.state.boat.value;

    let lat = latitude.toString();
    let long = longitude.toString();
    const Location = { Latitude: lat, Longitude: long, Address: address };

    var options = eventsArray.filter(function (item) {
      return item.isSelected;
    });
    const EventType = options.map(function (item) {
      return { EventTypeID: item.ID, Name: item.EventTitle };
    });
    this.props
      .createRideAction(
        this.props.client,
        rideTitle,
        ID,
        description,
        EventType,
        profilePic,
        skillLevel,
        startDate,
        endDate,
        startTime,
        endTime,
        Location,
        CreatedBy,
        gender,
        boat
      )
      .then((res) => {
        toast.success("Ride added successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  componentDidMount = () => {
    this.getAllEventsTypes();
    this.getBoatProfileList();
  };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {
    let countryArr = [];

    this.state.boatProfile.map((items) => {
      var obj = {};
      obj["value"] = items.ID;
      obj["label"] = items.Title;
      return countryArr.push(obj);
    });

    const {
      eventsArray,
      skillLevel,
      gender,
      startDate,
      endDate,
      endTime,
      startTime,
      rideTitle,
      description,
      profilePic,
      gpsCoordinate,
      zipCode,
      state,
      city,
      boat,
      button
    } = this.state;
    const format = "h:mm a";
    const label = profilePic
      ? "Profile image successfully added"
      : "Click or drop your image here";

    const enabled =
      skillLevel &&
      startDate &&
      endDate &&
      startTime &&
      endTime &&
      rideTitle &&
      description &&
      profilePic &&
      boat &&
      button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            <a
              href="#!"
              style={{ cursor: "pointer" }}
              className="desktop_back"
              onClick={this.goBack}
            >
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="#!" onClick={this.goBack} className="float-left mobile_back">
                <img src="../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Ride</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="typeOfEvent">Type of Event</label>

                    <div className="">
                      {eventsArray &&
                        eventsArray.map((type) => {
                          return (
                            <label
                              key={type.ID}
                              className={`${
                                type.isSelected
                                  ? "selectedEventTypeLabel"
                                  : "eventTypeLabel"
                              }`}
                              onClick={(e) => this.handleEventsSelect(type)}
                            >
                              {type.EventTitle}
                            </label>
                          );
                        })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="skillLevel">Skill Level</label>
                    <div>
                      <label
                        className={
                          skillLevel === "Any"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Any
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Any"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Beginner"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Beginner
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Beginner"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Intermediate"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Intermediate
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Intermediate"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                      <label
                        className={
                          skillLevel === "Advanced"
                            ? "selectedSkillLabel"
                            : "skillLabel"
                        }
                      >
                        Advanced
                        <input
                          type="radio"
                          name="skillLevel"
                          value={"Advanced"}
                          onChange={this.handleChanges}
                          style={{ opacity: "0" }}
                        />
                      </label>
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="gender">Gender</label>
                    <div className="" role="group" aria-label="Basic example">
                      <input
                        type="button"
                        className={
                          gender === "Male"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="gender"
                        value="Male"
                        onClick={this.handleChanges}
                      />
                      <input
                        type="button"
                        className={
                          gender === "Female"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        name="gender"
                        value="Female"
                        onClick={this.handleChanges}
                      />
                    </div>
                  </div>
                </div>

                <hr />

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label>Ride Start Date</label>
                    <DatePicker
                      id="startDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{ width: "250px" }}
                      selected={startDate}
                      minDate={startDate}
                      onChange={this.handleStartDateChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="rideEndDate">Ride End Date</label>
                    <DatePicker
                      id="endDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{ width: "250px" }}
                      selected={endDate}
                      minDate={startDate}
                      onChange={this.handleEndDateChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="rideStartTime">Ride Start Time</label>
                    <TimePicker
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleStartTimeChange}
                      value={startTime}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="rideEndTime">Ride End Time</label>
                    <TimePicker
                      showSecond={false}
                      className="form-control"
                      onChange={this.handleEndTimeChange}
                      value={endTime}
                      format={format}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                </div>

                <LocationContent callback={this.getLocationDetails} />

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="city">City</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="city"
                      placeholder="City"
                      name="city"
                      value={city}
                    />
                  </div>

                  <div className="col-sm-6 my-2">
                    <label htmlFor="state">State</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="state"
                      placeholder="State"
                      name="state"
                      value={state}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="zipCode">Zipcode</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="zipCode"
                      placeholder="Zipcode"
                      name="zipCode"
                      value={zipCode}
                      onChange={this.handleChanges}
                    />
                  </div>

                  <div className="col-sm-6 my-2">
                    <label htmlFor="gpsCoordinate">GPS Coordinates</label>
                    <input
                      readOnly
                      type="text"
                      className="form-control"
                      id="gpsCoordinate"
                      placeholder="GPS Coordinates"
                      name="gpsCoordinate"
                      value={gpsCoordinate}
                    />
                  </div>
                </div>

                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="rideTitle">Select Boat</label>
                    <Select
                      isSearchable={true}
                      isClearable={true}
                      className="basic-single"
                      classNamePrefix="select"
                      name="boat"
                      value={boat}
                      onChange={(value) => this.setState({ boat: value })}
                      options={
                        ({
                          value: "",
                          label: "Select boat",
                          isDisabled: true,
                        },
                        countryArr)
                      }
                      placeholder="Select boat"
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="rideTitle">Ride Title</label>
                    <input
                      type="text"
                      className="form-control"
                      id="rideTitle"
                      placeholder="Ride Title"
                      name="rideTitle"
                      value={rideTitle}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="description"
                      value={description}
                      onChange={this.handleChanges}
                    ></textarea>
                  </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="profilePicture">Profile Picture</label>
                    <StyledDropZone
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                {this.state.profilePic && (
                  <div className="row">
                    <div className="col-sm-12 my-2 text-center">
                      <img
                        src={this.state.profilePic}
                        className="img-fluid mb-3 member_pic_preview"
                        alt="Profile"
                      />
                    </div>
                  </div>
                )}

                {/* <hr /> */}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        disabled={!enabled}
                        className="btn btn-primary save_profile"
                        onClick={this.createRide}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

export default compose(
  connect(null, { createRideAction }),
  withApollo
)(AddRide);
