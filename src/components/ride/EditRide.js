import React from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";


import { createRideAction } from "../../data/actions/RideAction";
import { getRideByUrl } from '../../data/query';
import { StyledDropZone } from "react-drop-zone";
import "react-drop-zone/dist/styles.css";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class EditRide extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id:this.props.match.params.id,
      eventsArray: [],
      skillLevel: "",
      startDate: new Date(),
      endDate: new Date(),
      startTime: "",
      endTime: "",
      rideTitle: "",
      description: "",
      profilePic: "",
      city: "",
      latitude: "",
      longitude: "",
      address: "",
      state: "",
      gpsCoordinate: "",
      zipCode: "",
      country: "",
      boatProfile: [],
      button: true,
      rideID: 0
    };
  }

  handleChanges = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
  });

  componentDidMount(){
    window.scrollTo(0, 0);
    this.getRideByIdData()
  }

  getRideByIdData = () => {
    this.props.client
    .query({
        query: getRideByUrl,
        variables: {
            RideUrl:this.state.id
        },
        fetchPolicy: "network-only"
     }) .then(res => {  
          const data = res.data.getRideByUrl;
          this.setState({
            rideID: data.ID,
            rideTitle: data.Title,
            description: data.Description,
            profilePic: data.ProfileImage,
            locationObject: data.Location,
            skillLevel: data.skillLevel,
            startDate: data.StartDate,
            endDate: data.EndDate,
            startTime: data.StartTime,
            endTime: data.EndTime,
            gender: data.Gender,
            eventType: data.EventType,
            boat: data.BoatID
          }) 
      });          
  };

  createRide = (e) => {
    const {
      rideID,
      skillLevel,
      startDate,
      endDate,
      startTime,
      endTime,
      rideTitle,
      description,
      locationObject,
      eventType,
      profilePic,
      gender,
      boat
    } = this.state;
    const CreatedBy = ID;

    var Type =
			eventType.map(({EventTypeID, Name }) => ({EventTypeID: EventTypeID, Name: Name }));
			
		delete locationObject.__typename;

    this.props
      .createRideAction(
        this.props.client,
        rideTitle,
        ID,
        description,
        Type,
        profilePic,
        skillLevel,
        startDate,
        endDate,
        startTime,
        endTime,
        locationObject,
        CreatedBy,
        gender,
        boat,
        rideID
      )
      .then((res) => {
        toast.success("Ride Updated successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {
    let countryArr = [];

    const {
      rideTitle,
      description,
      profilePic,
    } = this.state;
    const label = profilePic
      ? "Profile image successfully added"
      : "Click or drop your image here";

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            <a
              href="#!"
              style={{ cursor: "pointer" }}
              className="desktop_back"
              onClick={this.goBack}
            >
              <img src="../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="#!" onClick={this.goBack} className="float-left mobile_back">
                <img src="../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Edit Ride</h1>
              <form className="mt-4">

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="rideTitle">Ride Title</label>
                    <input
                      type="text"
                      className="form-control"
                      id="rideTitle"
                      placeholder="Ride Title"
                      name="rideTitle"
                      value={rideTitle}
                      onChange={this.handleChanges}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="description"
                      value={description}
                      onChange={this.handleChanges}
                    ></textarea>
                  </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="profilePicture">Profile Picture</label>
                    <StyledDropZone
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                {this.state.profilePic && (
                  <div className="row">
                    <div className="col-sm-12 my-2 text-center">
                      <img
                        src={this.state.profilePic}
                        className="img-fluid mb-3 member_pic_preview"
                        alt="Profile"
                      />
                    </div>
                  </div>
                )}

                {/* <hr /> */}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        onClick={this.createRide}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

export default compose(
  connect(null, { createRideAction }),
  withApollo
)(EditRide);
