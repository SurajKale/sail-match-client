import React, { Component, Fragment } from 'react';
import { withApollo } from "react-apollo";
import { connect, useDispatch, useSelector } from "react-redux";
import { compose } from "redux";
import ChatApp from './ChatPage';
import { getChatNotificationList } from "../../data/query";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class ChatNotification extends Component {
    constructor(props) {
        super(props);
        this.state = {
            notificationArray: [],
            showChatPopup: false,
            objectType: "",
            objectTitle: "",
            objectProfilePic: "",
            objectID: 0,
            receiver: 0,
            Hidden: false
        }
    }

    componentDidMount () {
        if(ID) {
          this.getChatNotificationList()
        }
    }
    
    getChatNotificationList = () => {
    this.props.client
        .query({
            query: getChatNotificationList,
            variables: {
            UserID: parseInt(ID),
            },
            fetchPolicy: "network-only"
        })
        .then(res => {
            this.setState({
                notificationArray: res.data.getChatNotificationList
            })
        });
    }

    showChatBox = async (type, title, profile, objectId, senderID) => {
        await this.setState({ 
            showChatPopup: !this.state.showChatPopup,
            objectType: type,
            objectTitle: title,
            objectProfilePic: profile,
            objectID: objectId,
            receiver: senderID,
            Hidden: true
        })
    }

    render() {
        const { notificationArray, receiver, objectID, objectProfilePic, objectTitle,
            objectType, Hidden } = this.state;

        return (
            <Fragment>
                <div className={ Hidden ? "messageMotificationBox message-notification-hidden" : "messageMotificationBox" }>
                <div id="triangle-up"></div>
                    <ul className="notificationUl">
                        <li className="notificationText"><div><strong>Messages</strong> </div></li>
                        {
                            notificationArray && notificationArray.length > 0 ?
                            notificationArray.map((msg, index) => {
                                return <li className="msgItem" key={index} onClick={() => {this.showChatBox(msg.objectType, msg.ObjectDetails.Title, msg.ObjectDetails.ProfileImage, msg.ObjectDetails.ReferenceID, msg.sender.ID )}}>
                                    <div className="messageImage">
                                        <img src={ msg.ObjectDetails && msg.ObjectDetails.ProfileImage ? msg.ObjectDetails.ProfileImage : "../../assets/images/4.png"} alt="small-photo-2" className="msgImg"/>
                                        <img src={ msg.ObjectDetails && msg.ObjectDetails.UserID === msg.sender.ID ? msg.sender.ProfileImage : msg.receiver.ProfileImage } className="msgSubImg" alt="small-photo-2"/>
                                    </div>
                                    <div className="messageInfo">                      		
                                        <p className="messageCat"><strong>{msg.ObjectDetails && msg.ObjectDetails.Title}</strong><span>{msg.createdAt}</span></p>                        		
                                        <p className="messageName">{msg.sender.FirstName && msg.sender.LastName ? msg.sender.FirstName.concat(' ', msg.sender.LastName)  : msg.sender.UserName } </p>
                                        <p className="messageStatus">{msg.message} </p>
                                    </div>
                                </li>
                            })
                            :
                            <li><a href="notification-list.html" className="seeAll">No Messages</a></li>
                        }
                        { notificationArray && notificationArray.length > 3 ? <li><a href="notification-list.html" className="seeAll">See All</a></li> : null }
                    </ul>      
                </div> 
                {
                    this.state.showChatPopup && ID &&
                    <ChatApp 
                        sender={ID}
                        receiver={receiver}
                        profilePic={objectProfilePic}
                        title={objectTitle}
                        objectType={objectType}
                        referenceID={objectID}
                        popupHandler={this.showChatBox}
                        myID={ID}
                    />
                }
            </Fragment>
        )
    }
}

export default compose(
    connect(null),
    withApollo
  )(ChatNotification);
