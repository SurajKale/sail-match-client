import React, { Fragment } from "react";
import { Twemoji } from 'react-emoji-render';

function ChatCard(props) {

    const goToProfilePage = (username) => {
        props.history.push('/profile/' +username);
        window.location.reload()
    }

    return (
        <Fragment>
            <div className="row nomargin">
            { props.type === "VideoOrImage" ?

	            <div class="col-10 p-0 text-center">
                    <img
                        src={props.image}
                        alt="img"
                        className="youChatUploadImg"
                    />
                </div> :

                <div class="col-12 "> 
                    <p className={ props.myID === props.sender.ID ? "me" : "you"} >
                        <img 
                            src={ props.sender.ProfileImage ? props.sender.ProfileImage :  "../../assets/images/small-photo-1.png"}
                            className="chatUserImg"
                            alt="Back"
                            onClick={() => {goToProfilePage(props.sender.UserName)}}/>
                        &nbsp;<span><Twemoji text={props.message} /></span>
                    </p>
                </div>
            }
            </div>
        </Fragment>
    )
}

export default ChatCard;