import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import ReactStars from "react-rating-stars-component";
import  formatcoords from 'formatcoords';
import { ToastContainer, toast } from "react-toastify";

import { getServiceDetailsByID } from "../../data/actions/ServicesAction";
import {getViewAllPictures} from '../../data/actions/CommonActions';
import { deleteServices } from "../../data/mutation";

import { url } from "../../services/CommonServices";
import ImagePlaceholder from "../../services/ImagePlaceholder";
import PhotosContent from "../common/PhotosContent";
import VideosContent from "../common/VideosContent";
import PostListParent from "../common/PostListParent";
import ConfirmationPopup from "../common/ConfirmationPopup";
import ObjectLikeDislike from '../common/ObjectLikeDislike';
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class DisplayService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.id,
      title: "",
      description: "",
      startTime: "",
      endTime: "",
      profilePic: "",
      address: "",
      location: "",
      Latitude: "",
      Longitude: "",
      loaded: false,
      phone: "",
      serviceDetails:{},
      renderLike:false,
      metaData:[],
      showChatPopup: false,
      showShareModel: false,
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getServiceData()
  }

  getServiceData = async () => {
    await this.props
      .getServiceDetailsByID(this.props.client, this.props.match.params.id)
      .then((res) => {
        if (res.data.getServiceByUrl !== null) {
          let serviceData = res.data.getServiceByUrl;

          var metaArr = [];
          metaArr.push({
           title : serviceData.Title ?  serviceData.Title : SITE_STATIC_NAME,
           keywords : serviceData.ObjectType, SITE_STATIC_NAME,
           description : serviceData.Description ? serviceData.Description : '',
           og_url : window.location.href,
           og_title : serviceData.Title ?  serviceData.Title : SITE_STATIC_NAME,
           og_description : serviceData.Description ? serviceData.Description : '',
           canonical_url : window.location.href,
           og_image : serviceData.ProfilePicture ? serviceData.ProfilePicture : SITELOGO,
           amp_url : window.location.href,
          })

          this.setState({
            isLike:serviceData.isLike,
            renderLike:true,
            metaData:metaArr[0],
            serviceDetails:serviceData,
            serviceID: serviceData.ID,
            title: serviceData.Title,
            description: serviceData.Description,
            startDate: serviceData.StartDate,
            endDate: serviceData.EndDate,
            startTime: serviceData.StartTime,
            endTime: serviceData.EndTime,
            location: serviceData.Location.MapLocation,
            Latitude: serviceData.Location.Latitude,
            Longitude: serviceData.Location.Longitude,
            address: serviceData.Location.Address,
            profilePic: serviceData.ProfilePicture,
            websiteurl: serviceData.WebsiteUrl,
            pictures: serviceData.Pictures,
            rating: serviceData.Rating,
            createdBy: serviceData.CreatedBy,
            totalCount:serviceData.LikeCount,
            objectType:serviceData.ObjectType,
            status:serviceData.Status,
            phone: serviceData.Phone,
            userDetailsPic:serviceData.UserDetails,
          });
        } else {
          toast.error("Something wents wrong");
        }
      })
    .catch(function (error) {
      console.log(error);
    });
  }

  handleImageLoaded = () => {
    this.setState({ loaded: true });
  };

  deleteObject = () => {
    this.props.client
      .mutate({
        mutation: deleteServices,
        variables: {
          ServiceID: parseInt(this.state.serviceID),
          UserID: ID,
        },
      })
      .then((res) => {
        if (res.data.deleteServices) {
          toast.success("Service deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  openShareModal = () => {
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  openChatPopup = () => {
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  onStarClick(nextValue, prevValue, name) {
    this.setState({ rating: nextValue });
  }

  objectLike= (count) =>{
    console.log(count);
    this.setState({totalCount:count})
  }

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.serviceDetails.ID,this.state.serviceDetails.CreatedBy,this.state.serviceDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({ pictures:picturesData.Pictures, userDetailsPic:picturesData.UserDetails })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    });
   }

  viewLessPictures = () =>{
    let queryResult = this.state.pictures
    let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
      filterdata.push(queryResult[i]);
      if (filterdata.length == 4) {
        break;
      }
    }
    this.setState({pictures:filterdata})
  }

  goToEditPage = (url) => {
    this.props.history.push('/edit-service/'+this.state.url)
  }

  render() {
    const { createdBy, title, description, pictures, rating, profilePic, address, Latitude,
      Longitude, loaded, websiteurl, totalCount, objectType, phone, serviceDetails,
      isLike, renderLike } = this.state;
    
    var point = formatcoords(parseFloat(Latitude), parseFloat(Longitude)).format();
    
    return (
      <div className="container">
        <HelmetData metaDetails={this.state.metaData} history={this.props}/>
        <div className="row">
          <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a className="desktop_back cursor-pointer" onClick={this.goBack}>
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
        </div>
        <div className="col-md-12 col-12">
          <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2">
              {!loaded && <ImagePlaceholder />}
              <img
                src={
                  profilePic
                    ? profilePic
                    : "../../assets/images/photo-1535024966840-e7424dc2635b.png"
                }
                className="profImg sideImg"
                alt="Back"
                onLoad={this.handleImageLoaded}
              />
              <div className="userSectionSocialSection">
                <div className="like">
              {ID && renderLike == true &&  <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={objectType} 
                ReferenceID={this.state.serviceID} 
                status={isLike} 
                ReferenceUserID={createdBy} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                { ID == null && <> <img
                    src="../../assets/images/dislike.png"
                    alt="heart-icon"
                    className="pr-2"
                  /> 
                  <span>{totalCount}</span></>}
                </div>
                <div class="bar">|</div>
                <div className="comment">
                  <img
                    src="../../assets/images/comment-icon.png"
                    alt="comment-icon"
                    className="pl-6"
                  />
                  <span> {serviceDetails.CommentCount || 0}</span>
                </div>
                <div class="bar">|</div>
                <div className="forward" onClick={this.openShareModal}>
                  <img
                    src="../../assets/images/forward-icon.png"
                    alt="forward-icon"
                    className="pl-6"
                  />
                  <span> {this.state.serviceDetails.ShareCount || 0}</span>
                </div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                    ObjectType="Services"
                    ReferenceID={parseInt(this.state.serviceID)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={title}
                    quote={title} />
                }
              </div>
            </div>

            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {title}
                {ID && this.state.createdBy === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img">
                <img src="../../assets/images/Service.svg" alt="Back" />
              </div>
              <table className="addressTable">
                <tr>
                  <td className="add-icon">
                    <img
                      src="../../assets/images/location-icon.png"
                      alt="location-icon"
                      className="pr-2"
                    />
                  </td>
                  <td className="add">{address}</td>
                </tr>
                <tr>
                  <td className="add-icon">
                    <img
                      src="../../assets/images/position-icon.png"
                      alt="position-icon"
                      className="pr-2"
                    />
                  </td>
                  <td className="add">
                    {point}
                  </td>
                  <td className="add-icon">
                    <a href="#">
                      <img
                        src="../../assets/images/rating-icon.png"
                        alt="position-icon"
                        className="pr-2"
                        style={{ width: "36px" }}
                      />
                    </a>
                  </td>
                  <td className="add">
                    <ReactStars
                      size={15}
                      onChange={(newRating) => {
                        // console.log(newRating);
                      }}
                      value={rating}
                      emptyIcon={<i className="far fa-star" />}
                      halfIcon={<i className="fa fa-star-half-alt" />}
                      filledIcon={<i className="fa fa-star" />}
                      edit={false}
                    />
                  </td>
                </tr>
                <tr>
                  <td className="add-icon">
                    <a href="#">
                      <img
                        src="../../assets/images/internet-icon.png"
                        alt="position-icon"
                        className="pr-2"
                      />
                    </a>
                  </td>
                  <td className="add">{websiteurl}</td>
                  <td className="add-icon">
                    <a href="#">
                      <img
                        src="../../assets/images/smartphone-1.png"
                        alt="position-icon"
                        className="pr-2"
                      />
                    </a>
                  </td>
                  <td className="add">{phone}</td>
                </tr>
              </table>
              <p className="date-time-text">
                { this.state.createdBy !== ID && <button className="crewBtn" onClick={this.openChatPopup}>contact Us</button>}
                {ID && this.state.createdBy == ID && (
                  <ConfirmationPopup
                    callBack={this.deleteObject}
                    object={"Service"}
                  />
                )}
              </p>
            </div>
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <p className="about-event-info">{description}</p>
          </div>
        </div>
        <PhotosContent Pictures={pictures} Details={serviceDetails} postPictures={serviceDetails && serviceDetails.PostPictures} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getServiceData} />

        <VideosContent videos={serviceDetails.Videos} />

        {objectType &&
        <PostListParent ReferenceID={this.state.serviceID} ObjectType={objectType} updatePhotos={this.getServiceData} />}
        <ToastContainer autoClose={1000}/>
        { ID && this.state.showChatPopup && 
          <ChatApp
            sender={ID}
            receiver={this.state.createdBy}
            profilePic={profilePic}
            title={title}
            objectType="Services"
            referenceID={parseInt(this.state.serviceID)}
            popupHandler={this.openChatPopup}
            myID={ID}
          />
        }
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { getServiceDetailsByID,getViewAllPictures }),
  withApollo
)(DisplayService);
