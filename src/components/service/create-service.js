import React, { Fragment } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import Component from "../location/LocationContent";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { createServiceAction } from "../../data/actions/ServicesAction";
import { getListOfBoatTypes } from "../../data/query";
import ImageUploads from "../match/MutipleImageUploads";
import AddVideo from "../common/Video/AddVideo";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class CreateService extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      street: "",
      city: "",
      zipcode: "",
      state: "",
      country: "",
      profilePic: "",
      coordinates: "",
      website: "",
      businessName: "",
      description: "",
      boatArr: [],
      boatID: {},
      latitude: "",
      longitude: "",
      mobileNumber: "",
      button: true,
      videos: []
    };
  }

  componentDidMount() {
    this.getAllBoat();
  }

  getAllBoat = () => {
    this.props.client
      .query({
        query: getListOfBoatTypes,
        fetchPolicy: "network-only",
      })
      .then((res) => {
        res.data.getListOfBoatTypes.forEach((item) => {
          item.isSelected = false;
        });
        this.setState({ boatArr: res.data.getListOfBoatTypes });
      });
  };

  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };

  getImagesArray = (baseArray, imagesArray) => {
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray,
    });
  };

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
  }

  setProfilePic = (pic) => {
    this.setState({ profilePic: pic})
  }

  handleChanges = async category => { 
    let oldEvents = this.state.boatArr.slice();
    oldEvents.forEach(item => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });     
    await this.setState({ boatArr: oldEvents });    
  }

  goBack = () => {
    this.props.history.goBack();
  };

  getLocationDetails = (location) => {
    let cordinate = location.latitude + '"N' + "-" + location.longitude + '"E';
    let address =
      location.mainPlace +
      "," +
      "" +
      location.city +
      "," +
      "" +
      location.zipCode +
      "," +
      "" +
      location.state +
      "," +
      "" +
      location.country;
    this.setState({
      streetName: location.mainPlace,
      city: location.city,
      coordinates: cordinate,
      address: address,
      state: location.state,
      zipcode: location.zipCode,
      country: location.country,
      latitude: location.latitude,
      longitude: location.longitude,
    });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({ profilePic: result });
    });
  };

  toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  getvideoUrls = (data) => {
    this.setState({ videos: data })
  }

  createService = () => {
    this.setState({ button : !this.state.button })
    const { getHourData, address, profilePic, website, businessName, description,
      latitude, longitude, streetName, images, mobileNumber, boatArr, videos } = this.state;

    var options = boatArr.filter(function(item) { return item.isSelected });
    const serviceTypeList = options.map(function(item) {  
      return { ID : item.ID, Name : item.Name }
    })

    let Rating = "";
    let lat = latitude.toString();
    let long = longitude.toString();
    let Location = {
      Latitude: lat,
      Longitude: long,
      Address: address,
      StreetName: streetName,
    };

    this.props.createServiceAction(
      this.props.client, serviceTypeList, businessName, website, getHourData, Rating,
      profilePic, description, Location, ID, images, mobileNumber, videos
    )
    .then((res) => {
      toast.success("Service created successfully", {
        onClose: () => this.props.history.push("/"),
      });
    })
    .catch(function (error) {
      toast.error("Something went wrong", {});
    });
  };

  render() {
    const { boatArr, city, boatID, zipcode, state, country, coordinates, profilePic,
      website, businessName, description, streetName, button } = this.state;

    const enabled = boatID && city && zipcode && state && country && coordinates && profilePic &&
      website && businessName && description && button;
    
    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back cursor-pointer">
              <img
                src="../../assets/images/back_icon.png"
                alt="Back"
                onClick={this.goBack}
              />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a className="float-left mobile_back">
                <img
                  src="../../assets/images/back_icon.png"
                  alt="Back"
                  onClick={this.goBack}
                />
              </a>
              <h1>Create Service</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="boatType">Types of Service</label>
                    <div>
                    {boatArr &&
                  boatArr.map(type => {
                    return (                   
                        <input
                        type="button"
                        key={type.ID}
                        className={`${type.isSelected ? "btn-primary btn gender" : "btn btn-outline-secondary gender"}`}
                        onClick={(e) => this.handleChanges(type)}
                        value={type.Name} />
                    );
                  })}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <Component callback={this.getLocationDetails} />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="streetName">Street Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="streetName"
                      placeholder="Street Name"
                      disabled
                      value={streetName || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="city">City</label>
                    <input
                      type="text"
                      className="form-control"
                      id="city"
                      placeholder="City"
                      disabled
                      value={city || ""}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="state">State</label>
                    <input
                      type="text"
                      className="form-control"
                      id="state"
                      placeholder="State"
                      disabled
                      value={state || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="zipcode">Zip code</label>
                    <input
                      type="text"
                      className="form-control"
                      id="zipcode"
                      placeholder="Zip code"
                      disabled
                      value={zipcode || ""}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="country">Country</label>
                    <input
                      type="text"
                      className="form-control"
                      id="country"
                      placeholder="Country"
                      disabled
                      value={country || ""}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="gpsCoordinates">GPS Coordinates</label>
                    <input
                      type="text"
                      className="form-control"
                      id="gpsCoordinates"
                      placeholder="GPS Coordinates"
                      disabled
                      value={coordinates || ""}
                    />
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Business Name</label>
                    <input
                      type="text"
                      className="form-control"
                      name="businessName"
                      placeholder="Business Name"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="website">Website</label>
                    <input
                      type="text"
                      className="form-control"
                      name="website"
                      placeholder="Website"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="businessName">Mobile No</label>
                    <input
                      type="tel"
                      className="form-control"
                      name="mobileNumber"
                      placeholder="Mobile Number"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
                      className="form-control"
                      name="description"
                      rows="3"
                      onChange={this.handleChange}
                    ></textarea>
                  </div>
                </div>
                <hr />
                <AddVideo getvideoUrls={this.getvideoUrls} />
                <div className="row">
                    <div className="col-sm-12 my-2">
                      <label for="photo">Photo</label>
                      <ImageUploads callback={this.getImagesArray}/>
                    </div>   
                </div>

                <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                  {
                    this.state.imagesRender && this.state.imagesRender.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                  </div>
                  {
                    this.state.imagesRender && this.state.imagesRender.length > 0 && !profilePic &&
                    <div className="col-sm-12 my-2 pt-2 text-center">
                      <i className="text-danger h6">Please select profile picture from above images</i>
                    </div>
                  }
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  {profilePic && <label for="profilePicture">Profile Picture</label> }
                    {/* <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    /> */}
                  </div>
                </div>
                
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        disabled={!enabled}
                        onClick={this.createService}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { createServiceAction }),
  withApollo
)(CreateService);
