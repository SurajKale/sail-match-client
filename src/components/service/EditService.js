import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from "react-drop-zone";
import { updateServiceAction, getServiceDetailsByID } from "../../data/actions/ServicesAction";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { getListOfBoatTypes } from "../../data/query";
import SelectHours from "../rental/SelectHours";
import ImageUploads from "../match/MutipleImageUploads";

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;
if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}
class EditService extends Component {
  constructor(props) {
    super(props);
    this.state = {
			url: this.props.match.params.url,
			serviceID: 0,
      profilePic: "",
      website: "",
      businessName: "",
      description: "",
      mobileNumber: "",
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getServiceData()
  }

  getServiceData = async () => {
    await this.props
      .getServiceDetailsByID(this.props.client, this.state.url)
      .then((res) => {
        if (res.data.getServiceByUrl !== null) {
          let serviceData = res.data.getServiceByUrl;
          this.setState({
            serviceID: serviceData.ID,
            businessName: serviceData.Title,
            description: serviceData.Description,
            profilePic: serviceData.ProfilePicture,
            website: serviceData.WebsiteUrl,
            pictures: serviceData.Pictures,
            mobileNumber: serviceData.Phone
          });
        } else {
          toast.error("Something wents wrong");
        }
      })
    .catch(function (error) {
      console.log(error);
    });
  }
  
  handleChange = async (e) => {
    await this.setState({
      [e.target.name]: e.target.value,
    });
  };
  
  setProfilePic = (pic) => {
    this.setState({ profilePic: pic})
  }

  goBack = () => {
    this.props.history.goBack();
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  createService = () => {
    const { profilePic, website, businessName, description, mobileNumber, serviceID } = this.state;

		const serviceObject = { profilePic, website, businessName, description, mobileNumber, serviceID}
    this.props
      .updateServiceAction(
        this.props.client,
        serviceObject
      )
      .then((res) => {
        toast.success("Service updated successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        toast.error("Something went wrong", {});
      });
  };
  render() {
    const {  profilePic, website, businessName, description, mobileNumber } = this.state;
		
		const label = profilePic
      ? "Profile image successfully added"
			: "Click or drop your image here";
			
		const enabled = profilePic && businessName && description;
		
    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back cursor-pointer">
              <img
                src="../../assets/images/back_icon.png"
                alt="Back"
                onClick={this.goBack}
              />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a className="float-left mobile_back">
                <img
                  src="../../assets/images/back_icon.png"
                  alt="Back"
                  onClick={this.goBack}
                />
              </a>
              <h1>Edit Service</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Business Name</label>
                    <input
											defaultValue={businessName}
                      type="text"
                      className="form-control"
                      name="businessName"
                      placeholder="Business Name"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="website">Website</label>
                    <input
											defaultValue={website}
                      type="text"
                      className="form-control"
                      name="website"
                      placeholder="Website"
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="businessName">Mobile No</label>
                    <input
											defaultValue={mobileNumber}
                      type="tel"
                      className="form-control"
                      name="mobileNumber"
                      placeholder="Mobile Number"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="description">Description</label>
                    <textarea
											defaultValue={description}
                      className="form-control"
                      name="description"
                      rows="3"
                      onChange={this.handleChange}
                    ></textarea>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  <label for="profilePicture">Profile Picture</label>
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}

                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        className="btn btn-primary save_profile"
                        disabled={!enabled}
                        onClick={this.createService}
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({});

export default compose(
  connect(mapStateToProps, { updateServiceAction, getServiceDetailsByID }),
  withApollo
)(EditService);
