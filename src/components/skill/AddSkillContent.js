import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { getSkillCreateAction } from "../../data/actions";

import LocationComponent from "../location/LocationContent";
import ImageUploads from "../match/MutipleImageUploads";
import AddVideo from "../common/Video/AddVideo";

const LocalStorageData = require("../../services/LocalStorageData");
let UID = null;

if (LocalStorageData.ID) {
  UID = LocalStorageData.ID;
}

class AddSkillContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      skillType: [],
      streetName: "",
      city: "",
      state: "",
      zipcode: "",
      skillName: "",
      skillDescription: "",
      profilePic: "",
      file: undefined,
      gpsCoordinate: "",
      locationObject: {},
      files: [],
      images: [],
      imagesRender: [],
      button: true,
      videos: []
    };
  }

  handleEventsSelect = async (category) => {
    let oldEvents = this.state.eventsArray.slice();
    oldEvents.forEach((item) => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });
    await this.setState({
      eventsArray: oldEvents,
    });
  };

  handleChange = (e) => { this.setState({ [e.target.name]: e.target.value }) };

  getImagesArray = (baseArray, imagesArray) => {
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray,
    });
  };

  handleSail = (value, id) => { this.setState({ skillType: { ID: id, Name: value }})};
  handleChangess(files) { this.setState({ files: files })}
  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

  callback = (data) => {
    let address = data.mainPlace.concat(
      data.city,
      ",",
      data.state,
      ",",
      data.zipCode
    );
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
      },
      gpsCoordinate: data.latitude + '"N' + data.longitude + '"E',
      city: data.city,
      state: data.state,
      zipCode: data.zipCode,
    });
  };

  onDrop(picture) { this.setState({ pictures: this.state.pictures.concat(picture)})}
  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
  }

  setProfilePic = (pic) => { this.setState({ profilePic: pic}) }
  getvideoUrls = (data) => { this.setState({ videos: data }) }

  createSkill = async (e) => {
    this.setState({ button : !this.state.button })
    e.preventDefault();
    const { skillType, skillName, skillDescription, profilePic, locationObject,
      images, videos } = this.state;

    let skillObject = { skillType, skillName, skillDescription, profilePic, locationObject,
      images, videos };

    await this.props
      .getSkillCreateAction(this.props.client, skillObject, UID)
      .then((res) => {
          toast.success("Skill added successfully", {
            onClose: () => this.props.history.push("/"),
          });
      })
      .catch(function (error) {
        // console.log(error);
    });
  };

  goBack = () => { this.props.history.goBack() };

  render() {
    const { skillType, skillName, skillDescription, profilePic, locationObject, city,
      state, imagesRender, button } = this.state;

    const enabled = skillType && skillName && skillDescription && profilePic &&
      locationObject && imagesRender && imagesRender.length > 0 && button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="#" className="float-left mobile_back">
                <img src="images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Skill</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="skillType">Type of Skill</label>
                    <div className="">
                      <input
                        type="button"
                        className={
                          skillType.Name === "Knots"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Knots", 1)}
                        value="Knots"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Sailing"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Sailing", 2)}
                        value="Sailing"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Docking"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Docking", 3)}
                        value="Docking"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Food / Bev"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Food / Bev", 4)}
                        value="Food / Bev"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Navigation"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Navigation", 5)}
                        value="Navigation"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Maintenance"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Maintenance", 6)}
                        value="Maintenance"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Anchor"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Anchor", 7)}
                        value="Anchor"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Mooring"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Mooring", 8)}
                        value="Mooring"
                      />
                      <input
                        type="button"
                        className={
                          skillType.Name === "Racing"
                            ? "btn-primary btn gender"
                            : "btn btn-outline-secondary gender"
                        }
                        onClick={(e) => this.handleSail("Racing", 9)}
                        value="Racing"
                      />
                    </div>
                  </div>
                </div>

                <LocationComponent callback={this.callback} />
                <div class="row">
                  <div class="col-sm-6 my-2">
                    <label for="city">City</label>
                    <input
                      readOnly
                      type="text"
                      class="form-control"
                      id="city"
                      placeholder="City"
                      name="city"
                      value={city}
                    />
                  </div>

                  <div class="col-sm-6 my-2">
                    <label for="state">State</label>
                    <input
                      readOnly
                      type="text"
                      class="form-control"
                      id="state"
                      placeholder="State"
                      name="state"
                      value={state}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="skillName">Skill Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="skillName"
                      placeholder="Skill Name"
                      name="skillName"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="skillDescription"
                      onChange={this.handleChange}
                    />
                  </div>
                </div>
                <hr />
                <AddVideo getvideoUrls={this.getvideoUrls} />

                <div className="row">
                    <div className="col-sm-12 my-2">
                      <label for="photo">Photo</label>
                      <ImageUploads callback={this.getImagesArray}/>
                    </div>   
                </div>

                <div className="row" style={{ justifyContent: "center"}}>
                <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                  {
                    this.state.imagesRender && this.state.imagesRender.map((item,index) => {
                      return <div className="multiple-image-preview">
                        <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                        <div class="multiple-middle-text">
                          <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                          <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                        </div></div>
                    })
                  }
                  </div>
                  {
                    this.state.imagesRender && this.state.imagesRender.length > 0 && !profilePic &&
                    <div className="col-sm-12 my-2 pt-2 text-center">
                      <i className="text-danger h6">Please select profile picture from above images</i>
                    </div>
                  }
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    {profilePic && <label for="profilePicture">Profile Picture</label> }
                  </div>
                </div>
                
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}
                
                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        disabled={!enabled}
                        onClick={this.createSkill}
                        className="btn btn-primary save_profile"
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

export default compose(
  connect(null, { getSkillCreateAction }),
  withApollo
)(AddSkillContent);
