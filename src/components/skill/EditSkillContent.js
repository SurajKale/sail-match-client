import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from "react-drop-zone";
import "react-drop-zone/dist/styles.css";
import ReorderImages from "react-reorder-images";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getSkillCreateAction, getSkillDetailsByID } from "../../data/actions";
import LocationComponent from "../location/LocationContent";
import ImageUploads from "../match/MutipleImageUploads";

const LocalStorageData = require("../../services/LocalStorageData");
let UID = null;

if (LocalStorageData.ID) {
  UID = LocalStorageData.ID;
}

class AddSkillContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.url,
      skillID: 0,
      skillType: [],
      skillName: '',
      skillDescription: '',
      profilePic: '',
      locationObject: {}
    };
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    this.getSkillData()
  }

  getSkillData = async () => {
    await this.props.getSkillDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getSkillByUrl !== null ) {
          let skillData = res.data.getSkillByUrl;
          this.setState({
              skillID:skillData.ID,
              skillType: skillData.SkillType,
              skillName: skillData.Title,
              skillDescription: skillData.Description,
              profilePic: skillData.ProfilePicture,
              locationObject: skillData.Location
          })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
      console.log(error);
    })
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
    });
  };

  setFile = (file) => {
    this.toBase64(file).then((result) => {
      this.setState({
        profilePic: result,
      });
    });
  };

  toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
  });


  createSkill = async (e) => {
    e.preventDefault();

    const {
      skillType,
      skillName,
      skillDescription,
      profilePic,
      locationObject,
      skillID
    } = this.state;

    var selectedType =
    skillType.map(({ID, Name }) => ({ID: ID, Name: Name }));
			
		delete locationObject.__typename;

    let skillObject = {
      skillType,
      skillName,
      skillDescription,
      profilePic,
      locationObject,
      skillID,
      selectedType
    };

    await this.props
      .getSkillCreateAction(this.props.client, skillObject, UID)
      .then((res) => {
        toast.success("Skill updated successfully", {
          onClose: () => this.props.history.push("/"),
        });
      })
      .catch(function (error) {
        // console.log(error);
      });
  };

  goBack = () => {
    this.props.history.goBack();
  };

  render() {
    const { skillName, skillDescription, profilePic } = this.state;

    const label = profilePic
      ? "Image successfully added"
      : "Click or drop your image here";

    return (
      <div className="container">
        <div className="row pt-4">
          <div style={{ cursor: "pointer" }} className="col-md-3 mb-2">
            {" "}
            <a className="desktop_back" onClick={this.goBack}>
              <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>{" "}
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="#" className="float-left mobile_back">
                <img src="images/back_icon.png" alt="Back" />
              </a>
              <h1>Update Skill</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="skillName">Skill Name</label>
                    <input
                      type="text"
                      className="form-control"
                      id="skillName"
                      placeholder="Skill Name"
                      name="skillName"
                      defaultValue={skillName}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="description">Description</label>
                    <textarea
                      className="form-control"
                      id="description"
                      rows="3"
                      name="skillDescription"
                      defaultValue={skillDescription}
                      onChange={this.handleChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  <label for="profilePicture">Profile Picture</label>
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}
                
                <hr />

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center">
                      <button
                        type="button"
                        onClick={this.createSkill}
                        className="btn btn-primary save_profile"
                      >
                        Save
                      </button>
                    </p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>
    );
  }
}

export default compose(
  connect(null, { getSkillCreateAction, getSkillDetailsByID }),
  withApollo
)(AddSkillContent);
