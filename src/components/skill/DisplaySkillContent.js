import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { getSkillDetailsByID, getViewAllPictures } from "../../data/actions";
import {deleteSkill} from '../../data/mutation'

import { url } from "../../services/CommonServices";
import PhotosContent from '../common/PhotosContent';
import VideoContent from '../common/VideosContent';
import ConfirmationPopup from '../common/ConfirmationPopup';
import ObjectLikeDislike from '../common/ObjectLikeDislike';
import PostListParent from "../common/PostListParent";
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class DisplaySkillContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: this.props.match.params.url,
      skillType: "",
      skillName: "",
      skillDescription: "",
      location: "",
      Latitude: "",
      Longitude: "",
      address: "",
      skillProfilePic: "",
      skillPics: [],
      userDetailsPic:{},
      skillDetails:{},
      totalCount:"",
      doShowPostPopup: false,
      metaData:[],
      showChatPopup: false,
      showShareModel: false,
    }
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    this.getSkillData()
  }

  getSkillData = async () => {
    await this.props.getSkillDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getSkillByUrl !== null ) {
        let skillData = res.data.getSkillByUrl;
        var metaArr = [];
        metaArr.push({
          title : skillData.Title ?  skillData.Title : SITE_STATIC_NAME,
          keywords : skillData.ObjectType, SITE_STATIC_NAME,
          description : skillData.Description ? skillData.Description : '',
          og_url : window.location.href,
          og_title : skillData.Title ?  skillData.Title : SITE_STATIC_NAME,
          og_description : skillData.Description ? skillData.Description : '',
          canonical_url : window.location.href,
          og_image : skillData.ProfilePicture ? skillData.ProfilePicture : SITELOGO,
          amp_url : window.location.href,
        })
        this.setState({
          isLike:skillData.isLike,
          renderLike:true,          
          metaData:metaArr[0],
          skillDetails:skillData,
          skillID:skillData.ID,
          skillType: skillData.SkillType.Name,
          skillName: skillData.Title,
          skillDescription: skillData.Description,
          skillProfilePic: skillData.ProfilePicture,
          location: skillData.Location.MapLocation,
          Latitude:skillData.Location.Latitude,
          Longitude:skillData.Location.Longitude,
          address: skillData.Location.Address,
          skillPics: skillData.Pictures,
          userID:skillData.UserID,
          userDetailsPic:skillData.UserProfile,
          totalCount:skillData.LikeCount
        })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
      console.log(error);
    })
  }

  goBack = ()=> { this.props.history.goBack(); }
  openShareModal = () => { this.setState({ showShareModel: !this.state.showShareModel });};
  
  deleteObject=()=>{
    this.props.client
    .mutate({
        mutation: deleteSkill,
        variables: {
          SkillID:parseInt(this.state.skillID),
          UserID:ID
      },
     })
      .then(res => { 
        if(res.data.deleteSkill){
          toast.success("Skill deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
    });  
  }

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.skillID,this.state.userID,this.state.skillDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({ skillPics:picturesData.Pictures, userDetailsPic:picturesData.UserDetails })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    });
  }

  viewLessPictures = () =>{
    let queryResult = this.state.skillPics
    let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
      filterdata.push(queryResult[i]);
      if (filterdata.length == 4) {
        break;
      }
    }
    this.setState({skillPics:filterdata})
  }

  objectLike= (count) =>  { this.setState({totalCount:count})}

  openPostPopup = () => {
    document.body.style.overflowY = "hidden";
    this.setState({ doShowPostPopup: true });
  };

  closePostPopup = () => {
    document.body.style.overflowY = "auto";
    this.setState({ doShowPostPopup: false });
  };

  openChatPopup = () => {
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  goToEditPage = () => {
    this.props.history.push('/edit-skill/'+this.state.url)
  }

  render() {
    const { skillDetails, skillName,isLike,renderLike, skillDescription, skillProfilePic,totalCount, address, userID} = this.state;

    return (
      <div className="container">
        <HelmetData metaDetails={this.state.metaData} history={this.props}/>
        <div className="row">
          <div className="col-md-12">
            <div className="col-md-3 mb-2 mt-2"> <a className="desktop_back cursor-pointer" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back" /></a> </div>
          </div>
        </div>
        <div className="col-md-12 col-12  mb-2">
          <div className="row">
            <div className="col-md-3 col-sm-12 mb-2 mt-2"> <img src={ skillProfilePic ? skillProfilePic :"../../assets/images/photo-1535024966840-e7424dc2635b.png"} className="profImg sideImg" alt="Back" />
              <div className="userSectionSocialSection">
                <div className="like">
                  { ID && renderLike && <ObjectLikeDislike
                  LikeCount={totalCount} 
                  ObjectType={skillDetails.ObjectType} 
                  ReferenceID={skillDetails.ID} 
                  status={isLike} 
                  ReferenceUserID={skillDetails.UserID} 
                  updateOnParent={this.objectLike} 
                  isDetail={true}/> }
                  { ID == null && <><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2"/><span>{totalCount}</span></>}
                </div> 
                <div class="bar">|</div>
                <div className="comment">
                  <img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6" onClick={this.openPostPopup} style={{cursor:'pointer'}}/><span> {skillDetails.CommentCount || 0}</span>
                </div>
                <div class="bar">|</div>
                <div className="forward" onClick={this.openShareModal}>
                  <img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6"/><span> {skillDetails.ShareCount || 0}</span>
                </div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                    ObjectType="Skills"
                    ReferenceID={parseInt(this.state.skillID)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={skillName}
                    quote={skillName} />
                }
              </div>
            </div> 
            <div className="col-md-9 col-sm-10 mb-2 mt-2 event-info">
              <h2>{skillName}
                {ID && userID === ID &&
                  <i 
                      className="fa fa-pencil cursor-pointer"
                      title='Edit'
                      style={{ fontSize: 17, padding: 11, color: 'green' }}
                      onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img"><img src="../../assets/images/Skills.svg" alt="Back" /></div>
              <table className="addressTable">
                  <tr>
                      <td className="add-icon"><img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2" /></td>
                      <td className="add">{address}</td>
                  </tr>
                  {/* <tr>
                      <td className="add-icon"><img src="../../assets/images/position-icon.png" alt="position-icon" className="pr-2" /></td>
                      <td className="add"> {Latitude}"N {Longitude}"E</td>
                  </tr> */}                       
              </table>    
              <p className="date-time-text">  
                { ID !== userID && <button className="crewBtn" onClick={this.openChatPopup}> Contact</button>}
                { ID && ID == userID && <ConfirmationPopup callBack={this.deleteObject} object={"Skill"}/>}
              </p>                   
            </div>      
          </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <p className="about-event-info">{skillDescription}</p>    
          </div>
        </div>
        <PhotosContent Pictures={this.state.skillPics} Details={this.state.skillDetails} postPictures={this.state.skillDetails.PostPictures} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getSkillData} />
        <VideoContent videos={skillDetails.Videos} />
        {skillDetails.ObjectType &&
        <PostListParent ReferenceID={parseInt(this.state.skillID)} ObjectType={skillDetails.ObjectType} updatePhotos={this.getSkillData} />}
        <ToastContainer autoClose={1000}/>
        { ID && this.state.showChatPopup && 
          <ChatApp
            sender={ID}
            receiver={userID}
            profilePic={skillProfilePic}
            title={skillName}
            objectType="Skills"
            referenceID={parseInt(this.state.skillID)}
            popupHandler={this.openChatPopup}
            myID={ID}
          />
        }
      </div>
  )}
}

export default compose(
  connect(null, { getSkillDetailsByID ,getViewAllPictures}
  ),
  withApollo
)(DisplaySkillContent);
