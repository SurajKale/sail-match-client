import React, { Component, Fragment } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from 'react-drop-zone';
import 'react-drop-zone/dist/styles.css';
import ReorderImages from "react-reorder-images";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import { getClubCreateAction, getClubDetailsByID } from "../../data/actions";
import { getAllClubAmenities } from '../../data/query';
import LocationComponent from '../location/LocationContent';
import ImageUploads from '../match/MutipleImageUploads';

const LocalStorageData = require('../../services/LocalStorageData');
let UID = null;

 if(LocalStorageData.ID){
  UID = LocalStorageData.ID;
}

class EditClubContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
			url: this.props.match.params.url,
      clubID: 0,
      clubName: "",
      clubDescription: "",
			profilePic: "",
			amenities: []
    }
	}
	
	async componentDidMount () {
		window.scrollTo(0, 0);
		this.getClubData()
	}

	getClubData = async () => {
		await this.props.getClubDetailsByID(this.props.client, this.state.url ).then(res => { 
			if(res.data.getClubByUrl !== null ) {
					let clubData = res.data.getClubByUrl;
					this.setState({
							clubID:clubData.ID,
							clubDescription: clubData.Description,
							profilePic: clubData.ProfilePicture,
							clubName: clubData.Title,
							amenities: clubData.Amenities
					})
			} else {
					toast.error("Something wents wrong")
			}
		}).catch(function (error) {
		// console.log(error);
		})
	}

  handleChange = (e) => {
    this.setState({
      [e.target.name] : e.target.value
    })
  }

  setFile = (file) => {
    this.toBase64(file).then(result => {
      this.setState({
        profilePic: result
      })
    });    
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  createClub = async (e) => {
    this.setState({ button : !this.state.button })
    e.preventDefault();
    
    const { clubName, clubDescription, profilePic, clubID, amenities } = this.state;

    let skillObject = {
      clubName, clubDescription, profilePic, clubID, amenities
		}
		
		var selctedData = amenities.map(({Name, ID }) => ({Name: Name, ID: ID }));

      await this.props.getClubCreateAction(this.props.client, skillObject, selctedData, UID).then(res => {
          toast.success("Club updated successfully", {
              onClose: () => this.props.history.push('/')
          });
      }).catch(function (error) {
          // console.log(error);
      })
    }

  goBack = ()=> {
    this.props.history.goBack();
  }

  render() {
		const { clubName, clubDescription, profilePic } = this.state;
		
		const enabled = clubName && clubDescription && profilePic;

    const label = profilePic ? 'Profile image successfully added' : 'Click or drop your image here'

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            <a className="desktop_back" onClick={this.goBack} style={{ cursor: "pointer"}}>
                <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="/" className="float-left mobile_back">
                  <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Edit Club Profile</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm my-2">
                    <label htmlFor="location">Club Name</label>
                    <input defaultValue={clubName} type="text" className="form-control" id="location" placeholder="Club Name" name="clubName" onChange={this.handleChange} />
                  </div>
                </div>
                
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="aboutClub">About Club</label>
                    <textarea defaultValue={clubDescription} className="form-control" id="aboutClub" rows="3" name="clubDescription" onChange={this.handleChange} />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                  {profilePic && <label htmlFor="profilePicture">Profile Picture</label> }
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>

                
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile"  />
                  </div>
                </div>}

                {/* <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="clubProfileVid">Videos</label>
                    <input type="file" className="form-control" id="clubProfileVid" />
                  </div>
                </div> */}
              
                <hr />
                <div className="row">
                      <div className="col-sm-12 my-2">
                        <p className="text-center"><button disabled={!enabled} type="button" className="btn btn-primary save_profile" onClick={this.createClub}>Save</button></p>
                    </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>  
    );
  }
}

export default compose(
  connect(null, { getClubCreateAction, getClubDetailsByID }
  ),
  withApollo
)(EditClubContent);

