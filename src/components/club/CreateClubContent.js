import React, { Component, Fragment } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import ReorderImages from "react-reorder-images";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { getClubCreateAction } from "../../data/actions";
import { getAllClubAmenities } from '../../data/query';

import LocationComponent from '../location/LocationContent';
import ImageUploads from '../match/MutipleImageUploads';
import AddVideo from "../common/Video/AddVideo";
const LocalStorageData = require('../../services/LocalStorageData');
let UID = null;

if(LocalStorageData.ID){
  UID = LocalStorageData.ID;
}

class CreateClubContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      skillType: [],
      streetName: "",
      country: "",
      city: "",
      state: "",
      zipCode: "",
      clubName: "",
      clubDescription: "",
      profilePic: "",
      gpsCoordinate: "",
      locationObject: {},
      images: [],
      imagesRender: [],
      amenitiesArray: [],
      button: true,
      videos: []
    }
  }

  componentDidMount = () =>{
    this.getAllClubAmenities()
  }

  getAllClubAmenities = () => {
    this.props.client
    .query({
        query: getAllClubAmenities,
     })
      .then(res => { 
        res.data.getAllClubAmenities.forEach(item => {
          item.isSelected = false;
        });
        this.setState({ amenitiesArray : res.data.getAllClubAmenities }) 
    });          
  };

  handleEventsSelect = async category => { 
    let oldEvents = this.state.amenitiesArray.slice();
    oldEvents.forEach(item => {
      if (item.ID === category.ID) {
        item.isSelected = !item.isSelected;
      }
    });     
    await this.setState({
      amenitiesArray: oldEvents
    });    
  }

  handleChange = (e) => { this.setState({ [e.target.name] : e.target.value }) }
  getImagesArray = (baseArray,imagesArray) => {
    this.setState({
      images: imagesArray.map((arr) => arr.base64),
      imagesRender: imagesArray })
  }

  setFile = (file) => {
    this.toBase64(file).then(result => {
      this.setState({
        profilePic: result
      })
    });    
  }

  toBase64 = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });

  callback = (data) => {
    let address = data.mainPlace.concat(data.city, "," , data.state, ",", data.zipCode)
    this.setState({
      locationObject: {
        Latitude: data.latitude.toString(),
        Longitude: data.longitude.toString(),
        Address: address,
        City: data.city,
        State: data.state,
        Country: data.country,
        ZipCode: data.zipCode,
        StreetName: data.mainPlace
      },
      gpsCoordinate:data.latitude+'"N'+data.longitude+'"E',
      city:data.city,
      state:data.state,
      zipCode:data.zipCode,
      country: data.country,
      streetName: data.mainPlace
    })
  }

  onDrop(picture) { this.setState({ pictures: this.state.pictures.concat(picture) });}

  removeImage = (name,base64) => {
    const { imagesRender, images } = this.state
    const updatedImages = imagesRender.filter(img => img.url != name);
    while (images.indexOf(base64) !== -1) {
      images.splice(images.indexOf(base64), 1);
    }
    this.setState({
      imagesRender : updatedImages,
      images
    });
    
  }

  setProfilePic = (pic) => { this.setState({ profilePic: pic}) }
  getvideoUrls = (data) => { this.setState({ videos: data }) }

  createClub = async (e) => {
    this.setState({ button : !this.state.button })
    e.preventDefault();
    
    const { clubName, clubDescription, profilePic, locationObject, images, amenitiesArray, videos } = this.state;

    let skillObject = {
      clubName, clubDescription, profilePic, locationObject, images, videos
    }

    var options = amenitiesArray.filter(function(item) {
      return item.isSelected;
    });
    
    const selectedAmenities = options.map(function(item) {  
      return { ID : item.ID, Name : item.AmenityName }
    })

      await this.props.getClubCreateAction(this.props.client, skillObject, selectedAmenities, UID).then(res => {
          toast.success("Club added successfully", {
              onClose: () => this.props.history.push('/')
          });
      }).catch(function (error) {
          // console.log(error);
      })
  }

  goBack = ()=> { this.props.history.goBack();}

  render() {
    const { clubName, clubDescription, profilePic, gpsCoordinate, amenitiesArray,
      locationObject, city, state, images, zipCode, streetName, country, imagesRender, button } = this.state;

    const enabled = clubName && clubDescription && profilePic && images &&
      locationObject && button;

    return (
      <div className="container">
        <div className="row pt-4">
          <div className="col-md-3 mb-2">
            <a className="desktop_back" onClick={this.goBack} style={{ cursor: "pointer"}}>
              <img src="../../assets/images/back_icon.png" alt="Back" />
            </a>
          </div>
          <div className="col-md-6 mb-2">
            <div className="text-left">
              <a href="/" className="float-left mobile_back">
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>
              <h1>Create Club Profile</h1>
              <form className="mt-4">
                <div className="row">
                  <div className="col-sm my-2">
                    <label htmlFor="location">Club Name</label>
                    <input type="text" className="form-control" id="location" placeholder="Club Name" name="clubName" onChange={this.handleChange} />
                  </div>
                </div>
                <LocationComponent callback={this.callback} />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="streetName">Street Name</label>
                    <input readOnly type="text" className="form-control" id="streetName" placeholder="Street Name"  defaultValue={streetName} />
                  </div>
                  <div className="col-sm-6 my-2">
                    <label htmlFor="city">City</label>
                    <input readOnly type="text" className="form-control" id="city" placeholder="City"  defaultValue={city} />
                  </div> 
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="state">State</label>
                    <input readOnly type="text" className="form-control" id="state" placeholder="State" defaultValue={state} />
                  </div> 
                  <div className="col-sm-6 my-2">
                    <label htmlFor="zipcode">Zip code</label>
                    <input readOnly type="text" className="form-control" id="zipcode" placeholder="Zip code" defaultValue={zipCode} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label htmlFor="country">Country</label>
                    <input readOnly type="text" className="form-control" id="country" placeholder="Country"  defaultValue={country} />
                  </div> 
                  <div className="col-sm-6 my-2">
                    <label htmlFor="gpsCoordinates">GPS Coordinates</label>
                    <input readOnly type="text" className="form-control" id="gpsCoordinates" placeholder="GPS Coordinates"  defaultValue={gpsCoordinate} />
                  </div>       
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="boatType">Amenities</label>
                    <div className="">
                      {amenitiesArray && amenitiesArray.map(type => {
                        return (                   
                            <input
                            type="button"
                            key={type.ID}
                            className={`${type.isSelected ? "btn-primary btn gender" : "btn btn-outline-secondary gender"}`}
                            onClick={(e) => this.handleEventsSelect(type)}
                            value={type.AmenityName} />
                        )})
                      }
                    </div>
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="aboutClub">About Club</label>
                    <textarea className="form-control" id="aboutClub" rows="3" name="clubDescription" onChange={this.handleChange} />
                  </div>
                </div>
                <AddVideo getvideoUrls={this.getvideoUrls} />      
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="photo">Photo</label>
                    <ImageUploads callback={this.getImagesArray}/>
                  </div>   
                </div>
                <div className="row" style={{ justifyContent: "center"}}>
                  <div className="col-sm-12 my-2 " style={{display: "contents"}}>
                    {
                      imagesRender && imagesRender.map((item,index) => {
                        return <div className="multiple-image-preview">
                          <img key={index} src={item.base64} alt={index} className="multiple-image-render" />
                          <div class="multiple-middle-text">
                            <div className="close-button" onClick={() => {this.removeImage(item.url,item.base64)}}>X</div>
                            <div class="multiple-button-text" onClick={() => {this.setProfilePic(item.base64)}}>Set as Profile</div>
                          </div></div>
                      })
                    }
                    </div>
                    {
                      imagesRender && imagesRender.length > 0 && !profilePic &&
                      <div className="col-sm-12 my-2 pt-2 text-center">
                        <i className="text-danger h6">Please select profile picture from above images</i>
                      </div>
                    }
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    {profilePic && <label htmlFor="profilePicture">Profile Picture</label> }
                  </div>
                </div>
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile"  />
                  </div>
                </div>}
                {/* <div className="row">
                  <div className="col-sm-12 my-2">
                    <label htmlFor="clubProfileVid">Videos</label>
                    <input type="file" className="form-control" id="clubProfileVid" />
                  </div>
                </div> */}
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center"><button disabled={!enabled} type="button" className="btn btn-primary save_profile" onClick={this.createClub}>Save</button></p>
                  </div>
                </div>
              </form>
            </div>
            <div className="col-md-3 mb-2"></div>
          </div>
        </div>
        <ToastContainer autoClose={1000} />
      </div>  
    );
  }
}

export default compose(
  connect(null, { getClubCreateAction }
  ),
  withApollo
)(CreateClubContent);

