import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

import { getClubDetailsByID, getViewAllPictures } from "../../data/actions";
import {deleteClub} from '../../data/mutation'

import { url } from "../../services/CommonServices";
import ImagePlaceholder from '../../services/ImagePlaceholder';
import MemberContent from '../common/StaticMember';
import PhotosContent from '../common/PhotosContent';
import VideoContent from '../common/VideosContent';
import ConfirmationPopup from '../common/ConfirmationPopup';
import ObjectLikeDislike from '../common/ObjectLikeDislike';
import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import ShareContent from "../common/ShareModel/shareOption";
import PostListParent from "../common/PostListParent";

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class DisplayClubContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      url: this.props.match.params.url,
      clubTitle: "",
      clubDescription: "",
      clubAmenities: [],
      Latitude: "",
      Longitude: "",
      address: "",
      clubProfilePic: "",
      clubPictures: [],
      loaded: false,
      clubDeatils:{},
      userDetailsPic:{},
      totalCount:"",
      cCount: 0,
      metaData:[],
      showChatPopup: false,
      showShareModel: false,
      ObjectType:'',
    }
  }

  async componentDidMount () {
    window.scrollTo(0, 0);
    this.getClubData()
  }

  getClubData = async () => {
    await this.props.getClubDetailsByID(this.props.client, this.state.url ).then(res => { 
      if(res.data.getClubByUrl !== null ) {
          let clubData = res.data.getClubByUrl;

          var metaArr = [];
          metaArr.push({
            title : clubData.Title ?  clubData.Title : SITE_STATIC_NAME,
            keywords : clubData.ObjectType, SITE_STATIC_NAME,
            description : clubData.Description ? clubData.Description : '',
            og_url : window.location.href,
            og_title : clubData.Title ?  clubData.Title : SITE_STATIC_NAME,
            og_description : clubData.Description ? clubData.Description : '',
            canonical_url : window.location.href,
            og_image : clubData.ProfilePicture ? clubData.ProfilePicture : SITELOGO,
            amp_url : window.location.href,
          })
          this.setState({
              isLike:clubData.isLike,
              renderLike:true,
            metaData:metaArr[0],
              clubDetails:clubData,
              clubID:clubData.ID,
              clubAmenities: clubData.Amenities,
              clubDescription: clubData.Description,
              clubProfilePic: clubData.ProfilePicture,
              clubPictures: clubData.Pictures,
              Latitude:clubData.Location.Latitude,
              Longitude:clubData.Location.Longitude,
              address: clubData.Location.Address,
              clubTitle: clubData.Title,
              userID:clubData.UserID, 
              userDetailsPic:clubData.UserDetails,
              totalCount:clubData.LikeCount,
              ObjectType:clubData.ObjectType,
              cCount: clubData.CommentCount
          })
      } else {
          toast.error("Something wents wrong")
      }
    }).catch(function (error) {
    // console.log(error);
    })
  }

  goBack = ()=> {
    this.props.history.goBack();
  }

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  openChatPopup = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  handleImageLoaded = () => {
    this.setState({ loaded: true });
  }

  deleteObject=()=>{
    this.props.client
    .mutate({
        mutation: deleteClub,
        variables: {
          ClubID:parseInt(this.state.clubID),
          UserID:ID
      },
      })
      .then(res => { 
        if(res.data.deleteClub){
          toast.success("Club deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
    });  
  }

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.clubDetails.ID,this.state.clubDetails.UserID,this.state.clubDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
        this.setState({clubPictures:picturesData.Pictures,userDetailsPic:picturesData.UserDetails})
        console.log(picturesData,"picturesData");
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {});
  }
       
  viewLessPictures = () =>{
    let queryResult = this.state.clubPictures
    let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
      filterdata.push(queryResult[i]);
      if (filterdata.length == 4) {
        break;
      }
    }
    this.setState({clubPictures:filterdata})
  }

  objectLike= (count) =>{
    this.setState({totalCount:count})
  }

  goToEditPage = (url) => {
    this.props.history.push('/edit-club/'+this.state.url)
  }

  render() {
    const {clubTitle, clubAmenities, clubDescription, address,
      clubProfilePic, clubPictures, loaded,clubDetails,totalCount,renderLike,isLike,ObjectType,cCount} = this.state;    
    
    const imageStyle = !loaded ? { display: "none" } : {}
      
    return (
      <div className="container">
        <HelmetData metaDetails={this.state.metaData} history={this.props}/>
        <div className="col-md-12">
            <div className="row">
                <div className="col-md-3 mb-2 mt-2"> <a className="desktop_back" href="#" onClick={this.goBack}><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
            </div>
        </div>
        <div className="col-md-12">
          <div className="row">
            <div className="col-md-5 col-sm-12 mb-2 mt-2">
                {!loaded && <ImagePlaceholder/> }
                <img src={ clubProfilePic ? clubProfilePic : "../../assets/images/event-people.png"} style={imageStyle} className="profImg sideImg" alt={clubTitle} onLoad={this.handleImageLoaded}/>
            </div>
            <div className="col-md-7 col-sm-10 mb-2 mt-2 club-info">                       
              <h2>
                {clubTitle}
                {ID && this.state.userID === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img"><img src="../../assets/images/club-flag.png" alt="Back"/></div>
              <p className="date-time-text club-time" >Founded: 1926</p>   
              <table className="addressTable">
                  <tr>
                      <td className="add-icon"><img src="../../assets/images/location-icon.png" alt="location-icon" className="pr-2"/></td>
                      <td className="add">{address}</td>
                  </tr>
              </table>
              <p className="date-time-text"> 
                  <img src="../../assets/images/internet-icon.png" alt="internet-icon" className="pr-2"/> 
                  <img src="../../assets/images/facebook-icon.png" alt="facebook-icon" className="pr-2"/>  
                  { this.state.userID !== ID && <button className="clubContactOrgBtn" onClick={this.openChatPopup}> Contact Organizer</button>}&nbsp;
                  <button className="leaveMessageBtn"> <img src="../../assets/images/message-icon.png" className="pr-1" alt="tick-icon"/>Message</button>
                {ID && this.state.userID === ID && <ConfirmationPopup callBack={this.deleteObject} object={"Club"}/>}
              </p> 
              <hr/>
              <div className="like">
                {ID && clubDetails && renderLike && <ObjectLikeDislike
                  LikeCount={totalCount} 
                  ObjectType={clubDetails.ObjectType} 
                  ReferenceID={clubDetails.ID} 
                  status={isLike} 
                  ReferenceUserID={clubDetails.UserID} 
                  updateOnParent={this.objectLike} 
                  isDetail={true}/>
                }
                { ID == null && <><img src="../../assets/images/heart-icon.png" alt="heart-icon" className="pr-2"/><span>{totalCount}</span> </>}
              </div>
                <div className="comment"><img src="../../assets/images/comment-icon.png" alt="comment-icon" className="pl-6"/><span> {cCount}</span></div>
                <div className="forward" onClick={this.openShareModal}>
                  <img src="../../assets/images/forward-icon.png" alt="forward-icon" className="pl-6" /><span> 0</span>
                </div>
                { ID && this.state.showShareModel &&
                  <ShareContent 
                    ObjectType="Clubs"
                    ReferenceID={parseInt(clubDetails.ID)}
                    ReferenceUserID={ID}
                    PostUrl={this.state.url}
                    closeHandler={this.openShareModal}
                    shareUrl={url.concat(this.props.match.url)}
                    title={clubTitle}
                    quote={clubTitle} />
                }
                <select className="form-control col-12" id="membersOnly">
                    <option>Members Only</option>
                    <option>New York</option>
                    <option>New York</option>
                </select>
                <select className="form-control col-12" id="membersOnlyMobile">
                    <option>Members Only</option>
                    <option>New York</option>
                    <option>New York</option>
                </select>
                <img src="../../assets/images/white-drop-down-arrow.png" className="downArrowClub" alt="white-drop-down-arrow"/>
              </div>        
            </div>
          </div>
          <div className="col-md-12">
            <div className="row">
                <h5 className="about-event">About this club</h5>
                <p className="about-event-info">{clubDescription}</p>    
            </div>
          </div>
          <div className="col-md-12">
              <div className="row">
                  <h5 className="about-event">Amenities / Services </h5>           
              </div>
          </div>
          <div className="col-md-12">
              <div className="row"> 
                  <div className="servicesSection">
                      <ul className="services" style={{padding: "0"}}>
                          { clubAmenities ? 
                              clubAmenities.map((item) => {
                                  return <li key={item.ID} style={{width: "unset"}}>{item.Name}</li>
                              }) :
                              "No Amenities / Services "
                          }
                      </ul>
                  </div>    
              </div>
          </div>
          <MemberContent />
          <PhotosContent Pictures={clubPictures} Details={clubDetails} postPictures={ clubDetails && clubDetails.PostPictures} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getClubData}/>
          <VideoContent videos={clubDetails && clubDetails.Videos} />
          {ObjectType &&
          <PostListParent ReferenceID={parseInt(this.state.clubID)} ObjectType={ObjectType} updatePhotos={this.getClubData} />}
          <ToastContainer autoClose={1000}/>
          { ID && this.state.showChatPopup && 
            <ChatApp
              sender={ID}
              receiver={this.state.userID}
              profilePic={clubProfilePic}
              title={clubTitle}
              objectType="Clubs"
              referenceID={parseInt(this.state.clubID)}
              popupHandler={this.openChatPopup}
              myID={ID}
            />
          }
      </div>
    )}
}

export default compose(
  connect(null, { getClubDetailsByID, getViewAllPictures}
  ),
  withApollo
)(DisplayClubContent);
