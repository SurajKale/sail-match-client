import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import { dashBoarSeach } from "../../data/query";
import ImagePlaceholder from "../../services/ImagePlaceholder";

class DetailSearch extends Component {
  constructor(props) {
    super(props);

    // console.log('SSSSS:'+this.props.match.params.keyword)
    this.state = {
      searchText: this.props.match.params.keyword,
      searchData: [],

      loaded: false,
    };
    this.postSearch = this.postSearch.bind(this);
  }

  handleImageLoaded() {
    this.setState({ loaded: true });
  }

  postSearch(text) {
    let searchText = this.state.searchText;
    if (text) {
      searchText = text;
    }
    this.props.client
      .query({
        query: dashBoarSeach,
        variables: {
          Name: searchText,
        },
        fetchPolicy: "network-only",
      })
      .then((res) => {
        this.setState({ searchData: res.data.dashBoarSeach });
      });
  }

  componentDidMount() {
    this.postSearch();
  }

  componentWillReceiveProps(nextProps, prevState) {
    if (nextProps.match.params.keyword !== prevState.searchText) {
      this.postSearch(nextProps.match.params.keyword);
    }
  }

  gotoObject = (data) => {
    // this.props.history.push('/')
    this.props.history.push(
      "../" + data.ObjectTypeDetails.Url + "/" + data.Url
    );
  };

  gotoUser = (data) => {
    // this.props.history.push('/')
    this.props.history.push("../profile/" + data.Url);
  };

  render() {
    const { searchData, loaded } = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h6 className="searchList">Showing {searchData.length} results</h6>
          </div>
        </div>

        <div className="row">
          {searchData &&
            searchData.map((data, index) => (
              <div className="col-sm-3 mb-5" key={index}>
                <div className="post_single">
                  {!loaded && <ImagePlaceholder />}
                  {
                    data.ObjectTypeDetails ? (
                      //  <a href="javascript:void(0);" onClick={(e)=>this.gotoObject(data)} className="search_title">
                      <img
                        src={
                          data.ProfileImage
                            ? data.ProfileImage
                            : "../../assets/images/no_image.jpg"
                        }
                        onClick={(e) => this.gotoObject(data)}
                        className="no_image"
                        alt={data.Title}
                        title={data.Title}
                        style={imageStyle}
                        onLoad={this.handleImageLoaded.bind(this)}
                      />
                    ) : (
                      // {/* </a> */}
                      //  :<a href="javascript:void(0);"  className="search_title">
                      <img
                        src={
                          data.ProfileImage
                            ? data.ProfileImage
                            : "../../assets/images/no_image.jpg"
                        }
                        onClick={(e) => this.gotoUser(data)}
                        className="no_image"
                        alt={data.Title}
                        title={data.Title}
                        style={imageStyle}
                        onLoad={this.handleImageLoaded.bind(this)}
                      />
                    )
                    // </a>
                  }
                  <h5>
                    {data.ObjectTypeDetails ? (
                      <a
                        href="javascript:void(0);"
                        onClick={(e) => this.gotoObject(data)}
                        // className="search_title"
                      >
                        {data.Title  && data.Title.length > 15? data.Title.substring(0, 15) + "..." : data.Title}
                      </a>
                    ) : (
                      <a
                        href="javascript:void(0);"
                        onClick={(e) => this.gotoUser(data)}
                        // className="search_title"
                      >
                         {data.Title  && data.Title.length > 15? data.Title.substring(0, 15) + "..." : data.Title}
                      </a>
                    )}
                  </h5>
                  <p>{data.CreatedDate}</p>
                  {data.ObjectTypeDetails && (
                    <div class="author">
                      <span class="author_name">
                        <img
                          src={
                            data.CreatedBy && data.CreatedBy.ProfileImage
                              ? data.CreatedBy.ProfileImage
                              : "../../assets/images/no_image.jpg"
                          }
                        />{" "}
                        {data.CreatedBy &&
                          data.CreatedBy.FirstName +
                            " " +
                            data.CreatedBy.LastName}{" "}
                      </span>
                      <span class="category">
                        <a href="#">
                          <img
                            src={
                              data.ObjectTypeDetails.ProfileImage
                                ? data.ObjectTypeDetails.ProfileImage
                                : "../../assets/images/Crew.svg"
                            }
                            alt="home"
                          />
                        </a>
                      </span>
                    </div>
                  )}
                  {data.UserDetails && (
                    <div className="author">
                      <Link
                        to={"profile/" + data.UserDetails.UserName}
                        title="view profile"
                      >
                        <span
                          className="author_name"
                          title={
                            (data.UserDetails.FirstName
                              ? data.UserDetails.FirstName
                              : "") +
                            " " +
                            (data.UserDetails.LastName
                              ? data.UserDetails.LastName
                              : "")
                          }
                        >
                          <img
                            src={
                              data.UserDetails.ProfileImage
                                ? data.UserDetails.ProfileImage
                                : "./assets/images/user_icon.png"
                            }
                            title={
                              data.UserDetails.FirstName +
                              " " +
                              data.UserDetails.LastName
                            }
                            className="user-icon-home"
                          />
                          {(data.UserDetails.FirstName
                            ? data.UserDetails.FirstName
                            : "") +
                            " " +
                            (data.UserDetails.LastName
                              ? data.UserDetails.LastName
                              : "")}
                        </span>
                      </Link>
                    </div>
                  )}
                  <div className="divider">
                    <hr />
                  </div>
                  <div className="post_footer">
                    <span className="col text-left">
                      <i className="far fa-heart"></i>{" "}
                      {data.LikeCount ? data.LikeCount : 0}
                    </span>
                    <span className="col text-center">
                      <i className="far fa-comment-alt"></i>{" "}
                      {data.CommentCount ? data.CommentCount : 0}
                    </span>
                    <span className="col text-right">
                      <i className="fas fa-share"></i>{" "}
                      {data.ShareCount ? data.ShareCount : 0}
                    </span>
                  </div>
                </div>
              </div>
            ))}
        </div>
      </div>
    );
  }
}

export default compose(connect(null, {}), withApollo)(DetailSearch);
