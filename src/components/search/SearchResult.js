import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";

class SearchResult extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limitSearchResult: [],
    };
  }

  componentDidMount() {
    var notificationContainer = document.getElementById(
      "notificationContainer"
    );
    if (notificationContainer) {
      if (this.props.searchData.length > 0) {
        document.onchange = function (e) {
          if (e.target.id !== "notificationContainer") {
            notificationContainer.style.display = "none";
          }
        };
      }
    }
  }

  gotoObject = (search) => {
    this.props.history.push("/");
    this.props.history.push(search.ObjectTypeDetails.Url + "/" + search.Url);
  };

  gotoUser = (search) => {
    this.props.history.push("/");
    this.props.history.push("profile/" + search.Url);
  };
  componentDidMount = () => {
    let data = [];
    for (let i = 0; i < this.props.searchData.length; i++) {
      data.push(this.props.searchData[i]);
      if (data.length >= 5) {
        break;
      }
    }
    this.setState({ limitSearchResult: data });
  };
  render() {
    const { searchData, searchText } = this.props;
    const { limitSearchResult } = this.state;
    return (
      <div
        id="notificationContainer"
        class="searchBox"
        style={{ display: "block" }}
      >
        <ul class="searchUl">
          {searchData &&
            limitSearchResult.map((search, index) => (
              <li key={index}>
                <img
                  src={
                    search.ProfileImage
                      ? search.ProfileImage
                      : "../../assets/images/placeholder.png"
                  }
                  alt="small-photo-2"
                  class="objectSmallIcon"
                />
                {search.ObjectTypeDetails ? (
                  <a
                    onClick={(e) => this.gotoObject(search)}
                    className="search_title"
                  >
                    {search.Title ? search.Title : ""}
                    <img
                      src={
                        search.ObjectTypeDetails.ProfileImage &&
                        search.ObjectTypeDetails.ProfileImage
                      }
                      class="objectSmallIcon"
                      style={{ float: "right" }}
                    />
                  </a>
                ) : (
                  <a
                    onClick={(e) => this.gotoUser(search)}
                    className="search_title"
                  >
                    {search.Title ? search.Title : ""}{" "}
                    <img
                      src={"../../assets/images/Crew.svg"}
                      class="objectSmallIcon"
                      style={{ float: "right" }}
                    />
                  </a>
                )}
              </li>
            ))}
          {searchText && (
            <li>
              <Link class="seeAll" to={"/search-all/" + searchText}>
                See all results for "{searchText}"{" "}
              </Link>
            </li>
          )}
        </ul>
      </div>
    );
  }
}

export default compose(connect(null, {}), withRouter, withApollo)(SearchResult);
