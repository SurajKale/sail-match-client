import React, { Component } from "react";
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { withApollo } from "react-apollo";
import { compose } from "redux";
import search from "../../assets/images/icon-feather-search.svg";
import SearchResult from "./SearchResult";
import { dashBoarSeach } from "../../data/query";

const reWhiteSpace = new RegExp(/^\s+$/);

class SearchInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: "",
      searchData: [],
      limitSearchResult:[]
    };
  }
  componentDidMount() {
    var notificationContainer = document.getElementById(
      "notificationContainer"
    );
    if (notificationContainer) {
      if (this.props.searchData.length > 0) {
        document.onchange = function (e) {
          if (e.target.id !== "notificationContainer") {
            notificationContainer.style.display = "none";
          }
        };
      }
    }
  }
  clientdata = (data,inputText) =>{
    let queryResult =[]
    let filterdata = []
         data.forEach(function(searchItem){
            if(searchItem.Title.toLowerCase().indexOf(inputText)!=-1 )
            queryResult.push(searchItem);
        });
        this.setState({searchData:queryResult})
        console.log(queryResult,"queryResult");
        for (let i = 0; i <  queryResult.length; i++) {         
          filterdata.push(queryResult[i]);
          if (filterdata.length >= 5) {
            break;
          }
        }
        // console.log(filterdata,"limitSearchResult");
        this.setState({ limitSearchResult: filterdata });
  }

  handleChange = (e) => {
    e.preventDefault();
    let queryResult 
    let inputText =  e.target.value.toLowerCase()
    this.setState({ searchText: e.target.value });
    if (
      e.target.value.length == 3 &&
      reWhiteSpace.test(e.target.value) === false
    ) {
      this.props.client
      .query({
        query: dashBoarSeach,
        variables: {
          Name: e.target.value,
        },
      })
      .then((res) => {
       this.clientdata(res.data.dashBoarSeach,inputText)
      });
    }
     else {
      this.setState({ searchData: [] });
    }
    if(inputText.length > 3){
      this.clientdata(this.state.searchData,inputText)
    }
  };

  gotoObject = (search) => {
    this.setState({searchText:"",limitSearchResult:[]})
    this.props.history.push("/");
    this.props.history.push(search.ObjectTypeDetails.Url + "/" + search.Url); 
  };

  gotoUser = (search) => {
    this.setState({searchText:"",limitSearchResult:[]})
    this.props.history.push("/");
    this.props.history.push("profile/" + search.Url);  
  };

  navigateToAllSearch =(searchText) =>{
    this.props.history.push("/");
    this.props.history.push("search-all/" + searchText)
    this.setState({searchText:''})
  }

  render() {
    const { searchText, searchData, limitSearchResult} = this.state;
    return (
      <React.Fragment>
        <div className="searchbox">
          <div
            id="formDiv"
            className="form-inline d-flex justify-content-center md-form form-sm mt-0"
          >
            <a href="#">
              <img src={search} alt="Search" />
            </a>
            <input
              className="form-control form-control-sm withicon searchInput"
              type="text"
              placeholder="Search"
              name="searchText"
              value={searchText}
              onChange={this.handleChange}
              aria-label="Search"
            />
          </div>
        </div>
        {searchData && searchData.length > 0 && (
          <div
          id="notificationContainer"
          class="searchBox"
          style={{ display: "block" }}
        >
          <ul class="searchUl">
            {searchData &&
              limitSearchResult.map((search, index) => (
                <li key={index}>
                  <img
                    src={
                      search.ProfileImage
                        ? search.ProfileImage
                        : "../../assets/images/placeholder.png"
                    }
                    alt="small-photo-2"
                    class="objectSmallIcon"
                  />
                  {search.ObjectTypeDetails ? (
                    <a
                      onClick={(e) => this.gotoObject(search)}
                      className="search_title"
                    >
                      {search.Title ? search.Title : ""}
                      <img
                        src={
                          search.ObjectTypeDetails.ProfileImage &&
                          search.ObjectTypeDetails.ProfileImage
                        }
                        class="objectSmallIcon"
                        style={{ float: "right" }}
                      />
                    </a>
                  ) : (
                    <a
                      onClick={(e) => this.gotoUser(search)}
                      className="search_title"
                    >
                      {search.Title ? search.Title : ""}{" "}
                      <img
                        src={"../../assets/images/Crew.svg"}
                        class="objectSmallIcon"
                        style={{ float: "right" }}
                      />
                    </a>
                  )}
                </li>
              ))}
            {searchText && (
              <li>
                <Link class="seeAll"  onClick={(e)=>{this.navigateToAllSearch(searchText)}}>
                  See all results for "{searchText}"{" "}
                </Link>
              </li>
            )}
          </ul>
        </div>
        )}
      </React.Fragment>
    );
  }
}

export default compose(connect(null, {}),withRouter, withApollo)(SearchInput);
