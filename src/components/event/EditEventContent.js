import React, { Component } from 'react'
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { StyledDropZone } from 'react-drop-zone'
import 'react-drop-zone/dist/styles.css'
import { ToastContainer, toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import TimePicker from 'rc-time-picker';
import 'rc-time-picker/assets/index.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { getAllEventTypes } from '../../data/query';
import LocationComponent from '../location/LocationContent';
import { getEventCreateAction, getEventDetailsByID } from "../../data/actions";

const LocalStorageData = require('../../services/LocalStorageData');
let ID = null;

 if(LocalStorageData.ID){
  ID = LocalStorageData.ID;
}

class EditEventContent extends Component {
    constructor(props) {
      super(props)
      this.state = {
				url: this.props.match.params.url,
        startDate: new Date(),
        endDate: new Date(),
        eventType: [],
        eventsArray : [],
        skill: "",
        startTime: "",
        endTime: "",
        streetName: "",
        city: "",
        state: "",
        zipcode: "",
        eventTitle: "",
        description: "",
        profilePic: "",
        file: undefined,
        gpsCoordinate: "",
        locationObject: {},
        button: true
      }
    }

    componentDidMount = () =>{
			window.scrollTo(0, 0);
			this.getEventInfo()
    }
    
		getEventInfo = async () => {
			await this.props
				.getEventDetailsByID(this.props.client, this.state.url)
				.then((res) => {
					if (res.data.getEventByUrl !== null) {
						let eventData = res.data.getEventByUrl;
	
						this.setState({
							startDate: eventData.EventStartDate,
							endDate: eventData.EventEndDate,
							skill: eventData.SkillLevel,
							eventType: eventData.EventType,
							startTime: eventData.EventStartTime,
							endTime: eventData.EventEndTime,
							eventTitle: eventData.EventTitle,
							description: eventData.Description,
							locationObject: eventData.Location,
							profilePic: eventData.ProfilePicture,
							eventID: eventData.ID
						});
					} else {
						toast.error("Something wents wrong");
					}
				})
			.catch(function (error) {
				// console.log(error);
			});
		}

    handleChange = (e) => {
      this.setState({
        [e.target.name] : e.target.value
      })
    }

    setFile = (file) => {
      this.toBase64(file).then(result => {
        this.setState({
          profilePic: result
        })
      });    
    }

    toBase64 = file => new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = error => reject(error);
		});

    createEvent = async (e) => {
      this.setState({ button : !this.state.button })
      e.preventDefault();
     
      const { startDate, endDate, eventType, skill, startTime, endTime,
          eventTitle, description, profilePic, locationObject, eventsArray, eventID } = this.state;

      let eventObject = {
        startDate, endDate, skill, startTime, endTime,
        eventTitle, description, profilePic, ID, locationObject, eventID
			}

			var Type =
			eventType.map(({EventTypeID, Name }) => ({EventTypeID: EventTypeID, Name: Name }));
			
			delete locationObject.__typename;

			await this.props.getEventCreateAction(this.props.client, eventObject, Type).then(res => {
					toast.success("Event updated successfully", {
							onClose: () => this.props.history.push('/event-details/'+this.state.url)
					});
			}).catch(function (error) {
					console.log(error);
			})
    }

    render() {
      const { eventTitle, description, profilePic } = this.state;
				
			const label = profilePic ? 'Profile image successfully added' : 'Click or drop your image here'

      return (
        <div className="container">
          <div className="row  pt-4">
            <div className="cursor-pointer"  className="col-md-3 mb-2"> <a className="desktop_back" href="/"><img src="../../assets/images/back_icon.png" alt="Back"/></a> </div>
            <div className="col-md-6 mb-2"><div className="text-left">
              <a href="/" className="float-left mobile_back"><img src="../../assets/images/back_icon.png" alt="Back"/></a>
              <h1>Edit Event</h1>
              <br/>
              <form className="mt-2">
                {/* <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="typeOfEvent">Type of Event</label>
                    <div className="">
                    {this.state.eventsArray &&
                  this.state.eventsArray.map(type => {
                    return (                   
                        <input
                        type="button"
                        key={type.ID}
                        className={`${type.isSelected ? "btn-primary btn gender" : "btn btn-outline-secondary gender"}`}
                        onClick={(e) => this.handleEventsSelect(type)}
                        value={type.EventTitle} />
                    );
                  })}
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="skillLevel">Skill Level</label>
                    <div className="">
                      <input type="button" name="skill" onClick={this.handleChange} className={skill==="Any" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Any" />
                      <input type="button" name="skill" onClick={this.handleChange} className={skill==="Beginner" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Beginner" />
                      <input type="button" name="skill" onClick={this.handleChange} className={skill==="Intermediate" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Intermediate" />
                      <input type="button" name="skill" onClick={this.handleChange} className={skill==="Advanced" ? "btn-primary btn gender" : "btn btn-outline-secondary gender"} value="Advanced" />
                    </div>
                  </div>
                </div>

                <hr />
                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="eventStartDate">Event Start Date</label>
                    <input type="text" className="form-control" id="eventStartDate" placeholder="Event Start Date" name="startDate" onChange={this.handleChange} />
                    <DatePicker
                      id="startDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{width:"250px"}}
                      selected={startDate}
                      minDate={startDate}
                      onChange={this.handleStartDateChange}
                    />
                  </div>
                  
                  <div className="col-sm-6 my-2">
                    <label for="eventEndDate">Event End Date</label>
                    <input type="text" className="form-control" id="eventEndDate" placeholder="Event End Date"  name="endDate" onChange={this.handleChange} />
                    <DatePicker
                      id="endDate"
                      dateFormat="yyyy-MM-dd"
                      className="form-control"
                      style={{width:"250px"}}
                      selected={endDate}
                      minDate={startDate}
                      onChange={this.handleEndDateChange}
                    />
                  </div>
                </div>

                <div className="row">
                  <div className="col-sm-6 my-2">
                    <label for="eventStartTime">Event Start Time</label>
                    <input type="text" className="form-control" id="eventStartTime" placeholder="Event Start Time"  name="startTime" onChange={this.handleChange} />
                    <TimePicker
                      id="eventStartTime"
                      name="startTime"
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleStartTimeChange}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                  
                  <div className="col-sm-6 my-2">
                    <label for="eventEndTime">Event End Time</label>
                    <input type="text" className="form-control" id="eventEndTime" placeholder="Event End Time" name="endTime" onChange={this.handleChange} />
                    <TimePicker
                      id="eventEndTime"
                      name="endTime"
                      showSecond={false}
                      className="form-control"
                      format={format}
                      onChange={this.handleEndTimeChange}
                      use12Hours
                      inputReadOnly
                    />
                  </div>
                </div>
                <LocationComponent callback={this.callback} />
                <div class="row">
                  <div class="col-sm-6 my-2">
                      <label for="city">City</label>
                      <input readOnly type="text" class="form-control" id="city" placeholder="City" name="city" value={city} />
                  </div>       
                
                  <div class="col-sm-6 my-2">
                      <label for="state">State</label>
                      <input readOnly type="text" class="form-control" id="state" placeholder="State" name="state" value={state} />
                  </div>       
                  </div>
              
                  <div class="row">
                    <div class="col-sm-6 my-2">
                      <label for="zipCode">Zipcode</label>
                      <input readOnly type="text" class="form-control" id="zipCode" placeholder="Zipcode" name="zipCode" value={zipCode} onChange={this.handleChanges} />
                  </div>       
              
                  <div class="col-sm-6 my-2">
                      <label for="gpsCoordinate">GPS Coordinates</label>
                      <input readOnly type="text" class="form-control" id="gpsCoordinate" placeholder="GPS Coordinates" name="gpsCoordinate" value={gpsCoordinate} />
                  </div>       
                </div>
                <hr /> */}
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="eventTitle">Event Title</label>
                    <input defaultValue={eventTitle} type="text" className="form-control" id="eventTitle" placeholder="Event Title"  name="eventTitle" onChange={this.handleChange} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="description">Description</label>
                    <textarea defaultValue={description} className="form-control" id="description" rows="3"  name="description" onChange={this.handleChange} />
                  </div>
                </div>
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <label for="profilePicture">Profile Picture</label>
                    <StyledDropZone 
                      className="form-control"
                      onDrop={this.setFile}
                      label={label}
                    />
                  </div>
                </div>
                {profilePic &&
                <div className="row">
                  <div className="col-sm-12 my-2 text-center">
                        <img src={profilePic} className="img-fluid mb-3 member_pic_preview" alt="Profile" />
                  </div>
                </div>}
                <hr />
                <div className="row">
                  <div className="col-sm-12 my-2">
                    <p className="text-center"><button type="button"  className="btn btn-primary save_profile" onClick={this.createEvent}>Save</button></p>
                  </div>
                </div>
              </form>
            </div>
          <div className="col-md-3 mb-2"></div>
        </div>
      </div>
      <ToastContainer autoClose={1000} />
    </div>
  )}
}

const mapStateToProps = (state) => ({
  myProfile: state.profiles
})


export default compose(
  connect(mapStateToProps, { getEventCreateAction, getEventDetailsByID }
  ),
  withApollo
)(EditEventContent);
