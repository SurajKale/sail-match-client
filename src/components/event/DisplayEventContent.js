import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withApollo } from "react-apollo";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { getEventCreateAction, getEventDetailsByID } from "../../data/actions";
import { _formatDate, _formatTime, url } from "../../services/CommonServices";
import MemberContent from "../common/StaticMember";
import PhotosContent from "../common/PhotosContent";
import VideosContent from "../common/VideosContent";
import PostListParent from "../common/PostListParent";
import ShareContent from "../common/ShareModel/shareOption";
import {deleteEvent} from "../../data/mutation.js";
import ConfirmationPopup from "../common/ConfirmationPopup";
import MatchesCommon  from '../common/MatchesCommon';
import ObjectLikeDislike from "../common/ObjectLikeDislike";

import CreatePost from "../common/CreatePost";

import HelmetData from '../common/_helmet' ;
import ChatApp from '../chat-app/ChatPage';
import  formatcoords from 'formatcoords';

const CONFIG = require('../config');
var SITELOGO = CONFIG.SITELOGO;
var SITE_STATIC_NAME = CONFIG.SITE_STATIC_NAME;

const LocalStorageData = require("../../services/LocalStorageData");
let ID = null;

if (LocalStorageData.ID) {
  ID = LocalStorageData.ID;
}

class CreateEventContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.match.params.url,
      eventType: "",
      skill: "",
      startTime: "",
      endTime: "",
      streetName: "",
      city: "",
      state: "",
      zipcode: "",
      eventTitle: "",
      description: "",
      profilePic: "",
      address: "",
      location: "",
      Latitude: "",
      Longitude: "",
      eventMatch: {},
      eventID:'',
      eventPictures: [],
      eventDetails: {},
      userDetailsPic:{},

      totalCount:"",
      Status:"",
      CreatedBy:'',
      ObjectType:"",

      showChatPopup: false,
      showShareModel: false,

      metaData:[],
    };
  }

  async componentDidMount() {
    window.scrollTo(0, 0);
    this.getEventInfo()
    
  }

  getEventInfo = async () => {
    await this.props
      .getEventDetailsByID(this.props.client, this.state.url)
      .then((res) => {
        if (res.data.getEventByUrl !== null) {
          let eventData = res.data.getEventByUrl;


          var metaArr = [];
          metaArr.push({
           title : eventData.EventTitle ?  eventData.EventTitle : SITE_STATIC_NAME,
           keywords : eventData.ObjectType, SITE_STATIC_NAME,
           description : eventData.Description ? eventData.Description : '',
           og_url : window.location.href,
           og_title : eventData.EventTitle ?  eventData.EventTitle : SITE_STATIC_NAME,
           og_description : eventData.Description ? eventData.Description : '',
           canonical_url : window.location.href,
           og_image : eventData.ProfilePicture ? eventData.ProfilePicture : SITELOGO,
           amp_url : window.location.href,
          })

          this.setState({
            metaData:metaArr[0],
            eventID:eventData.ID,
            isLike:eventData.isLike,
            renderLike:true,
            ownerID :eventData.EventOwnerId,
            eventTitle: eventData.EventTitle,
            description: eventData.Description,
            startDate: eventData.EventStartDate,
            endDate: eventData.EventEndDate,
            startTime: eventData.EventStartTime,
            endTime: eventData.EventEndTime,
            location: eventData.Location.MapLocation,
            Latitude: eventData.Location.Latitude,
            Longitude: eventData.Location.Longitude,
            address: eventData.Location.Address,
            profilePic: eventData.ProfilePicture,
            eventMatch: eventData.EventMatch,
            totalCount:eventData.LikeCount,
            Status:eventData.Status,
            CreatedBy:eventData.EventOwnerId,
            ObjectType:eventData.ObjectType,
            shareCount: eventData.ShareCount,
            commentCount: eventData.CommentCount,
            eventPictures: eventData.Pictures,
            eventDetails: eventData,
          });
        } else {
          toast.error("Something wents wrong");
        }
      })
    .catch(function (error) {
      // console.log(error);
    });
  }

  goBack = () => {
    this.props.history.goBack();
  };

  deleteObject=()=>{
    this.props.client
    .mutate({
        mutation: deleteEvent,
        variables: {
          EventID:parseInt(this.state.eventID),
          UserID:ID
      },
     })
      .then(res => { 
        if(res.data.deleteEvent){
          toast.success("Event deleted successfully", {
            onClose: () => this.props.history.push("/"),
          });
        }
      });  
  }

  objectLike= (count) =>{
    console.log(count);
  this.setState({totalCount:count})
  }

  openChatPopup = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showChatPopup: !this.state.showChatPopup });
  };

  openShareModal = () => {
    // document.body.style.overflowY = "hidden";
    this.setState({ showShareModel: !this.state.showShareModel });
  };

  viewAllPictures= () =>{
    this.props.getViewAllPictures(this.props.client, this.state.eventDetails.ID,this.state.eventDetails.UserID,this.state.eventDetails.ObjectType)
    .then((res) => {
      if (res.data.viewAllObjectTypePictures !== null) {
        let picturesData = res.data.viewAllObjectTypePictures;
      this.setState({eventPictures:picturesData.Pictures, 
                     userDetailsPic:picturesData.UserDetails,
                    })
      } else {
        toast.error("Something wents wrong");
      }
    })
    .catch(function (error) {
    
    });
   }
   
   viewLessPictures = () =>{
       let queryResult = this.state.eventPictures
       let filterdata = [];
    for (let i = 0; i <  queryResult.length; i++) {         
        filterdata.push(queryResult[i]);
        if (filterdata.length == 4) {
          break;
        }
      }
      this.setState({eventPictures:filterdata})
   }

   goToEditPage = (url) => {
     this.props.history.push('/edit-event/'+this.state.url)
   }

  render() {
    const {
      startDate,
      endDate,
      startTime,
      endTime,
      eventTitle,
      description,
      profilePic,
      address,
      Latitude,
      Longitude,
      eventMatch,
      eventID,
      CreatedBy,
      totalCount,
      ObjectType,
      isLike,
      renderLike,
      eventPictures,
      eventDetails
    } = this.state;
    var point = formatcoords(parseFloat(Latitude), parseFloat(Longitude)).format();

    return (
      <div className="container pt-2">
         <HelmetData metaDetails={this.state.metaData} />

        <div className="col-md-12">
          <div className="row">
            <div className="col-md-3 mb-2 mt-2">
              {" "}
              <a href="#!" className="desktop_back" onClick={this.goBack}>
                <img src="../../assets/images/back_icon.png" alt="Back" />
              </a>{" "}
            </div>
          </div>
        </div>

        <div className="col-md-12">
          <div className="row">
            <div className="col-md-5 col-sm-12 mb-2 mt-2">
              {" "}
              <img src={profilePic} className="sideImg" alt={eventTitle} />{" "}
            </div>
            <div className="col-md-7 col-sm-10 mb-2 mt-2 event-info">
              <h2>
                {eventTitle}&nbsp;
                {ID && this.state.ownerID === ID &&
                  <i 
                    className="fa fa-pencil cursor-pointer"
                    title='Edit'
                    style={{ fontSize: 17, padding: 11, color: 'green' }}
                    onClick={this.goToEditPage}
                  />
                }
              </h2>
              <div className="date-time-img">
                <img src="../../assets/images/date-time.png" alt="Back" />
              </div>
              <p className="date-time-text" id="date-time-text">
                <span>From :</span>
                <span>
                  {_formatDate(startDate)}, {_formatTime(startTime)}
                </span>{" "}
                - To :
                <span>
                  {" "}
                  {_formatDate(endDate)}, {_formatTime(endTime)}
                </span>
              </p>
              <p className="date-time-text">
                {" "}
                <img
                  src="../../assets/images/location-icon.png"
                  alt="location-icon"
                  className="pr-2"
                />{" "}
                <span> {address}</span>{" "}
              </p>
              <p className="date-time-text">
                {" "}
                <img
                  src="../../assets/images/position-icon.png"
                  alt="position-icon"
                  className="pr-2"
                />{" "}
                <span>
                  {" "}
                  {point}
                </span>{" "}
              </p>
              <p className="date-time-text">
                <img
                  src="../../assets/images/internet-icon.png"
                  alt="internet-icon"
                  className="pr-2"
                />
                <img
                  src="../../assets/images/facebook-icon.png"
                  alt="facebook-icon"
                  className="pr-2"
                />
                <button className="attendBtn">
                  {" "}
                  <img
                    src="../../assets/images/tick-icon.png"
                    className="pr-1"
                    alt="tick-icon"
                  />
                  Attend
                </button>
               { ID !== this.state.ownerID && <button className="contactOrgBtn" onClick={this.openChatPopup}> Contact Organizer</button> }
               {ID && this.state.ownerID === ID && <ConfirmationPopup callBack={this.deleteObject} object={"Event"}/>}

               {/* <button className="contactOrgBtn" onClick={this.openPostPopup}> Create Post</button> */}
               <hr />

               { ID && this.state.showChatPopup && 
                  <ChatApp
                    sender={ID}
                    receiver={this.state.ownerID}
                    profilePic={profilePic}
                    title={eventTitle}
                    objectType="Events"
                    referenceID={this.state.eventID}
                    popupHandler={this.openChatPopup}
                    myID={ID}
                  />
               }
              
              </p>
              {/* <hr/>   */}

              {/* <div className="like">
                <img
                  src="../../assets/images/heart-icon.png"
                  alt="heart-icon"
                  className="pr-2"
                />
                <span> 140</span>
              </div> */}

            <div className="like">
            {ID && renderLike &&  <ObjectLikeDislike
                LikeCount={totalCount} 
                ObjectType={ObjectType} 
                ReferenceID={eventID} 
                status={isLike} 
                ReferenceUserID={CreatedBy} 
                updateOnParent={this.objectLike} 
                isDetail={true}/>}
                { ID == null && <> <img
                    src="../../assets/images/heart-icon.png"
                    alt="heart-icon"
                    className="pr-2"
                  />
                  <span>{totalCount}</span></>}
            </div>
              <div className="comment">
                <img
                  src="../../assets/images/comment-icon.png"
                  alt="comment-icon"
                  className="pl-6"
                />
                <span> {this.state.commentCount || 0}</span>
              </div>
              <div className="forward" onClick={this.openShareModal}>
                <img
                  src="../../assets/images/forward-icon.png"
                  alt="forward-icon"
                  className="pl-6"
                />
                <span> {this.state.shareCount || 0}</span>
              </div>
              { ID && this.state.showShareModel &&
                <ShareContent 
                ObjectType="Events"
                ReferenceID={parseInt(eventID)}
                ReferenceUserID={ID}
                PostUrl={this.state.url}
                closeHandler={this.openShareModal}
                shareUrl={url.concat(this.props.match.url)}
                title={eventTitle}
                quote={eventTitle} />
              }

              <select className="form-control col-12" id="membersOnly">
                <option>Members Only</option>
                <option>New York</option>
                <option>New York</option>
              </select>

              <select className="form-control col-12" id="membersOnlyMobile">
                <option>Members Only</option>
                <option>New York</option>
                <option>New York</option>
              </select>
              <img
                src="../../assets/images/white-drop-down-arrow.png"
                className="downArrow"
                alt="white-drop-down-arrow"
              />
            </div>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <h5 className="about-event">About this event</h5>
            <p className="about-event-info">{description}</p>
          </div>
        </div>
        <MemberContent />
        <PhotosContent Pictures={eventPictures} postPictures={eventDetails.PostPictures} Details={eventDetails} callBack={this.viewAllPictures} UserDetails={this.state.userDetailsPic} viewLess={this.viewLessPictures} updatePhotos={this.getEventInfo} />
        <VideosContent videos={eventDetails.Videos} />
        {/* <MatchesCommon dataList={eventMatch} startDate={startDate} startTime={startTime} /> */}
        <div>
          <div className="col-md-12">
          <div className="row">
            <h5 className="about-event">
            {this.state.eventMatch &&  " Match for event" }
              <span>
                {this.state.eventMatch && this.state.eventMatch.length}
              </span>
            </h5>
          </div>
          </div>
          {this.state.eventMatch && (
            <div className="col-sm-3 mb-5 ">
              <div className="post_single">
                <img
                  src={this.state.eventMatch.ProfilePicture}
                  alt=""
                  style={{ borderRadius: 15 }}
                />
                <h5>
                  <a href="#!">
                    {this.state.eventMatch.Title &&
                    this.state.eventMatch.Title.length > 15
                      ? this.state.eventMatch.Title.substring(0, 15) + "..."
                      : this.state.eventMatch.Title}
                  </a>
                </h5>
                <p>
                  {_formatDate(startDate)}, {_formatTime(startTime)}
                </p>
                 {/* <div className="author"><span className="author_name"><img src="../../assets/images/4.png" alt=""/> Frank Hail</span><span className="category"><a href="#"><img src="../../assets/images/Crew.svg" alt="home"/></a></span></div>   */}
                <div className="divider">
                  <hr />
                </div>
                <div className="post_footer">
                  <span className="col text-left">
                    <i className="far fa-heart"></i> 0
                  </span>
                  <span className="col text-center">
                    <i className="far fa-comment-alt"></i> 0
                  </span>
                  <span className="col text-right">
                    <i className="fas fa-share"></i> 0
                  </span>
                </div>
              </div>
            </div>
          )}
        </div>
        {ObjectType &&
        <PostListParent ReferenceID={eventID} ObjectType={ObjectType} updatePhotos={this.getEventInfo} />}
        <ToastContainer autoClose={1000}/>         
                
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  myProfile: state.profiles,
});

export default compose(
  connect(mapStateToProps, { getEventCreateAction, getEventDetailsByID }),
  withApollo
)(CreateEventContent);
