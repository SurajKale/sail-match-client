import React, { useState, useEffect, Fragment } from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { Provider } from 'react-redux';
import { setContext } from 'apollo-link-context';
import { BrowserRouter, Switch, Route,Redirect } from "react-router-dom";
import { createBrowserHistory } from 'history'

import './App.css'
import store from './data/store';
import AppLayout from "./components/App";
import SignUp from './components/auth/SignUp';
import LoginContent from './components/auth/SignIn'
import Activation from './components/auth/Activation'
import RenderContent from './components/RenderContent';
import EditProfilePage from './components/EditProfileContent';
import ResetPassword from './components/auth/ResetPassword';
import ForgotPassword from './components/auth/ForgotPassword';
import { Observable } from 'apollo-link';
import axios from "axios";
import Cookie from 'js-cookie';
import CreateCrew from './components/CreateCrew'
import CreateEvent from './components/event/CreateEventContent';
import EditEventPage from './components/event/EditEventContent';
import EditCrewPage from './components/crew/EditCrew';
import EditRidePage from './components/ride/EditRide';
import EditCoursePage from './components/course/EditCourseContent';
import EditSkillPage from './components/skill/EditSkillContent';
import EditClubPage from './components/club/EditClubContent';
import EditServicePage from './components/service/EditService';
import EditMatchPage from './components/match/EditMatch';
import CrewDetails from "./components/crew/CrewDetails";
import DisplyEvent from './components/event/DisplayEventContent';
import LocationContent from './components/location/LocationContent';
import AddRide from './components/ride/AddRide';
import ProtectedRoute from './ProtectedRoute'
import RideList from './components/ride/RideList';
import HomePage from './components/home/HomePage';
import ProfileContent from './components/profile/ProfileContent';
import DisplayBoat from './components/boat/DisplayBoatContent';
import EditBoatDetais from './components/boat/EditBoatContent';
import CreateBoat from './components/boat//CreateBoatContent';
import DisplayMatch from "./components/match/DisplayMatch";
import AddSkill from './components/skill/AddSkillContent';
import DisplaySkill from './components/skill/DisplaySkillContent';
import CreateMatchPub from './components/match/CreateMatchPub'
import CreateService from './components/service/create-service';
import DisplayService from './components/service/display-service'
import AddCourse from './components/course/AddCourseContent';
import DisplayCourse from './components/course/DisplayCourseContent';
import AddRental from './components/rental/AddRental';
import DisplayRental from './components/rental/DisplayRental';
import DetailSearch from './components/search/DetailSearch';
import CreateClub from './components/club/CreateClubContent';
import DisplayClub from './components/club/DisplayClubContent';
import DisplayGroup from './components/group/DisplayGroupContent';
import CreateGroup from './components/group/CreateGroupContent';
import EditGroup from './components/group/EditGroupContent';
import ViewBlog from './components/home/ViewBlog';
import BlogDetails from './components/home/BlogDetails';
import BlogDetailsNew from './components/home/BlogDetailsUpdated';
import AllNotification from './components/common/Notification/allNotification';
import ChangePassword from './components/profile/ChangePassword';


const history = createBrowserHistory();
global.refreshToken = ''
global.token =  ''
const client = new ApolloClient({
  uri: "http://ec2-54-236-24-40.compute-1.amazonaws.com:9100/sails",
  onError: ({ graphQLErrors,networkError, operation, forward }) => {
    if (graphQLErrors) {
        for (let err of graphQLErrors) {
            // handle errors differently based on its error code
            switch (err.message) {
                case 'TokenExpiredError: jwt expired':
                    return new Observable(observer => {
                        axios({
                            url: 'http://ec2-54-236-24-40.compute-1.amazonaws.com:9100/sails',
                            method: 'post',
                            headers: {
                                authorization: global.refreshToken ? `Bearer ${global.refreshToken}` : JSON.parse(localStorage.getItem('sessionUser')).RefreshToken
                            },
                            data: {                            
                                query: `
                                    mutation {
                                    regenerateToken (
                                      RefreshToken : "${global.refreshToken ? `Bearer ${global.refreshToken}` : JSON.parse(localStorage.getItem('sessionUser')).RefreshToken}"
                                    ) {
                                        ID
                                        Token
                                        RefreshToken
                                    }
                                    }
                                    `
                            },                    
                        }).then(refreshResponse => {
                            global.token = refreshResponse.data.data.regenerateToken.Token;
                            global.refreshToken = refreshResponse.data.data.regenerateToken.RefreshToken;
                            if (typeof document != 'undefined') {
                                if (global.token ) {
                                    Cookie.set('token', global.token, { expires: 1 });
                                    Cookie.set('refreshToken', global.refreshToken, { expires: 1 });
                                    if (localStorage.getItem('sessionUser')) {
                                        let sessionUser = JSON.parse(localStorage.getItem('sessionUser'))
                                        sessionUser.Token = global.token;
                                        sessionUser.RefreshToken = global.refreshToken;
                                        localStorage.setItem('sessionUser', JSON.stringify(sessionUser))
                                    }
                                }
                            }
                        })
                            .then(() => {
                                const subscriber = {
                                    next: observer.next.bind(observer),
                                    error: observer.error.bind(observer),
                                    complete: observer.complete.bind(observer)
                                }
                                // Retry last failed request
                                forward(operation).subscribe(subscriber)
                            })
                            .catch(error => {
                              // debugger
                                // No refresh or client token available, we force user to login
                                localStorage.clear();
                                window.location.href = "/login";
                                observer.error(error)
                            })
                    })
            }
        }
    }
},
  request: (operation) => {
    let token
    operation.setContext({
      headers: {
        Authorization:token ? `Bearer ${token}` : typeof localStorage != 'undefined' ? `${localStorage.getItem('sessionUser') ? `Bearer ${JSON.parse(localStorage.getItem('sessionUser')).Token}` : ''}` : ''
        // authorization:token ? `Bearer ${token}` : typeof localStorage != 'undefined' ? `${localStorage.getItem('sessionUser') ? `Bearer ${"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjg5LCJpYXQiOjE1OTIyOTIzMjAsImV4cCI6MTU5MjM3ODcyMH0.dOtN8HDEm8z1fGxB7hK_W9X-uFOzY47ibwkvuLCRHp7By7Q_RM8V58rvpt7a_Qpiol4x7j3MhnKW1A5Pd5sYIbiWMxLFPoAUGgJxRKJTtp9ekbfRc3f-jdjPZfr51jaIYpBVohg67W-UT83OB5nXqgzfG_9RqTiJSu3JnA6tTH"}` : ''}` : ''
      }
    })  
  }
});

const LocalStorageData = require('./services/LocalStorageData');
let is_login  = false;
 if(LocalStorageData.ID){
  is_login = true;
}

function App(props) {
  return (
    <div className="App">
      <ApolloProvider client={client}>
      <Provider store={store}>
          <BrowserRouter history={history}>
            <Switch>
              <Route exact path="/signup" render={props => <SignUp {...props} />} />
              <Route exact path="/login" render={props => <LoginContent {...props} />} />
              <Route path="/activation-link" render={props => <Activation {...props} />} />
              <Route path="/reset-password/:link" render={props => <ResetPassword {...props} />} />
              <Route path="/forgot-password" render={props => <ForgotPassword {...props} />} />
              <AppLayout>
                <Route exact path="/home" ID={LocalStorageData.ID} component={HomePage}  render={props => <HomePage {...props} />} />
                <Route exact path="/" render={props => <HomePage {...props} />} ID={LocalStorageData.ID} component={HomePage}/>
                <Route path="/blogs" render={props => <ViewBlog {...props} />} ID={LocalStorageData.ID} component={ViewBlog}  />
                <ProtectedRoute path="/edit-profile/:username" ID={LocalStorageData.ID} component={EditProfilePage} render={props => <EditProfilePage {...props} />} />
                <ProtectedRoute exact path="/add-crew"  ID={LocalStorageData.ID} component={CreateCrew} render={props => <CreateCrew {...props} />} />
                <ProtectedRoute exact  path="/create-event"   ID={LocalStorageData.ID} component={CreateEvent} />
                <Route path="/event-details/:url" render={props => <DisplyEvent {...props} />} />
                <ProtectedRoute exact  path="/edit-event/:url"   ID={LocalStorageData.ID} component={EditEventPage} />
                <Route path="/crew-details/:id" render={props => <CrewDetails {...props} />} />
                <ProtectedRoute exact  path="/edit-crew/:url"   ID={LocalStorageData.ID} component={EditCrewPage} />
                <ProtectedRoute path="/map" render={props => <LocationContent {...props} />} />
                <ProtectedRoute path="/add-ride" render={props => <AddRide {...props} />} ID={LocalStorageData.ID} component={AddRide} />   
                <Route path="/ride-details/:id" render={props => <RideList {...props} />} ID={LocalStorageData.ID} component={RideList} /> 
                <ProtectedRoute exact  path="/edit-ride/:id"   ID={LocalStorageData.ID} component={EditRidePage} />
                <ProtectedRoute path="/create-boat" render={props => <CreateBoat {...props} />} ID={LocalStorageData.ID} component={CreateBoat} />
                <ProtectedRoute path="/boat-details/:url" render={props => <DisplayBoat {...props} />} ID={LocalStorageData.ID} component={DisplayBoat}  />
                <ProtectedRoute path="/edit-boat-details/:url" render={props => <EditBoatDetais {...props} />} ID={LocalStorageData.ID} component={EditBoatDetais}  />
                <ProtectedRoute path="/profile/:username" render={props => <ProfileContent {...props} />} ID={LocalStorageData.ID} component={ProfileContent}  />
                {/* <ProtectedRoute path="/create-match" render={props => <CreateMatch {...props} />} ID={LocalStorageData.ID} component={CreateMatch}  />  */}
                <Route path="/match-details/:id" render={props => <DisplayMatch {...props} />}/> 
                <ProtectedRoute path="/add-skill" render={props => <AddSkill {...props} />} ID={LocalStorageData.ID} component={AddSkill}  />
                <ProtectedRoute path="/create-match" render={props => <CreateMatchPub {...props} />} ID={LocalStorageData.ID} component={CreateMatchPub}  />  
                <ProtectedRoute exact  path="/edit-match/:url"   ID={LocalStorageData.ID} component={EditMatchPage} />
                <Route path="/skill-details/:url" render={props => <DisplaySkill {...props} />} ID={LocalStorageData.ID} component={DisplaySkill}  /> 
                <ProtectedRoute exact  path="/edit-skill/:url"   ID={LocalStorageData.ID} component={EditSkillPage} />
                <ProtectedRoute path="/add-course" render={props => <AddCourse {...props} />} ID={LocalStorageData.ID} component={AddCourse}  />
                <Route path="/course-details/:url" render={props => <DisplayCourse {...props} />} ID={LocalStorageData.ID} component={DisplayCourse}  />
                <ProtectedRoute exact  path="/edit-course/:url"   ID={LocalStorageData.ID} component={EditCoursePage} />
                <ProtectedRoute path="/add-rental" render={props => <AddRental {...props} />} ID={LocalStorageData.ID} component={AddRental}  />            
                <Route path="/rental-details/:id" render={props => <DisplayRental {...props} />} ID={LocalStorageData.ID} component={DisplayRental}  />
                <ProtectedRoute path="/create-service" render={props => <CreateService {...props} />} ID={LocalStorageData.ID} component={CreateService}  />
                <ProtectedRoute exact  path="/edit-service/:url"   ID={LocalStorageData.ID} component={EditServicePage} />
                <Route path="/service-details/:id" render={props => <DisplayService {...props} />} ID={LocalStorageData.ID} component={DisplayService}  /> 
                <Route path="/search-all/:keyword" render={props => <DetailSearch {...props} />} />               
                <ProtectedRoute path="/create-club" render={props => <CreateClub {...props} />} ID={LocalStorageData.ID} component={CreateClub}  /> 
                <Route path="/club-details/:url" render={props => <DisplayClub {...props} />} ID={LocalStorageData.ID} component={DisplayClub}  />
                <ProtectedRoute exact  path="/edit-club/:url"   ID={LocalStorageData.ID} component={EditClubPage} />
                <ProtectedRoute path="/create-group" render={props => <CreateGroup {...props} />} ID={LocalStorageData.ID} component={CreateGroup}  /> 
                <Route path="/group-details/:url" render={props => <DisplayGroup {...props} />} ID={LocalStorageData.ID} component={DisplayGroup}  />
                <ProtectedRoute path="/update-group/:url" render={props => <EditGroup {...props} />} ID={LocalStorageData.ID} component={EditGroup}  />
                <Route path="/blog-details/:url" render={props => <BlogDetails {...props} />} ID={LocalStorageData.ID} component={BlogDetails}  />
                <ProtectedRoute path="/all-notification" render={props => <AllNotification {...props} />} ID={LocalStorageData.ID} component={AllNotification}  />
                <ProtectedRoute path="/change-password" render={props => <ChangePassword {...props} />} ID={LocalStorageData.ID} component={ChangePassword}  />
                 {/* <Redirect from="/" to="/home" />
                 <Redirect from="*" to="/"/> */}
              </AppLayout>
            </Switch>
          </BrowserRouter>
        </Provider>
      </ApolloProvider>
    </div>
  );
}

export default App;

