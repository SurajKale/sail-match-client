import React from "react";
import { Route, Redirect } from "react-router-dom";
// import auth from "./auth";
import { connect } from "react-redux";
import { PropTypes } from "prop-types";
import { compose, mapProps } from "recompose";
import { useLocation } from "react-router";


const ProtectedRoute = ({ component: Component,ID, ...rest }) => {
  return (
 <Route {...rest} render={(props)=> (
  // console.log(ID,"ID"),
   ID ?
     <Component {...props} />
   : <Redirect to='/' />
  )} />
  )
}

// const mapStateToProps = reducerObj => {
//   // const userObject = reducerObj.userReducer.userObject;
//   // return { userObject };
// };

// export default connect(mapStateToProps)(ProtectedRoute);
export default ProtectedRoute
ProtectedRoute.propTypes = {
  // userObject: PropTypes.object.isRequired
};