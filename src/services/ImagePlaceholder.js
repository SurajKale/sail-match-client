import React, { Fragment } from "react";
import ReactPlaceholder from "react-placeholder";

class ImagePlaceholder extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loaded: false
    }
  }

  handleImageLoaded() {
    this.setState({ loaded: true });
  }

  render() {
    const { loaded } = this.state;
    const imageStyle = !loaded ? { display: "none" } : {};
    const image = <img src={'./assets/images/image_placeholder.png'} alt="" style={imageStyle} onLoad={this.handleImageLoaded.bind(this)} />;
    return (
      <Fragment>
        <ReactPlaceholder type="rect" ready={this.state.loaded} style={{height: 160}} >  
            {image}
        </ReactPlaceholder>
        {!loaded && image}
      </Fragment>
    )
  }
}

export default ImagePlaceholder;