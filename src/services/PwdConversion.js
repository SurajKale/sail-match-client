import { Base64 } from 'js-base64';

export function _encryptPwd(string) {
     if(string){
        return Base64.encode(string);  
    }else{
        return null;
    }
}

export function _decryptPwd(string) {
    if(string){
       return Base64.decode(string);  
   }else{
       return null;
   }
}
