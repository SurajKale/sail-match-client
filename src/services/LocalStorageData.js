   let Token = null;
   let Email = null;
   let FirstName = null;
   let UserName = null;
   let LastName = null;
   let ID = null;
   let ProfileImage = null;
   let SchoolAdmin = null;
   let isProfile = null;

    let sessionUser = JSON.parse(localStorage.getItem('sessionUser'));
        if (sessionUser) {
            ID = parseInt(sessionUser.ID);
             Email = sessionUser.Email;
             Token = sessionUser.Token;
             FirstName = sessionUser.FirstName;
             UserName = sessionUser.UserName;
             LastName = sessionUser.LastName;
             ProfileImage = sessionUser.ProfileImage;
             SchoolAdmin = sessionUser.isSchoolAdmin
             isProfile = sessionUser.isProfile
         }
module.exports = {ID,Email,Token,FirstName,UserName,LastName,ProfileImage,SchoolAdmin,isProfile}
