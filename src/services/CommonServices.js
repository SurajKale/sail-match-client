export const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

export const DatesArr = Array.from({length:31},(v,k)=>k+1);
export const MonthArr = ["January","February","March","April","May","June","July",
"August","September","October","November","December"];
export const YearsArr = Array.from({length:50},(v,k)=>k+1990);

export const DayName = [
            {name:'Sunday'}, {name:'Monday'}, {name:'Tuesday'}, {name:'Wednesday'}, {name:'Thursday'}, {name:'Friday'}, {name:'Saturday'}];

var moment = require('moment');

export function _formatDate(date) {
    if(moment(date).isValid()){
     return moment(date).format('dddd, MMM DD'); 
    }else{
      return '-'; 
    }
  }
  export function _formatDateUserdetails(date) {
    if(moment(date).isValid()){
    let time =  moment(date).format('HH.mm'); 
    let resdate =  moment(date).format('dddd, MMM DD'); 
    let currentdate = new Date()
    let newdate =  moment(currentdate).format('dddd, MMM DD'); 
    let yesterdate =  new Date(currentdate.setDate(currentdate.getDate() - 1))
    let formatyesterdate =  moment(yesterdate).format('dddd, MMM DD'); 
    if(resdate == newdate){
     let toadytime = moment(date).format('HH'); 
      return "Today"
    }
    else{
      if(resdate == formatyesterdate ){
         return "Yesterday"
      }
      else{
        return moment(date).format('dddd, MMM DD');
      }
    }
    }else{
      return '-'; 
    }
  }

  export function _formatTime(time) {
     return moment(time,"HH:mm:ss").format('LT'); 
  }

  export function _formatRentalTime(time) {
    return moment(time,"HH").format('hh A'); 
 }

  export function _setStorage(image) {
    let sessionUser = JSON.parse(localStorage.getItem('sessionUser'));
    if (sessionUser) {

    var setUSer = {
      ActivationLink:sessionUser.ActivationLink,
      Bio:sessionUser.Bio,
      BoatProfile:sessionUser.BoatProfile,
      Certifications:sessionUser.Certifications,
      City:sessionUser.City,
      ClubID:sessionUser.ClubID,
      ClubName:sessionUser.ClubName,
      CreatedDate:sessionUser.CreatedDate,
      DateOfBirth:sessionUser.DateOfBirth,
      Desire:sessionUser.Desire,
      Drink:sessionUser.Drink,
      Email:sessionUser.Email,
      Experience:sessionUser.Experience,
      ExternalProfileImage:sessionUser.ExternalProfileImage,
      ExternalSiteID:sessionUser.ExternalSiteID,
      FirstName:sessionUser.FirstName,
      Gender:sessionUser.Gender,
      ID:sessionUser.ID,
      IpAddress:sessionUser.IpAddress,
      LastName:sessionUser.LastName,
      Location:sessionUser.Location,
      ModifiedDate:sessionUser.ModifiedDate,
      Password:sessionUser.Password,
      Phone:sessionUser.Phone,
      Pot:sessionUser.Pot,
      ProfileImage:image,
      RefreshToken:sessionUser.RefreshToken,
      SignUpMethod:sessionUser.SignUpMethod,
      Smoke:sessionUser.Smoke,
      State:sessionUser.State,
      Status:sessionUser.Status,
      Token:sessionUser.Token,
      UniqueAutoSinginKey:sessionUser.UniqueAutoSinginKey,
      UserName:sessionUser.UserName,
      UserType:sessionUser.UserType,
      isBoatOwner:sessionUser.isBoatOwner,
      isClubMember:sessionUser.isClubMember,
      isProfile:sessionUser.isProfile,
      isVerified:sessionUser.isVerified
     };
     localStorage.setItem('sessionUser',JSON.stringify(setUSer))
    }
    return true; 
 }

 export const url = "http://ec2-54-236-24-40.compute-1.amazonaws.com";
 export const GoogleMapsAPI = 'AIzaSyBRs7NRGpboLvwqN9zqFZiuhCXqe9URYBQ';