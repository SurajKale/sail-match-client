const express = require('express');
const app = express();
const port = process.env.PORT || 5000;
const path = require('path');
const fs = require('fs')
const { createApolloFetch } = require('apollo-fetch');

const SITE_URL = 'http://localhost:5000/';
var SITELOGO     = 'http://socialenginespecialist.com/PuneDC/sailmatch/images/logo.png';


const fetch = createApolloFetch({
uri: 'http://ec2-54-236-24-40.compute-1.amazonaws.com:9100/sails',
});

getUrl = (url) =>{
  return SITE_URL+url;
}

setMetaContents = (data,title,obj_type,description,profile_pic,og_url,) =>{
  data = data.replace(/\$OG_TITLE/g, title);
  data = data.replace(/\$KEYWORDS/g, obj_type);
  data = data.replace(/\$OG_DESCRIPTION/g, description);
  data = data.replace(/\$OG_IMAGE/g, profile_pic);
  data = data.replace(/\$OG_URL/g, og_url);
  return data;
}

app.get('/match-details/:url', function(request, response) {
  var MatchUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getMatchByUrl(MatchUrl:"'+MatchUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getMatchByUrl = res.data.getMatchByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }

      new Promise((resolve, reject) => {
        const result = setMetaContents(data,getMatchByUrl.Title,getMatchByUrl.ObjectType,getMatchByUrl.Description,getMatchByUrl.ProfilePicture,currentUrl);
         resolve(result);
        }).then((result)=> {
        response.send(result);
      });
    });
  });
});

app.get('/crew-details/:url', function(request, response) {
  var CrewUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getCrewByUrl(CrewUrl:"'+CrewUrl+'"){ Title ID ObjectType Description ProfileImage}}',
    }).then(res => {
      const getCrewByUrl = res.data.getCrewByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getCrewByUrl.Title,getCrewByUrl.ObjectType,getCrewByUrl.Description,getCrewByUrl.ProfileImage.ProfileImage,currentUrl);
       resolve(result);
        }).then((result)=> {
           response.send(result);
      });
    });
  });
});

app.get('/ride-details/:url', function(request, response) {
  var RideUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getRideByUrl(RideUrl:"'+RideUrl+'"){ Title ID ObjectType Description ProfileImage}}',
    }).then(res => {
      const getRideByUrl = res.data.getRideByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getRideByUrl.Title,getRideByUrl.ObjectType,getRideByUrl.Description,getRideByUrl.ProfileImage,currentUrl);
         resolve(result);
            }).then((result)=> {
         response.send(result);
      });
    })
  });
});

app.get('/course-details/:url', function(request, response) {
  var CourseUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getCourseByUrl(CourseUrl:"'+CourseUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getCourseByUrl = res.data.getCourseByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getCourseByUrl.Title,getCourseByUrl.ObjectType,getCourseByUrl.Description,getCourseByUrl.ProfilePicture,currentUrl);
     resolve(result);
            }).then((result)=> {
       response.send(result);
    });
  });
  });
});

app.get('/skill-details/:url', function(request, response) {
  var SkillUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getSkillByUrl(SkillUrl:"'+SkillUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getSkillByUrl = res.data.getSkillByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getSkillByUrl.Title,getSkillByUrl.ObjectType,getSkillByUrl.Description,getSkillByUrl.ProfilePicture,currentUrl);
     resolve(result);
            }).then((result)=> {
    response.send(result);
    });
   });
  });
});

app.get('/club-details/:url', function(request, response) {
  var ClubUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getClubByUrl(ClubUrl:"'+ClubUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getClubByUrl = res.data.getClubByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {

     const result = setMetaContents(data,getClubByUrl.Title,getClubByUrl.ObjectType,getClubByUrl.Description,getClubByUrl.ProfilePicture,currentUrl);
     resolve(result);
      }).then((result)=> {
        response.send(result);
    });
  });
  });
});

app.get('/service-details/:url', function(request, response) {
  var ServiceUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getServiceByUrl(ServiceUrl:"'+ServiceUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getServiceByUrl = res.data.getServiceByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getServiceByUrl.Title,getServiceByUrl.ObjectType,getServiceByUrl.Description,getServiceByUrl.ProfilePicture,currentUrl);
     resolve(result);
         }).then((result)=> {
             response.send(result);
      });
    });
  });
});

app.get('/rental-details/:url', function(request, response) {
  var BoatRentalUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getBoatRentalProfileByUrl(BoatRentalUrl:"'+BoatRentalUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getBoatRentalProfileByUrl = res.data.getBoatRentalProfileByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
     const result = setMetaContents(data,getBoatRentalProfileByUrl.Title,getBoatRentalProfileByUrl.ObjectType,getBoatRentalProfileByUrl.Description,getBoatRentalProfileByUrl.ProfilePicture,currentUrl);
       resolve(result);
        }).then((result)=> {
          response.send(result);
      });
    });
  });
});

app.get('/group-details/:url', function(request, response) {
  var GroupUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getGroupByUrl(GroupUrl:"'+GroupUrl+'"){ Title ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getGroupByUrl = res.data.getGroupByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
      
     const result = setMetaContents(data,getGroupByUrl.Title,getGroupByUrl.ObjectType,getGroupByUrl.Description,getGroupByUrl.ProfilePicture,currentUrl);
      resolve(result);
        }).then((result)=> {
            response.send(result);
      });
    });
  });
});

app.get('/event-details/:url', function(request, response) {
  var EventUrl = request.params.url;
  const currentUrl = getUrl(request.url);
  fetch({
    query: '{getEventByUrl(EventUrl:"'+EventUrl+'"){ EventTitle ID ObjectType Description ProfilePicture}}',
    }).then(res => {
      const getEventByUrl = res.data.getEventByUrl;
    const filePath = path.resolve(__dirname, './build', 'index.html')
    fs.readFile(filePath, 'utf8', function (err,data) {
      if (err) {
        return console.log(err);
      }
      new Promise((resolve, reject) => {
        const result = setMetaContents(data,getEventByUrl.EventTitle,getEventByUrl.ObjectType,getEventByUrl.Description,getEventByUrl.ProfilePicture,currentUrl);
        resolve(result);
          }).then((result)=> {
            response.send(result);
      });
   });
  });
});



app.get('/', function(request, response) {
  const filePath = path.resolve(__dirname, './build', 'index.html');
  fs.readFile(filePath, 'utf8', function (err,data) {
    if (err) {
      return console.log(err);
    }
    const title = 'Matching Local Sailing Enthusiast';
    const keywords = 'Crews,Rides,Events,Courses,Skills,Clubs,Services,Rentals,Photo,Video,Matches,Crews,Rides,Events,Courses,Skills,Clubs,Services,Rentals,Photo';
    const description = 'Matching Local Sailing Enthusiast with Day sailors and Weekend Cruising in the US';
    const currentUrl = getUrl(request.url);
    const og_image = SITELOGO;

    new Promise((resolve, reject) => {
    const result = setMetaContents(data,title,keywords,description,og_image,currentUrl);
      resolve(result);
          }).then((result)=> {
     response.send(result);
  });
});
});


app.use(express.static(path.resolve(__dirname, './build')));

app.get('*', function(request, response) {
  console.log('ddddd')
  const filePath = path.resolve(__dirname, './build', 'index.html');
  response.sendFile(filePath);
});


app.listen(port, () => console.log(`Listening on port ${port}`));